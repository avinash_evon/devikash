//
//  HomeChangePasswordVC.h
//  iKash
//
//  Created by indianic on 27/01/16.
//  Copyright © 2016 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeChangePasswordVC : UIViewController{
    IBOutlet NSLayoutConstraint *imgViewLogoTopConstraint;
}
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrllView;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewBackground;
@property (strong, nonatomic) IBOutlet UIView *innerView;
@property (weak, nonatomic) IBOutlet UILabel *lblAllreadyMember;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnChangePass;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)btnActionToChangePassClick:(id)sender;
- (IBAction)btnLoginClick:(id)sender;
@end
