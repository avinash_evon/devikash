//
//  HomeChangePasswordVC.m
//  iKash
//
//  Created by indianic on 27/01/16.
//  Copyright © 2016 indianic. All rights reserved.
//

#import "HomeChangePasswordVC.h"
#import "LoginVC.h"

@interface HomeChangePasswordVC ()

@end

@implementation HomeChangePasswordVC

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];

    _btnChangePass.layer.cornerRadius = 2.5f;
    
    [_innerView.layer setCornerRadius:2.5f];
    
    [_innerView.layer setMasksToBounds:YES];
    
    self.navigationController.navigationBar.hidden=TRUE;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

#pragma mark - Button Event Click Methods
- (IBAction)btnActionToChangePassClick:(id)sender {
    
   
    if (![_txtNewPassword.text isValidString]){
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_NEW_PASSWORD"] Tag:@"0"];
        return;
    }else if ([_txtNewPassword.text length] < 8){
        [_txtNewPassword becomeFirstResponder];
        [appDelegate showAlertView: [appDelegate getString:@"ENTER_MIN_PASSWORD"] Tag:@"0"];
        return;
    }
    else if (![_txtNewPassword.text isValidSecuredPassword]){
        [_txtNewPassword becomeFirstResponder];
        [appDelegate showAlertView: [appDelegate getString:@"ENTER_MIN_PASSWORD"] Tag:@"0"];
        return;
    }else if (![_txtNewPassword.text isEqualToString:_txtConfirmPassword.text]){
        [appDelegate showAlertView:[appDelegate getString:@"PASSWORDS_NOT_MATCH"] Tag:@"0"];
        return;
    }
   
    [_txtConfirmPassword resignFirstResponder];
    [_txtNewPassword resignFirstResponder];
    
    [self.view endEditing:YES];
    
    [self CangePasswordWS]; 
}

- (IBAction)btnLoginClick:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    LoginVC *objLoginVC= [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:objLoginVC animated:YES];
}

#pragma mark - TextField Methods
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
}

#pragma mark - Webservice Methods
-(void)CangePasswordWS{
   
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"ChangePassword"],@"forgot_key",
                                  _txtNewPassword.text,@"new_pwd",
                                  [USERDEFAULTS objectForKey:@"Langauge"],@"lang_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_HomeChangePasswordWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        
        [self.view endEditing:YES];
        
        [_txtConfirmPassword resignFirstResponder];
        
        [_txtNewPassword resignFirstResponder];
        
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:APPTITLE message:[[responseData objectForKey:@"settings"] objectForKey:@"message"] delegate:self cancelButtonTitle:[appDelegate getString:@"OK"] otherButtonTitles:nil, nil];
        alertView.tag=200;
        [alertView show];
        
    } withFailureBlock:^(NSError *error) {
        [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedInstance].window animated:YES];
        [appDelegate showAlertView:[appDelegate getString:@"SOMETHINGWENTWRONG"] Tag:@"0"];

    }];
}


#pragma mark AlertMethod

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag==200){
       
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        LoginVC *objLoginVC= [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        [self.navigationController pushViewController:objLoginVC animated:YES];
    }
}
@end
