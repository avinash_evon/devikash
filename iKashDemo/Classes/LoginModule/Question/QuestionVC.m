//
//  ForgotPassword.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "QuestionVC.h"

@interface QuestionVC ()

@end

@implementation QuestionVC

#pragma mark - Synthesize
@synthesize mutArrQuestionAnswer;

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary *aDictLocationData = [[NSUserDefaults standardUserDefaults] objectForKey:@"LocationData"];
    
    if (![aDictLocationData objectForKey:@"mc_city_image"]) {
        
        [self.imgViewBackground setImage:[UIImage imageNamed:@"homenew"]];
        
    }else{
        
        [self.imgViewBackground sd_setImageWithURL:[aDictLocationData objectForKey:@"mc_city_image"]
                                  placeholderImage:nil
                                           options:SDWebImageProgressiveDownload
                                          progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                          }
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                             self.imgViewBackground.clipsToBounds = YES;
                                         }];
    }

    [_ViewText.layer setCornerRadius:2.5f];
    
    [_ViewText.layer setMasksToBounds:YES];
    
    _btnSubmit.layer.cornerRadius = 2.5f;
    
    _lblQuestion.text=[[mutArrQuestionAnswer objectAtIndex:0] objectForKey:@"sql_question"];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //[_txtAwnser becomeFirstResponder];
    
    UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
    
    [self updateMotionEffectForOrientation:orientation];
    
}



#pragma mark UIInterfaceOrientation Methods

- (void)updateMotionEffectForOrientation:(UIInterfaceOrientation)orientation{
   
    if(UIInterfaceOrientationIsPortrait(orientation)){
    
        if (IS_IPAD) {
        
            imgViewLogoTopConstraint.constant = 200;
            
        }else{
            
            imgViewLogoTopConstraint.constant = 120;
        }
    }else{
        
        if (IS_IPAD) {
            
            imgViewLogoTopConstraint.constant = 150;
            
        }else{
            
            imgViewLogoTopConstraint.constant = 50;
            
        }
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation ==
        UIInterfaceOrientationPortraitUpsideDown) {
        
        if (IS_IPAD) {
        
            imgViewLogoTopConstraint.constant = 200;
            
        }else{
            
            imgViewLogoTopConstraint.constant = 120;
            
        }
    }
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft){
        
        if (IS_IPAD) {
            
            imgViewLogoTopConstraint.constant = 150;
            
        }else{
            
            imgViewLogoTopConstraint.constant = 50;
            
        }
    }
}

#pragma mark- UIButton Click Action Methods

- (IBAction)btnSubmitClick:(id)sender {
    
    if (![_txtAwnser.text isValidString]) {
    
        [_txtAwnser becomeFirstResponder];
        
        [appDelegate showAlertView : [appDelegate getString:@"ENTER_ANSWER"] Tag:@"0"];
        
        return;
        
    }
    
    [self AnswerTheQuestionWS];
}

- (IBAction)btnForgotClick:(id)sender{
   
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark- Web Service Methods

-(void)AnswerTheQuestionWS{
  
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                           _txtAwnser.text,@"answer",
                           [[mutArrQuestionAnswer objectAtIndex:0] objectForKey:@"sql_security_questions_id"],@"question_id",
                           [[mutArrQuestionAnswer objectAtIndex:0] objectForKey:@"c_customer_id"],@"customer_id",
                           nil];
    
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_ForgotPassword_AnswerWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        
        NSLog(@"responseData = %@", responseData);
         _txtAwnser.text=@"";
    
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
        
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:APPTITLE message:[[responseData objectForKey:@"settings"] objectForKey:@"message"] delegate:self cancelButtonTitle:[appDelegate getString:@"OK"] otherButtonTitles:nil, nil];
            alertView.tag=100;
            [alertView show];
            
        }else{
            
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark AlertMethod
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag==100){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

@end
