//
//  RegisterVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterVC : UIViewController{
    IBOutlet NSLayoutConstraint *imgViewLogoTopConstraint;
}

 @property (nonatomic) IBInspectable CGFloat padding;// avinash


@property (weak, nonatomic) IBOutlet UIImageView *imgViewBackground;
@property (strong, nonatomic) IBOutlet UIView *innerView;
@property (strong, nonatomic) IBOutlet UIButton *btnGetStarted;

@property(nonatomic,strong)NSString *strFbId;
@property(nonatomic,strong)NSString *strFbEmail;

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtZipcode;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UILabel *lblTermsText;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsandCondition;
@property (weak, nonatomic) IBOutlet UILabel *lblAllreadyMember;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UILabel *lblVersion;
// Confirmation Alert View
@property (strong, nonatomic) IBOutlet UIView *confrimationAlertView;
@property (strong, nonatomic) IBOutlet UIView *confirmationInnerView;
@property (strong, nonatomic) IBOutlet UILabel *lblComfirmationText;

- (IBAction)btnActionToResendClick:(id)sender;
- (IBAction)btnActionToOkClick:(id)sender;

// terms Alert View
@property (strong, nonatomic) IBOutlet UIView *termsAndConditionsView;
@property (strong, nonatomic) IBOutlet UIView *termsAndConditionsInnerView;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)btnActionToCloseTermsViewClick:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *passwordBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *confPasswordOutlet;



- (IBAction)passwordBtnAction:(UIButton *)sender;

- (IBAction)confirmPasswordBtnAction:(UIButton *)sender;

- (IBAction)btnGetStartedClick:(id)sender;
- (IBAction)btnTermsAndConditionClick:(id)sender;
- (IBAction)btnLoginClick:(id)sender;
- (IBAction)btnActionToClickLogin:(id)sender;
@end
