//
//  RegisterVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "RegisterVC.h"
#import "ProfileContainerVC.h"
#import "LoginVC.h"
#import "AppDelegate.h"

@interface RegisterVC ()<UITextFieldDelegate>{
    int isShowOrNot;
    NSString *strCityId;
    NSString *strCountryId;
    BOOL pwdFlag;
    BOOL cnfPwdFlag;
}

@end

@implementation RegisterVC

#pragma mark - Synthesize
@synthesize strFbEmail, strFbId, padding;

-(CGRect)textRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, padding, padding);
}

-(CGRect)editingRectForBounds:(CGRect)bounds{
    return [self textRectForBounds:bounds];
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    pwdFlag = FALSE;
    cnfPwdFlag = FALSE;
    
    
    
    [self SetUpPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
     _confrimationAlertView.hidden = YES;
    
 //   [[AppDelegate sharedInstance].strFlag = @"1";
    
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     [defaults setObject:@"0" forKey:@"firstRun"];
    
    UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
    
    [self updateMotionEffectForOrientation:orientation];
}



#pragma mark UIInterfaceOrientation Methods

- (void)updateMotionEffectForOrientation:(UIInterfaceOrientation)orientation{
    if(UIInterfaceOrientationIsPortrait(orientation)){
        if (IS_IPAD) {
            imgViewLogoTopConstraint.constant = 200;
        }else{
            imgViewLogoTopConstraint.constant = 120;
        }
    }else{
        if (IS_IPAD) {
            imgViewLogoTopConstraint.constant = 150;
        }else{
            imgViewLogoTopConstraint.constant = 50;
        }
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
        if (IS_IPAD) {
            imgViewLogoTopConstraint.constant = 200;
        }else{
            imgViewLogoTopConstraint.constant = 120;
        }
    }
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft){
        if (IS_IPAD) {
            imgViewLogoTopConstraint.constant = 150;
        }else{
            imgViewLogoTopConstraint.constant = 50;
        }
    }
}

#pragma mark- UIButton Click Action Methods

- (IBAction)btnActionToClickLogin:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnActionToResendClick:(id)sender {
    
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_resendEmailWS withParams:[@{@"email":_txtEmail.text} mutableCopy] showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"respnce resend email WS = %@",responseData);
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1){
                [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:@"0"];
                _confrimationAlertView.hidden = YES;
                [self.navigationController popViewControllerAnimated:YES];
            }
        });
    } withFailureBlock:^(NSError *error) {
        
    }];
    
    
}

- (IBAction)btnActionToOkClick:(id)sender {
   
    
    _confrimationAlertView.hidden = YES;
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (IBAction)btnActionToCloseTermsViewClick:(id)sender {
    _termsAndConditionsView.hidden = YES;
}

- (IBAction)passwordBtnAction:(UIButton *)sender {
    
    if(pwdFlag == FALSE)
    {
        
        pwdFlag = TRUE;
        _passwordBtnOutlet.selected = TRUE;
        _txtPassword.SecureTextEntry = NO;
       
    }
    else{
        pwdFlag = FALSE;
        _passwordBtnOutlet.selected = FALSE;
        _txtPassword.SecureTextEntry = YES;
        
    }
    
}

- (IBAction)confirmPasswordBtnAction:(UIButton *)sender {
    
    if(cnfPwdFlag == FALSE)
     {
         
         cnfPwdFlag = TRUE;
         _confPasswordOutlet.selected = TRUE;
         
        _txtConfirmPassword.secureTextEntry = NO;
         
        
     }
     else{
         cnfPwdFlag = FALSE;
         _confPasswordOutlet.selected = FALSE;
          _txtConfirmPassword.secureTextEntry = YES;
         
     }
}

- (IBAction)btnGetStartedClick:(id)sender {
    
    if (![_txtZipcode.text isEqualToString:@""] && ([strCityId isEqualToString:@""] || [strCountryId isEqualToString:@""])){
        
        [self getCityIsFromSubmit:YES];
     return;
    }
    
    if (![_txtEmail.text isValidString]) {
        
        [_txtEmail becomeFirstResponder];
        [appDelegate showAlertView: [appDelegate getString:@"ENTER_EMAIL_ADDRESS"] Tag:@"0"];
        return;
    }else if (![_txtEmail.text isValidEmail]) {
        [_txtEmail becomeFirstResponder];
        [appDelegate showAlertView:[appDelegate getString:@"INVALID_EMAIL_ADDRESS"] Tag:@"0"];
        return;
    }else if (![_txtEmail.text isEqualToString:_txtConfirmEmail.text]) {
        [_txtConfirmEmail becomeFirstResponder];
        [appDelegate showAlertView: [appDelegate getString:@"EMAIL_NOT_MATCH"] Tag:@"0"];
        return;
    }else if (![_txtPassword.text isValidString]){
        [_txtPassword becomeFirstResponder];
        [appDelegate showAlertView: [appDelegate getString:@"ENTER_PASSWORD"] Tag:@"0"];
        return;
    }else if ([_txtPassword.text length] < 8){
        [_txtPassword becomeFirstResponder];
        [appDelegate showAlertView: [appDelegate getString:@"ENTER_MIN_PASSWORD"] Tag:@"0"];
        return;
    }
    else if (![_txtPassword.text isValidSecuredPassword]){
        [_txtPassword becomeFirstResponder];
        [appDelegate showAlertView: [appDelegate getString:@"ENTER_MIN_PASSWORD"] Tag:@"0"];
        return;
    }
    else if (![_txtPassword.text isEqualToString:_txtConfirmPassword.text]){
        [_txtConfirmPassword becomeFirstResponder];
        [appDelegate showAlertView:[appDelegate getString:@"PASSWORDS_NOT_MATCH"] Tag:@"0"];
        return;
    }
    
    if ([_txtZipcode.text isEqualToString:@""]){
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_ZIPCODE"] Tag:@"123"];
        return;
    }
    
    [self.view endEditing:YES];
    [self RegistrationWS];
}


- (IBAction)btnTermsAndConditionClick:(id)sender{
    
    [self.view endEditing:YES];
    
    _termsAndConditionsView.hidden = NO;
}

- (IBAction)btnLoginClick:(id)sender {
    
    [self.view endEditing:YES];
    
    NSLog(@"%@",self.navigationController.viewControllers);
    if([self.navigationController.viewControllers count]>1){
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        LoginVC *objLoginVC = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        [self.navigationController pushViewController:objLoginVC animated:YES];
    }
}

- (IBAction)btnShowPasswordClick:(id)sender {
    if (_txtPassword.text.length>0){
        if (!isShowOrNot){
            
            _txtPassword.enabled = NO;
            _txtPassword.secureTextEntry = NO;
            _txtPassword.enabled = YES;
            isShowOrNot=YES;
        }else{
            
            _txtPassword.enabled = NO;
            _txtPassword.secureTextEntry = YES;
            _txtPassword.enabled = YES;
            isShowOrNot=NO;
        }
    }
    
}

#pragma mark- Web Service Methods

- (void)RegistrationWS{
    
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  _txtEmail.text,@"email",
                                  _txtPassword.text,@"password",
                                  strFbId != nil ? strFbId : @"",@"fb_id",
//                                  [USERDEFAULTS objectForKey:@"DeviceToken"],@"device_id",
//                                  [USERDEFAULTS objectForKey:@"device_type"],@"device_type",
//                                  @"",@"ip_address",
                                  strCityId,@"city_id",
//                                  @"",@"is_newsletter",
                                  _txtZipcode.text,@"zipcode",
                                  strCountryId,@"country_id",
                                  nil];
    NSLog(@"%@",aDict);
    
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_Registration_loginWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        
        NSLog(@"responseData = %@", responseData);
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            
            [USERDEFAULTS setObject:[[[responseData objectForKey:@"data"]objectForKey:@"get_activation_key"] objectForKey:@"access_token"]  forKey:@"access_token"];
            
            if([[responseData objectForKey:@"data"]objectForKey:@"insert_customer_details"] != nil){
                
                [USERDEFAULTS setObject:[[[responseData objectForKey:@"data"] objectForKey:@"insert_customer_details"]  objectForKey:@"c_customer_id"]  forKey:@"customer_id"];
            }else{
                
                [USERDEFAULTS setObject:[[[[responseData objectForKey:@"data"]objectForKey:@"check_in_customer_table"] objectAtIndex:0]objectForKey:@"c_customer_id"]  forKey:@"customer_id"];
            }
            [USERDEFAULTS synchronize];
            
            [_txtConfirmPassword resignFirstResponder];
            if(strFbId.length==0)
            {
                
                _lblComfirmationText.text = [NSString stringWithFormat:@"%@ %@. %@", [appDelegate getString:@"A confirmation mail has been sent to"], _txtEmail.text, [appDelegate getString:@"Click on the confirmation link in the mail to active your account"]];
                
                _confrimationAlertView.hidden = YES;
                
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Confirm your mail"
                                                                              message:[NSString stringWithFormat:@"%@ %@. %@", [appDelegate getString:@"A confirmation mail has been sent to"], self->_txtEmail.text, [appDelegate getString:@"Click on the confirmation link in the mail to active your account"]]
                                                                       preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Cancel"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action)
                {
                    /** What we write here???????? **/
                    NSLog(@"No Action");

                    // call method whatever u need
                }];

                UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"OK"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action)
                {
                    /** What we write here???????? **/
                    NSLog(@"Going to Login Again");
                    
                     [self.navigationController popViewControllerAnimated:YES];
                    // call method whatever u need
                }];

                [alert addAction:yesButton];
                [alert addAction:noButton];

                [self presentViewController:alert animated:YES completion:nil];
                
            }else{
                
                [self GetCategoriesWS];
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
                
                ProfileContainerVC *objProfileContainerVC = [storyboard instantiateViewControllerWithIdentifier:@"ProfileContainerVC"];
                
                [AppDelegate sharedInstance].strFlag=@"1";
                
                [self.navigationController pushViewController:objProfileContainerVC animated:YES];
                
            }
            
        }
        else if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 2) {
            
            _lblComfirmationText.text = [[responseData objectForKey:@"settings"] objectForKey:@"message"];
            _confrimationAlertView.hidden =YES; //NO;
            
       UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Confirm Your Email"
                        message:[[responseData objectForKey:@"settings"] objectForKey:@"message"]
                                                              preferredStyle:UIAlertControllerStyleAlert];

       UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Resend"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action)
       {
           
           NSLog(@"Request Resend");

          [[Webservice sharedInstance] callWebserviceWithMethodName:customer_resendEmailWS withParams:[@{@"email":_txtEmail.text} mutableCopy] showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
                 NSLog(@"respnce resend email WS = %@",responseData);
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     if([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1){
                         [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:@"0"];
                         _confrimationAlertView.hidden = YES;
                         [self.navigationController popViewControllerAnimated:YES];
                     }
                 });
             } withFailureBlock:^(NSError *error) {
                 
             }];
       }];

       UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action)
       {
           /** What we write here???????? **/
           NSLog(@"No Action");
           // call method whatever u need
       }];

       [alert addAction:yesButton];
       [alert addAction:noButton];

       [self presentViewController:alert animated:YES completion:nil];

            
        }else{
            
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
            
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)GetCategoriesWS{
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCategoryWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        
        NSLog(@"responseData = %@", responseData);
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            
            [USERDEFAULTS setObject:[responseData objectForKey:@"data"] forKey:@"CategoryList"];
            
        }else{
            
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
        
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}


#pragma mark SetUpPage

-(void)SetUpPage{
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    #if DEV_SERVER
    version = [version stringByAppendingFormat:@" DEV"];
    #else
    version = [version stringByAppendingFormat:@" TEST"];
    #endif
    [_lblVersion setText:version];
    _txtZipcode.delegate = self;
    
    [self.navigationController.navigationBar setHidden:YES];
    NSDictionary *aDictLocationData = [[NSUserDefaults standardUserDefaults] objectForKey:@"LocationData"];
    if (![aDictLocationData objectForKey:@"mc_city_image"]) {
        [self.imgViewBackground setImage:[UIImage imageNamed:@"homenew"]];
    }else{
        [self.imgViewBackground sd_setImageWithURL:[aDictLocationData objectForKey:@"mc_city_image"]
                                  placeholderImage:nil
                                           options:SDWebImageProgressiveDownload
                                          progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                          }
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                             self.imgViewBackground.clipsToBounds = YES;
                                         }];
    }
    [_innerView.layer setCornerRadius:2.5f];
    [_innerView.layer setMasksToBounds:YES];
    
    _btnGetStarted.layer.cornerRadius = 2.5f;
    
    [_confirmationInnerView.layer setCornerRadius:2.5f];
    [_confirmationInnerView.layer setMasksToBounds:YES];
    
    [_termsAndConditionsInnerView.layer setCornerRadius:2.5f];
    [_termsAndConditionsInnerView.layer setMasksToBounds:YES];
    
    if(![strFbEmail isEqualToString:@""]){
        _txtEmail.text=strFbEmail;
        _txtConfirmEmail.text=strFbEmail;
    }else{
        strFbId=@"";
    }
    
    
    NSString *urlAddress = @"http://www.lipsum.com/";
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == _txtZipcode && ![_txtZipcode.text isEqualToString:@""]){
        [self getCityIsFromSubmit:NO];
    }
}

-(void)getCityIsFromSubmit : (BOOL)flag{
    
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  _txtZipcode.text,@"zipcode",
                                  
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCityfromZipWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            
            NSMutableArray *mutArrCityCountry=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
            
            _txtCity.text=[[mutArrCityCountry objectAtIndex:0] objectForKey:@"city"];
            strCityId=[[mutArrCityCountry objectAtIndex:0] objectForKey:@"city_id_zip"];
            strCountryId=[[mutArrCityCountry objectAtIndex:0] objectForKey:@"country_id"];
            
            //If from btngetStarted
            if(flag){ [self btnGetStarted]; }
            
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
            
            _txtCity.text = @"";
            _txtZipcode.text = @"";
            strCityId = @"";
            strCountryId = @"";
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

if(textField == _txtZipcode)
{
    
    //previous code
   /* NSString *filter = @"####### @@";//4
    
    if(!filter) return YES; // No filter provided, allow anything
    
    NSString *changedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if(range.length == 1 && // Only do for single deletes
       string.length < range.length &&
       [[textField.text substringWithRange:range] rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]].location == NSNotFound)
    {
        // Something was deleted.  Delete past the previous number
        NSInteger location = changedString.length-1;
        if(location > 0)
        {
            for(; location > 0; location--)
            {
                if(isdigit([changedString characterAtIndex:location]))
                {
                    break;
                }
            }
            changedString = [changedString substringToIndex:location];
        }
    }
    
    textField.text = [self filteredPhoneStringFromStringWithFilter:changedString andFilter:filter];
    
    return NO;
}else{
    return YES;
}*/
    
    // code by Avinash
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
       return (newString.length<=10);
    
}
    return YES;
}

// To make filter for NNNN CC format
-(NSString*)filteredPhoneStringFromStringWithFilter :(NSString *) string andFilter: (NSString *) filter
{
    NSUInteger onOriginal = 0, onFilter = 0, onOutput = 0;
    char outputString[([filter length])];
    BOOL done = NO;
    
    while(onFilter < [filter length] && !done)
    {
        char filterChar = [filter characterAtIndex:onFilter];
        char originalChar = onOriginal >= string.length ? '\0' : [string characterAtIndex:onOriginal];
        switch (filterChar) {
            case '#':
                if(originalChar=='\0')
                {
                    // We have no more input numbers for the filter.  We're done.
                    done = YES;
                    break;
                }
                if(isdigit(originalChar))
                {
                    outputString[onOutput] = originalChar;
                    onOriginal++;
                    onFilter++;
                    onOutput++;
                }
                else
                {
                    onOriginal++;
                }
                break;
            case '@':
                if(originalChar=='\0')
                {
                    // We have no more input numbers for the filter.  We're done.
                    done = YES;
                    break;
                }
                if(isalpha(originalChar))
                {
                    outputString[onOutput] = originalChar;
                    onOriginal++;
                    onFilter++;
                    onOutput++;
                }
                else
                {
                    onOriginal++;
                }
                break;
            default:
                // Any other character will automatically be inserted for the user as they type (spaces, - etc..) or deleted as they delete if there are more numbers to come.
                outputString[onOutput] = filterChar;
                onOutput++;
                onFilter++;
                if(originalChar == filterChar)
                    onOriginal++;
                break;
        }
    }
    outputString[onOutput] = '\0'; // Cap the output string
    return [NSString stringWithUTF8String:outputString];
}

@end
