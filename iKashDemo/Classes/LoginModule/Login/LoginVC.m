//
//  LoginVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "LoginVC.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "ForgotPassword.h"
#import "RegisterVC.h"
#import "ProfileContainerVC.h"
#import "HomeChangePasswordVC.h"
#import "Constant.h"



@interface LoginVC (){
    int isShowOrNot;
    NSString *strFirstName;
    BOOL withoutLogin;
    BOOL flagRemember;
    BOOL switchFlag;
    
    
    //Call service again if not found (Server issue -> Some times google does not gives as city name that time try once again)
    BOOL flagIsCityImageGetServiceCalledTwise;
}
    
    @end

IB_DESIGNABLE

@implementation LoginVC

@synthesize padding;
    

-(CGRect)textRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, padding, padding);
}

-(CGRect)editingRectForBounds:(CGRect)bounds{
    return [self textRectForBounds:bounds];
}
#pragma mark - View lifecycle
- (void)viewDidLoad{
    [super viewDidLoad];
    
    switchFlag = TRUE;
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    [_switchBtnOutlet setBackgroundImage:[UIImage imageNamed:@"btn-switch-deselected"] forState:UIControlStateNormal];
          
    
    [self initSetup];
}
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void)initSetup{
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    #if DEV_SERVER
    version = [version stringByAppendingFormat:@" DEV"];
    #else
    version = [version stringByAppendingFormat:@" TEST"];
    #endif
    [_lblVersion setText:version];
    
    flagIsCityImageGetServiceCalledTwise = false;
    
    //-----------
    
    NSMutableAttributedString *forgotPwdString = [[NSMutableAttributedString alloc] initWithString:@"Forgot password"];

    [forgotPwdString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [forgotPwdString length])];

    [_btnForgotPassword setAttributedTitle:forgotPwdString forState:UIControlStateNormal];
    
    NSMutableAttributedString *registerHereString = [[NSMutableAttributedString alloc] initWithString:@"New User? Register Here"];

                [registerHereString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [registerHereString length])];

    [_btnRegister setAttributedTitle:registerHereString forState:UIControlStateNormal];
    
    
    
    NSMutableAttributedString *continueWithoutLoginString = [[NSMutableAttributedString alloc] initWithString:@"Continue Without Login"];

                  [continueWithoutLoginString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [continueWithoutLoginString length])];

      [_btnWithoutLogin setAttributedTitle:continueWithoutLoginString forState:UIControlStateNormal];
    
    NSMutableAttributedString *supplierLoginString = [[NSMutableAttributedString alloc] initWithString:@"Supplier Login"];

          [supplierLoginString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [supplierLoginString length])];

          [_btnSupplierLogin setAttributedTitle:supplierLoginString forState:UIControlStateNormal];
    
    
    //------------
    
    [_switchLanguage addTarget:self action:@selector(setState:) forControlEvents:UIControlEventValueChanged];
    
    [_viewLogingfield.layer setCornerRadius:2.5f];
    
    [_viewLogingfield.layer setMasksToBounds:YES];
    
    _btnFBLogin.layer.cornerRadius = 2.5f;
    
    _btnLogin.layer.cornerRadius = 2.5f;
    
    if(![USERDEFAULTS objectForKey:@"Language"]){
        
        [USERDEFAULTS setValue:@"en" forKey:@"Language"];
        
        [USERDEFAULTS synchronize];
        
    }
    
    BOOL state = [[USERDEFAULTS valueForKey:@"Language"] isEqualToString:@"nl"];
    [_switchLanguage setOn:state];
    
    
    if(!IS_IPAD){
        
        if([[USERDEFAULTS objectForKey:@"Language"] isEqualToString:@"en"]){
            //_btnRegister.imageEdgeInsets = UIEdgeInsetsMake(0, _btnRegister.imageView.frame.origin.x, 0, 0);
        }else{
            
            _btnRegister.imageEdgeInsets = UIEdgeInsetsMake(0, _btnRegister.imageView.frame.origin.x+(_btnRegister.imageView.frame.size.width/2)+5, 0, 0);
        }
    }
}

-(void)setBackgroundImage{
    
        NSString *aOldCityImageURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastCityBGImg"];
        
        if (aOldCityImageURL != nil){
            
            [self.imgViewBackground sd_setImageWithURL:[NSURL URLWithString:aOldCityImageURL]];
        }
}
    
-(void)SetUpPageForLanguageChange{
    
    [self resetLanguage:_btnLogin withTitle:@"Login now"];
  //  [self resetLanguage:_btnFBLogin withTitle:@"Login with Facebook"];
  //  [self resetLanguage:_btnRegister withTitle:@"New User? Register Here"];
  //  [self resetLanguage:_btnWithoutLogin withTitle:@"Continue Without Login"];
    
    
  //  [self resetLanguage:_btnOR withTitle:@"OR"];
  //  [self resetLanguage:_btnSupplierLogin withTitle:@"Supplier Login"];
  //  [self resetLanguage:_btnForgotPassword withTitle:@"Forgot password"];
    [self resetLanguage:_btnRemember withTitle:@"Remember me"];
    
    _lblDutch.text = @"Dutch";
    _lblEnglish.text = @"English";
    
  //  _txtEmail.placeholder = @"Email Address";
  //  _txtPassword.placeholder = @"Password";
    
}

-(void)resetLanguage : (UIButton*)btn withTitle :(NSString*) title{
        NSString *aStrTitle = title;
        [btn setTitle:aStrTitle forState:UIControlStateNormal];
}

    
- (void)setState:(id)sender
    {
        BOOL state = [sender isOn];
        NSString *newLang = state == YES ? @"nl" : @"en";
        NSLog(@"%@", newLang);
        
        [USERDEFAULTS setValue:newLang forKey:@"Language"];
        [USERDEFAULTS synchronize];
        
        NSDictionary *aDictLanguageData = [USERDEFAULTS objectForKey:@"Language"];
        NSLog(@"aDictLanguageData = %@", aDictLanguageData);
        
        [self SetUpPageForLanguageChange];
        
    }
    
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    withoutLogin=FALSE;
    
    _txtEmail.delegate = self;
    
   //  _languageSpacing.constant = -13;
    
    _languageHeightBox.constant = 56;
    
    _languageRightPadding.constant = 5;
    
UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
_txtEmail.leftView = paddingView;
_txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
   
    
     [[UIApplication sharedApplication] setStatusBarHidden:NO];
       [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //Set default old city image
    [self setBackgroundImage];
    
    [self SetUpPage];
}
    
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"getLocation" object:nil];
    
}
    
-(void)viewDidLayoutSubviews{
    
    UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
    
    if(UIInterfaceOrientationIsPortrait(orientation)){
        
     //  [_languageOutlet.frame.size.height = 56];
        _languageHeightBox.constant = 56;
          
          _languageRightPadding.constant = 5;
          
        
    }else{
        //        [_scrlView setContentSize:CGSizeMake(_scrlView.frame.size.width, 500.0)];
    }
}
    
    
- (IBAction)btnActionToRememberClick:(id)sender{
    
    flagRemember = !flagRemember;
    
    NSString *aStrImgRemember = flagRemember ? @"radio_on": @"radio_off";
    _imgRadio.image = [UIImage imageNamed:aStrImgRemember];
}
    
#pragma mark UIInterfaceOrientation Methods
    
- (void)updateMotionEffectForOrientation:(UIInterfaceOrientation)orientation{
    
    if(UIInterfaceOrientationIsPortrait(orientation)){
        if (IS_IPAD) {
            imgViewLogoTopConstraint.constant = 200;
        }else{
            imgViewLogoTopConstraint.constant = 120;
        }
    }else{
        if (IS_IPAD) {
            imgViewLogoTopConstraint.constant = 150;
        }else{
            imgViewLogoTopConstraint.constant = 50;
        }
    }
}
    
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
        if (IS_IPAD) {
            imgViewLogoTopConstraint.constant = 200;
        }else{
            imgViewLogoTopConstraint.constant = 120;
            
            _languageRightPadding.constant = 5; // Avinash
            
        }
    }
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft){
        if (IS_IPAD) {
            imgViewLogoTopConstraint.constant = 150;
        }else{
            imgViewLogoTopConstraint.constant = 50;
            
            _languageRightPadding.constant = 5;
         //   _languageSpacing.constant = 250;// Avinash
        }
    }
}
    
#pragma mark Facebook SDK
    
- (IBAction)btnFBLoginClick:(id)sender{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    [FBSDKAccessToken setCurrentAccessToken:nil];
    
    [FBSDKProfile setCurrentProfile:nil];
    
    [login logInWithPermissions:@[@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
     
     {
         if (error){
             [MBProgressHUD hideHUDForView:[AppDelegate sharedInstance].window animated:YES];
             
         }else if (result.isCancelled){
             
             [MBProgressHUD hideHUDForView:[AppDelegate sharedInstance].window animated:YES];
             
         }else{
             if ([FBSDKAccessToken currentAccessToken]){
                 
                 [MBProgressHUD hideHUDForView:[AppDelegate sharedInstance].window animated:YES];
                 
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id,first_name,last_name, picture.type(large),email"}]
                  
                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                  {
                      if (!error){
                          
                          [self FBLoginWS:[result objectForKey:@"id"] Email:[result objectForKey:@"email"]];
                          
                      }else{
                          
                          [MBProgressHUD hideHUDForView:[AppDelegate sharedInstance].window animated:YES];
                          
                      }
                  }];
             }else{
                 
                 [MBProgressHUD hideHUDForView:[AppDelegate sharedInstance].window animated:YES];
                 
             }
         }
     }];
}

- (IBAction)switctBtnAction:(UIButton *)sender {
    
    if(switchFlag == TRUE)
    {
        switchFlag = FALSE;
        ;
         NSLog(@"language switch selected dutch");
        
        [_btnOR setTitle:@"OF" forState:UIControlStateNormal];
        
      
        
         [_btnFBLogin setTitle:@"Inloggen met Facebook" forState:UIControlStateNormal];
        
         [_btnLogin setTitle:@"Log nu in" forState:UIControlStateNormal];
        
        
        NSMutableAttributedString *registerHereString = [[NSMutableAttributedString alloc] initWithString:@"Nieuwe gebruiker? Registreer hier"];

        [registerHereString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [registerHereString length])];

        [_btnRegister setAttributedTitle:registerHereString forState:UIControlStateNormal];
        
       //  [_btnRegister setTitle:@"Nieuwe gebruiker? Registreer hier" forState:UIControlStateNormal];
        
        
        NSMutableAttributedString *forgotPwdString = [[NSMutableAttributedString alloc] initWithString:@"Wachtwoord vergeten"];

        [forgotPwdString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [forgotPwdString length])];

        [_btnForgotPassword setAttributedTitle:forgotPwdString forState:UIControlStateNormal];
        
        NSMutableAttributedString *continueWithoutLoginString = [[NSMutableAttributedString alloc] initWithString:@"Doorgaan zonder in te loggen"];

                        [continueWithoutLoginString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [continueWithoutLoginString length])];

            [_btnWithoutLogin setAttributedTitle:continueWithoutLoginString forState:UIControlStateNormal];
        
        
    //    [_btnForgotPassword setTitle:@"Wachtwoord vergeten" forState:UIControlStateNormal];
        
        NSMutableAttributedString *supplierLoginString = [[NSMutableAttributedString alloc] initWithString:@"Inloggen leverancier"];

             [supplierLoginString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [supplierLoginString length])];

             [_btnSupplierLogin setAttributedTitle:supplierLoginString forState:UIControlStateNormal];
             
        
        
          [_switchBtnOutlet setBackgroundImage:[UIImage imageNamed:@"btn-switch-selected"] forState:UIControlStateNormal];
        
       
        BOOL state = YES;//[sender isOn];
             NSString *newLang = state == YES ? @"nl" : @"en";
             NSLog(@"%@", newLang);
             
             [USERDEFAULTS setValue:newLang forKey:@"Language"];
             [USERDEFAULTS synchronize];
             
             NSDictionary *aDictLanguageData = [USERDEFAULTS objectForKey:@"Language"];
             NSLog(@"aDictLanguageData = %@", aDictLanguageData);
             
             [self SetUpPageForLanguageChange];
        
    }
    else{
        
        switchFlag = TRUE;
     NSLog(@"language switch selected English");
        
        [_btnOR setTitle:@"OR" forState:UIControlStateNormal];
        
          [_btnFBLogin setTitle:@"Login with Facebook" forState:UIControlStateNormal];
        
          [_btnLogin setTitle:@"Login now" forState:UIControlStateNormal];
        
        
        NSMutableAttributedString *registerHereString = [[NSMutableAttributedString alloc] initWithString:@"New User? Register Here"];

              [registerHereString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [registerHereString length])];

              [_btnRegister setAttributedTitle:registerHereString forState:UIControlStateNormal];
        
        // [_btnRegister setTitle:@"New User? Register Here" forState:UIControlStateNormal];
        
        
        
     NSMutableAttributedString *forgotPwdString = [[NSMutableAttributedString alloc] initWithString:@"Forgot password"];

          [forgotPwdString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [forgotPwdString length])];

          [_btnForgotPassword setAttributedTitle:forgotPwdString forState:UIControlStateNormal];
        
        
NSMutableAttributedString *continueWithoutLoginString = [[NSMutableAttributedString alloc] initWithString:@"Continue Without Login"];

    [continueWithoutLoginString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [continueWithoutLoginString length])];

[_btnWithoutLogin setAttributedTitle:continueWithoutLoginString forState:UIControlStateNormal];
        
        
        NSMutableAttributedString *supplierLoginString = [[NSMutableAttributedString alloc] initWithString:@"Supplier Login"];

        [supplierLoginString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [supplierLoginString length])];

        [_btnSupplierLogin setAttributedTitle:supplierLoginString forState:UIControlStateNormal];
        
     
        
      
        
         [_switchBtnOutlet setBackgroundImage:[UIImage imageNamed:@"btn-switch-deselected"] forState:UIControlStateNormal];
        
        BOOL state = NO;//[sender isOn];
                  NSString *newLang = state == YES ? @"nl" : @"en";
                  NSLog(@"%@", newLang);
                  
                  [USERDEFAULTS setValue:newLang forKey:@"Language"];
                  [USERDEFAULTS synchronize];
                  
                  NSDictionary *aDictLanguageData = [USERDEFAULTS objectForKey:@"Language"];
                  NSLog(@"aDictLanguageData = %@", aDictLanguageData);
                  
                  [self SetUpPageForLanguageChange];
        
         
    }
}

#pragma mark- UIButton Click Action Methods
    
- (IBAction)btnLoginClick:(id)sender {
    
    NSLog(@">>>>>>>>select city lat :%@",[AppDelegate sharedInstance].selectedCityLatitude);
    
    [AppDelegate sharedInstance].SettingFlagForLogn = @"0";
    
    if (![_txtEmail.text isValidString]) {
        [_txtEmail becomeFirstResponder];
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_EMAIL_ADDRESS"] Tag:@"0"];
        return;
    }else if (![_txtEmail.text isValidEmail]) {
        [_txtEmail becomeFirstResponder];
        [appDelegate showAlertView:[appDelegate getString:@"INVALID_EMAIL_ADDRESS"] Tag:@"0"];
        return;
    }else if (![_txtPassword.text isValidString]){
        [_txtPassword becomeFirstResponder];
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_PASSWORD"] Tag:@"0"];
        return;
    }else{
        [_txtEmail resignFirstResponder];
        [_txtPassword resignFirstResponder];
    }
    [self LoginWS];
}
    
- (IBAction)btnForgotPasswordClick:(id)sender{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    ForgotPassword *objForgotPassword = [storyboard instantiateViewControllerWithIdentifier:@"Forgot password"];
    [self.navigationController pushViewController:objForgotPassword animated:YES];
    
}
    
- (IBAction)btnRegisterClick:(id)sender{
    
    [_txtEmail resignFirstResponder];
    [_txtPassword resignFirstResponder];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    RegisterVC *objRegisterVC = [storyboard instantiateViewControllerWithIdentifier:@"RegisterVC"];
    [self.navigationController pushViewController:objRegisterVC animated:YES];
}
    
- (IBAction)btnShowPasswordClick:(id)sender {
    
    if (_txtPassword.text.length>0){
        if (!isShowOrNot){
            
            UIImage *btnImage = [UIImage imageNamed:@"passwordHide.png"];
        [_passwordViewImg setImage:btnImage forState:UIControlStateNormal];
            
         //   _passwordViewImg.ima
            _txtPassword.enabled = NO;
            _txtPassword.secureTextEntry = NO;
            _txtPassword.enabled = YES;
            isShowOrNot=YES;
        }else{
            UIImage *btnImage = [UIImage imageNamed:@"passwordShow.png"];
            [_passwordViewImg setImage:btnImage forState:UIControlStateNormal];
            _txtPassword.enabled = NO;
            _txtPassword.secureTextEntry = YES;
            _txtPassword.enabled = YES;
            isShowOrNot=NO;
        }
    }
    
}
    
- (IBAction)btnWithoutLoginClick:(id)sender {
    
    withoutLogin=TRUE;
    [AppDelegate sharedInstance].SettingFlagForLogn = @"1";
    
    [self GetCategoriesWS];
}

- (IBAction)btnSupplierLoginClick:(id)sender{
    
    NSURL *aURL = [NSURL URLWithString:SUPPLIER_APP_IDENTIFIER];
    
    if ([[UIApplication sharedApplication] canOpenURL:aURL]){
        [[UIApplication sharedApplication] openURL:aURL];
    }else{
    
        NSString *iTunesLink = @"itms://itunes.apple.com/us/app/apple-store/id375380948?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }
}

    
#pragma mark- UIButton Click Action Methods
    
- (void)FBLoginWS: (NSString *)strFbid Email:(NSString *)EmailAddress{
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  strFbid,@"fb_id",
                                  [USERDEFAULTS objectForKey:@"DeviceToken"],@"device_id",
                                  EmailAddress,@"email",
                                  @"",@"ip_address",
                                  
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_FB_loginWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            
            [[NSUserDefaults standardUserDefaults] setObject:[responseData objectForKey:@"data"]  forKey:@"UserLoginDetails"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [USERDEFAULTS setObject:[[[responseData objectForKey:@"data"] objectForKey:@"get_access_token"] objectForKey:@"rand_number"]  forKey:@"access_token"];
            [USERDEFAULTS setObject:[[[[responseData objectForKey:@"data"] objectForKey:@"get_fb_id"] objectAtIndex:0] objectForKey:@"c_customer_id"]  forKey:@"customer_id"];
            [USERDEFAULTS synchronize];
            
            [self GetCategoriesWS];
            
        }else{
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            RegisterVC *objRegistration = [storyboard instantiateViewControllerWithIdentifier:@"RegisterVC"];
            objRegistration.strFbId=strFbid;
            objRegistration.strFbEmail=EmailAddress;
            [self.navigationController pushViewController:objRegistration animated:YES];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
    
#pragma mark - CLLocationManagerDelegate
    
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
//    [self.imgViewBackground setImage:[UIImage imageNamed:@"BGHome"]];
    [self setBackgroundImage];
    
    NSLog(@"didFailWithError: %@", error);
//    NSDictionary *aDictLocationData = [[NSUserDefaults standardUserDefaults] objectForKey:@"LocationData"];
//    NSLog(@"aDictLocationData = %@", aDictLocationData);
//    [self.imgViewBackground sd_setImageWithURL:[aDictLocationData objectForKey:@"mc_city_image"]
//                              placeholderImage:nil
//                                       options:SDWebImageProgressiveDownload
//                                      progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//                                      }
//                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                                         self.imgViewBackground.clipsToBounds = YES;
//                                     }];
}
    
    
#pragma mark- Web Service Methods
    
-(void)callWebServicesForGetLoginBackImage{
    
    
    NSString *strLatitude = [USERDEFAULTS objectForKey:@"latitude"];
    NSString *strLongitude = [USERDEFAULTS objectForKey:@"longitude"];
    NSString *strLanguageId = [NSString stringWithFormat:@"EN"];
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  strLatitude,@"lat",
                                  strLongitude,@"long",
                                  strLanguageId,@"lang_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:get_background_imageWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData of callWebServicesForGetLoginBackImage = %@", responseData);
        [self GetCityFilterWS];
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            
            if([[responseData objectForKey:@"data"] count]>0)
            {
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"LocationData"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] setObject:[[responseData objectForKey:@"data"] objectAtIndex:0] forKey:@"LocationData"];
                [[NSUserDefaults standardUserDefaults] setObject:[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"mc_city_image"] forKey:@"lastCityBGImg"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                [AppDelegate sharedInstance].strCityId=[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"mc_city_id"];
                
                [AppDelegate sharedInstance].strCampaignPageTitle=[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"mc_city_2"];
                
                //Set top category
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SetTopCategory" object:nil];
                
                [self.imgViewBackground sd_setImageWithURL:[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"mc_city_image"]
                                          placeholderImage:nil
                                                   options:SDWebImageProgressiveDownload
                                                  progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                  }
                                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                     self.imgViewBackground.clipsToBounds = YES;
                                                 }];
                
                [AppDelegate sharedInstance].CurrentLat = [[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"mc_latitude"] floatValue];
                [AppDelegate sharedInstance].CurrentLong = [[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"mc_longitude"] floatValue];
                
            }else{
            
                //Call service again if not found (Server issue -> Some times google does not gives as city name that time try once again)
                //This problem occures only first time after installation
                
                if (!flagIsCityImageGetServiceCalledTwise){
                    
                    [self callWebServicesForGetLoginBackImage];
                    flagIsCityImageGetServiceCalledTwise = true;
                }
            }
        }
    } withFailureBlock:^(NSError *error) {
        [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedInstance].window animated:YES];
        [appDelegate showAlertView:[appDelegate getString:@"SOMETHINGWENTWRONG"] Tag:@"0"];
    }];
}
    
    
    
-(void)LoginWS{
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  _txtEmail.text,@"email",
                                  _txtPassword.text,@"password",
                                  [USERDEFAULTS objectForKey:@"DeviceToken"],@"device_id",
                                  nil];
    
    NSLog(@"%@",aDict);
    
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_loginWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        
        NSLog(@"responseData = %@", responseData);
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            
            [[NSUserDefaults standardUserDefaults] setObject:[[responseData objectForKey:@"data"] objectAtIndex:0] forKey:@"UserLoginDetails"];
            
            //Remember Me Setup
            NSString* aNewRememberEmail = flagRemember ? _txtEmail.text : nil;
            NSString* aNewRememberPass = flagRemember ? _txtPassword.text : nil;
            
            [USERDEFAULTS setObject:aNewRememberEmail forKey:kRememberEmail];
            [USERDEFAULTS setObject:aNewRememberPass forKey:kRememberPassword];
            
            [USERDEFAULTS setObject:[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"rand_number"]  forKey:@"access_token"];
            [USERDEFAULTS setObject:[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"c_customer_id"]  forKey:@"customer_id"];
            [USERDEFAULTS setObject:[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"c_first_name"]  forKey:@"FirstName"];
            [USERDEFAULTS setObject:[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"c_last_name"]  forKey:@"LastName"];
            [USERDEFAULTS setObject:[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"c_city_id"]  forKey:@"CustomerCity"];
            
            [USERDEFAULTS synchronize];
            
            
            NSString *strCustomerid=[USERDEFAULTS objectForKey:@"customer_id"];
            
            if(strCustomerid.length==0){
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:APPTITLE message:[appDelegate getString:@"Please check your credentials"] delegate:nil cancelButtonTitle:[appDelegate getString:@"OK"] otherButtonTitles:@"", nil];
                
                [alertView show];
            }else{
                strFirstName=[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"c_first_name"] ;
                [self GetCategoriesWS];
            }
        }else if([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 2){
            _lblComfirmationText.text = [NSString stringWithFormat:@"%@ %@. %@", [appDelegate getString:@"A confirmation mail has been sent to"], _txtEmail.text, [appDelegate getString:@"Click on the confirmation link in the mail to active your account"]];
            
            _confrimationAlertView.hidden = NO;
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
        
        [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedInstance].window animated:YES];
    } withFailureBlock:^(NSError *error) {
        [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedInstance].window animated:YES];
        [appDelegate showAlertView:[appDelegate getString:@"SOMETHINGWENTWRONG"] Tag:@"0"];
    }];
}
    
- (IBAction)btnActionToResendClick:(id)sender{
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_resendEmailWS withParams:[@{@"email":_txtEmail.text} mutableCopy] showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"respnce resend email WS = %@",responseData);
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1){
                [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:@"0"];
                _confrimationAlertView.hidden = YES;
            }
        });
    } withFailureBlock:^(NSError *error) {
        
    }];
    
}
- (IBAction)btnActionToOkClick:(id)sender{
    _confrimationAlertView.hidden = YES;
}
    
- (void)GetCategoriesWS{
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCategoryWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            NSLog(@"responseData = %@", responseData);
        
   if([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1)
        {
            
            [USERDEFAULTS setObject:[responseData objectForKey:@"data"] forKey:@"CategoryList"];
            
            
            NSLog(@">>>>>>>>select city lat :%@",[AppDelegate sharedInstance].selectedCityLatitude);
            
          //  if(strFirstName.length==0 && withoutLogin==FALSE)
            
            
          //   if(strFirstName.length==0 && withoutLogin==FALSE)
            
          //  [AppDelegate sharedInstance].strFlag=@"1"
           
         //    if([[AppDelegate sharedInstance].SettingFlagForLogn isEqualToString:@"0"] && self->withoutLogin==FALSE)
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                if (![defaults objectForKey:@"firstRun"]) {
                    [defaults setObject:@"0" forKey:@"firstRun"];
                   // [self displayTutorial];
                }
            
           /// if([[defaults objectForKey:@"firstRun"] isEqualToString:@"0"])
           // if([[defaults objectForKey:@"firstRun"] isEqualToString:@"0"]&& self->withoutLogin==FALSE)
            
            if(0)
            {
                 [defaults setObject:@"1" forKey:@"firstRun"];
                
                
                [AppDelegate sharedInstance].SettingFlagForLogn = @"1";
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
                ProfileContainerVC *objProfileContainerVC = [storyboard instantiateViewControllerWithIdentifier:@"ProfileContainerVC"];
                
                [self.navigationController pushViewController:objProfileContainerVC animated:YES];
            }
             else
            {
                
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
                [AppDelegate sharedInstance].window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                UITabBarController *tb = [storyboard instantiateViewControllerWithIdentifier:@"tabBarController"];
                tb.tabBar.barTintColor= [UIColor whiteColor];//[appDelegate colorWithHexString:@"eeeeee"];
                [tb.tabBar setBackgroundImage:nil];
                [tb.tabBar setClipsToBounds:YES];
                UIViewController *leftSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"leftSideMenuVC"];
                MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                                containerWithCenterViewController:tb
                                                                leftMenuViewController:leftSideMenuViewController
                                                                rightMenuViewController:nil];
                
                
                //Get Campaigns data in CampaignListVC campaigns in it's ViewWillAppear
                [AppDelegate sharedInstance].isLocationGetCalledBeforeListVCLoaded = TRUE;
                
                [AppDelegate sharedInstance].window.rootViewController = container;
                [[AppDelegate sharedInstance].window makeKeyAndVisible];
            }
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedInstance].window animated:YES];
        [appDelegate showAlertView:[appDelegate getString:@"SOMETHINGWENTWRONG"] Tag:@"0"];
    }];
}
    
#pragma mark SetUpPage
    
-(void)SetUpPage{
    
    //Remember Me Setup
    
    flagRemember = [USERDEFAULTS objectForKey:kRememberEmail] && [USERDEFAULTS objectForKey:kRememberPassword];
    
    NSString *aStrImgRemember = flagRemember ? @"radio_on": @"radio_off";
    _imgRadio.image = [UIImage imageNamed:aStrImgRemember];
    
    _txtEmail.text = flagRemember ? [USERDEFAULTS objectForKey:kRememberEmail] : @"";
    _txtPassword.text = flagRemember ? [USERDEFAULTS objectForKey:kRememberPassword] : @"";
    
    //
    
    
    [self.navigationController.navigationBar setHidden:YES];
    
//    NSDictionary *aDictLocationData = [[NSUserDefaults standardUserDefaults] objectForKey:@"LocationData"];
    
//    if (![aDictLocationData objectForKey:@"mc_city_image"]) {
    
//        [self.imgViewBackground setImage:[UIImage imageNamed:@"BGHome"]];
    
//    }else{
//        
//        [self.imgViewBackground sd_setImageWithURL:[aDictLocationData objectForKey:@"mc_city_image"]
//                                  placeholderImage:nil
//                                           options:SDWebImageProgressiveDownload
//                                          progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//                                          }
//                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                                             self.imgViewBackground.clipsToBounds = YES;
//                                         }];
//    }
    
    
    [UIApplication sharedApplication].statusBarHidden = NO;
    
    self.navigationController.navigationBarHidden = YES;
    
    UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
    
    [self updateMotionEffectForOrientation:orientation];
    
    // SetBackground image notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callWebServicesForGetLoginBackImage) name:@"getLocation" object:nil];
    
}
- (void)GetCityFilterWS{
    
    [USERDEFAULTS setObject:[USERDEFAULTS objectForKey:@"latitude"] forKey:@"Userlatitude"];
    [USERDEFAULTS setObject:[USERDEFAULTS objectForKey:@"longitude"] forKey:@"Userlongitude"];
    
    [USERDEFAULTS synchronize];
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"latitude"],@"latitude",
                                  [USERDEFAULTS objectForKey:@"longitude"],@"longitude",
                                  nil];
    
    
    
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCityLocationWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        
        NSLog(@"responseData of GetCityFilterWS = %@", responseData);
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            if([[responseData objectForKey:@"data"] count]>0 )
            {
                [AppDelegate sharedInstance].strCampaignPageTitle=[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"city"];
                [AppDelegate sharedInstance].strCityId=[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"city_id_zip"];
                
                [USERDEFAULTS setObject:[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"city_id_zip"] forKey:@"City_id"];
                
                [USERDEFAULTS setObject:[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"city"] forKey:@"City_Name"];
            }
            
            
        }
    } withFailureBlock:^(NSError *error) {
        
        NSLog(@"%@",error);
    }];
}

    @end
