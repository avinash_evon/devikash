//
//  LoginVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface LoginVC : UIViewController
{
    IBOutlet NSLayoutConstraint *imgViewLogoTopConstraint;
    
    
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *languageHeightBox;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *languageRightPadding;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *languageSpacing;


@property (weak, nonatomic) IBOutlet UIButton *btnOR;


 @property (nonatomic) IBInspectable CGFloat padding;// avinash

@property (weak, nonatomic) IBOutlet UIView *languageOutlet;


@property (weak, nonatomic) IBOutlet UIButton *switchBtnOutlet;


@property (retain, nonatomic) IBOutlet UIScrollView *scrlView;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewBackground;

@property (weak, nonatomic) IBOutlet UIButton *passwordViewImg;


@property (weak, nonatomic) IBOutlet UIView *viewLogingfield;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@property (weak, nonatomic) IBOutlet UIButton *btnFBLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnWithoutLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnSupplierLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnRemember;

@property (weak, nonatomic) IBOutlet UIImageView *imgRadio;

@property (weak, nonatomic) IBOutlet UILabel *lblEnglish;
@property (weak, nonatomic) IBOutlet UILabel *lblDutch;
@property (weak, nonatomic) IBOutlet UILabel *lblVersion;

// Confirmation Alert View
@property (strong, nonatomic) IBOutlet UIView *confrimationAlertView;
@property (strong, nonatomic) IBOutlet UIView *confirmationInnerView;
@property (strong, nonatomic) IBOutlet UILabel *lblComfirmationText;
@property (strong, nonatomic) IBOutlet UISwitch *switchLanguage;

- (IBAction)btnActionToResendClick:(id)sender;
- (IBAction)btnActionToOkClick:(id)sender;

- (IBAction)btnRegisterClick:(id)sender;
- (IBAction)btnShowPasswordClick:(id)sender;
- (IBAction)btnForgotPasswordClick:(id)sender;
- (IBAction)btnWithoutLoginClick:(id)sender;
- (IBAction)btnLoginClick:(id)sender;
- (IBAction)btnFBLoginClick:(id)sender;
- (IBAction)switctBtnAction:(UIButton *)sender;

@end
