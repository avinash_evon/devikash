//
//  ForgotPassword.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "ForgotPassword.h"
#import "QuestionVC.h"

@interface ForgotPassword ()

@end

@implementation ForgotPassword

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary *aDictLocationData = [[NSUserDefaults standardUserDefaults] objectForKey:@"LocationData"];
    
    if (![aDictLocationData objectForKey:@"mc_city_image"]) {
        
       // [self.imgViewBackground setImage:[UIImage imageNamed:@"homenew"]];
        
        NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: @"http://d3tsh6gqrcm0nz.cloudfront.net/supplier_image/5/51556881355_1000x800.jpg"]];
        self.imgViewBackground.image = [UIImage imageWithData: imageData];
        
    }else{
        
        [self.imgViewBackground sd_setImageWithURL:[aDictLocationData objectForKey:@"mc_city_image"]
                                  placeholderImage:nil
                                           options:SDWebImageProgressiveDownload
                                          progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                          }
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                             self.imgViewBackground.clipsToBounds = YES;
                                         }];
    }
    
    [_ViewText.layer setCornerRadius:2.5f];
    
    [_ViewText.layer setMasksToBounds:YES];
    
    _btnSendmail.layer.cornerRadius = 2.5f;
    
    _lblFirstDescription.text = [appDelegate getString:@"Enter email address associated with your account to reset your password"];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    
    UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
    
    [self updateMotionEffectForOrientation:orientation];
    
}



#pragma mark UIInterfaceOrientation Methods

- (void)updateMotionEffectForOrientation:(UIInterfaceOrientation)orientation{
   
    if(UIInterfaceOrientationIsPortrait(orientation)){
    
        if (IS_IPAD) {
        
            imgViewLogoTopConstraint.constant = 200;
            
        }else{
            
            imgViewLogoTopConstraint.constant = 120;
            
        }
    }else{

        if (IS_IPAD) {
            
            imgViewLogoTopConstraint.constant = 150;
            
        }else{
            
            imgViewLogoTopConstraint.constant = 50;
            
        }
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
   
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
    
        if (IS_IPAD) {
            
            imgViewLogoTopConstraint.constant = 200;
            
        }else{
            
            imgViewLogoTopConstraint.constant = 120;
            
        }
    }
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft){

        if (IS_IPAD) {
            
            imgViewLogoTopConstraint.constant = 150;
            
        }else{
            
            imgViewLogoTopConstraint.constant = 50;
            
        }
    }
}

#pragma mark- UIButton Click Action Methods

- (IBAction)btnSendMailClick:(id)sender {
    
    if (![_txtEmail.text isValidString]) {
        
        
        [appDelegate showAlertView :[appDelegate getString:@"ENTER_EMAIL_ADDRESS"] Tag:@"0"];
        
        return;
        
    }else if (![_txtEmail.text isValidEmail]){
        
        
        [appDelegate showAlertView:[appDelegate getString:@"INVALID_EMAIL_ADDRESS"] Tag:@"0"];
        
        return;
        
    }
    [self.view endEditing:YES];
    [self ForgotPasswordWS];
}

- (IBAction)btnLoginClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- Web Service Methods

-(void)ForgotPasswordWS{
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                           _txtEmail.text,@"email",
                           nil];
   
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_ForgotPassword_QuestionWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
       
        NSLog(@"responseData = %@", responseData);
    
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
        
            _txtEmail.text=@"";
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            
            QuestionVC *objQuestionVC = [storyboard instantiateViewControllerWithIdentifier:@"QuestionVC"];
            
            objQuestionVC.mutArrQuestionAnswer=[responseData objectForKey:@"data"];
            
            [self.navigationController pushViewController:objQuestionVC animated:YES];
            
        }else if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 2) {
            
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:APPTITLE message:[[responseData objectForKey:@"settings"] objectForKey:@"message"] delegate:self cancelButtonTitle:[appDelegate getString:@"OK"] otherButtonTitles:nil, nil];
            alertView.tag=200;
            [alertView show];
            
        }else{
            
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark AlertMethod
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag==200){
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
