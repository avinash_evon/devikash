//
//  FilterHomePageButton.h
//  iKash
//
//  Created by indianic on 05/01/16.
//  Copyright © 2016 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterHomePageButton : UIButton
@property (strong, nonatomic) NSIndexPath *btnCampaignFilterButtonIndexPath;
@end
