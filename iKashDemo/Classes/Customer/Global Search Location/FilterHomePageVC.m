//
//  FilterHomePageVC.m
//  iKash
//
//  Created by indianic on 05/01/16.
//  Copyright © 2016 indianic. All rights reserved.
//

#import "FilterHomePageVC.h"
#import "FilterHomePageCell.h"
@interface FilterHomePageVC ()

@end

@implementation FilterHomePageVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [self SetUpPage];
    isSearching = NO;
    if(!IS_IPAD)
    {
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        
        [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            self.ContraintHeight.constant=38;
            
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            self.ContraintHeight.constant=38;
            
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:break;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(!IS_IPAD)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

#pragma mark - UICollectionViewDelegate methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([strSelectedCampaign isEqualToString:@"City"]){
        if (isSearching) {
            return [mutArrCampaignCitySearchList count];
        }else{
            return [[dicCampaignDetails objectForKey:@"dicCityDataList"] count];
        }
    }else{
        if (isSearching) {
            return [mutArrCampaignRadiusSearchList count];
        }else{
            return [[dicCampaignDetails objectForKey:@"dicRadiusDataList"] count];
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FilterHomePageCell *cellCampaignFilter = [tableView dequeueReusableCellWithIdentifier:@"FilterHomePageCell"];
    cellCampaignFilter.btnFilterSelection.btnCampaignFilterButtonIndexPath = indexPath;
    if ([strSelectedCampaign isEqualToString:@"City"]) {
        if (isSearching){
            cellCampaignFilter.lblFilterTitle.text = [[mutArrCampaignCitySearchList objectAtIndex:indexPath.row] objectForKey:@"city_name"];
            [cellCampaignFilter.btnFilterSelection setImage:[UIImage imageNamed:@"btn-radio"] forState:UIControlStateNormal];
            NSMutableDictionary *dictItem =[mutArrCampaignCitySearchList objectAtIndex:indexPath.row];
            if ([mutArrSelectedCampaignCityListAdded containsObject:dictItem]) {
                [cellCampaignFilter.btnFilterSelection setImage:[UIImage imageNamed:@"btn-radio-filled"] forState:UIControlStateNormal];
            }
        }else{
            cellCampaignFilter.lblFilterTitle.text = [[[dicCampaignDetails objectForKey:@"dicCityDataList"] objectAtIndex:indexPath.row] objectForKey:@"city_name"];
            [cellCampaignFilter.btnFilterSelection setImage:[UIImage imageNamed:@"btn-radio"] forState:UIControlStateNormal];
            NSMutableDictionary *dictItem =[[dicCampaignDetails objectForKey:@"dicCityDataList"] objectAtIndex:indexPath.row];
            if ([mutArrSelectedCampaignCityListAdded containsObject:dictItem]) {
                [cellCampaignFilter.btnFilterSelection setImage:[UIImage imageNamed:@"btn-radio-filled"] forState:UIControlStateNormal];
            }
        }
    }else{
        if (isSearching){
            
            if([[mutArrCampaignRadiusSearchList objectAtIndex:indexPath.row] isEqualToString:@"NONE"])
            {
                cellCampaignFilter.lblFilterTitle.text = [mutArrCampaignRadiusSearchList objectAtIndex:indexPath.row];
                
            }else{
                cellCampaignFilter.lblFilterTitle.text = [NSString stringWithFormat:@"%@%@",[mutArrCampaignRadiusSearchList objectAtIndex:indexPath.row],@" km"];
                
                
            }
            [cellCampaignFilter.btnFilterSelection setImage:[UIImage imageNamed:@"btn-radio"] forState:UIControlStateNormal];
            NSMutableDictionary *dictItem =[mutArrCampaignRadiusSearchList objectAtIndex:indexPath.row];
            if ([mutArrSelectedCampaignRadiusListAdded containsObject:dictItem]) {
                //                [cellCampaignFilter.btnFilterSelection setBackgroundImage:[UIImage imageNamed:@"btn-radio-filled"] forState:UIControlStateNormal];
                [cellCampaignFilter.btnFilterSelection setImage:[UIImage imageNamed:@"btn-radio-filled"] forState:UIControlStateNormal];
            }
        }else{
            
            if([[[dicCampaignDetails objectForKey:@"dicRadiusDataList"] objectAtIndex:indexPath.row] isEqualToString:@"NONE"])
            {
                cellCampaignFilter.lblFilterTitle.text = [[dicCampaignDetails objectForKey:@"dicRadiusDataList"] objectAtIndex:indexPath.row];
                
            }else{
                cellCampaignFilter.lblFilterTitle.text = [NSString stringWithFormat:@"%@%@",[[dicCampaignDetails objectForKey:@"dicRadiusDataList"] objectAtIndex:indexPath.row],@" km"];
                
                
            }
            
            
            [cellCampaignFilter.btnFilterSelection setImage:[UIImage imageNamed:@"btn-radio"] forState:UIControlStateNormal];
            NSMutableDictionary *dictItem =[[dicCampaignDetails objectForKey:@"dicRadiusDataList"] objectAtIndex:indexPath.row];
            if ([mutArrSelectedCampaignRadiusListAdded containsObject:dictItem]) {
                
                [cellCampaignFilter.btnFilterSelection setImage:[UIImage imageNamed:@"btn-radio-filled"] forState:UIControlStateNormal];
            }
        }
    }
    
    [[cellCampaignFilter btnFilterSelection] addTarget:self action:@selector(CampainFilterSelectionClick:) forControlEvents:UIControlEventTouchUpInside];
    return cellCampaignFilter;
}

#pragma mark ----------------------
#pragma mark CampainFilterSelectionClick Selection..

- (IBAction)CampainFilterSelectionClick:(FilterHomePageButton*)sender{
    indFilterPath = sender.btnCampaignFilterButtonIndexPath;
    NSLog(@"selected index = %ld",(long)indFilterPath.row);
    NSMutableDictionary *dictItem;
    if (isSearching){
        if ([strSelectedCampaign isEqualToString:@"City"]){
            dictItem =[mutArrCampaignCitySearchList objectAtIndex:indFilterPath.row];
        }else{
            dictItem =[mutArrCampaignRadiusSearchList objectAtIndex:indFilterPath.row];
        }
    }
    else{
        if ([strSelectedCampaign isEqualToString:@"City"]){
            dictItem =[[dicCampaignDetails objectForKey:@"dicCityDataList"] objectAtIndex:indFilterPath.row];
        }else{
            dictItem =[[dicCampaignDetails objectForKey:@"dicRadiusDataList"] objectAtIndex:indFilterPath.row];
        }
    }
    
    if ([strSelectedCampaign isEqualToString:@"Radius"]) {
        
        [mutArrSelectedCampaignRadiusListAdded removeAllObjects];
        [mutArrSelectedCampaignRadiusListAdded addObject:dictItem];
        [tblCampaignFliter reloadData];
        
    }else{
        [mutArrSelectedCampaignCityListAdded removeAllObjects];
        [mutArrSelectedCampaignCityListAdded addObject:dictItem];
        [tblCampaignFliter reloadData];
    }
}

#pragma mark- UIButton Click Action Methods

- (IBAction)btnBackAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if (_popoverDismiss) {
            _popoverDismiss();
        }
    }];
}

- (IBAction)btnApplyAction:(id)sender {
    
    NSLog(@"City array = %@",mutArrSelectedCampaignCityListAdded);
    NSLog(@"Radius array = %@",mutArrSelectedCampaignRadiusListAdded);
    
    NSLog(@"my city latitude : %@",[[mutArrSelectedCampaignCityListAdded objectAtIndex:0] objectForKey:@"mc_latitude"]);
    
    [AppDelegate sharedInstance].selectedCityLatitude = [[mutArrSelectedCampaignCityListAdded objectAtIndex:0] objectForKey:@"mc_latitude"];
   
    [AppDelegate sharedInstance].selectedCityLongitude = [[mutArrSelectedCampaignCityListAdded objectAtIndex:0] objectForKey:@"mc_longitude"];
    
    
    //    if([mutArrSelectedCampaignRadiusListAdded count]>0){
    //        
    //        if([[mutArrSelectedCampaignRadiusListAdded objectAtIndex:0] isEqualToString:@"NONE"]){
    //            
    //            [mutArrSelectedCampaignRadiusListAdded removeAllObjects];
    //            
    //        }
    //    }
    
    NSMutableArray *mutArrtempRadius=[[NSMutableArray alloc]initWithArray:mutArrSelectedCampaignRadiusListAdded];
    
    [[AppDelegate sharedInstance].mutArrRadius removeAllObjects];
    
    [AppDelegate sharedInstance].mutArrRadius=mutArrtempRadius;
    
    [[AppDelegate sharedInstance].mutArrCityIDS removeAllObjects];
    [AppDelegate sharedInstance].strPageTitleFilter=@"";
    [AppDelegate sharedInstance].mutArrSelectedCampaignCityListAdded = mutArrSelectedCampaignCityListAdded;
    [AppDelegate sharedInstance].mutArrSelectedCampaignRadiusListAdded = mutArrSelectedCampaignRadiusListAdded;
    
    NSString *strCityId;
    NSString *strCityName;
    
    if([mutArrSelectedCampaignCityListAdded count]>0)
    {
        [[AppDelegate sharedInstance].mutArrCityIDS addObject:[[mutArrSelectedCampaignCityListAdded objectAtIndex:0] objectForKey:@"city_id"]];
        
        strCityId=[[mutArrSelectedCampaignCityListAdded objectAtIndex:0] objectForKey:@"city_id"];
        strCityName=[[mutArrSelectedCampaignCityListAdded objectAtIndex:0] objectForKey:@"city_name"];
    [USERDEFAULTS setObject:[[mutArrSelectedCampaignCityListAdded objectAtIndex:0] objectForKey:@"mc_latitude"] forKey:@"latitude"];
        [USERDEFAULTS setObject:[[mutArrSelectedCampaignCityListAdded objectAtIndex:0] objectForKey:@"mc_longitude"] forKey:@"longitude"];
        
        [AppDelegate sharedInstance].CurrentLat = [[[mutArrSelectedCampaignCityListAdded objectAtIndex:0] objectForKey:@"mc_latitude"] floatValue];
        [AppDelegate sharedInstance].CurrentLong = [[[mutArrSelectedCampaignCityListAdded objectAtIndex:0] objectForKey:@"mc_longitude"] floatValue];
        
    }
    
    if(strCityId.length>0){
        
        [AppDelegate sharedInstance].strCityId=strCityId;
        [AppDelegate sharedInstance].strFilterCityId = strCityId;
        
        [AppDelegate sharedInstance].strCampaignPageTitle=strCityName;
        [AppDelegate sharedInstance].strFlgSearch=@"1";
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchCityCampaign" object:nil];
        
        
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PageTitle" object:nil];
    
    [self dismissViewControllerAnimated:YES completion:^{
        if (_popoverDismiss) {
            _popoverDismiss();
        }
    }];
}

- (IBAction)btnCityAction:(id)sender {
    
    if(![strSelectedCampaign isEqualToString:@"City"])
    {
        isSearching = NO;
        searchBar.text=@"";
        [searchBar setShowsCancelButton:NO animated:YES];
        [searchBar resignFirstResponder];
        
        searchBar.placeholder = [appDelegate getString:@"Search City"];
        
        
        [tblCampaignFliter reloadData];
        
        strSelectedCampaign = @"City";
        
        searchBar.hidden = NO;
//        topTableCampaign.constant = 108.0f;

//        tblCampaignFliter.frame = CGRectMake(140,108,180,460);
        [btnCity setBackgroundColor:[UIColor whiteColor]];
        [btnCity setTitleColor:[UIColor colorWithRed:42.0/255.0 green:92.0/255.0 blue:230.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        
        [btnRadis setBackgroundColor:[UIColor clearColor]];
        [btnRadis setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        searchBar.keyboardType = UIKeyboardTypeDefault;
        //Get Campaign City Deails.
        //[self GetCampaignCityFilterWS];
    }
}

- (IBAction)btnRadiusAction:(id)sender {
    
    if(![strSelectedCampaign isEqualToString:@"Radius"])
    {
        isSearching = NO;
        searchBar.text=@"";
        [searchBar setShowsCancelButton:NO animated:YES];
        [searchBar resignFirstResponder];
        searchBar.placeholder = [appDelegate getString:@"km"];
        strSelectedCampaign = @"Radius";
        [tblCampaignFliter reloadData];
        searchBar.hidden = NO;
//        tblCampaignFliter.frame = CGRectMake(140,108,180,460);
//        topTableCampaign.constant = 108.0f;
        [btnRadis setBackgroundColor:[UIColor whiteColor]];
        [btnRadis setTitleColor:[UIColor colorWithRed:42.0/255.0 green:92.0/255.0 blue:230.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [btnCity setBackgroundColor:[UIColor clearColor]];
        [btnCity setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        searchBar.keyboardType = UIKeyboardTypeNumberPad;
    }
}

- (IBAction)btnClearAllAction:(id)sender {
    [mutArrSelectedCampaignCityListAdded removeAllObjects];
    [mutArrSelectedCampaignRadiusListAdded removeAllObjects];
    [tblCampaignFliter reloadData];
}

#pragma mark- UISearchbar Methods

-(void)searchBarSearchButtonClicked:(UISearchBar *)aSearchBar{
    [searchBar resignFirstResponder];
    isSearching = YES;
    if ([strSelectedCampaign isEqualToString:@"City"]) {
        NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"city_name BEGINSWITH[c] %@", aSearchBar.text];
        mutArrCampaignCitySearchList = [[mutArrCampaignCityList filteredArrayUsingPredicate:aPredicate] mutableCopy];
        // aSearchBar.keyboardType = UIKeyboardTypeDefault;
    }else{
        NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS %@", aSearchBar.text];
        mutArrCampaignRadiusSearchList = [[mutArrCampaignRadiusList filteredArrayUsingPredicate:aPredicate] mutableCopy];
        // aSearchBar.keyboardType = UIKeyboardTypeNumberPad;
    }
    [tblCampaignFliter reloadData];
}

-(void)searchBar:(UISearchBar *)aSearchBar textDidChange:(NSString *)aSearchText{
    if (![aSearchText isValidString]) {
        isSearching = NO;
        [aSearchBar resignFirstResponder];
        [tblCampaignFliter reloadData];
    }else{
        isSearching = YES;
        if ([strSelectedCampaign isEqualToString:@"City"]) {
            NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"city_name BEGINSWITH[c] %@", aSearchText];
            mutArrCampaignCitySearchList = [[mutArrCampaignCityList filteredArrayUsingPredicate:aPredicate] mutableCopy];
        }else{
            NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS %@", aSearchText];
            mutArrCampaignRadiusSearchList = [[mutArrCampaignRadiusList filteredArrayUsingPredicate:aPredicate] mutableCopy];
        }
        [tblCampaignFliter reloadData];
    }
    if([aSearchText length] == 0) {
        [searchBar performSelector: @selector(resignFirstResponder)
                        withObject: nil
                        afterDelay: 0.1];
    }
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)aSearchBar{
    //    if ([strSelectedCampaign isEqualToString:@"City"]) {
    //        aSearchBar.keyboardType = UIKeyboardTypeDefault;
    //    }else{
    //        aSearchBar.keyboardType = UIKeyboardTypeNumberPad;
    //    }
    //  [aSearchBar setShowsCancelButton:YES animated:YES];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)aSearchBar{
    isSearching = NO;
    [aSearchBar setShowsCancelButton:NO animated:YES];
    [aSearchBar resignFirstResponder];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)aSearchBar{
    [aSearchBar resignFirstResponder];
    [aSearchBar setShowsCancelButton:NO animated:YES];
    
    if ([aSearchBar.text length] <= 0){
        isSearching = NO;
        [tblCampaignFliter reloadData];
    }else{
        isSearching = YES;
        [tblCampaignFliter reloadData];
    }
}

#pragma mark SetUpPage

-(void)SetUpPage{
    self.navigationController.navigationBarHidden = YES;
    
    NSLog(@"City selected = %@",[AppDelegate sharedInstance].mutArrCityIDS);
    NSLog(@"radius selected = %@",[AppDelegate sharedInstance].mutArrRadius);
    
    strSelectedCampaign=@"City";
    mutArrCampaignCitySearchList = [[NSMutableArray alloc] init];
    mutArrCampaignRadiusSearchList = [[NSMutableArray alloc] init];
    mutArrCampaignCityList = [[NSMutableArray alloc] init];
    mutArrSelectedCampaignCityListAdded = [[NSMutableArray alloc] init];
    mutArrSelectedCampaignRadiusListAdded = [[NSMutableArray alloc] init];
    
    //    if ([[AppDelegate sharedInstance].mutArrSelectedCampaignCityListAdded count] > 0) {
    //        mutArrSelectedCampaignCityListAdded = [AppDelegate sharedInstance].mutArrSelectedCampaignCityListAdded;
    //    }
    if ([[AppDelegate sharedInstance].mutArrSelectedCampaignRadiusListAdded count] > 0) {
        mutArrSelectedCampaignRadiusListAdded = [AppDelegate sharedInstance].mutArrSelectedCampaignRadiusListAdded;
    }else if([AppDelegate sharedInstance].strRadius.length>0) {
        [mutArrSelectedCampaignRadiusListAdded addObject:[AppDelegate sharedInstance].strRadius];
        
    }
    mutArrCampaignRadiusList = [NSMutableArray arrayWithObjects:@"NONE",@"3",@"5",@"10",@"15",@"20",@"25",@"30",@"35",@"50",@"75", nil];
    dicCampaignDetails = [NSMutableDictionary dictionaryWithObjects:@[mutArrCampaignCityList,mutArrCampaignRadiusList] forKeys:@[@"dicCityDataList",@"dicRadiusDataList"]];
    
    strSelectedCampaign = @"City";
    [btnCity setTitleColor:[UIColor colorWithRed:42.0/255.0 green:92.0/255.0 blue:230.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [btnCity setBackgroundColor:[UIColor whiteColor]];
    [btnRadis setBackgroundColor:[UIColor clearColor]];
    [btnRadis setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    //    searchBar.hidden = YES;
    [searchBar setImage:[[UIImage imageNamed: @"ic-nav-search"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    searchBar.backgroundColor = [UIColor whiteColor];
    UITextField *textField = [searchBar searchTextField];
    textField.layer.borderWidth = 0.5f; //To hide the square corners
    textField.layer.borderColor = [[appDelegate colorWithHexString:@"CCCCCC"] CGColor]; //assigning the default border color
    textField.layer.cornerRadius = 2.5f;
    [textField setFont:[UIFont fontWithName:FontRobotoLight size:12]];
//    [searchBar setValue:[UIColor colorWithHexString:@"#666666"] forKeyPath:@"_searchField._placeholderLabel.textColor"];
    searchBar.searchTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#666666"]}];
    searchBar.layer.borderColor = [[UIColor whiteColor] CGColor];//.CGColor;
    searchBar.backgroundColor = [UIColor whiteColor];
    searchBar.layer.borderWidth = 1;
    searchBar.placeholder = [appDelegate getString:@"Search City"];
    
    topTableCampaign.constant = 108.0f;
    tblCampaignFliter.frame = CGRectMake(140,108,180,460);
    
    tblCampaignFliter.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [_btnCancel setTitle:[appDelegate getString:@"Cancel"] forState:UIControlStateNormal];
    [_btnApply setTitle:[appDelegate getString:@"Apply"] forState:UIControlStateNormal];
    [btnCity setTitle:[appDelegate getString:@"City"] forState:UIControlStateNormal];
    [btnRadis setTitle:[appDelegate getString:@"Radius"] forState:UIControlStateNormal];
    [_btnClearAll setTitle:[appDelegate getString:@"Clear all"] forState:UIControlStateNormal];
    _lblNavTitle.text=self.strNavTitle;//[appDelegate getString:@"Filter"];
    [self GetCampaignCityFilterWS];
    //Get Comapign Category Deails.
}

- (void)GetCampaignCityFilterWS{
    
    
    //    NSString *strLocalRadius;
    //
    //    if([mutArrSelectedCampaignRadiusListAdded count]>0){
    //
    //        strLocalRadius=[mutArrSelectedCampaignRadiusListAdded objectAtIndex:0];
    //        
    //    }else{
    //        
    //        strLocalRadius=[AppDelegate sharedInstance].strRadius;
    //        
    //    }
    //    
    //    
    //    NSMutableDictionary *aDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
    //                                  [NSString stringWithFormat:@"%f",[AppDelegate sharedInstance].CurrentLat],@"latitude",
    //                                  [NSString stringWithFormat:@"%f",[AppDelegate sharedInstance].CurrentLong],@"longitude",
    //                                  strLocalRadius,@"radius",
    //                                  
    //                                  nil];
    //    NSLog(@"%@",aDictionary);
    
    NSMutableDictionary *aDictionary = [NSMutableDictionary dictionaryWithObjects:@[@""] forKeys:@[@"city"]];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCityWS withParams:aDictionary showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            mutArrCampaignCityList = [responseData objectForKey:@"data"];
            dicCampaignDetails[@"dicCityDataList"] = mutArrCampaignCityList;
            if([mutArrSelectedCampaignCityListAdded count]==0 )
            {
                
                if(![[AppDelegate sharedInstance].strCityId isEqualToString:@"0"])
                {
                    NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"city_id matches[c] %@", [AppDelegate sharedInstance].strCityId];
                    mutArrSelectedCampaignCityListAdded = [[mutArrCampaignCityList filteredArrayUsingPredicate:aPredicate] mutableCopy];
                }
            }
            [tblCampaignFliter reloadData];
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
    
    
    //    NSMutableDictionary *aDictionary = [NSMutableDictionary dictionaryWithObjects:@[@""] forKeys:@[@"city"]];
    //    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCityWS withParams:aDictionary showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
    //        NSLog(@"responseData = %@", responseData);
    //        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
    //            mutArrCityList = [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"]];
    //            [_tblViewGlobalSearchLocation reloadData];
    //        }else{
    //            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
    //        }
    //    } withFailureBlock:^(NSError *error) {
    //        NSLog(@"%@",error);
    //    }];
    
}

@end
