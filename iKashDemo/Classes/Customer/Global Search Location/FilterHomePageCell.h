//
//  FilterHomePageCell.h
//  iKash
//
//  Created by indianic on 05/01/16.
//  Copyright © 2016 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterHomePageButton.h"
@interface FilterHomePageCell : UITableViewCell
@property (strong, nonatomic) IBOutlet FilterHomePageButton *btnFilterSelection;
@property (strong, nonatomic) IBOutlet UILabel *lblFilterTitle;
@end


