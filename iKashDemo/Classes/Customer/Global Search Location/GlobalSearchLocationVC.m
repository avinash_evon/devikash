//
//  GlobalSearchLocationVC.m
//  iKashDemo
//
//  Created by IndiaNIC on 05/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "GlobalSearchLocationVC.h"
#import "GlobalSearchLocationCell.h"
#import "NotificationVC.h"

@interface GlobalSearchLocationVC (){
    NSMutableArray *mutArrCityList;
    NSString *strCurrentCityName;
}

@end

@implementation GlobalSearchLocationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tblViewGlobalSearchLocation.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [self SetUpPage];
    NSDictionary *aDictLocationData = [[NSUserDefaults standardUserDefaults] objectForKey:@"LocationData"];
    
    if([aDictLocationData count]>0){
        strCurrentCityName=[aDictLocationData objectForKey:@"mc_city_2"];
    }
    NSLog(@"%@",aDictLocationData);
    [self GetDefaultCampaignCityFilterWS];
    if(!IS_IPAD)
    {
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        
        [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            self.ContraintHeight.constant=38;
            
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            self.ContraintHeight.constant=38;
            
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:break;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(!IS_IPAD)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

# pragma mark - Tableview Delegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [mutArrCityList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    GlobalSearchLocationCell *globalSearchLocationCell = (GlobalSearchLocationCell *)[tableView dequeueReusableCellWithIdentifier:@"GlobalSearchLocationCell"];
    //shopListTableCell.backgroundColor=[UIColor redColor];
    [globalSearchLocationCell setSelectionStyle:UITableViewCellSelectionStyleNone];
     globalSearchLocationCell.lblTitle.text=[[mutArrCityList objectAtIndex:indexPath.row] objectForKey:@"city_name"];
     globalSearchLocationCell.lblSubTitle.text=[[mutArrCityList objectAtIndex:indexPath.row] objectForKey:@"ms_state"];
    
    if([strCurrentCityName isEqualToString:[[mutArrCityList objectAtIndex:indexPath.row] objectForKey:@"city_name"]]){
        globalSearchLocationCell.lblTitle.textColor = kAppSupportedColorThemeColor;
        globalSearchLocationCell.lblSubTitle.textColor = kAppSupportedColorThemeColor;
    }else{
        globalSearchLocationCell.lblTitle.textColor = kAppSupportedColorNormalColor;
        globalSearchLocationCell.lblSubTitle.textColor = kAppSupportedColorNormalColor;
    }
    
    return globalSearchLocationCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [USERDEFAULTS setObject:[[mutArrCityList objectAtIndex:indexPath.row] objectForKey:@"mc_latitude"] forKey:@"latitude"];
    [USERDEFAULTS setObject:[[mutArrCityList objectAtIndex:indexPath.row] objectForKey:@"mc_longitude"] forKey:@"longitude"];
    [USERDEFAULTS synchronize];
    
    [AppDelegate sharedInstance].strCityId=[[mutArrCityList objectAtIndex:indexPath.row] objectForKey:@"city_id"];
    [AppDelegate sharedInstance].strCampaignPageTitle=[[mutArrCityList objectAtIndex:indexPath.row] objectForKey:@"city_name"];
    [AppDelegate sharedInstance].strFlgSearch=@"1";

    //[[NSNotificationCenter defaultCenter] postNotificationName:@"PageTitle" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GetNewSearchCampaign" object:nil];

    [self dismissViewControllerAnimated:YES completion:^{
        if (_popoverDismiss) {
            _popoverDismiss();
        }
    }];
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 50;
//}

- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"click");
}

#pragma mark- UIButton Click Action Methods

- (IBAction)btnActionToGoBack:(id)sender {
     [AppDelegate sharedInstance].strFlgSearch=@"0";
     [self dismissViewControllerAnimated:YES completion:nil]; 
}

- (IBAction)btnActionToClickSideMenu:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

- (IBAction)btnActionToClickNotification:(id)sender {
    [self.searchBar resignFirstResponder];
    int intCustomerId=[[USERDEFAULTS objectForKey:@"customer_id"] intValue];
    if(intCustomerId==0){
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        objPopOverAlertVC = [aStoryboard instantiateViewControllerWithIdentifier:@"PopOverAlertVC"];
        [[AppDelegate sharedInstance].window addSubview:objPopOverAlertVC.view];
    }else{
        self.navigationController.navigationBarHidden = YES;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        NotificationVC *objNotificationVC = [storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
        [self.navigationController pushViewController:objNotificationVC animated:YES];
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    self.view.alpha = 1.0;
    self.notifyAndLocationPopover.delegate = nil;
    return YES;
}

#pragma mark -
#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:YES animated:YES];
    
    UIButton *cButton = nil;
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=7) {
        UIView *topView = searchBar.subviews[0];
        for (UIView *searchbuttons in topView.subviews){
            if ([searchbuttons isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
                cButton = (UIButton*)searchbuttons;
                break;
            }
        }
        if (cButton) {
            //Set the new title of the cancel button
            [cButton setTitle:[appDelegate getString:@"Cancel"] forState:UIControlStateNormal];
            [cButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cButton.titleLabel setFont:[cButton.titleLabel.font fontWithSize: 12]];
        }
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if ([searchText length] >2) {
        [self GetCampaignCityFilterWS];
    }
    //    if ([mutFilterArray count]>0) {
    //
    //        if ([searchText length] == 0) {
    //
    //            strPreviousSearch = nil;
    //            pageNumber = 0;
    //            [self setupWebServiceForGallery];
    //            [searchBar resignFirstResponder];
    //            isSearching = FALSE;
    //        }
    //    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    
    [searchBar setShowsCancelButton:NO animated:YES];
}

#pragma mark- Web Service Methods
- (void)GetCampaignCityFilterWS{
    NSMutableDictionary *aDictionary = [NSMutableDictionary dictionaryWithObjects:@[_searchBar.text] forKeys:@[@"city"]];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCityWS withParams:aDictionary showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            mutArrCityList = [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"]];
            [_tblViewGlobalSearchLocation reloadData];
        }else{
           [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
- (void)GetDefaultCampaignCityFilterWS{
    NSMutableDictionary *aDictionary = [NSMutableDictionary dictionaryWithObjects:@[@""] forKeys:@[@"city"]];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCityWS withParams:aDictionary showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            mutArrCityList = [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"]];
            [_tblViewGlobalSearchLocation reloadData];
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark SetUpPage

-(void)SetUpPage{
    _lblNavTitle.text = [appDelegate getString:@"Global Search"];
    
    [self.searchBar setImage:[UIImage imageNamed:@"ic-web"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateNormal];
    [self.searchBar setImage:[[UIImage imageNamed: @"ic-nav-search"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    UITextField *textField = [self.searchBar searchTextField];
    textField.layer.borderWidth = 1.0f; //To hide the square corners
    textField.layer.borderColor = [kAppSupportedColorThemeColor CGColor]; //assigning the default border color
    textField.layer.cornerRadius = 2.5f;
    [textField setFont:[UIFont fontWithName:FontRobotoLight size:12]];
//    [self.searchBar setValue:[UIColor colorWithHexString:@"#666666"] forKeyPath:@"_searchField._placeholderLabel.textColor"];
    self.searchBar.searchTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.searchBar.searchTextField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#666666"]}];
    self.searchBar.layer.borderColor = [[UIColor colorWithHexString:@"#FE7935"] CGColor];//.CGColor;
    self.searchBar.backgroundColor = kAppSupportedColorThemeColor;
    self.searchBar.layer.borderWidth = 1;
    
//    self.navigationController.navigationBar.clipsToBounds = YES;
//    self.navigationController.topViewController.title = [appDelegate getString:@"Global Search"];;
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
//                                                                      NSFontAttributeName:[UIFont fontWithName:FontRobotoRegular size:17]}];
//    self.navigationController.navigationBarHidden = NO;
//    self.navigationController.navigationBar.translucent = NO;
//    self.navigationController.navigationBar.barTintColor = kAppSupportedColorThemeColor;
//    self.navigationController.navigationBar.backgroundColor = kAppSupportedColorThemeColor;

//    float fltWidth;
//    if (IS_IPAD) {
//        fltWidth = 50.0f;
//    }else{
//        fltWidth = 28.0f;
//    }
    //
    //    UIImage *leftbuttonImage = [UIImage imageNamed:@"ic-nav-drawer"];
    //    UIButton *aLeftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [aLeftButton setImage:leftbuttonImage forState:UIControlStateNormal];
    //    aLeftButton.frame = CGRectMake(0.0, 0.0, leftbuttonImage.size.width, leftbuttonImage.size.height);
    //    UIBarButtonItem *aLeftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:aLeftButton];
    //    [aLeftButton addTarget:self action:@selector(btnActionToClickSideMenu:) forControlEvents:UIControlEventTouchUpInside];
    //    [self.navigationItem setLeftBarButtonItem:aLeftBarButtonItem];
    
//    UIImage *buttonImage = [UIImage imageNamed:@"ic-nav-notification"];
//    UIButton *btnNotification = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btnNotification setImage:buttonImage forState:UIControlStateNormal];
//    btnNotification.frame = CGRectMake(0.0, 0.0, fltWidth, buttonImage.size.height);
//    UIBarButtonItem *aBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnNotification];
//    [btnNotification addTarget:self action:@selector(btnActionToClickNotification:) forControlEvents:UIControlEventTouchUpInside];
    
    //        UIImage *rightbuttonImage = [UIImage imageNamed:@"ic-nav-location"];
    //      UIButton  *btnLocation = [UIButton buttonWithType:UIButtonTypeCustom];
    //        [btnLocation setImage:rightbuttonImage forState:UIControlStateNormal];
    //        btnLocation.frame = CGRectMake(0.0, 0.0, fltWidth, rightbuttonImage.size.height);
    //        UIBarButtonItem *aRightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnLocation];
    //        [btnLocation addTarget:self action:@selector(btnActionToClickLocation:) forControlEvents:UIControlEventTouchUpInside];
    
//    self.navigationItem.rightBarButtonItems = @[aBarButtonItem];
   // _lblNoRecoredFound.text=[appDelegate getString:@"Search Coupons"];
    _searchBar.placeholder=[appDelegate getString:@"Search Location"];
    
//    [self.searchBar setValue:[UIColor colorWithHexString:@"#666666"] forKeyPath:@"_searchField._placeholderLabel.textColor"];
    self.searchBar.searchTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.searchBar.searchTextField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#666666"]}];
    UIImage *imgClear = [UIImage imageNamed:@"ic-nav-delete"];
    [self.searchBar setImage:imgClear forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
}

@end
