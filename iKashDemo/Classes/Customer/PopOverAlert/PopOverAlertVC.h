//
//  PopOverAlertVC.h
//  iKash
//
//  Created by indianic on 26/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopOverAlertVC : UIViewController {
    
}
@property (strong, nonatomic) IBOutlet UIView *innerPopUpView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleFirst;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSecond;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)btnLoginClick:(id)sender;
- (IBAction)btnRegisterClick:(id)sender;
- (IBAction)btnCancelClick:(id)sender;


@end
