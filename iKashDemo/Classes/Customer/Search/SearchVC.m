//
//  SearchVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "SearchVC.h"
#import "NotificationVC.h"
#import "GlobalSearchLocationVC.h"
#import "SearchCollectionCell.h"
#import "SidePenalSearch.h"
#import "CampaignDetailsVC.h"
#import "CampaignCollectionCell.h"

@interface SearchVC (){
    NSMutableArray *mutArrSearchCampaign;
}

@end

@implementation SearchVC

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //Register cell from XIB
    [self.collectionViewSearch registerNib:[UINib nibWithNibName:tileCellXibNibName bundle:nil] forCellWithReuseIdentifier:tileCellReuseIdentifier];
    
    self.objSearchBar.layer.borderColor = kAppSupportedColorThemeColor.CGColor;
    self.objSearchBar.backgroundColor = kAppSupportedColorThemeColor;
    self.objSearchBar.layer.borderWidth = 1;
    [self.objSearchBar setImage:[[UIImage imageNamed: @"ic-nav-search"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    _lblNoRecoredFound.hidden=FALSE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = NO;
    
    // SetUp notification on side penal close
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpHeaderView) name:@"MFSideMenuStateClosed" object:nil];
    
    self.navigationController.navigationBarHidden = YES;
    isSearching = NO;
    [self SetUpPage];
    
    if(!IS_IPAD){
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

#pragma mark- Notification Methods

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientationDevice {
    switch (orientationDevice){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientationDevice {
    switch (orientationDevice){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

#pragma mark- Notification Methods

-(void)setUpHeaderView{
    
    if([[AppDelegate sharedInstance].strTabSelectedIndex isEqualToString:@"1"]){
        if([[AppDelegate sharedInstance].mutArrSidePenalCategoryList count]>0){
            UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            SidePenalSearch *objSidePenalSearch = [aStoryboard instantiateViewControllerWithIdentifier:@"SidePenalSearch"];
            [self.navigationController pushViewController:objSidePenalSearch animated:YES];
            [[NSNotificationCenter defaultCenter] removeObserver:self
                                                            name:@"MFSideMenuStateClosed"
                                                          object:nil];
        }
    }
}


#pragma mark- UIButton Click Action Methods

- (IBAction)btnActionToSideMenuClick:(id)sender {
    [self.objSearchBar resignFirstResponder];
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

- (IBAction)btnActionToNotificationClick:(id)sender {
    
    if([[AppDelegate sharedInstance] checkNetworkStatus]){
        [self.objSearchBar resignFirstResponder];
        int intCustomerId=[[USERDEFAULTS objectForKey:@"customer_id"] intValue];
        if(intCustomerId==0){
            UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            objPopOverAlertVC = [aStoryboard instantiateViewControllerWithIdentifier:@"PopOverAlertVC"];
            [[AppDelegate sharedInstance].window addSubview:objPopOverAlertVC.view];
        }else{
            if (IS_IPAD) {
                UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
                NotificationVC *objNotificationVC = [aStoryboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
                self.notifyAndLocationPopover = [[UIPopoverController alloc] initWithContentViewController:objNotificationVC];
                self.notifyAndLocationPopover.delegate = self;
                self.notifyAndLocationPopover.popoverContentSize = CGSizeMake(320.0, 600.0);
                self.view.alpha = 0.6;
                [self.notifyAndLocationPopover presentPopoverFromRect:[(UIButton *)sender bounds]
                                                               inView:self.btnNotification
                                             permittedArrowDirections:UIPopoverArrowDirectionUp
                                                             animated:YES];
            }else{
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
                NotificationVC *objNotificationVC = [storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
                [self.navigationController pushViewController:objNotificationVC animated:YES];
            }
        }
    }else{
        [[AppDelegate sharedInstance] showAlertView:[appDelegate getString:@"INTERNET_CONNECTION_ERROR"] Tag:@"0"];
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    self.view.alpha = 1.0;
    self.notifyAndLocationPopover.delegate = nil;
    return YES;
}

- (IBAction)btnActionToLocationClick:(id)sender {
    
    if([[AppDelegate sharedInstance] checkNetworkStatus]){
        [self.objSearchBar resignFirstResponder];
        if (IS_IPAD) {
            UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            GlobalSearchLocationVC *objGlobalSearchLocationVC = [aStoryboard instantiateViewControllerWithIdentifier:@"GlobalSearchLocationVC"];
            [objGlobalSearchLocationVC setPopoverDismiss:^{
                self.view.alpha = 1.0;
            }];
            self.notifyAndLocationPopover = [[UIPopoverController alloc] initWithContentViewController:objGlobalSearchLocationVC];
            self.notifyAndLocationPopover.delegate = self;
            self.notifyAndLocationPopover.popoverContentSize = CGSizeMake(320.0, 600.0);
            self.view.alpha = 0.6;
            [self.notifyAndLocationPopover presentPopoverFromRect:[(UIButton *)sender bounds]
                                                           inView:self.btnLocation
                                         permittedArrowDirections:UIPopoverArrowDirectionUp
                                                         animated:YES];
        }else{
            UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            GlobalSearchLocationVC *objGlobalSearchLocationVC = [aStoryboard instantiateViewControllerWithIdentifier:@"GlobalSearchLocationVC"];
            objGlobalSearchLocationVC.modalPresentationStyle = UIModalPresentationCustom;
            [self presentViewController:objGlobalSearchLocationVC animated:YES completion:nil];
        }
    }else{
        [[AppDelegate sharedInstance] showAlertView:[appDelegate getString:@"INTERNET_CONNECTION_ERROR"] Tag:@"0"];
    }
}

#pragma mark -
#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    [self.objSearchBar resignFirstResponder];
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.objSearchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.objSearchBar resignFirstResponder];
    isSearching = YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    //    [searchBar setShowsCancelButton:YES animated:YES];
    //
    //    UIButton *cButton = nil;
    //
    //    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=7) {
    //
    //        UIView *topView = searchBar.subviews[0];
    //
    //        for (UIView *searchbuttons in topView.subviews){
    //
    //            if ([searchbuttons isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
    //
    //                cButton = (UIButton*)searchbuttons;
    //                break;
    //
    //            }
    //        }
    //
    //        if (cButton) {
    //            //Set the new title of the cancel button
    //
    //            [cButton setTitle:[appDelegate getString:@"Cancel"] forState:UIControlStateNormal];
    //
    //            [cButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //
    //            [cButton.titleLabel setFont: [cButton.titleLabel.font fontWithSize: 12]];
    //
    //        }
    //    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)aSearchText{
    
    if (![aSearchText isValidString]) {
        isSearching = NO;
        [searchBar resignFirstResponder];
    }else{
        isSearching = YES;
    }
    if([aSearchText length] == 0) {
        [searchBar performSelector: @selector(resignFirstResponder)
                        withObject: nil
                        afterDelay: 0.1];
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)aSearchBar {
    
    [aSearchBar resignFirstResponder];
    [aSearchBar setShowsCancelButton:NO animated:YES];
    
    if ([aSearchBar.text length] <= 0){
        isSearching = NO;
        
    }else{
        isSearching = YES;
        [self SearchCampaignWS];
    }
}

# pragma mark - CollectionView Delegate Method

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [mutArrSearchCampaign count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CampaignCollectionCell *shopCollectionViewCell = (CampaignCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:tileCellReuseIdentifier forIndexPath:indexPath];
    
    [shopCollectionViewCell setDataWithDic:[mutArrSearchCampaign objectAtIndex:indexPath.row]];
    
    return shopCollectionViewCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    if (IS_IPAD) {
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        CampaignDetailsVC *objCampaignDetailsVC = [aStoryboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
        objCampaignDetailsVC.strCampaignId =  [[mutArrSearchCampaign objectAtIndex:indexPath.row] objectForKey:@"c_campaign_id"] ;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objCampaignDetailsVC];
        navigationController.navigationBar.hidden = YES;
        self.notifyAndLocationPopover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
        self.notifyAndLocationPopover.delegate = self;
        self.notifyAndLocationPopover.popoverContentSize = CGSizeMake(370.0, 645.0);
        self.view.alpha = 0.6;
        if ([self.notifyAndLocationPopover respondsToSelector:@selector(backgroundColor)]) {
            self.notifyAndLocationPopover.backgroundColor = [UIColor blackColor];
        }
        [self.notifyAndLocationPopover presentPopoverFromRect:[_btnPopupView bounds]
                                                       inView:_btnPopupView
                                     permittedArrowDirections:0
                                                     animated:YES];
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        CampaignDetailsVC *objCampaignDetailsVC = [storyboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
        objCampaignDetailsVC.strCampaignId =  [[mutArrSearchCampaign objectAtIndex:indexPath.row] objectForKey:@"c_campaign_id"] ;
        [self.navigationController pushViewController:objCampaignDetailsVC animated:YES];
    }
}

#pragma mark - UICollectionViewFlowLayout methods

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    
//    if (IS_IPHONE) {
//        CGRect screenRect = [[UIScreen mainScreen] bounds];
//        CGFloat screenWidth = screenRect.size.width;
//        CGFloat screenHeight = screenRect.size.width;
//        float gridColumn;
//        float gridRow;
//        
//        UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
//        if(UIInterfaceOrientationIsPortrait(orientation)){
//            gridRow = 2.0;
//            gridColumn = 1.5;
//        }else{
//            gridRow = 3.0;
//            gridColumn = 2.5;
//        }
//        
//        float cellWidth = (screenWidth / gridRow)-22.5; //Replace the divisor with the column count requirement. Make sure to have it in float.
//        float cellHeight = (screenHeight / gridColumn)-22.5; //Replace the divisor with the column count requirement. Make sure to have it in float.
//        CGSize size = CGSizeMake(cellWidth, cellHeight + 30); // +50 for description lable
//        return size;
//        
//    }else{
//        
//        CGRect screenRect = [[UIScreen mainScreen] bounds];
//        CGFloat screenWidth = screenRect.size.width;
//        CGFloat screenHeight = screenRect.size.width;
//        float gridColumn;
//        float gridRow;
//        UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
//        if(UIInterfaceOrientationIsPortrait(orientation)){
//            gridRow = 4.0;
//            gridColumn = 3.2;
//        }else{
//            gridRow = 5.0;
//            gridColumn = 4.2;
//        }
//        float cellWidth = (screenWidth / gridRow)-18; //Replace the divisor with the column count requirement. Make sure to have it in float.
//        float cellHeight = (screenHeight / gridColumn)-18; //Replace the divisor with the column count requirement. Make sure to have it in float.
//        CGSize size = CGSizeMake(cellWidth, cellHeight + 30);
//        return size;
//    }
//}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    if (IS_IPHONE) {
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }else{
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    if (IS_IPAD) {
        return 15;
    }else{
        return 15;
    }
}

#pragma mark- Web Service Methods

- (void)SearchCampaignWS{
    
    if([self NointerNetCheking]){
        
        NSString *aCustId = [USERDEFAULTS objectForKey:@"customer_id"] != nil ? [USERDEFAULTS objectForKey:@"customer_id"] : @"";
        NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      _objSearchBar.text,@"search_text",
                                      aCustId,@"customer_id",
                                      @"",@"city_id",
                                      @"",@"is_city",
                                      nil];
        [[Webservice sharedInstance] callWebserviceWithMethodName:customer_SearchCampaignWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
                _collectionViewSearch.hidden=FALSE;
                mutArrSearchCampaign=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"][@"city_wise_campaign"]];
                _lblNoRecoredFound.hidden=TRUE;
                [_collectionViewSearch reloadData];
            }else{
                _collectionViewSearch.hidden=TRUE;
                _lblNoRecoredFound.hidden=FALSE;
                _lblNoRecoredFound.text=[appDelegate getString:@"NO_CAMP_FOUND_MESSAGE"];
                
                [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:@"0"];
            }
        } withFailureBlock:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
}

- (void)CitySearchCampaignWS{
    
    if([self NointerNetCheking]){
        _objSearchBar.text=[AppDelegate sharedInstance].strCampaignPageTitle;
        NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      [AppDelegate sharedInstance].strCampaignPageTitle,@"search_text",
                                      [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                      @"",@"city_id",
                                      @"",@"is_city",
                                      nil];
        NSLog(@"%@",aDict);
        [[Webservice sharedInstance] callWebserviceWithMethodName:customer_SearchCampaignWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
                _collectionViewSearch.hidden=FALSE;
                mutArrSearchCampaign=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"][@"city_wise_campaign"]];
                _lblNoRecoredFound.hidden=TRUE;
                [_collectionViewSearch reloadData];
            }else{
                _collectionViewSearch.hidden=TRUE;
                _lblNoRecoredFound.hidden=FALSE;
                _lblNoRecoredFound.text=[appDelegate getString:@"NO_CAMP_FOUND_MESSAGE"];
            }
        } withFailureBlock:^(NSError *error) {
            _collectionViewSearch.hidden=TRUE;
            _viewNoInternet.hidden=FALSE;
            _lblNoInternet.text=[appDelegate getString:@"WEBSERVICEISSUE"];
            _lblNoRecoredFound.hidden=TRUE;
            NSLog(@"%@",error);
        }];
    }
}

#pragma mark SetUpPage

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
    [self.collectionViewSearch.collectionViewLayout invalidateLayout];
}

-(void)SetUpPage{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CitySearchCampaignWS) name:@"GetNewSearchCampaign" object:nil];
    
    _lblNoRecoredFound.text=[appDelegate getString:@"Search Coupons"];
    
    _objSearchBar.placeholder=[appDelegate getString:@"Search Coupons"];
    UITextField *textField = [self.objSearchBar searchTextField];
    textField.layer.borderWidth = 1.0f; //To hide the square corners
    textField.layer.borderColor = [kAppSupportedColorThemeColor CGColor]; //assigning the default border color
    textField.layer.cornerRadius = 2.5f;
    [textField setFont:[UIFont fontWithName:FontRobotoLight size:12]];
    self.objSearchBar.searchTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.objSearchBar.searchTextField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#666666"]}];
    //    [self.objSearchBar setValue:[UIColor colorWithHexString:@"#666666"] forKeyPath:@"_searchField._placeholderLabel.textColor"];
    UIImage *imgClear = [UIImage imageNamed:@"ic-nav-delete"];
    [self.objSearchBar setImage:imgClear forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
    _lblNavTitle.text=[appDelegate getString:@"Search"];
}

-(BOOL)NointerNetCheking{
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        _collectionViewSearch.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        _lblNoRecoredFound.hidden=TRUE;
        return FALSE;
    }else{
        _collectionViewSearch.hidden=FALSE;
        _viewNoInternet.hidden=TRUE;
        return TRUE;
    }
}

@end
