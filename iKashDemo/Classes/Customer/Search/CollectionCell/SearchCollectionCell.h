//
//  SearchCollectionCell.h
//  iKash
//
//  Created by indianic on 24/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DJWStarRatingView.h"
@interface SearchCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imgCampaign;
@property (weak, nonatomic) IBOutlet DJWStarRatingView *starRatingView;
@property (weak, nonatomic) IBOutlet UIImageView *imgCampaignBranch;
@property (weak, nonatomic) IBOutlet UILabel *lblColour;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@end
