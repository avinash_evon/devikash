//
//  SearchVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverAlertVC.h"

@interface SearchVC : UIViewController<UIPopoverControllerDelegate, UISearchBarDelegate>{

    PopOverAlertVC *objPopOverAlertVC;
    BOOL isSearching;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (strong, nonatomic) IBOutlet UIButton *btnPopupView;
@property (assign, nonatomic) IBOutlet UIButton *btnSideMenu;
@property (assign, nonatomic) IBOutlet UIButton *btnNotification;
@property (assign, nonatomic) IBOutlet UIButton *btnLocation;
@property (assign, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (assign, nonatomic) IBOutlet UILabel *lblNoRecoredFound;
@property (assign, nonatomic) IBOutlet UISearchBar *objSearchBar;

@property(nonatomic,assign)IBOutlet UICollectionView *collectionViewSearch;
@property (nonatomic, strong) UIPopoverController *notifyAndLocationPopover;

- (IBAction)btnActionToSideMenuClick:(id)sender;
- (IBAction)btnActionToNotificationClick:(id)sender;
- (IBAction)btnActionToLocationClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;
@end
