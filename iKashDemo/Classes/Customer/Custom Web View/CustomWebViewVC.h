//
//  CustomWebViewVC.h
//  iKash
//
//  Created by IndiaNIC on 10/12/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomWebViewVC : UIViewController<UIWebViewDelegate>{
    
}
@property(nonatomic, retain) NSString *strURL;

@property (strong, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)btnActionToCloseWebView:(id)sender;
@end
