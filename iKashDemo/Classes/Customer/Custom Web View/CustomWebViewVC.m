//
//  CustomWebViewVC.m
//  iKash
//
//  Created by IndiaNIC on 10/12/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "CustomWebViewVC.h"

@interface CustomWebViewVC ()

@end

@implementation CustomWebViewVC

#pragma mark - Synthesize
@synthesize strURL;

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    strURL = [strURL stringByReplacingOccurrencesOfString:@"http://" withString:@""];
    strURL = [strURL stringByReplacingOccurrencesOfString:@"https://" withString:@""];
    strURL = [NSString stringWithFormat:@"http://%@",strURL];
    NSURL *url = [NSURL URLWithString:strURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Button Event Click Methods
- (IBAction)btnActionToCloseWebView:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIWebView Delegate Methods
- (void)webViewDidStartLoad:(UIWebView *)webView{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:_webView animated:YES];
    hud.labelText = @"Loading...";
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [MBProgressHUD hideAllHUDsForView:_webView animated:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{

    [MBProgressHUD hideAllHUDsForView:_webView animated:YES];
    UIAlertView *aAlertShow=[[UIAlertView alloc]initWithTitle:APPTITLE message:[appDelegate getString:@"This webpage is not available"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [aAlertShow show];
}

#pragma mark - AlertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
     [self dismissViewControllerAnimated:YES completion:nil];
}
@end
