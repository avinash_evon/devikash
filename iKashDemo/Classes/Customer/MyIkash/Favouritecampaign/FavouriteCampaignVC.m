//
//  FavouriteCampaignVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "FavouriteCampaignVC.h"
#import "FavouriteCampaignCell.h"
#import "FavouriteCollectionViewCell.h"
#import "CampaignDetailsVC.h"

static NSString *kCollectionViewIdentifier = @"FavouriteCollectionViewCell";

@interface FavouriteCampaignVC (){
    NSMutableArray *mutFavouriteCampaign;
    NSString *strCampaignID;
    NSIndexPath *indexPathTable;
}

@end

@implementation FavouriteCampaignVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _lblNoRecordFound.text=[appDelegate getString:@"NO_RECORD_FOUND_MESSAGE"];
    [self FavouriteCampaignListWS];
     [_favouriteClnView reloadData];// added by avinash
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return mutFavouriteCampaign.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FavouriteCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewIdentifier forIndexPath:indexPath];
    
    cell.viewBackground.layer.cornerRadius=5.0;
    cell.viewBackground.layer.borderColor = [appDelegate colorWithHexString:@"eeeeee"].CGColor;
  //  cell.viewBackground.layer.borderWidth = 0.5f;
  //  cell.imgCamapign.layer.cornerRadius=5.0;
//    int aintStar=[[[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"averge_rating"] intValue];
    double aintStar=[[[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"averge_rating"] doubleValue];

    NSString *strColor=[[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"b_colour_code"];
    if(strColor.length>0)
    {
        strColor = [strColor substringFromIndex:1];
    }else{
        strColor=@"000000";
    }
    
    cell.viewRating.hidden = TRUE;
    cell.viewRating.starSize=CGSizeMake(9, 9);
    cell.viewRating.numberOfStars=5;
    cell.viewRating.rating=aintStar;
    cell.viewRating.padding=5.0;
    cell.viewRating.fillColor=[appDelegate colorWithHexString:strColor];
    
    cell.viewRating.unfilledColor=[UIColor clearColor];
    cell.viewRating.strokeColor=[appDelegate colorWithHexString:strColor];
    
    cell.lblCampaignTitle.text=[[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"c_title"];
    
    cell.lblDescription.text=[[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"c_short_description_1"];
    
    cell.lblStatus.text=[[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"c_status_1"];
    if([cell.lblStatus.text isEqualToString:@"Expired"]){
       // [cell.lblStatus setTextColor:[appDelegate colorWithHexString:@"999999"]];
        
    //    [cell.lblStatus setTextColor:[appDelegate colorWithHexString:@"FF0000"]];
        
        
          [cell.lblStatus setTextColor:[UIColor whiteColor]];
        
    }else{
        [cell.lblStatus setTextColor:[UIColor whiteColor]];
    }
    
    
    
    NSString *aStrImgUrl = [[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"c_logo"];
    NSURL *aUrlImg = [NSURL URLWithString:aStrImgUrl];
    [cell.imgCamapign sd_setImageWithURL:aUrlImg
                        placeholderImage:nil
                                 options:SDWebImageProgressiveDownload
                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                    [cell.activityIndicator startAnimating];
                                }
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   [cell.activityIndicator stopAnimating];
                                   cell.imgCamapign.clipsToBounds = YES;
                               }];
    
    cell.lblDate.text = [[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"c_end_date_1"];
    cell.btnDelete.tag=indexPath.row;
    [cell.btnDelete addTarget:self action:@selector(btnDeleteCampaignClick:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
 if(![[[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"c_status_1"] isEqualToString:@"Expired"] || ![[[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"c_status_1"] isEqualToString:@"Inactive"])
 {
    NSLog(@"indexPath.row = %ld",(long)indexPath.row);
     
     //If campaign is expired then don't open detail
     if([[[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"c_status_1"] isEqualToString:@"Expired"]) { return; }
     
    if (IS_IPAD) {
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        CampaignDetailsVC *objCampaignDetailsVC = [aStoryboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
        objCampaignDetailsVC.strCampaignId =  [[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"c_campaign_id"];
        objCampaignDetailsVC.strCampaignStatus = [[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"c_status_1"];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objCampaignDetailsVC];
        navigationController.navigationBar.hidden = YES;
        self.campaignDetailsPopover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
        self.campaignDetailsPopover.delegate = self;
        self.campaignDetailsPopover.popoverContentSize = CGSizeMake(370.0, 645.0);
        self.view.alpha = 0.6;
        if ([self.campaignDetailsPopover respondsToSelector:@selector(backgroundColor)]) {
            self.campaignDetailsPopover.backgroundColor = [UIColor blackColor];
        }
        [self.campaignDetailsPopover presentPopoverFromRect:[_lblNoRecordFound bounds]
                                                     inView:_lblNoRecordFound
                                   permittedArrowDirections:0
                                                   animated:YES];
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        CampaignDetailsVC *objCampaignDetailsVC = [storyboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
        objCampaignDetailsVC.isFromFavourites = @"Yes";
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objCampaignDetailsVC];
        objCampaignDetailsVC.strCampaignId = [[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"c_campaign_id"];
        objCampaignDetailsVC.strCampaignStatus = [[mutFavouriteCampaign objectAtIndex:indexPath.row] objectForKey:@"c_status_1"];

        
        objCampaignDetailsVC.modalPresentationStyle = UIModalPresentationCustom;
        [self presentViewController:navigationController animated:YES completion:nil];
    }
 }
}

#pragma mark - UICollectionViewFlowLayout methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (IS_IPAD) {
        return CGSizeMake(collectionView.frame.size.width/2-20, 120.f);
    }else{
        return CGSizeMake(collectionView.frame.size.width, 180.f);/// 120
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(15, 15, 10, 15);
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [self.favouriteClnView.collectionViewLayout invalidateLayout];
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    self.view.alpha = 1.0;
    self.campaignDetailsPopover.delegate = nil;
    return YES;
}

#pragma mark- UIButton Click Action Methods

-(IBAction)btnDeleteCampaignClick:(id)sender{
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:APPTITLE message:[appDelegate getString:@"DELETECAMPAIGN"] delegate:self cancelButtonTitle:[appDelegate getString:@"No"] otherButtonTitles:[appDelegate getString:@"Yes"], nil];
    alertView.tag=200;
    [alertView show];
    strCampaignID=[[mutFavouriteCampaign objectAtIndex:[sender tag]] objectForKey:@"c_campaign_id"];
    // NSString *strIndex=[NSString stringWithFormat:@"%li",(long)[sender tag]];
    indexPathTable = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
}

#pragma mark- Web Service Methods

- (void)FavouriteCampaignListWS{
    
 if([self NointerNetCheking])
 {
    NSLog(@"%@",[USERDEFAULTS objectForKey:@"Language"]);
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    NSLog(@"%@",aDict);
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetFavouriteCampaignWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            _favouriteClnView.hidden = NO;
            mutFavouriteCampaign = [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"]];
            [_favouriteClnView reloadData];
            if (mutFavouriteCampaign.count == 0) {
                _favouriteClnView.hidden = YES;//YES
                _lblNoRecordFound.hidden = NO;
            }
        }else{
            _favouriteClnView.hidden = YES;
            _lblNoRecordFound.hidden = NO;
            //            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
 }
}

- (void)UnFavouriteCampaignWS:(NSString *)strCampaignId indexPath:(NSIndexPath *)strIndex{
    
    
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  strCampaignId,@"campaign_id",
                                  nil];
    NSLog(@"%@",aDict);
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_UnFavouriteCampaignWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            [self FavouriteCampaignListWS];
            
            //            [mutFavouriteCampaign removeObjectAtIndex:strIndex.row];
            //            //            [_tblFavouriteCampaign deleteRowsAtIndexPaths:[NSArray arrayWithObjects:strIndex, nil] withRowAnimation:UITableViewRowAnimationFade];
            //            [self.favouriteClnView deleteItemsAtIndexPaths:[NSArray arrayWithObject:strIndex]];
            //            if (mutFavouriteCampaign.count == 0) {
            //                _lblNoRecordFound.hidden = NO;
            //            }
        }else{
            _favouriteClnView.hidden = YES;
            _lblNoRecordFound.hidden = NO;
            //            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];

}

#pragma mark AlertMethod

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag==200){
        if(buttonIndex==1){
            [self UnFavouriteCampaignWS:strCampaignID indexPath:indexPathTable];
        }
    }
}
-(BOOL)NointerNetCheking{
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        
        
        _favouriteClnView.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        return FALSE;
    }else{
        _favouriteClnView.hidden=FALSE;
        _viewNoInternet.hidden=TRUE;
        return TRUE;
        
    }
}
@end
