//
//  FavouriteCampaignCell.m
//  iKash
//
//  Created by indianic on 20/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "FavouriteCampaignCell.h"

@implementation FavouriteCampaignCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
