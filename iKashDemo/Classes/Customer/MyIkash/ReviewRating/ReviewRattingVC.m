//
//  ReviewRattingVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "ReviewRattingVC.h"
#import "ReviewRatingCell.h"
#import "ReviewRatingCollectionViewCell.h"
#import "CampaignDetailsVC.h"

static NSString *kCollectionViewIdentifier = @"ReviewRatingCollectionViewCell";

@interface ReviewRattingVC (){
    NSMutableArray *mutArrRating;
    NSString *strCampaignID;
    NSIndexPath *indexPathTable;
    
    int intCurrentPage,intNextPage;
    BOOL boolWebCallInProgress;
    UIActivityIndicatorView *spinner;

}

@end

@implementation ReviewRattingVC

#pragma mark - Synthesize

@synthesize campaignDetailsPopover;

- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    intCurrentPage=1;
    _lblNoRecordFound.text=[appDelegate getString:@"NO_RECORD_FOUND_MESSAGE"];
    [self ReviewCampaignListWS];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return mutArrRating.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ReviewRatingCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewIdentifier forIndexPath:indexPath];
    
    // check if row is odd or even and set color accordingly
    cell.viewBackground.layer.cornerRadius=5.0;
    cell.viewBackground.layer.borderColor = [appDelegate colorWithHexString:@"eeeeee"].CGColor;
    cell.viewBackground.layer.borderWidth = 0.5f;
    cell.imgCamapign.layer.cornerRadius=0.0;
    
//    int aintStar=[[[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"rating"] intValue];
    double aintStar=[[[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"rating"] doubleValue];

    NSString *strColor=[[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"b_colour_code"];
   if(strColor.length>0)
   {
    strColor = [strColor substringFromIndex:1];
   }else{
      strColor=@"000000";
   }
    cell.viewRating.starSize=CGSizeMake(20, 20);//9,9
    cell.viewRating.numberOfStars=5;
    cell.viewRating.rating=aintStar;
    cell.viewRating.padding=5.0;
    cell.viewRating.fillColor=[appDelegate colorWithHexString:@"F77739"];
    
    cell.viewRating.unfilledColor=[UIColor clearColor];
    cell.viewRating.strokeColor=[appDelegate colorWithHexString:strColor];
    
    cell.lblCampaignTitle.text=[[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"c_title"];
    
    cell.lblDescription.text=[[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"c_short_description"];
    cell.lblReview.text=[[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"review"];
    
    NSData *dataa = [cell.lblReview.text dataUsingEncoding:NSUTF8StringEncoding];
    NSString *valueEmoj = [[NSString alloc] initWithData:dataa encoding:NSNonLossyASCIIStringEncoding];
    
    cell.lblReview.text = valueEmoj;
    
    NSString *aStrImgUrl = [[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"c_logo"];
    NSURL *aUrlImg = [NSURL URLWithString:aStrImgUrl];
    [cell.imgCamapign sd_setImageWithURL:aUrlImg
                        placeholderImage:nil
                                 options:SDWebImageProgressiveDownload
                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                    [cell.activityIndicator startAnimating];
                                }
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   [cell.activityIndicator stopAnimating];
                                   cell.imgCamapign.clipsToBounds = YES;
                               }];
    
    cell.btnDelete.tag=indexPath.row;
    [cell.btnDelete addTarget:self action:@selector(btnDeleteCampaignClick:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"indexPath.row = %ld",(long)indexPath.row);
    
    if (IS_IPAD) {
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        CampaignDetailsVC *objCampaignDetailsVC = [aStoryboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
        objCampaignDetailsVC.strCampaignId =  [[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"c_campaign_id"];
//        objCampaignDetailsVC.strCampaignStatus = [[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"c_status_1"];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objCampaignDetailsVC];
        navigationController.navigationBar.hidden = YES;
        self.campaignDetailsPopover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
        self.campaignDetailsPopover.delegate = self;
        self.campaignDetailsPopover.popoverContentSize = CGSizeMake(370.0, 645.0);
        self.view.alpha = 0.6;
        if ([self.campaignDetailsPopover respondsToSelector:@selector(backgroundColor)]) {
            self.campaignDetailsPopover.backgroundColor = [UIColor blackColor];
        }
        [self.campaignDetailsPopover presentPopoverFromRect:[_lblNoRecordFound bounds]
                                                     inView:_lblNoRecordFound
                                   permittedArrowDirections:0
                                                   animated:YES];
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        CampaignDetailsVC *objCampaignDetailsVC = [storyboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
        objCampaignDetailsVC.isFromFavourites = @"Yes";
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objCampaignDetailsVC];
        objCampaignDetailsVC.strCampaignId = [[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"c_campaign_id"];
//        objCampaignDetailsVC.strCampaignStatus = [[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"c_status_1"];
        
        
        objCampaignDetailsVC.modalPresentationStyle = UIModalPresentationCustom;
        [self presentViewController:navigationController animated:YES completion:nil];
    }

}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    self.view.alpha = 1.0;
    return YES;
}

#pragma mark - UICollectionViewFlowLayout methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (IS_IPAD) {
        return CGSizeMake(collectionView.frame.size.width/2-20, 160.f);
    }else{
        return CGSizeMake(collectionView.frame.size.width, 190);//160.f
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(15, 15, 10, 15);
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [self.reviewRatingClnView.collectionViewLayout invalidateLayout];
}

#pragma mark -
#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [mutArrRating count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ReviewRatingCell *cell = (ReviewRatingCell *) [tableView dequeueReusableCellWithIdentifier:@"ReviewRatingCell"];
    
    // check if row is odd or even and set color accordingly
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.viewBackground.layer.cornerRadius=5.0;
    cell.viewBackground.layer.borderColor = [appDelegate colorWithHexString:@"eeeeee"].CGColor;
    cell.viewBackground.layer.borderWidth = 0.5f;
    cell.imgCamapign.layer.cornerRadius=5.0;
    
    int aintStar=[[[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"rating"] intValue];
    [cell.viewRating setStars:aintStar];
    
    cell.lblCampaignTitle.text=[[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"c_title"];
    
    //        cell.lblDescription.text=[[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"c_short_description"];
    //        cell.lblReview.text=[[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"review"];
    //
    NSString *aStrImgUrl = [[mutArrRating objectAtIndex:indexPath.row] objectForKey:@"c_logo"];
    NSURL *aUrlImg = [NSURL URLWithString:aStrImgUrl];
    [cell.imgCamapign sd_setImageWithURL:aUrlImg
                        placeholderImage:nil
                                 options:SDWebImageProgressiveDownload
                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                    [cell.activityIndicator startAnimating];
                                }
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   [cell.activityIndicator stopAnimating];
                                   cell.imgCamapign.clipsToBounds = YES;
                               }];
    
    cell.btnDelete.tag=indexPath.row;
    [cell.btnDelete addTarget:self action:@selector(btnDeleteCampaignClick:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 165;
}

-(IBAction)btnDeleteCampaignClick:(id)sender{
    
    strCampaignID=[[mutArrRating objectAtIndex:[sender tag]] objectForKey:@"c_campaign_id"];
    // NSString *strIndex=[NSString stringWithFormat:@"%li",(long)[sender tag]];
    indexPathTable = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:APPTITLE message:[appDelegate getString:@"DELETEREVIEW"] delegate:self cancelButtonTitle:[appDelegate getString:@"No"] otherButtonTitles:[appDelegate getString:@"Yes"], nil];
    alertView.tag=200;
    [alertView show];
    
}

- (void)ReviewCampaignListWS{
    
  if([self NointerNetCheking])
  {
    NSLog(@"%@",[USERDEFAULTS objectForKey:@"Language"]);
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    NSLog(@"%@",aDict);
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetReviewRatingWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
           
            if(intCurrentPage==1){
                
                [mutArrRating removeAllObjects];
                
            }
            
            
            mutArrRating = [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
            
            if (mutArrRating.count == 0) {
                _lblNoRecordFound.hidden = NO;
            }else{
                
                intNextPage=[[[responseData objectForKey:@"settings"] objectForKey:@"next_page"] intValue];
                
                boolWebCallInProgress=NO;
                
                intCurrentPage++;

               [_reviewRatingClnView reloadData];
            }
        }else{
            _reviewRatingClnView.hidden = YES;
            _lblNoRecordFound.hidden=FALSE;

        }
        if ([mutArrRating count]>0) {
            
            [self SetAnimation];
            
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
  }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    if(intNextPage>0){
        [self CallStartAnimating];
    }else{
        return;
    }
    
    
    
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    if(boolWebCallInProgress == NO){
        if (maximumOffset - currentOffset == 0)
        {
            if(intNextPage>0){
                
                boolWebCallInProgress = YES;
                [_reviewRatingClnView setContentOffset:CGPointMake(0, scrollView.contentSize.height-scrollView.frame.size.height) animated:YES];
                [self performSelector:@selector(DelayWebservice) withObject:nil afterDelay:3.0];
                
            }else{
                [self CallStoptAnimating];
            }
        }else{
            [self CallStoptAnimating];
        }
    }else{
        [self CallStoptAnimating];
    }
}
-(void)SetAnimation{
    
    //_reviewRatingClnView.tableFooterView = nil;
    if (spinner) {
        spinner = nil;
    }
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
    //[spinner startAnimating];
    spinner.hidesWhenStopped = YES;
    spinner.frame = CGRectMake(0, 0, 320, 44);
   // _tblViewReviewsList.tableFooterView = spinner;
}
-(void)DelayWebservice{
    [spinner stopAnimating];
    [self ReviewCampaignListWS];
}

-(void)CallStartAnimating{
    [spinner startAnimating];
}

-(void)CallStoptAnimating{
    [spinner stopAnimating];
}
- (void)DeleteReviewWS:(NSString *)strCampaignId indexPath:(NSIndexPath *)strIndex{
 
    
    NSLog(@"%@",[USERDEFAULTS objectForKey:@"Language"]);
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  strCampaignId,@"campaign_id",
                                  nil];
    NSLog(@"%@",aDict);
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_DeleteMyReviewWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            [self ReviewCampaignListWS];
//            [mutArrRating removeObjectAtIndex:strIndex.row];
//            [self.reviewRatingClnView deleteItemsAtIndexPaths:[NSArray arrayWithObject:strIndex]];
//            
////            [_tblReviewRating deleteRowsAtIndexPaths:[NSArray arrayWithObjects:strIndex, nil] withRowAnimation:UITableViewRowAnimationFade];
//            if (mutArrRating.count == 0) {
//                _lblNoRecordFound.hidden = NO;
//                _reviewRatingClnView.hidden = YES;
//  
//            }
        }else{
  
            _reviewRatingClnView.hidden = YES;
            _lblNoRecordFound.hidden=FALSE;
//            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark Alert Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag==200){
        if(buttonIndex==1){
            [self DeleteReviewWS:strCampaignID indexPath:indexPathTable];
        }
    }
}
-(BOOL)NointerNetCheking{
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        
        
        _reviewRatingClnView.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        return FALSE;
    }else{
        _reviewRatingClnView.hidden=FALSE;
        _viewNoInternet.hidden=TRUE;
        return TRUE;
        
    }
}

@end
