//
//  ReviewRattingVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DXStarRatingView.h"

@interface ReviewRattingVC : UIViewController<UIPopoverControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblReviewRating;
@property (strong, nonatomic) IBOutlet UICollectionView *reviewRatingClnView;
@property (nonatomic, strong) UIPopoverController *campaignDetailsPopover;
@property (strong, nonatomic) IBOutlet UILabel *lblNoRecordFound;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;
@end
