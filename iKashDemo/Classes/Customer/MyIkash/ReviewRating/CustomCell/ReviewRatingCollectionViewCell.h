//
//  ReviewRatingCollectionViewCell.h
//  iKash
//
//  Created by IndiaNIC on 14/12/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DJWStarRatingView.h"

@interface ReviewRatingCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imgCamapign;
@property (weak, nonatomic) IBOutlet UILabel *lblCampaignTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet DJWStarRatingView *viewRating;
@property (weak, nonatomic) IBOutlet UILabel *lblReviewText;
@property (weak, nonatomic) IBOutlet UILabel *lblReview;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
