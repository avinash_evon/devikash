//
//  ReviewRatingCell.h
//  iKash
//
//  Created by indianic on 20/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DXStarRatingView.h"

@interface ReviewRatingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imgCamapign;
@property (weak, nonatomic) IBOutlet UILabel *lblCampaignTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet DXStarRatingView *viewRating;
@property (weak, nonatomic) IBOutlet UILabel *lblReview;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
