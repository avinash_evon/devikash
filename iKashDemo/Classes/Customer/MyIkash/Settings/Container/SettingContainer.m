
//
//  SettingContainer.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "SettingContainer.h"
#import "ContainerViewController.h"
#import "SidePenalSearch.h"
#import "NotificationVC.h"
#import "AppDelegate.h"

@interface SettingContainer ()

//@synthesize btnVoucher=_btnVoucher;

@property (nonatomic, weak) ContainerViewController *containerViewController;

@end


@implementation SettingContainer

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = TRUE;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateView) name:@"ScanCompleted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DisplaySaveButton) name:@"SaveButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupHeaderTitleSettingsPersonalClick) name:@"SettingsPersonalClick" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupHeaderTitleSettingsSecurityClick) name:@"SettingsSecurityClick" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupHeaderTitleSettingsCategoryClick) name:@"SettingsCategoryClick" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupHeaderTitleSettingsSupplierClick) name:@"SettingsSupplierClick" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupHeaderTitleSettingsUnsubscribeClick) name:@"SettingsUnsubscribeClick" object:nil];
    
    [AppDelegate sharedInstance].objSettingContainer=self;
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    ///
    
    NSMutableArray *mutArrVoucher;
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    
  /*  [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetdownalodedVoucherWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
    
        NSLog(@"responseData >>>>>>>= %@", responseData);
        
       // if([[responseData objectForKey:@"data"] count])
        
        
        
        NSLog(@"#########>>>>>>>>>>>%d",[[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue]);
        
        
    /*  // if([[responseData objectForKey:@"data"] count])
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1)
        {
            NSLog(@"#############################");
            
            [self->_btnVoucher setHidden:NO];
            [self->_imgVoucher setHidden:NO];
            [self->_lblVoucher setHidden:NO];
        }
        else{
            
            [self->_btnVoucher setHidden:YES];
            [self->_imgVoucher setHidden:YES];
            [self->_lblVoucher setHidden:YES];
            
        }
    */
            
        
      /*  if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
        
          //  _userVoucherClnView.hidden=FALSE;
            
         //   [mutArrVoucher removeAllObjects];
            
          //  mutArrVoucher = [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"]];
            
          //  [_userVoucherClnView reloadData];
            
            if (mutArrVoucher.count == 0) {
                
               /// _lblNoRecordFound.hidden = NO;
            }
        }else{
            
           // _userVoucherClnView.hidden = YES;
        
          //  _lblNoRecordFound.hidden=FALSE;

        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];*/
    
    
    
    
//////
    [AppDelegate sharedInstance].WelCometopConstraint = self.ContraintHeight;
    
    _lblSettings.text = [appDelegate getString:@"Profile & Settings"];
    _lblNewsletter.text = [appDelegate getString:@"Newsletter & email"];
    _lblFavourite.text = [appDelegate getString:@"Favourite campaign"];
    _lblReview.text = [appDelegate getString:@"My Review & Rating"];
    _lblVoucher.text = [appDelegate getString:@"Downloaded Vouchers"];
    
    _btnDrawer.hidden=FALSE;
    _btnNotification.hidden=TRUE;
    imgPointerNotication.hidden=TRUE;
    //    if([[AppDelegate sharedInstance].strGotNotification isEqualToString:@"1"]){
    //        imgPointerNotication.hidden=FALSE;
    //    }else{
    //        imgPointerNotication.hidden=TRUE;
    //    }
    _btnBack.hidden=TRUE;
    _btnSave.hidden=TRUE;
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpHeaderView) name:@"MFSideMenuStateClosed" object:nil];
    self.navigationController.navigationBarHidden = YES;
    
    _lblNavTitle.text = [appDelegate getString:@"My iKash"];
    
    if([[USERDEFAULTS objectForKey:@"ProfileContinerPage"] isEqualToString:@"Favourite"]){
        [self btnFavouriteClick:nil];
    }else if([[USERDEFAULTS objectForKey:@"ProfileContinerPage"] isEqualToString:@"Newsletter"]){
        [self btnNewsletterClick:nil];
    }else if([[USERDEFAULTS objectForKey:@"ProfileContinerPage"] isEqualToString:@"Voucher"]){
        [self btnVoucherClick:nil];
    }else if([[USERDEFAULTS objectForKey:@"ProfileContinerPage"] isEqualToString:@"Review"]){
        [self btnReviewClick:nil];
    }else{
        [self btnSettingClick:nil];
    }
    if(!IS_IPAD){
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

//-(void)setUpHeaderView{
//    if([[AppDelegate sharedInstance].mutArrSidePenalCategoryList count]>0){
//        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
//        SidePenalSearch *objSidePenalSearch = [aStoryboard instantiateViewControllerWithIdentifier:@"SidePenalSearch"];
//        [self.navigationController pushViewController:objSidePenalSearch animated:YES];
//        [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                        name:@"MFSideMenuStateClosed"
//                                                      object:nil];
//    }
//}

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if(!IS_IPAD){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

-(void)DisplaySaveButton{
    _btnSave.hidden=FALSE;
    _btnNotification.hidden=TRUE;
    
    if([[AppDelegate sharedInstance].strMyikashSaveButton isEqualToString:@"1"]){
        _btnSave.hidden=FALSE;
        [_btnSave setTitle:[appDelegate getString:@"Done"] forState:UIControlStateNormal];
    }else if([[AppDelegate sharedInstance].strMyikashSaveButton isEqualToString:@"2"]){
        _btnSave.hidden=TRUE;
    }else{
        _btnSave.hidden=FALSE;
        [_btnSave setTitle:[appDelegate getString:@"Save"] forState:UIControlStateNormal];
    }
    
}

-(void)setupHeaderTitleSettingsPersonalClick{
    _lblNavTitle.text = [appDelegate getString:@"Personal"];
    _btnSave.hidden = false;
}

-(void)setupHeaderTitleSettingsSecurityClick{
    _lblNavTitle.text = [appDelegate getString:@"Security"];
    _btnSave.hidden = TRUE;
}

-(void)setupHeaderTitleSettingsCategoryClick{
    _lblNavTitle.text = [appDelegate getString:@"Category"];
}

-(void)setupHeaderTitleSettingsSupplierClick{
    _lblNavTitle.text = [appDelegate getString:@"Supplier"];
}

-(void)setupHeaderTitleSettingsUnsubscribeClick{
    _lblNavTitle.text = [appDelegate getString:@"Unsubscribe"];
}

-(void)updateView{
    //    [[NSNotificationCenter defaultCenter] removeObserver:self];
    _btnBack.hidden=FALSE;
    _imgViewBack.hidden=FALSE;
    _btnDrawer.hidden=TRUE;
    if (IS_IPAD) {
        _btnBack.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if ([segue.identifier isEqualToString:@"embedContainer"]) {
        self.containerViewController = segue.destinationViewController;
    }
}

- (IBAction)btnSettingClick:(id)sender {
    [USERDEFAULTS setObject:@"Setting" forKey:@"ProfileContinerPage"];
    [USERDEFAULTS synchronize];
    
    isSettingsClick = YES;
    [self HideButtons];
    [self SettingSelected];
    [self NewsletterNotSelected];
    [self FavouriteNotSelected];
    [self ReviewNotSelected];
    [self VoucherNotSelected];
    
    NSString *strSegue=@"embedSetting";
    [self.containerViewController swapViewControllers:strSegue];
}

- (IBAction)btnNewsletterClick:(id)sender {
    [USERDEFAULTS setObject:@"Newsletter" forKey:@"ProfileContinerPage"];
    [USERDEFAULTS synchronize];
    
    isSettingsClick = NO;
    [self HideButtons];
    [AppDelegate sharedInstance].strMyikashSaveButton = @"0";
    [self NewsletterSelected];
    [self SettingNotSelected];
    [self FavouriteNotSelected];
    [self ReviewNotSelected];
    [self VoucherNotSelected];
    
    NSString *strSegue=@"embedNewsletter";
    [self.containerViewController swapViewControllers:strSegue];
}

- (IBAction)btnFavouriteClick:(id)sender {
    
    [USERDEFAULTS setObject:@"Favourite" forKey:@"ProfileContinerPage"];
    [USERDEFAULTS synchronize];
    isSettingsClick = NO;
    [self HideButtons];
    [self FavouriteSelected];
    [self SettingNotSelected];
    [self NewsletterNotSelected];
    [self ReviewNotSelected];
    [self VoucherNotSelected];
    
    NSString*strSegue=@"embedFavourite";
    [self.containerViewController swapViewControllers:strSegue];
}

- (IBAction)btnReviewClick:(id)sender {
    
    [USERDEFAULTS setObject:@"Review" forKey:@"ProfileContinerPage"];
    [USERDEFAULTS synchronize];
    
    isSettingsClick = NO;
    [self HideButtons];
    [self ReviewSelected];
    [self SettingNotSelected];
    [self NewsletterNotSelected];
    [self FavouriteNotSelected];
    [self VoucherNotSelected];
    
    NSString *strSegue=@"embedReview";
    [self.containerViewController swapViewControllers:strSegue];
}

- (IBAction)btnVoucherClick:(id)sender{
    
    [USERDEFAULTS setObject:@"Voucher" forKey:@"ProfileContinerPage"];
    [USERDEFAULTS synchronize];
    
    isSettingsClick = NO;
    [self HideButtons];
    [self VoucherSelected];
    [self SettingNotSelected];
    [self NewsletterNotSelected];
    [self FavouriteNotSelected];
    [self ReviewNotSelected];
    
    NSString*strSegue=@"embedVoucher";
    [self.containerViewController swapViewControllers:strSegue];
}

- (IBAction)btnBackClick:(id)sender {
    _btnBack.hidden=TRUE;
    _btnDrawer.hidden=FALSE;
    _imgViewBack.hidden=TRUE;
    _btnSave.hidden=TRUE;
    //    if([[AppDelegate sharedInstance].strGotNotification isEqualToString:@"1"]){
    //        imgPointerNotication.hidden=FALSE;
    //    }else{
    //        imgPointerNotication.hidden=TRUE;
    //    }
    _btnNotification.hidden=TRUE;
    _lblNavTitle.text = [appDelegate getString:@"My iKash"];
    [self.containerViewController BackViewController];
}

- (IBAction)btnActionToClickDrawer:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{

    }];
}

- (IBAction)btnActionToClickNotification:(id)sender {
    if([[AppDelegate sharedInstance] checkNetworkStatus]){
        if (IS_IPAD) {
            UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            NotificationVC *objNotificationVC = [aStoryboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
            self.notifyAndLocationPopover = [[UIPopoverController alloc] initWithContentViewController:objNotificationVC];
            self.notifyAndLocationPopover.delegate = self;
            self.notifyAndLocationPopover.popoverContentSize = CGSizeMake(320.0, 600.0);
            self.view.alpha = 0.6;
            [self.notifyAndLocationPopover presentPopoverFromRect:[(UIButton *)sender bounds]
                                                           inView:self.btnNotification
                                         permittedArrowDirections:UIPopoverArrowDirectionUp
                                                         animated:YES];
        }else{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            NotificationVC *objNotificationVC = [storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
            [self.navigationController pushViewController:objNotificationVC animated:YES];
        }
    }else{
        [[AppDelegate sharedInstance] showAlertView:[appDelegate getString:@"INTERNET_CONNECTION_ERROR"] Tag:@"0"];
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    self.view.alpha = 1.0;
    return YES;
}

-(void)SettingSelected{
    [_viewSettings setBackgroundColor:[appDelegate colorWithHexString:@"F77739"] ];
    _imgSettings.image=[UIImage imageNamed:@"ic-setting-selected"];
    _lblSettings.textColor= [UIColor whiteColor];
    //[appDelegate colorWithHexString:@"ffffff"];
}

-(void)SettingNotSelected{
   // [_viewSettings setBackgroundColor:[UIColor lightGrayColor]];
    [_viewSettings setBackgroundColor:  [appDelegate colorWithHexString:@"E1E1E5"]];
    _imgSettings.image=[UIImage imageNamed:@"ic-setting"];
    _lblSettings.textColor= [UIColor blackColor];
    //[appDelegate colorWithHexString:@"6F6F70"];//6F6F70 323232
}

-(void)NewsletterSelected{
    
    if([[AppDelegate sharedInstance].strGotNotification isEqualToString:@"1"]){
        imgPointerNotication.hidden=FALSE;
    }else{
        imgPointerNotication.hidden=TRUE;
    }
    
    [_viewNewsLetter setBackgroundColor:[appDelegate colorWithHexString:@"333333"] ];
    _imgNewsletter.image=[UIImage imageNamed:@"ic-newsletter&email-selected"];
    _lblNewsletter.textColor= [UIColor whiteColor];
}

-(void)NewsletterNotSelected{
  //  [_viewNewsLetter setBackgroundColor:[appDelegate colorWithHexString:@"ffffff"] ];
     [_viewNewsLetter setBackgroundColor:  [appDelegate colorWithHexString:@"E1E1E5"]];
    
    
    _imgNewsletter.image=[UIImage imageNamed:@"ic-newsletter&email"];
    _lblNewsletter.textColor= [UIColor blackColor];
}

-(void)FavouriteSelected{
    [_viewFavourite setBackgroundColor:[appDelegate colorWithHexString:@"F77739"] ];
    _imgFavourite.image=[UIImage imageNamed:@"ic-fav.campaign-selected"];
    _lblFavourite.textColor= [UIColor whiteColor];
}

-(void)FavouriteNotSelected{
 //   [_viewFavourite setBackgroundColor:[UIColor lightGrayColor]] ;
    
       [_viewFavourite setBackgroundColor:  [appDelegate colorWithHexString:@"E1E1E5"]];
     
     //[appDelegate colorWithHexString:@"ffffff"] ];
    _imgFavourite.image=[UIImage imageNamed:@"ic-fav.campaign"];
    _lblFavourite.textColor= [UIColor blackColor];
}

-(void)ReviewSelected{
    [_viewReview setBackgroundColor:[appDelegate colorWithHexString:@"F77739"] ];
    
    
    
    _imgReview.image=[UIImage imageNamed:@"ic-review&rating-selected"];
    _lblReview.textColor= [UIColor whiteColor];
}

-(void)ReviewNotSelected{
  //  [_viewReview setBackgroundColor:[UIColor lightGrayColor]];
    
     [_viewReview setBackgroundColor:  [appDelegate colorWithHexString:@"E1E1E5"]];
     
     //[appDelegate colorWithHexString:@"ffffff"] ];
    _imgReview.image=[UIImage imageNamed:@"ic-review&rating"];
    _lblReview.textColor= [UIColor blackColor];
}

-(void)VoucherSelected{
    [_viewVouchers setBackgroundColor:[appDelegate colorWithHexString:@"F77739"] ];
    _imgVoucher.image=[UIImage imageNamed:@"ic-vouchers-selected"];
    _lblVoucher.textColor= [UIColor whiteColor];
}

-(void)VoucherNotSelected{
  //  [_viewVouchers setBackgroundColor:[UIColor lightGrayColor]];
    
      [_viewVouchers setBackgroundColor:  [appDelegate colorWithHexString:@"E1E1E5"]];
     
     //[appDelegate colorWithHexString:@"ffffff"] ];
    _imgVoucher.image=[UIImage imageNamed:@"ic-vouchers"];
    _lblVoucher.textColor= [UIColor blackColor];
}

- (IBAction)btnSaveClick:(id)sender{
    if (isSettingsClick) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EditData" object:nil];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EditNewsData" object:nil];
    }
}

-(void)HideButtons{
    _lblNavTitle.text = [appDelegate getString:@"My iKash"];
    _btnBack.hidden=TRUE;
    _btnDrawer.hidden=TRUE;
    _btnSave.hidden=TRUE;
    _btnNotification.hidden=TRUE;
    _imgViewBack.hidden=TRUE;
    //    if([[AppDelegate sharedInstance].strGotNotification isEqualToString:@"1"]){
    //        imgPointerNotication.hidden=FALSE;
    //    }else{
    //        imgPointerNotication.hidden=TRUE;
    //    }
}

-(void)setGotNotifcation{
    
    [AppDelegate sharedInstance].strGotNotification=@"1";
    imgPointerNotication.hidden=FALSE;
}

@end
