//
//  ProfileContainerVC.h
//  iKashDemo
//
//  Created by indianic on 17/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileContainerVC : UIViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (weak, nonatomic) IBOutlet UILabel *lblPersonalInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblManageBranch;
@property (weak, nonatomic) IBOutlet UILabel *lblPickSupplier;
@property (weak, nonatomic) IBOutlet UILabel *lblSecuriety;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPersonalInfo;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewManageBranch;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewicSupplier;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSecurity;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstTab;
@property (weak, nonatomic) IBOutlet UILabel *lblSecondTab;
@property (weak, nonatomic) IBOutlet UILabel *lblThirdTab;
@property (weak, nonatomic) IBOutlet UILabel *lblFourthTab;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnSkip;
@property (strong, nonatomic) IBOutlet UIButton *btnPersonalInfo;
@property (strong, nonatomic) IBOutlet UIButton *btnManageBranch;
@property (strong, nonatomic) IBOutlet UIButton *btnPickSupplier;
@property (strong, nonatomic) IBOutlet UIButton *btnSecurity;
- (IBAction)btnPersonalInfoClick:(id)sender;
- (IBAction)btnManageBranchClick:(id)sender;
- (IBAction)btnPickSupplierClick:(id)sender;
- (IBAction)btnSecurityClick:(id)sender;
- (IBAction)btnSkipToHomeClick:(id)sender;
- (IBAction)btnBackClick:(id)sender;
@end
