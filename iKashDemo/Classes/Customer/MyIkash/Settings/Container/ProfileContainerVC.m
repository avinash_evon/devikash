//
//  ProfileContainerVC.m
//  iKashDemo
//
//  Created by indianic on 17/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "ProfileContainerVC.h"
#import "PersonalInfoContainerVC.h"
#import "CategoryVC.h"

@interface ProfileContainerVC (){
    NSString *strFirstName;
}

@property (strong, nonatomic) CategoryVC *CategoryVC;

@property (nonatomic, weak) PersonalInfoContainerVC *PersonalInfoContainerVC;

@end

@implementation ProfileContainerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [AppDelegate sharedInstance].WelCometopConstraint = self.ContraintHeight;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(btnManageBranchClick:) name:@"CategoryTab" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(btnPickSupplierClick:) name:@"SupplierTab" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(btnSecurityClick:) name:@"SecurityTab" object:nil];
    [self btnPersonalInfoClick:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!IS_IPAD)
    {
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        
        [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    }
}

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:break;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(!IS_IPAD)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if ([segue.identifier isEqualToString:@"embedInfoContainer"]) {
        self.PersonalInfoContainerVC = segue.destinationViewController;
        self.PersonalInfoContainerVC.editEmailShouldEnable = @"0";
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btnPersonalInfoClick:(id)sender{
    _btnBack.hidden = YES;
    _btnSkip.hidden = YES;
    [self SetHeaderTitle];
    _imgViewPersonalInfo.image=[UIImage imageNamed:@"ic-personal-info"];
    _imgViewManageBranch.image=[UIImage imageNamed:@"ic-manage-branch"];
    _imgViewicSupplier.image=[UIImage imageNamed:@"ic-pick-supplier"];
    _imgViewSecurity.image=[UIImage imageNamed:@"ic-security"];
    
    _lblFirstTab.hidden=FALSE;
    _lblSecondTab.hidden=TRUE;
    _lblThirdTab.hidden=TRUE;
    _lblFourthTab.hidden=TRUE;
    
    [self ButtonDisbale];

   
    NSString *strSegue=@"embedEditProfile";
    [self.PersonalInfoContainerVC swapViewControllers:strSegue];
}

- (IBAction)btnManageBranchClick:(id)sender {
    _btnBack.hidden = NO;
    _btnSkip.hidden = NO;
    [self SetHeaderTitle];
    _imgViewPersonalInfo.image=[UIImage imageNamed:@"ic-personal-info"];
    _imgViewManageBranch.image=[UIImage imageNamed:@"ic-manage-branch-selected"];
    _imgViewicSupplier.image=[UIImage imageNamed:@"ic-pick-supplier"];
    _imgViewSecurity.image=[UIImage imageNamed:@"ic-security"];
    
    _lblFirstTab.hidden=TRUE;
    _lblSecondTab.hidden=FALSE;
    _lblThirdTab.hidden=TRUE;
    _lblFourthTab.hidden=TRUE;
        [self ButtonDisbale];
    NSString *strSegue=@"embedCategory";
    [self.PersonalInfoContainerVC swapViewControllers:strSegue];
}

- (IBAction)btnPickSupplierClick:(id)sender {
    _btnBack.hidden = NO;
    _btnSkip.hidden = NO;
    [self SetHeaderTitle];
    _imgViewPersonalInfo.image=[UIImage imageNamed:@"ic-personal-info"];
    _imgViewManageBranch.image=[UIImage imageNamed:@"ic-manage-branch-selected"];
    _imgViewicSupplier.image=[UIImage imageNamed:@"ic-pick-supplier-selected"];
    _imgViewSecurity.image=[UIImage imageNamed:@"ic-security"];
    
    _lblFirstTab.hidden=TRUE;
    _lblSecondTab.hidden=TRUE;
    _lblThirdTab.hidden=FALSE;
    _lblFourthTab.hidden=TRUE;
        [self ButtonDisbale];
    NSString *strSegue=@"embedSupplier";
    [self.PersonalInfoContainerVC swapViewControllers:strSegue];
}

- (IBAction)btnSecurityClick:(id)sender {
    _btnBack.hidden = NO;
    [self SetHeaderTitle];
    _imgViewPersonalInfo.image=[UIImage imageNamed:@"ic-personal-info"];
    _imgViewManageBranch.image=[UIImage imageNamed:@"ic-manage-branch-selected"];
    _imgViewicSupplier.image=[UIImage imageNamed:@"ic-pick-supplier-selected"];
    _imgViewSecurity.image=[UIImage imageNamed:@"ic-security-selected"];
    
    _lblFirstTab.hidden=TRUE;
    _lblSecondTab.hidden=TRUE;
    _lblThirdTab.hidden=TRUE;
    _lblFourthTab.hidden=FALSE;
        [self ButtonDisbale];
    NSString *strSegue=@"embedSecurity";
    [self.PersonalInfoContainerVC swapViewControllers:strSegue];
}

- (IBAction)btnSkipToHomeClick:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    [AppDelegate sharedInstance].window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UITabBarController *tb = [storyboard instantiateViewControllerWithIdentifier:@"tabBarController"];
    tb.tabBar.barTintColor= [UIColor whiteColor];//[appDelegate colorWithHexString:@"eeeeee"];
    [tb.tabBar setBackgroundImage:nil];
    [tb.tabBar setClipsToBounds:YES];
    UIViewController *leftSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"leftSideMenuVC"];
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:tb
                                                    leftMenuViewController:leftSideMenuViewController
                                                    rightMenuViewController:nil];
    [AppDelegate sharedInstance].window.rootViewController = container;
    [[AppDelegate sharedInstance].window makeKeyAndVisible];
}

- (IBAction)btnBackClick:(id)sender {
    // _btnBack.hidden=TRUE;
    NSLog(@"self.PersonalInfoContainerVC %@", self.PersonalInfoContainerVC.EditProfileVC);
    NSString *currentSegueIdentifier = nil;
    if ([self.PersonalInfoContainerVC.currentSegueIdentifier isEqualToString:@"embedEditProfile"]) {
        return;
    }else if ([self.PersonalInfoContainerVC.currentSegueIdentifier isEqualToString:@"embedCategory"]){
        currentSegueIdentifier = @"embedEditProfile";
    }else if ([self.PersonalInfoContainerVC.currentSegueIdentifier isEqualToString:@"embedSupplier"]){
        currentSegueIdentifier = @"embedCategory";
    }else{
        currentSegueIdentifier = @"embedSupplier";
    }
    if ([self.PersonalInfoContainerVC.currentSegueIdentifier isEqualToString:@"embedEditProfile"]) {
        return;
    }else if ([self.PersonalInfoContainerVC.currentSegueIdentifier isEqualToString:@"embedCategory"]){
        [self btnPersonalInfoClick:nil];
    }else if ([self.PersonalInfoContainerVC.currentSegueIdentifier isEqualToString:@"embedSupplier"]){
        [self btnManageBranchClick:nil];
    }else{
        [self btnPickSupplierClick:nil];
    }
    [self.PersonalInfoContainerVC swapViewControllers:currentSegueIdentifier];
    //    [self.PersonalInfoContainerVC BackViewController];
}

-(void)SetHeaderTitle{
    strFirstName=[USERDEFAULTS objectForKey:@"FirstName"];
    
    if(strFirstName.length>0){
        _lblTitle.text= [NSString stringWithFormat:@"%@ %@",[appDelegate getString:@"Welcome"],[USERDEFAULTS objectForKey:@"FirstName"]];
    }else{
        _lblTitle.text=[appDelegate getString:@"Welcome"];
    }
}
-(void)ButtonDisbale{
    NSString *strUserName=[USERDEFAULTS objectForKey:@"FirstName"];
    if(strUserName.length==0){
        _btnManageBranch.userInteractionEnabled=FALSE;
        _btnPickSupplier.userInteractionEnabled=FALSE;
        _btnSecurity.userInteractionEnabled=FALSE;
    }else{
        _btnManageBranch.userInteractionEnabled=TRUE;
        _btnPickSupplier.userInteractionEnabled=TRUE;
        _btnSecurity.userInteractionEnabled=TRUE;
    }
}

@end
