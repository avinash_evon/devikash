//
//  ChangePasswordVC.m
//  iKash
//
//  Created by indianic on 25/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "ChangePasswordVC.h"
#import "ChangePasswordCell.h"

@interface ChangePasswordVC (){
    NSMutableArray *mutArrQuestionList;
    BOOL visible;
    NSString *strQuestionID;
    UIInterfaceOrientation orientationDevice;
    NSMutableArray *questArr;
    BOOL questionFlag;
}

@end

@implementation ChangePasswordVC
- (IBAction)backAction:(UIButton *)sender {
  //  [UINavigationController.UINavigationItem pop]
    
      [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
      [self setNeedsStatusBarAppearanceUpdate];
    
     questArr = [[NSMutableArray alloc]init];
   
    [self.imgViewSelectQuestion.layer setBorderColor:[[appDelegate colorWithHexString:@"cdcbcc"] CGColor]];
    [self.imgViewSelectQuestion.layer setBorderWidth:0.5f];
    [self.imgViewSelectQuestion.layer setCornerRadius:2.5f];
    
    self.txtViewAnswer.layer.borderColor = [[appDelegate colorWithHexString:@"cdcbcc"] CGColor];
    self.txtViewAnswer.layer.borderWidth = 0.5f;
    self.txtViewAnswer.layer.cornerRadius = 2.5f;
    
    [_viewtblBackground.layer setCornerRadius:2.5f];
    [_viewtblBackground.layer setMasksToBounds:YES];
    visible=TRUE;
    
    _txtViewAnswer.delegate=self;
    
    [self QuestionListWS];
    [_scrllView setContentSize:CGSizeMake(_scrllView.frame.size.width, 600.0)];
    
    
    [_btnChangePass setTitle:[appDelegate getString:@"Save Password"] forState:UIControlStateNormal];
     [_btnChangeSecurity setTitle:[appDelegate getString:@"Save Question"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
         [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    
    questionFlag = FALSE;
    _txtViewAnswer.text = @"";
    // _txtViewAnswer.editable = NO;
    
    toolBarTextView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    UIBarButtonItem *aBarBtnFlexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *aBarBtnDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(btnDoneClick:)];
    aBarBtnDone.tintColor = [UIColor blackColor];
    [items addObject:aBarBtnFlexibleItem];
    [items addObject:aBarBtnDone];
    [toolBarTextView setItems:items animated:NO];
    
    _txtViewAnswer.inputAccessoryView = toolBarTextView;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ScanCompleted" object:nil];
    orientationDevice = [UIApplication sharedApplication].statusBarOrientation;
    if(!IS_IPAD)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
}

- (IBAction)btnDoneClick:(id)sender {
    // Hide keyboard on tap event..
    
    
    if ([_txtViewAnswer isFirstResponder]) {
        [_txtViewAnswer resignFirstResponder];
    }
}


-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    orientationDevice=[UIApplication sharedApplication].statusBarOrientation;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    if(orientationDevice == UIInterfaceOrientationLandscapeLeft || orientationDevice == UIInterfaceOrientationLandscapeRight)
    {
        [UIView animateWithDuration:2.0 animations:^{
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            [AppDelegate sharedInstance].WelCometopConstraint.constant=38;
        }];
    }
    NSLog(@"Hide");
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    if(orientationDevice == UIInterfaceOrientationLandscapeLeft || orientationDevice == UIInterfaceOrientationLandscapeRight)
    {
        [UIView animateWithDuration:2.0 animations:^{
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            [AppDelegate sharedInstance].WelCometopConstraint.constant=0;
        }];
    }else{
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [AppDelegate sharedInstance].WelCometopConstraint.constant=64;
    }
    
    NSLog(@"Show");
}

# pragma mark - Tableview Delegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [questArr count]; //[mutArrItemList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ChangePasswordCell *Cell = (ChangePasswordCell*)[tableView dequeueReusableCellWithIdentifier:@"ChangePasswordCell"];
    [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
   
    Cell.lblQuestion.text= [questArr objectAtIndex:indexPath.row]; //objectForKey:@"question"];
    
    return Cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    _viewtblBackground.hidden=TRUE;
    
    visible=TRUE;
    
    _txtFieldSelectQuestion.text= [questArr objectAtIndex:indexPath.row];
    if(indexPath.row == 0)
    {
        questionFlag = FALSE;
        strQuestionID = @"0";   //textView1.editable = NO;
        _txtViewAnswer.editable = NO;
        _txtViewAnswer.text = @"";
    }
    else{
        questionFlag = TRUE;
        _txtViewAnswer.editable = YES;
        
    strQuestionID=[[mutArrQuestionList objectAtIndex:indexPath.row-1] objectForKey:@"questions_id"];
    }
    
}

- (IBAction)btnSelectQuestionClick:(id)sender {
    
    if(visible){
        _viewtblBackground.hidden=FALSE;
        visible=FALSE;
    }else{
        _viewtblBackground.hidden=TRUE;
        visible=TRUE;
    }
}

- (IBAction)cancelBtnClk:(UIButton *)sender {
    
    _txtCurrentPassword.text = @"";
    _txtNewPassword.text = @"";
    _txtConfirmPassword.text = @"";
}

- (IBAction)btnActionToChangePassClick:(id)sender {
    
    if (![_txtCurrentPassword.text isValidString]){
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_OLD_PASSWORD"] Tag:@"0"];
        return;
    }
    if (![_txtNewPassword.text isValidString]){
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_NEW_PASSWORD"] Tag:@"0"];
        return;
    }else if ([_txtNewPassword.text length] < 8){
        [_txtNewPassword becomeFirstResponder];
        [appDelegate showAlertView: [appDelegate getString:@"ENTER_MIN_PASSWORD"] Tag:@"0"];
        return;
    }
    else if (![_txtNewPassword.text isValidSecuredPassword]){
        [_txtNewPassword becomeFirstResponder];
        [appDelegate showAlertView: [appDelegate getString:@"ENTER_MIN_PASSWORD"] Tag:@"0"];
        return;
    }else if (![_txtNewPassword.text isEqualToString:_txtConfirmPassword.text]){
        [appDelegate showAlertView:[appDelegate getString:@"PASSWORDS_NOT_MATCH"] Tag:@"0"];
        return;
    }
    
    [self CangePasswordWS];
}

- (IBAction)btnActionToChangeSecurityClick:(id)sender {
  
//    if (![_txtFieldSelectQuestion.text isValidString]) {
//
//        [appDelegate showAlertView:[appDelegate getString:@"ENTER_SELECT_QUESTION"] Tag:@"0"];
//        return;
//
//    }else      //  if (![_txtViewAnswer.text isValidString]) {
//    {
        if (_txtViewAnswer.text.length > 0){
            [self SaveQuestionWS];
        }else {
            NSLog(@"\n\n\nYes the text field is empty");
            
            _txtViewAnswer.text = @"";
            _txtViewAnswer.editable = YES;
            
             if([_txtFieldSelectQuestion.text isEqualToString:@"Do not use the security Question"] )
             {
                 _txtViewAnswer.editable = NO;
                 
                  [self SaveQuestionWS];
                 
             }
             else{
            
            [appDelegate showAlertView:[appDelegate getString:@"ENTER_YOUR_ANSWER"] Tag:@"0"];
                  return;
             }
        }
            
//             [self SaveQuestionWS];
//        }
      //  NSLog(@"checking for empty text %@"[_txtViewAnswer is])
        
      //  [appDelegate showAlertView:[appDelegate getString:@"ENTER_YOUR_ANSWER"] Tag:@"0"];
      ///  return;
        
    }
    
 //   [self SaveQuestionWS];


- (void)QuestionListWS{
   
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                           [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                           nil];
    
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_SecurityQuestionWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {

        NSLog(@"responseData = %@", responseData);
        
        self->questArr = [[NSMutableArray alloc]init];
        
       
        
        NSLog(@"my array >>>>> %@",questArr);
        
         [questArr insertObject:@"Do not use the security Question" atIndex:0];
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
        
            if([[[responseData objectForKey:@"data"] objectForKey:@"get_personal_details"] count]>0){
            
                
                NSLog(@">>>>>>>>>>>>%@",[[[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"] objectAtIndex:0] objectForKey:@"question"]);
                
                [questArr insertObject:[[[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"] objectAtIndex:0] objectForKey:@"question"] atIndex:1];
                
                 [questArr insertObject:[[[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"] objectAtIndex:1] objectForKey:@"question"] atIndex:2];
                
                 [questArr insertObject:[[[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"] objectAtIndex:2] objectForKey:@"question"] atIndex:3];
                
                 [questArr insertObject:[[[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"] objectAtIndex:3] objectForKey:@"question"] atIndex:4];
                
                 [questArr insertObject:[[[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"] objectAtIndex:4] objectForKey:@"question"] atIndex:5];
                
                
                NSLog(@"===============%@",questArr);
                
                [questArr objectAtIndex:0];
                
                
                if( [[[[[responseData objectForKey:@"data"] objectForKey:@"get_personal_details"] objectAtIndex:0] objectForKey:@"security_question"]isEqualToString:@""])
                {
                    self->_txtFieldSelectQuestion.text = @"Do not use the security Question";
                }
                else{
                
                    self->_txtFieldSelectQuestion.text = [[[[responseData objectForKey:@"data"] objectForKey:@"get_personal_details"] objectAtIndex:0] objectForKey:@"security_question"];
                }
                
                _txtViewAnswer.text = [[[[responseData objectForKey:@"data"] objectForKey:@"get_personal_details"] objectAtIndex:0] objectForKey:@"answer"];
                
                if([[[[[responseData objectForKey:@"data"] objectForKey:@"get_personal_details"] objectAtIndex:0] objectForKey:@"answer"] isEqualToString:@""])
                {
                    _txtViewAnswer.editable = NO;
                }
                else{
                    _txtViewAnswer.editable = YES;
                }
                
                //[[[[responseData objectForKey:@"data"] objectForKey:@"get_personal_details"] objectAtIndex:0] objectForKey:@"answer"];
                
                strQuestionID = [[[[responseData objectForKey:@"data"] objectForKey:@"get_personal_details"] objectAtIndex:0] objectForKey:@"security_questions_id"];
            }

            mutArrQuestionList = [[NSMutableArray alloc]initWithArray:[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"]];

            [_tblQuestion reloadData];

        }else{
            
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (IBAction)btnGetStartedClick:(id)sender{

    if (![_txtFieldSelectQuestion.text isValidString]) {
    
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_SELECT_QUESTION"] Tag:@"0"];
        return;
        
    }else if (![_txtViewAnswer.text isValidString]) {
        
      //  _txtViewAnswer.editable
        
     //   if([[questArr objectAtIndex:0])
        
        if(questionFlag == TRUE)
        {
        
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_YOUR_ANSWER"] Tag:@"0"];
        return;
        }
    }

    [self SaveQuestionWS];
}

-(void)SaveQuestionWS{
    
    
    NSLog(@"$$$$$$$$$$$$$$$$ %@",_txtViewAnswer.text);
    
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                           [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                           strQuestionID,@"security_ques_id",
                           _txtViewAnswer.text,@"answer",
                           nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_PersonalInfoWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
      
        NSLog(@"responseData = %@", responseData);
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
        
            [_txtViewAnswer resignFirstResponder];
            
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
            
        }else{
            
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
            
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)CangePasswordWS{
   
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                           [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                           [USERDEFAULTS objectForKey:@"access_token"],@"access_token",
                           _txtCurrentPassword.text,@"password",
                           _txtNewPassword.text,@"new_password",
                           [USERDEFAULTS objectForKey:@"Langauge"],@"lang_id",
                           nil];
    
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_ChangePasswordWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
    
        NSLog(@"responseData = %@", responseData);
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            _txtCurrentPassword.text=@"";
            _txtNewPassword.text=@"";
            _txtConfirmPassword.text=@"";
            
            [_txtConfirmPassword resignFirstResponder];
           
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }else{
            
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
            
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
#pragma mark- UITextField

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
   
    [textField resignFirstResponder];
   
    return YES;
}
- (IBAction)backBtnClk:(UIButton *)sender {
}

- (IBAction)saveEmailBtnClk:(UIButton *)sender {
}
@end
