//
//  SecurityVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "SecurityVC.h"
#import "EditProfileVC.h"
#import "CategoryVC.h"
#import "SupplierVC.h"
#import "SecurityCell.h"
#import "AppDelegate.h"
#import "ChangePasswordVC.h"

@interface SecurityVC (){
    NSMutableArray *mutArrQuestionList;
    BOOL visible;
    NSString *strQuestionID;
    UIInterfaceOrientation orientationDevice;
    NSMutableArray *questionArr;
}

@end

@implementation SecurityVC

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSLog(@"security Edit Flag %@",[AppDelegate sharedInstance].securityEditFlag);
    
  //  btnSelectQuestionClick
    if([[AppDelegate sharedInstance].securityEditFlag isEqualToString:@"0"])
    {
        
        _selectQuestionOutlet.hidden = TRUE;
        
        _txtViewYourAnswer.editable = NO;
       // btnSelectQuestionClick
    }
    else{
        
       
        _selectQuestionOutlet.hidden = FALSE;
               
        _txtViewYourAnswer.editable = YES;
    }
    
    
    [self.imgViewSelectQuestion.layer setBorderColor:[[appDelegate colorWithHexString:@"cdcbcc"] CGColor]];
    [self.imgViewSelectQuestion.layer setBorderWidth:0.5f];
    [self.imgViewSelectQuestion.layer setCornerRadius:2.5f];
    
    self.txtViewYourAnswer.layer.borderColor = [[appDelegate colorWithHexString:@"cdcbcc"] CGColor];
    self.txtViewYourAnswer.layer.borderWidth = 0.5f;
    self.txtViewYourAnswer.layer.cornerRadius = 2.5f;
    
    _tblQuestion.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [_viewtblBackground.layer setCornerRadius:2.5f];
    [_viewtblBackground.layer setMasksToBounds:YES];
    visible=TRUE;
    _txtFieldSelectQuestion.text=@"";
    _txtFieldSelectQuestion.placeholder=[NSString stringWithFormat:@"%@",[appDelegate getString:@"Select question"]];
    
    toolBarTextView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    UIBarButtonItem *aBarBtnFlexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *aBarBtnDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(btnDoneClick:)];
    aBarBtnDone.tintColor = [UIColor blackColor];
    [items addObject:aBarBtnFlexibleItem];
    [items addObject:aBarBtnDone];
    [toolBarTextView setItems:items animated:NO];
    
    _txtViewYourAnswer.inputAccessoryView = toolBarTextView;
    
    [self QuestionListWS];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
 
    
    _txtViewYourAnswer.editable = NO;
    
     _txtFieldSelectQuestion.text= @"Do not use the security Question"; //objectForKey:@"question"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ScanCompleted" object:nil];
    orientationDevice = [UIApplication sharedApplication].statusBarOrientation;
    if(!IS_IPAD)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


#pragma mark- Notification Methods
-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    orientationDevice=[UIApplication sharedApplication].statusBarOrientation;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    if(orientationDevice == UIInterfaceOrientationLandscapeLeft || orientationDevice == UIInterfaceOrientationLandscapeRight)
    {
        [UIView animateWithDuration:2.0 animations:^{
            [AppDelegate sharedInstance].WelCometopConstraint.constant=38;
        }];
    }
    NSLog(@"Hide");
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    if(orientationDevice == UIInterfaceOrientationLandscapeLeft || orientationDevice == UIInterfaceOrientationLandscapeRight)
    {
        [UIView animateWithDuration:2.0 animations:^{
            [AppDelegate sharedInstance].WelCometopConstraint.constant=0;
        }];
    }else{
        [AppDelegate sharedInstance].WelCometopConstraint.constant=64;
    }
    
    NSLog(@"Show");
}

# pragma mark - Tableview Delegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [questionArr count]; //[mutArrItemList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SecurityCell *Cell = (SecurityCell*)[tableView dequeueReusableCellWithIdentifier:@"SecurityCell"];
    [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    Cell.lblQuestion.text= [questionArr objectAtIndex:indexPath.row]; //objectForKey:@"question"];
    
    return Cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _viewtblBackground.hidden=TRUE;
    visible=TRUE;
    
    
    NSLog(@"my index number %d",indexPath.row);
    
    _txtFieldSelectQuestion.text= [questionArr objectAtIndex:indexPath.row]; //objectForKey:@"question"];
    
    
    
    if(indexPath.row == 0)
       {
           strQuestionID = @"0";   //textView1.editable = NO;
           _txtViewYourAnswer.editable = NO;
           _txtViewYourAnswer.text = @"";
       }
       else{
           
           _txtViewYourAnswer.editable = YES;
           
       strQuestionID=[[mutArrQuestionList objectAtIndex:indexPath.row-1] objectForKey:@"questions_id"];
       }
    
//     if(indexPath.row == 0)
//     {
//
//     }
//    else
//    {
//
//    strQuestionID=[[mutArrQuestionList objectAtIndex:indexPath.row] objectForKey:@"questions_id"];
//    }
}


#pragma mark - Button Event Click Methods

- (IBAction)btnDoneClick:(id)sender {
    // Hide keyboard on tap event..
    if ([_txtViewYourAnswer isFirstResponder]) {
        [_txtViewYourAnswer resignFirstResponder];
    }
}

- (IBAction)btnSelectQuestionClick:(id)sender {
    if(visible){
        _viewtblBackground.hidden=FALSE;
        visible=FALSE;
    }else{
        _viewtblBackground.hidden=TRUE;
        visible=TRUE;
    }
}

-(void)viewDidLayoutSubviews{
    UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
    if(UIInterfaceOrientationIsPortrait(orientation)){
        _scrllView.scrollEnabled = NO;
    }else{
        _scrllView.scrollEnabled = YES;
        [_scrllView setContentSize:CGSizeMake(_scrllView.frame.size.width, 250.0)];
    }
}



- (IBAction)btnGetStartedClick:(id)sender{
   /* if (![_txtFieldSelectQuestion.text isValidString])
    {
        //[_txtFieldSelectQuestion.text isEqualToString:@"Select question"]) {
        [appDelegate showAlertView: [appDelegate getString:@"ENTER_SELECT_QUESTION"] Tag:@"0"];
        return;
    }
    else if (![_txtViewYourAnswer.text isValidString]) {
        if (_txtViewYourAnswer.text.length > 0){
                [self SaveQuestionWS];
            }else {
                NSLog(@"\n\n\nYes the text field is empty");
                
                _txtViewYourAnswer.text = @"";
                
                _txtViewYourAnswer.editable = YES;
                
                [appDelegate showAlertView:[appDelegate getString:@"ENTER_YOUR_ANSWER"] Tag:@"0"];
                      return;
            }*/ if (_txtViewYourAnswer.text.length > 0){
                
                [AppDelegate sharedInstance].securityEditFlag = @"0";
                
                           [self SaveQuestionWS];
                       }else {
                           NSLog(@"\n\n\nYes the text field is empty");
                           
                           _txtViewYourAnswer.text = @"";
                           
                           _txtViewYourAnswer.editable = YES;
                           
                           if([_txtFieldSelectQuestion.text isEqualToString:@"Do not use the security Question"] )
                           {
                               [AppDelegate sharedInstance].securityEditFlag = @"0";
                               
                               _txtViewYourAnswer.editable = NO;
                               
                               [self SaveQuestionWS];
                           }
                           else{
                           
                           [appDelegate showAlertView:[appDelegate getString:@"ENTER_YOUR_ANSWER"] Tag:@"0"];
                                 return;
                           }
                       }
                
               //  [self SaveQuestionWS];
    }

- (IBAction)changePwnBtnClk:(UIButton *)sender {
    
    UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    
   // ChangePasswordVC *objChangePwdVC = [UIStoryboard insta];
    
      ChangePasswordVC *objChangePwdVC = [aStoryboard instantiateViewControllerWithIdentifier:@"ChangePasswordVC"];
    
    [self.navigationController pushViewController:objChangePwdVC animated:TRUE];
    
    
}
//  [self SaveQuestionWS];
//}


#pragma mark - TextView Methods

//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
//    NSRange resultRange = [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet] options:NSBackwardsSearch];
//    if ([text length] == 1 && resultRange.location != NSNotFound) {
//        [textView resignFirstResponder];
//        return NO;
//    }
//
//    return YES;
//}

#pragma mark - Webservice Methods
-(void)SaveQuestionWS{
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  strQuestionID,@"security_ques_id",
                                  _txtViewYourAnswer.text,@"answer",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_PersonalInfoWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            [AppDelegate sharedInstance].window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            UITabBarController *tb = [storyboard instantiateViewControllerWithIdentifier:@"tabBarController"];
            tb.tabBar.barTintColor= [UIColor whiteColor];//[appDelegate colorWithHexString:@"eeeeee"];
            [tb.tabBar setBackgroundImage:nil];
            [tb.tabBar setClipsToBounds:YES];
            UIViewController *leftSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"leftSideMenuVC"];
            MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                            containerWithCenterViewController:tb
                                                            leftMenuViewController:leftSideMenuViewController
                                                            rightMenuViewController:nil];
            [AppDelegate sharedInstance].window.rootViewController = container;
            [[AppDelegate sharedInstance].window makeKeyAndVisible];
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
- (void)QuestionListWS{
    
    questionArr = [[NSMutableArray alloc]init];
    
     [questionArr insertObject:@"Do not use the security Question" atIndex:0];
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_SecurityQuestionWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            mutArrQuestionList=[[NSMutableArray alloc]initWithArray:[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"]];
            
            
            NSLog(@">>>>>>>>>>>>>>>>>>>>>>>%@",[[[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"] objectAtIndex:1] objectForKey:@"question"]);
            
            [questionArr insertObject:[[[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"] objectAtIndex:0] objectForKey:@"question"] atIndex:1];
            
             [questionArr insertObject:[[[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"] objectAtIndex:1] objectForKey:@"question"] atIndex:2];
            
             [questionArr insertObject:[[[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"] objectAtIndex:2] objectForKey:@"question"] atIndex:3];
            
             [questionArr insertObject:[[[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"] objectAtIndex:3] objectForKey:@"question"] atIndex:4];
            
             [questionArr insertObject:[[[[responseData objectForKey:@"data"] objectForKey:@"get_security_questions"] objectAtIndex:4] objectForKey:@"question"] atIndex:5];
            
            NSLog(@"MY Security Question: %@",[[[[responseData objectForKey:@"data"] objectForKey:@"get_personal_details"] objectAtIndex:0] objectForKey:@"security_question"]);
            
            _txtFieldSelectQuestion.text = [[[[responseData objectForKey:@"data"] objectForKey:@"get_personal_details"] objectAtIndex:0] objectForKey:@"security_question"];
            
            //answer
            


            _txtViewYourAnswer.text = [[[[responseData objectForKey:@"data"] objectForKey:@"get_personal_details"] objectAtIndex:0] objectForKey:@"answer"];
            
            if([[[[[responseData objectForKey:@"data"] objectForKey:@"get_personal_details"] objectAtIndex:0] objectForKey:@"answer"]isEqualToString:@""])
            {
                _txtViewYourAnswer.editable = NO;
                
                _txtFieldSelectQuestion.text =  @"Do not use the security Question";
            }
            else{
                _txtViewYourAnswer.editable = YES;
           
              //  _txtFieldSelectQuestion.text = @"security_question";
            
            }
            
            
            [_tblQuestion reloadData];
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
- (IBAction)backBtnClk:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:TRUE];
}



- (IBAction)SecurityEditBtnClk:(UIButton *)sender {
    
    [AppDelegate sharedInstance].securityEditFlag = @"1";
    
    
    NSLog(@"Updated security Flag %@",[AppDelegate sharedInstance].securityEditFlag);
    
    
}



@end
