//
//  SupplierVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupplierVC : UIViewController<UITextFieldDelegate>{
    IBOutlet NSLayoutConstraint *tblViewBottomConstraint;
    UIToolbar *toolBarTextView;    
}

@property (strong, nonatomic) IBOutlet UITableView *tblSuppiler;

@property (strong, nonatomic) IBOutlet UITextField *txtPromotionCode;
@property (strong, nonatomic) IBOutlet UILabel *lblSupplierOptions;

@property (strong, nonatomic) IBOutlet UIImageView *imgViewBorder;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewCheck;
@property (strong, nonatomic) IBOutlet UILabel *lblPromotion;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
-(IBAction)btnNextClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;

- (IBAction)editSupplierBckBtnClk:(id)sender;



@end
