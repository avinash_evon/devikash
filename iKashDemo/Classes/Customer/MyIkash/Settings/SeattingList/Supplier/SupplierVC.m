//
//  SupplierVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "SupplierVC.h"
#import "CategoryVC.h"
#import "SecurityVC.h"
#import "EditProfileVC.h"
#import "SuppilerCell.h"
#import "UIImageView+WebCache.h"



@interface SupplierVC (){
    NSMutableArray *mutArrSupplierList;
    NSMutableArray *mutArrFavSupplierList;
    NSMutableDictionary *mutDicSelectedSupplier;
    NSString *strPromotionCodeFlag;
    NSMutableArray *mutArrSelectedSupplier;
    UIInterfaceOrientation orientationDevice;
    NSMutableArray *favouriteSuppplyer;
   

}

@end

@implementation SupplierVC

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.imgViewBorder.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [self.imgViewBorder.layer setBorderWidth:0.5];
    [self.imgViewBorder.layer setCornerRadius:2.5];
    mutDicSelectedSupplier=[[NSMutableDictionary alloc]init];
    favouriteSuppplyer = [[NSMutableArray alloc]init];
  //  favouriteSupplyerDict =  [[NSMutableDictionary alloc]init];
    
    // Do any additional setup after loading the view.
}

- (IBAction)backBtnClk:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:TRUE];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [AppDelegate sharedInstance].strMyikashSaveButton=@"0";
    mutArrSelectedSupplier=[[NSMutableArray alloc]init];
    _tblSuppiler.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self SupplierListWS];
    [self SetUpPage];
    if([[AppDelegate sharedInstance].strFlag isEqualToString:@"0"]){
        _btnNext.hidden=TRUE;
        tblViewBottomConstraint.constant = 0;
        [self CustomerFavSupplierWS];
    }
    orientationDevice = [UIApplication sharedApplication].statusBarOrientation;
    if(!IS_IPAD)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }

}


#pragma mark- Notification Methods
-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    orientationDevice=[UIApplication sharedApplication].statusBarOrientation;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    if(orientationDevice == UIInterfaceOrientationLandscapeLeft || orientationDevice == UIInterfaceOrientationLandscapeRight)
    {
        [UIView animateWithDuration:2.0 animations:^{
            [AppDelegate sharedInstance].WelCometopConstraint.constant=38;
        }];
    }
    NSLog(@"Hide");
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    if(orientationDevice == UIInterfaceOrientationLandscapeLeft || orientationDevice == UIInterfaceOrientationLandscapeRight)
    {
        [UIView animateWithDuration:2.0 animations:^{
            [AppDelegate sharedInstance].WelCometopConstraint.constant=0;
        }];
    }else{
        [AppDelegate sharedInstance].WelCometopConstraint.constant=64;
    }
    
    NSLog(@"Show");
}

#pragma mark -
#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
   // mutArrFavSupplierList mutArrSupplierList
    
    NSLog(@"Favoueite list %@",mutArrSupplierList);
    
    if([favouriteSuppplyer count] != 0)
    {
        return [favouriteSuppplyer count];
    }
   else{
        
       return 0;      //[mutArrSupplierList count];
        
    }
    
   // return [favouriteSuppplyer count];//favouriteSuppplyer mutArrSupplierList
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SuppilerCell *cell = (SuppilerCell *) [tableView dequeueReusableCellWithIdentifier:@"SuppilerCell"];
    // check if row is odd or even and set color accordingly
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
   if([favouriteSuppplyer count] != 0)
   {
                       
    NSLog(@"Favourite######>>>>>>%@",[[self->favouriteSuppplyer objectAtIndex:indexPath.row]valueForKey:@"CompayName"]);
        
      
    cell.lblSupplierName.text=[[self->favouriteSuppplyer objectAtIndex:indexPath.row]valueForKey:@"CompayName"];
        
   
//    cell.lblSupplierName.text=[[mutArrSupplierList objectAtIndex:indexPath.row] objectForKey:@"s_company_name"];
    
    
    NSString *strImgName = [[self->favouriteSuppplyer objectAtIndex:indexPath.row]valueForKey:@"s_logo"];
    NSURL *urlSupliermage = [NSURL URLWithString:strImgName];
    
    [cell.imgViewSupplierImage sd_setImageWithURL:urlSupliermage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        cell.imgViewSupplierImage.clipsToBounds = YES;
    }];
    
    
    //cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:urlSupliermage]];
    
    
//    [cell.imageView.image sd_setImageWithURL:urlSupliermage
//                    placeholderImage:nil
//                             options:SDWebImageProgressiveDownload
//                            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//                            }
//                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                               _imgCampaign.clipsToBounds = YES;
//                           }];
    
    cell.btnCheck.tag=indexPath.row;
    [cell.btnCheck addTarget:self action:@selector(btnSelectSupplierClick:) forControlEvents:UIControlEventTouchUpInside];
    NSMutableDictionary *dictItem =[self->favouriteSuppplyer objectAtIndex:indexPath.row];
    
    //self->favouriteSuppplyer mutArrSupplierList
    
    
    //mutArrFavSupplierList mutArrSelectedSupplier
    if ([self->favouriteSuppplyer containsObject:dictItem])
    {
        
        cell.btnCheck.selected=FALSE;
    }else{
        cell.btnCheck.selected=TRUE;
    }
       
   }
  /*  else if ([mutArrSupplierList count] != 0)
    {
         NSLog(@"Favourite######>>>>>>%@",[[self->mutArrSupplierList objectAtIndex:indexPath.row]valueForKey:@"s_company_name"]);
                
              
            cell.lblSupplierName.text=[[self->mutArrSupplierList objectAtIndex:indexPath.row]valueForKey:@"s_company_name"];
                
           
        //    cell.lblSupplierName.text=[[mutArrSupplierList objectAtIndex:indexPath.row] objectForKey:@"s_company_name"];
            
            
            NSString *strImgName = [[self->mutArrSupplierList objectAtIndex:indexPath.row]valueForKey:@"s_logo"];
            NSURL *urlSupliermage = [NSURL URLWithString:strImgName];
            
            [cell.imgViewSupplierImage sd_setImageWithURL:urlSupliermage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                cell.imgViewSupplierImage.clipsToBounds = YES;
            }];
            
            
            //cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:urlSupliermage]];
            
            
        //    [cell.imageView.image sd_setImageWithURL:urlSupliermage
        //                    placeholderImage:nil
        //                             options:SDWebImageProgressiveDownload
        //                            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        //                            }
        //                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        //                               _imgCampaign.clipsToBounds = YES;
        //                           }];
            
            cell.btnCheck.tag=indexPath.row;
            [cell.btnCheck addTarget:self action:@selector(btnSelectSupplierClick:) forControlEvents:UIControlEventTouchUpInside];
            NSMutableDictionary *dictItem =[self->mutArrSupplierList objectAtIndex:indexPath.row];
            
            //self->favouriteSuppplyer mutArrSupplierList
            
            
            //mutArrFavSupplierList mutArrSelectedSupplier
            if ([self->mutArrSupplierList containsObject:dictItem])
            {
                
                cell.btnCheck.selected=FALSE;
            }else{
                cell.btnCheck.selected=TRUE;
            }
    }*/
    
    return cell;
}

#pragma mark - Button Event Click Methods
-(IBAction)btnSelectSupplierClick:(id)sender{
    UIButton *button = (UIButton *)sender;
    NSString *strTag=[NSString stringWithFormat:@"%ld",(long)[sender tag]];
    
   
    
    NSMutableDictionary *dictItem;
    
    dictItem =[mutArrSupplierList objectAtIndex:[sender tag]];
    
    //SuppilerCell *cell = [(SuppilerCell *)[[button superview] superview] superview];
    if(button.selected){
        button.selected=FALSE;
        [mutDicSelectedSupplier removeObjectForKey:strTag];
        [mutArrSelectedSupplier removeObject:dictItem];
    }else{
        [mutDicSelectedSupplier setValue:[[mutArrSupplierList objectAtIndex:[sender tag]] objectForKey:@"s_supplier_id"] forKey:strTag];
        [mutArrSelectedSupplier addObject:dictItem];
        
        button.selected=TRUE;
    }
    NSLog(@"%@",mutDicSelectedSupplier);
}

-(IBAction)btnNextClick:(id)sender{
    [self SaveSupplierWS];
}

- (IBAction)btnDoneClick:(id)sender {
   
    if ([_txtPromotionCode isFirstResponder]) {
        [_txtPromotionCode resignFirstResponder];
    }
}

#pragma mark - Webservice Methods
- (void)SupplierListWS{
 
    if([self NointerNetCheking])
    {
        
        
        
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetSupplierWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            mutArrSupplierList=    [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
            _tblSuppiler.hidden=FALSE;
            for(int i=0;i<[mutArrSupplierList count];i++){
                
                if([[[mutArrSupplierList objectAtIndex:i] objectForKey:@"is_fav"] isEqualToString:@"1"]){
                    
                     NSMutableDictionary *favouriteSupplyerDict;
                    
                    favouriteSupplyerDict = [[NSMutableDictionary alloc]init];
                    
                    [favouriteSupplyerDict setObject:[[mutArrSupplierList objectAtIndex:i] objectForKey:@"s_company_name"] forKey:@"CompayName"];
                    
                    [favouriteSupplyerDict setValue:[[mutArrSupplierList objectAtIndex:i] objectForKey:@"s_supplier_id"] forKey:@"s_supplier_id"];
                    
                    [favouriteSupplyerDict
                     setValue:[[mutArrSupplierList objectAtIndex:i] objectForKey:@"s_logo"] forKey:@"s_logo"];
                  //   setObject:[[mutArrSupplierList objectAtIndex:i] objectForKey:@"s_logo"] forKey:@"s_logo"];
                    
                    [favouriteSuppplyer addObject:favouriteSupplyerDict];
                   // NSLog(@"FAC DICT %@",favouriteSupplyerDict);
                    
                    
                    NSLog(@">>>>>>>>>>>%@",favouriteSupplyerDict);
                    
                    NSLog(@"Favourite>>>>>>%@",[[mutArrSupplierList objectAtIndex:i] objectForKey:@"s_company_name"]);
                    
                    [mutArrSelectedSupplier addObject:[mutArrSupplierList objectAtIndex:i]];
                    [mutDicSelectedSupplier setValue:[[mutArrSupplierList objectAtIndex:i] objectForKey:@"s_supplier_id"] forKey:[NSString stringWithFormat:@"%d",i]];
                    
                    
                }
               
                
               
                
            }
            
           //  NSLog(@"Fav Dic  Array %@",self->favouriteSuppplyer);
            
         //   NSLog(@"Getting values of dict %@",[[self->favouriteSuppplyer objectAtIndex:0]valueForKey:@"CompayName"]);
            
            //[mutDicSelectedSupplier setValue:[[mutArrSupplierList objectAtIndex:[sender tag]] objectForKey:@"s_supplier_id"] forKey:strTag];
            //[mutArrSelectedSupplier addObject:dictItem];
            
            [_tblSuppiler reloadData];
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
 }
}

- (void)CustomerFavSupplierWS{
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_FavSupplierWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            mutArrFavSupplierList=    [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
            
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        _btnNext.hidden=TRUE;
        _tblSuppiler.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        _lblNoInternet.text=[appDelegate getString:@"WEBSERVICEISSUE"];        
        NSLog(@"%@",error);
    }];
}

-(void)SaveSupplierWS{
    NSString *strSupplier;
    for(int i=0;i<[mutArrSupplierList count];i++){
        if([mutDicSelectedSupplier objectForKey:[NSString stringWithFormat:@"%i",i]]){
            if(strSupplier.length==0){
                strSupplier=[mutDicSelectedSupplier objectForKey:[NSString stringWithFormat:@"%i",i]];
                
                NSLog(@"save supplier >>>>>%@",strSupplier);
                
            }else{
                strSupplier=[NSString stringWithFormat:@"%@,%@",strSupplier,[mutDicSelectedSupplier objectForKey:[NSString stringWithFormat:@"%i",i]]];
                
                  NSLog(@"save supplier >>>>>%@",strSupplier);
            }
        }
    }
    
    if(strSupplier.length ==0)
    {
        strSupplier=@"0";
    }
    

        NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                      strSupplier,@"fav_supplier_ids",
                                      strPromotionCodeFlag,@"is_prom_code_valid",
                                      nil];
        [[Webservice sharedInstance] callWebserviceWithMethodName:customer_PersonalInfoWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            NSLog(@"responseData = %@", responseData);
            if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
                [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
                if([[AppDelegate sharedInstance].strFlag isEqualToString:@"1"]){
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"SecurityTab" object:nil];
                }
            }else{
                [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
            }
        } withFailureBlock:^(NSError *error) {
            NSLog(@"%@",error);
        }];

}

- (void)CustomerPramotionCodeWS{
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  _txtPromotionCode.text,@"promotion_code",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_PramotionCodeWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            _imgViewCheck.image=[UIImage imageNamed:@"ic-checkmark"];
            strPromotionCodeFlag=@"1";
        }else{
            _imgViewCheck.image=[UIImage imageNamed:@"ic-check"];
             strPromotionCodeFlag=@"0";
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}



#pragma mark- UITextField

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidChange:(UITextField *)theTextField{
    if(theTextField.text.length>=10){
        [self CustomerPramotionCodeWS];
    }
}


#pragma mark SetUpPage

-(void)SetUpPage{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ScanCompleted" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(btnNextClick:) name:@"EditData" object:nil];
    
    [_txtPromotionCode addTarget:self
                          action:@selector(textFieldDidChange:)
                forControlEvents:UIControlEventEditingChanged];
    
    toolBarTextView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    NSMutableArray *items = [[NSMutableArray alloc] init];
    UIBarButtonItem *aBarBtnFlexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *aBarBtnDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(btnDoneClick:)];
    aBarBtnDone.tintColor = [UIColor blackColor];
    [items addObject:aBarBtnFlexibleItem];
    [items addObject:aBarBtnDone];
    [toolBarTextView setItems:items animated:NO];
     _txtPromotionCode.inputAccessoryView = toolBarTextView;
}


-(BOOL)NointerNetCheking{
    
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        
        _btnNext.hidden=TRUE;
        _tblSuppiler.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        return FALSE;
    }else{
        _viewNoInternet.hidden=TRUE;
        
        return TRUE;
        
    }
}

- (IBAction)editSupplierBckBtnClk:(id)sender {
    
    [self.navigationController popViewControllerAnimated:TRUE];
}
@end
