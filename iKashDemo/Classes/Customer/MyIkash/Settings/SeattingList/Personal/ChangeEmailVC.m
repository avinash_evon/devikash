//
//  AddPromotionalCreditVC.m
//  iKashSupplier
//
//  Created by ind502 on 10/25/16.
//  Copyright © 2016 IndiaNIC. All rights reserved.
//

#import "ChangeEmailVC.h"

@interface ChangeEmailVC ()

@end

@implementation ChangeEmailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _txtCurrentEmail.enabled = YES;
    
  //  _txtCurrentEmail.text = _strEmailId;
    
    if(!IS_IPAD)
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(btnSaveClick:) name:@"EditData" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    if(!IS_IPAD)
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"EditData" object:nil];
}

- (IBAction)btnActionClickBack:(id)sender {
    if(_backButtonClicked){
        _backButtonClicked();
    }
    if(IS_IPAD){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Update Email WebService -
- (IBAction)btnSaveClick:(id)sender {
    
    if (
        ![_txtCurrentEmail.text isValidString] ||
        ![_txtNewEmail.text isValidString]
        ) {
        
        [appDelegate showAlertView:[appDelegate getString:@"Please enter your new Email address."] Tag:@"0"];
        return;
        
    } else if (
               ![_txtCurrentEmail.text isValidEmail] ||
               ![_txtNewEmail.text isValidEmail]
               ) {
        
        [appDelegate showAlertView:[appDelegate getString:@"INVALID_EMAIL_ADDRESS"] Tag:@"0"];
        return;
        
    }else if ( ![_txtPassword.text isValidString] ) {
        
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_PASSWORD"] Tag:@"0"];
        return;
    }
        
    else if ( [_txtCurrentEmail.text.trim isEqualToString:_txtNewEmail.text.trim]) {
        
        [appDelegate showAlertView:[appDelegate getString:@"Your new Email should not be like your current Email."] Tag:@"0"];
        return;
        
    }else if ( ![_txtConfirmNewEmail.text.trim isEqualToString:_txtNewEmail.text.trim]) {
        
        [appDelegate showAlertView:[appDelegate getString:@"Confirm email does not match with new email."] Tag:@"0"];
        return;
        
    } else {
        
        [self.view endEditing:YES];
    }
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  _txtNewEmail.text,@"new_email",
                                  _txtPassword.text,@"password",
                                  _txtCurrentEmail.text,@"old_email",
                                  nil];
    
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_changeEmailWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
            
            if(_changeEmailSucessCB){
                _changeEmailSucessCB(_txtNewEmail.text);
            }
            if(IS_IPAD){
                [self dismissViewControllerAnimated:YES completion:nil];
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        } else {
            
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
        
    } withFailureBlock:^(NSError *error) {
        [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedInstance].window animated:YES];
        [appDelegate showAlertView:[appDelegate getString:@"SOMETHINGWENTWRONG"] Tag:@"0"];
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)backBtnClk:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (IBAction)saveEmailBtnClk:(UIButton *)sender {
}

- (IBAction)cancelBtnClk:(UIButton *)sender {
    
    _txtCurrentEmail.text = @"";
    _txtNewEmail.text = @"";
    _txtConfirmNewEmail.text = @"";
}
@end
