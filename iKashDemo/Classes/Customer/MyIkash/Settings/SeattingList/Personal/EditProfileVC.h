//
//  EditProfileVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "customAutoCompleteTextField.h"
#import "MLPAutoCompleteTextField.h"
#import "MLPAutoCompleteTextFieldDataSource.h"
#import "MLPAutoCompleteTextFieldDelegate.h"

@interface EditProfileVC : UIViewController<UITextViewDelegate,UIPopoverControllerDelegate,MLPAutoCompleteTextFieldDelegate,MLPAutoCompleteTextFieldDataSource>{
    IBOutlet NSLayoutConstraint *scrllViewBottomConstraint;
    NSDateFormatter *dateFormat;
    UIToolbar *toolBarTextView;
}
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrllview;
@property (strong, nonatomic) IBOutlet UILabel *lblIam;
@property (strong, nonatomic) IBOutlet UIButton *btnMale;
@property (strong, nonatomic) IBOutlet UIButton *btnFemale;
@property (strong, nonatomic) IBOutlet UIButton *btnChangeEmail;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewMale;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewFemale;

@property (strong, nonatomic) IBOutlet UILabel *lblFirstName;
@property (strong, nonatomic) IBOutlet UILabel *lblLastName;
@property (strong, nonatomic) IBOutlet UILabel *lblDOB;
@property (strong, nonatomic) IBOutlet UILabel *lblPhoneNo;
@property (strong, nonatomic) IBOutlet UILabel *lblCountry;
@property (strong, nonatomic) IBOutlet UILabel *lblCity;
@property (strong, nonatomic) IBOutlet UILabel *lblZipCode;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;

@property (strong, nonatomic) IBOutlet UITextField *txtFieldFirstName;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldLastName;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldDOB;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldPhoneNo;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldCountry;

@property (strong, nonatomic) IBOutlet customAutoCompleteTextField *txtFieldCity;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *ConstHeightViewCity;


@property (strong, nonatomic) IBOutlet UITextField *txtFieldZipCode;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldHouseNo;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldExtention;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldEmail;
@property (strong, nonatomic) IBOutlet UITextView *txtViewAddress;

@property (weak, nonatomic) IBOutlet customAutoCompleteTextField *txtStreetAddress;


@property (strong, nonatomic) IBOutlet UIButton *btnEmail;

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;


@property (nonatomic,strong) NSString *strEditEmailShouldEnable;

@property (nonatomic, strong) UIPopoverController *changeEmailPopover;

- (IBAction)btnMaleClick:(id)sender;
- (IBAction)btnFemaleClick:(id)sender;
- (IBAction)btnChangeEmailClick:(id)sender;

- (IBAction)editPersonalInfo:(id)sender;


- (IBAction)cancel:(UIButton *)sender;

- (IBAction)back:(id)sender;

- (IBAction)BackAddress:(id)sender;

- (IBAction)cancelAddress:(id)sender;

- (IBAction)SaveAddress:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *editInfoBackBtnClk;


@end
