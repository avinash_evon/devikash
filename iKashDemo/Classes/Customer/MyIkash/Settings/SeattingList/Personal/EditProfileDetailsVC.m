//
//  EditProfileDetailsVC.m
//  iKash
//
//  Created by AVINASH on 03/01/21.
//  Copyright © 2021 indianic. All rights reserved.
//

#import "EditProfileDetailsVC.h"

@interface EditProfileDetailsVC ()
{
    
        UIDatePicker *datePicker;
         NSDate *date;
         UIToolbar *keyboardToolbar;
}

@end

@implementation EditProfileDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self datePicker];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BackBtnAction:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (IBAction)personalInfoSaveBtnClk:(UIButton *)sender {
}

- (IBAction)cancelBtnClk:(UIButton *)sender {
}
- (IBAction)maleBtnClk:(UIButton *)sender {
}

- (IBAction)femaleBtnClk:(UIButton *)sender {
}

-(void)datePicker
{
    if (keyboardToolbar == nil) {
           keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
           [keyboardToolbar setBarStyle:UIBarStyleBlackTranslucent];

           UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

           UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(closeKeyboard)];

           [keyboardToolbar setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
       }

       _DateOfBirthTxt.inputAccessoryView = keyboardToolbar;

       datePicker = [[UIDatePicker alloc] init];
       datePicker.datePickerMode = UIDatePickerModeDate;
       [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];

      _DateOfBirthTxt.inputView = datePicker;
}

- (void)datePickerValueChanged:(id)sender{

date = datePicker.date;

NSDateFormatter *df = [[NSDateFormatter alloc] init];
[df setDateFormat:@"dd/MM/YYYY"];


    [_DateOfBirthTxt setText:[df stringFromDate:date]];
}

-(void)closeKeyboard
{
    [self.view endEditing:TRUE];
}


@end
