//
//  addressEditVC.h
//  iKash
//
//  Created by AVINASH on 03/01/21.
//  Copyright © 2021 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface addressEditVC : UIViewController
- (IBAction)backBtnClk:(UIButton *)sender;
- (IBAction)saveAddressBtnClk:(UIButton *)sender;
- (IBAction)cancelBtnClk:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
