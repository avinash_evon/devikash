//
//  CategoryDetailsVC.m
//  iKash
//
//  Created by IndiaNIC on 03/12/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "CategoryDetailsVC.h"
#import "CategoryDetailsCell.h"

@interface CategoryDetailsVC (){
    NSMutableDictionary *mutDicSelectedSupplier;
    NSMutableArray *arrSelectedCategory;
    NSString *strSubBranch;
    int intFlagSelectAll;
}

@end

@implementation CategoryDetailsVC

#pragma mark - Synthesize
@synthesize mutArrSubCatList,strTitle,strBranchId;

#pragma mark - View lifecycle
- (void)viewDidLoad {
    
    [super viewDidLoad];
    mutDicSelectedSupplier = [[NSMutableDictionary alloc]init];
    arrSelectedCategory = [[NSMutableArray alloc]init];
    self.navigationController.navigationBarHidden = YES;
    _lblTitle.text=strTitle;
    _lblNoRecordFound.text = [appDelegate getString:@"NO_SUB_CATEGORY_FOUND_MESSAGE"];
    
    self.tblCategoryDetails.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    intFlagSelectAll=0;
    [self SetUpPage];
    if(!IS_IPAD)
    {
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        
        [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(!IS_IPAD)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}


#pragma mark- Notification Methods
- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:break;
    }
}



#pragma mark -
#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [mutArrSubCatList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CategoryDetailsCell *cell = (CategoryDetailsCell *) [tableView dequeueReusableCellWithIdentifier:@"CategoryDetailsCell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    cell.lblCategoryDetailsName.text=[[mutArrSubCatList objectAtIndex:indexPath.row] objectForKey:@"c_branch_name"];
    
    cell.btnCheck.tag=indexPath.row;
    [cell.btnCheck addTarget:self action:@selector(btnSelectSupplierClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnCheck.selected=FALSE;
    for(int i=0; i<[arrSelectedCategory count];i++){
        if([[arrSelectedCategory objectAtIndex:i] isEqualToString:[[mutArrSubCatList objectAtIndex:indexPath.row] objectForKey:@"c_branch_id"]]){
            cell.btnCheck.selected=TRUE;
            NSString *strTag=[NSString stringWithFormat:@"%li",(long)indexPath.row];
            [mutDicSelectedSupplier setValue:[[mutArrSubCatList objectAtIndex:indexPath.row] objectForKey:@"c_branch_id"] forKey:strTag];
            break;
        }
    }
    
    return cell;
}

#pragma mark- UIButton Click Action Methods

-(IBAction)btnDoneClick:(id)sender{
    if (mutArrSubCatList.count == 0) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    
    if ([mutDicSelectedSupplier count]>0) {
        
        for(int i=0;i<[mutArrSubCatList count];i++){
            if([mutDicSelectedSupplier objectForKey:[NSString stringWithFormat:@"%i",i]]){
                if(strSubBranch.length==0){
                    strSubBranch=[mutDicSelectedSupplier objectForKey:[NSString stringWithFormat:@"%i",i]];
                }else{
                    strSubBranch=[NSString stringWithFormat:@"%@,%@",strSubBranch,[mutDicSelectedSupplier objectForKey:[NSString stringWithFormat:@"%i",i]]];
                }
            }
        }
        NSLog(@"%@",strSubBranch);
        
        for(int i=0;i<[[AppDelegate sharedInstance].mutArrCategoryIDS count]>0;i++){
            if([[[[AppDelegate sharedInstance].mutArrCategoryIDS objectAtIndex:i] objectForKey:@"main_branch_ids"] isEqualToString:strBranchId]){
                [[AppDelegate sharedInstance].mutArrCategoryIDS removeObjectAtIndex:i];
            }
        }
        
        NSDictionary *aDict = [NSDictionary dictionaryWithObjectsAndKeys:
                               strBranchId,@"main_branch_ids",
                               strSubBranch,@"sub_branch_ids",
                               nil];
        [[AppDelegate sharedInstance].mutArrCategoryIDS addObject:aDict];
        
        
        [self SaveCategoryWS];
        if([[AppDelegate sharedInstance].mutArrCategoryIDS count]>0){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadPage" object:nil];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        for(int i=0;i<[[AppDelegate sharedInstance].mutArrCategoryIDS count]>0;i++){
            if([[[[AppDelegate sharedInstance].mutArrCategoryIDS objectAtIndex:i] objectForKey:@"main_branch_ids"] isEqualToString:strBranchId]){
                [[AppDelegate sharedInstance].mutArrCategoryIDS removeObjectAtIndex:i];
            }
        }
        [self SaveCategoryWS];
        
    }
    
    
}

-(IBAction)btnBackClick:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnSelectAllAction:(id)sender {
    NSLog(@"%@",mutArrSubCatList);
    [mutDicSelectedSupplier removeAllObjects];
    [arrSelectedCategory removeAllObjects];
    
    NSString *strSubAllCat;
    
    if(intFlagSelectAll==0){
        for(int i=0;i<[mutArrSubCatList count];i++){
            if(i==0){
                strSubAllCat=[[mutArrSubCatList objectAtIndex:i] objectForKey:@"c_branch_id"];
            }else{
                strSubAllCat=[NSString stringWithFormat:@"%@,%@",strSubAllCat,[[mutArrSubCatList objectAtIndex:i] objectForKey:@"c_branch_id"]];
            }
            
            NSMutableDictionary *DIC = [[NSMutableDictionary alloc] init];
            DIC = [[mutArrSubCatList objectAtIndex:i] mutableCopy];
            [DIC setObject:@"1" forKey:@"is_fav_sub"];
            [mutArrSubCatList replaceObjectAtIndex:i withObject:DIC];
        }
        intFlagSelectAll=1;
        NSArray *srrSuBranch=[strSubAllCat componentsSeparatedByString:@"," ];
        arrSelectedCategory=[[NSMutableArray alloc]initWithArray:srrSuBranch];
        [_btnSelectAll setTitle:[appDelegate getString:@"Deselect all"] forState:UIControlStateNormal];
        
    }else{
        intFlagSelectAll=0;
        [_btnSelectAll setTitle:[appDelegate getString:@"Select all"] forState:UIControlStateNormal];
    }
    NSLog(@"%@",mutDicSelectedSupplier);
    [_tblCategoryDetails reloadData];
}

-(IBAction)btnSelectSupplierClick:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    NSString *strTag=[NSString stringWithFormat:@"%ld",(long)[sender tag]];
    //SuppilerCell *cell = [(SuppilerCell *)[[button superview] superview] superview];
    if(button.selected){
        button.selected=FALSE;
        [mutDicSelectedSupplier removeObjectForKey:strTag];
    }else{
        [mutDicSelectedSupplier setValue:[[mutArrSubCatList objectAtIndex:[sender tag]] objectForKey:@"c_branch_id"] forKey:strTag];
        button.selected=TRUE;
    }
    if(mutArrSubCatList.count == mutDicSelectedSupplier.count){
        intFlagSelectAll=1;
        [_btnSelectAll setTitle:[appDelegate getString:@"Deselect all"] forState:UIControlStateNormal];
    }else{
        intFlagSelectAll=0;
        [_btnSelectAll setTitle:[appDelegate getString:@"Select all"] forState:UIControlStateNormal];
    }
    NSLog(@"%@",mutDicSelectedSupplier);
    
}
#pragma mark - Webservice Methods
-(void)SaveCategoryWS{
    NSString *strCategory;
    NSString *strSubCategory;
    for(int i=0;i<[[AppDelegate sharedInstance].mutArrCategoryIDS count];i++){
        if(i==0){
            strCategory=[[[AppDelegate sharedInstance].mutArrCategoryIDS objectAtIndex:i] objectForKey:@"main_branch_ids"];
            strSubCategory=[[[AppDelegate sharedInstance].mutArrCategoryIDS objectAtIndex:i] objectForKey:@"sub_branch_ids"];
        }else{
            strCategory=[NSString stringWithFormat:@"%@,%@",strCategory,[[[AppDelegate sharedInstance].mutArrCategoryIDS objectAtIndex:i] objectForKey:@"main_branch_ids"]];
            strSubCategory=[NSString stringWithFormat:@"%@,%@",strSubCategory,[[[AppDelegate sharedInstance].mutArrCategoryIDS objectAtIndex:i] objectForKey:@"sub_branch_ids"]];
        }
    }
    // strCategory = (strSubCategory.length>0)?strCategory:@"";
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  (strCategory.length>0)?strCategory:@"",@"main_branch_ids",
                                  (strSubCategory.length>0)?strSubCategory:@"",@"sub_branch_ids",
                                  nil];
    NSLog(@"%@",aDict);
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_PersonalInfoWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        //if([[AppDelegate sharedInstance].mutArrCategoryIDS count]>0){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadCategory" object:nil];
        // }
        
        //Update home list when user change fav category from setting
        [AppDelegate sharedInstance].IsFavCategoryUpdated = YES;
        
        [self dismissViewControllerAnimated:YES completion:nil];
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark SetUpPage

-(void)SetUpPage{
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"main_branch_ids==%@",strBranchId];
    NSLog(@"%@",[AppDelegate sharedInstance].mutArrCategoryIDS);
    NSArray * filteredarray  = [[AppDelegate sharedInstance].mutArrCategoryIDS filteredArrayUsingPredicate:predicate];
    
    if([filteredarray count]>0){
        NSArray *srrSuBranch=[[[filteredarray objectAtIndex:0] objectForKey:@"sub_branch_ids"] componentsSeparatedByString:@"," ];
        arrSelectedCategory=[[NSMutableArray alloc]initWithArray:srrSuBranch];
    }
    
    if (mutArrSubCatList.count == 0) {
        self.tblCategoryDetails.hidden = YES;
        self.lblNoRecordFound.hidden = NO;
    }
    if(mutArrSubCatList.count == arrSelectedCategory.count){
        intFlagSelectAll=1;
        [_btnSelectAll setTitle:[appDelegate getString:@"Deselect all"] forState:UIControlStateNormal];
    }else{
        intFlagSelectAll=0;
        [_btnSelectAll setTitle:[appDelegate getString:@"Select all"] forState:UIControlStateNormal];
    }
    
    [_btnDone setTitle:[appDelegate getString:@"Done"] forState:UIControlStateNormal];
}

@end
