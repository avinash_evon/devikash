//
//  CategoryVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "CategoryVC.h"
#import "SecurityVC.h"
#import "EditProfileVC.h"
#import "SupplierVC.h"
#import "CategoryCollectionCell.h"
#import "CategoryDetailsVC.h"
#import "CategoryCollectionReusableView.h"
@interface CategoryVC (){
    
    NSMutableArray *mutArrCategoryList;
    NSMutableArray *mutArrFavBranch;
    NSMutableDictionary *mutDicSelectedCategory;
    NSIndexPath *indexPathCell;
}

@end

@implementation CategoryVC

#pragma mark - Synthesize
@synthesize categoryDetailsPopover;

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ResetCategorySelection) name:@"ReloadPage" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CategoryListWS) name:@"ReloadCategory" object:nil];
    mutDicSelectedCategory=[[NSMutableDictionary alloc]init];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [AppDelegate sharedInstance].strMyikashSaveButton=@"2";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ScanCompleted" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SaveCategoryWS) name:@"EditData" object:nil];
    
    if([[AppDelegate sharedInstance].strFlag isEqualToString:@"0"]){
        _btnNext.hidden=TRUE;
        clnViewBottomConstraint.constant = -40;
        
    }
    [self CategoryListWS];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}


#pragma mark - Button Event Click Methods
-(void)ResetCategorySelection{
    
    self.view.alpha = 1.0;
    [_collectionViewCategory reloadData];
    NSLog(@"%@",[AppDelegate sharedInstance].mutArrCategoryIDS);
    CategoryCollectionCell *Cell = (CategoryCollectionCell *)[_collectionViewCategory cellForItemAtIndexPath:indexPathCell];
    Cell.imgViewSelection.image=[UIImage imageNamed:@"ic-rate-orange"];
    Cell.btnSelect.selected=TRUE;
}

-(IBAction)btnNextClick:(id)sender{
    [self SaveCategoryWS];
}

# pragma mark - CollectionView Delegate Method

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [mutArrCategoryList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"CategoryCollectionCell";
    CategoryCollectionCell *Cell = (CategoryCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    Cell.viewBackground.layer.cornerRadius=2.0;
    Cell.viewBackground.layer.borderColor = [appDelegate colorWithHexString:@"eeeeee"].CGColor;
    Cell.viewBackground.layer.borderWidth = 0.5f;
    
    Cell.viewBackground.clipsToBounds=YES;
    Cell.viewBackground.backgroundColor=[UIColor whiteColor];
    Cell.lblCategoryName.text=[[mutArrCategoryList objectAtIndex:indexPath.row] objectForKey:@"branch_name"];
    
    
    if([[[mutArrCategoryList objectAtIndex:indexPath.row] objectForKey:@"first_child"] count]==0){
        
        Cell.lblCategoryName.textColor=kAppSupportedColorDisableColor;
        
    }else{
        
        Cell.lblCategoryName.textColor=kAppSupportedColorLightColor;
        
        if([[[mutArrCategoryList objectAtIndex:indexPath.row] objectForKey:@"is_fav_main"] isEqualToString:@"1"]){
            Cell.imgViewSelection.image=[UIImage imageNamed:@"ic-rate-orange"];
            
        }else{
            Cell.imgViewSelection.image=[UIImage imageNamed:@"ic-rate-blank"];
            
        }
    }
    return Cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if([[[mutArrCategoryList objectAtIndex:indexPath.row] objectForKey:@"first_child"] count]>0){
        
        indexPathCell=indexPath;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        CategoryDetailsVC *objCategoryDetailsVC = [storyboard instantiateViewControllerWithIdentifier:@"CategoryDetailsVC"];
        if (IS_IPAD) {
            objCategoryDetailsVC.mutArrSubCatList=[[[mutArrCategoryList objectAtIndex:indexPath.row] objectForKey:@"first_child"] mutableCopy];
            objCategoryDetailsVC.strTitle=[[mutArrCategoryList objectAtIndex:indexPath.row] objectForKey:@"branch_name"];
            objCategoryDetailsVC.strBranchId=[[mutArrCategoryList objectAtIndex:indexPath.row] objectForKey:@"branch_id"];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objCategoryDetailsVC];
            self.categoryDetailsPopover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
            self.categoryDetailsPopover.delegate = self;
            self.categoryDetailsPopover.popoverContentSize = CGSizeMake(370.0, 600.0);
            self.view.alpha = 0.6;
            if ([self.categoryDetailsPopover respondsToSelector:@selector(backgroundColor)]) {
                self.categoryDetailsPopover.backgroundColor = [UIColor blackColor];
            }
            [self.categoryDetailsPopover presentPopoverFromRect:[_btnPopOver bounds]
                                                         inView:_btnPopOver
                                       permittedArrowDirections:0
                                                       animated:YES];
        }else{
            objCategoryDetailsVC.mutArrSubCatList=[[[mutArrCategoryList objectAtIndex:indexPath.row] objectForKey:@"first_child"] mutableCopy];
            objCategoryDetailsVC.strTitle=[[mutArrCategoryList objectAtIndex:indexPath.row] objectForKey:@"branch_name"];
            objCategoryDetailsVC.strBranchId=[[mutArrCategoryList objectAtIndex:indexPath.row] objectForKey:@"branch_id"];
            objCategoryDetailsVC.modalPresentationStyle = UIModalPresentationCustom;
            [self presentViewController:objCategoryDetailsVC animated:YES completion:nil];
        }
    }
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionFooter) {
        CategoryCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"CategoryCollectionReusableView" forIndexPath:indexPath];
        
        if([[AppDelegate sharedInstance].strFlag isEqualToString:@"0"]){
            
            headerView.btnNext.hidden=TRUE;
        }else{
            headerView.btnNext.hidden=FALSE;
        }
        
        reusableview = headerView;
    }
    return reusableview;
}

#pragma mark - UICollectionViewFlowLayout methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (IS_IPHONE) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.width;
        float gridColumn;
        float gridRow;
        
        UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
        if(UIInterfaceOrientationIsPortrait(orientation)){
            gridRow = 3.0;
            gridColumn = 4.0;
            float cellWidth = (screenWidth / gridRow)-12.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
            float cellHeight = (screenHeight / gridColumn)-12.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
            CGSize size = CGSizeMake(cellWidth, cellHeight);
            return size;
        }else{
            gridRow = 6.0;
            gridColumn = 7.0;
            float cellWidth = (screenWidth / gridRow)-10.5; //Replace the divisor with the column count requirement. Make sure to have it in float.
            float cellHeight = (screenHeight / gridColumn)-12.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
            CGSize size = CGSizeMake(cellWidth, cellHeight);
            return size;
        }
    }else{
        return CGSizeMake(94.f, 78.f);
    }
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(8.5, 8.5, 8.5, 8.5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 7;
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self.collectionViewCategory.collectionViewLayout invalidateLayout];
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    self.view.alpha = 1.0;
    return YES;
}


#pragma mark - Webservice Methods

-(void)SetUpSelectedCategory{
    NSString *strBranchId;
    NSString *strSubBranch;
    [[AppDelegate sharedInstance].mutArrCategoryIDS removeAllObjects];
    for(int i =0;i<[mutArrCategoryList count];i++){
        if([[[mutArrCategoryList objectAtIndex:i] objectForKey:@"is_fav_main"] isEqualToString:@"1"]){
            strSubBranch=@"";
            strBranchId=@"";
            strBranchId=[[mutArrCategoryList objectAtIndex:i] objectForKey:@"branch_id"];
            for(int j=0;j<[[[mutArrCategoryList objectAtIndex:i] objectForKey:@"first_child"] count]>0;j++){
                if([[[[[mutArrCategoryList objectAtIndex:i] objectForKey:@"first_child"] objectAtIndex:j] objectForKey:@"is_fav_sub"] isEqualToString:@"1"]){
                    if(strSubBranch.length==0){
                        strSubBranch=[[[[mutArrCategoryList objectAtIndex:i] objectForKey:@"first_child"] objectAtIndex:j] objectForKey:@"c_branch_id"];
                    }else{
                        strSubBranch=[NSString stringWithFormat:@"%@,%@",strSubBranch,[[[[mutArrCategoryList objectAtIndex:i] objectForKey:@"first_child"] objectAtIndex:j] objectForKey:@"c_branch_id"]];
                    }
                }
            }
            NSDictionary *aDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                   strBranchId,@"main_branch_ids",
                                   strSubBranch,@"sub_branch_ids",
                                   nil];
            [[AppDelegate sharedInstance].mutArrCategoryIDS addObject:aDict];
        }
    }
    NSLog(@"%@",[AppDelegate sharedInstance].mutArrCategoryIDS);
}




- (void)CategoryListWS{
    
    if([self NointerNetCheking])
    {
        
        NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      @"",@"cat_id",
                                      [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                      nil];
        [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCategoryWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            NSLog(@"responseData = %@", responseData);
            if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
                mutArrCategoryList = [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
                _collectionViewCategory.hidden=FALSE;
                [self SetUpSelectedCategory];
                [_collectionViewCategory reloadData];
            }else{
                [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
            }
        } withFailureBlock:^(NSError *error) {
            _btnNext.hidden=TRUE;
            _collectionViewCategory.hidden=TRUE;
            _viewNoInternet.hidden=FALSE;
            _lblNoInternet.text=[appDelegate getString:@"WEBSERVICEISSUE"];
            NSLog(@"%@",error);
        }];
    }
}

-(void)SaveCategoryWS{
    NSString *strCategory;
    NSString *strSubCategory;
    for(int i=0;i<[[AppDelegate sharedInstance].mutArrCategoryIDS count];i++){
        if(i==0){
            strCategory=[[[AppDelegate sharedInstance].mutArrCategoryIDS objectAtIndex:i] objectForKey:@"main_branch_ids"];
            strSubCategory=[[[AppDelegate sharedInstance].mutArrCategoryIDS objectAtIndex:i] objectForKey:@"sub_branch_ids"];
        }else{
            strCategory=[NSString stringWithFormat:@"%@,%@",strCategory,[[[AppDelegate sharedInstance].mutArrCategoryIDS objectAtIndex:i] objectForKey:@"main_branch_ids"]];
            strSubCategory=[NSString stringWithFormat:@"%@,%@",strSubCategory,[[[AppDelegate sharedInstance].mutArrCategoryIDS objectAtIndex:i] objectForKey:@"sub_branch_ids"]];
        }
    }
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  strCategory,@"main_branch_ids",
                                  strSubCategory,@"sub_branch_ids",
                                  nil];
    NSLog(@"%@",aDict);
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_PersonalInfoWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
            if([[AppDelegate sharedInstance].strFlag isEqualToString:@"1"]){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SupplierTab" object:nil];
            }
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)GetFavBranchesWS{
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_FavCategoryWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            mutArrFavBranch = [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"]];
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
-(BOOL)NointerNetCheking{
    
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        
        _btnNext.hidden=TRUE;
        _collectionViewCategory.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        return FALSE;
    }else{
        
        _viewNoInternet.hidden=TRUE;
        
        return TRUE;
        
    }
}
@end
