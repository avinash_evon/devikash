//
//  CategoryCollectionCell.h
//  iKash
//
//  Created by indianic on 19/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property(nonatomic,assign)IBOutlet UILabel *lblCategoryName;
@property(nonatomic,assign)IBOutlet UIImageView *imgViewCategory;
@property(nonatomic,assign)IBOutlet UIImageView *imgViewSelection;
@property(nonatomic,assign)IBOutlet UIButton *btnSelect;
@end
