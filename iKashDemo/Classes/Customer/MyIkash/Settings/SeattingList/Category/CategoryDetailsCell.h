//
//  CategoryDetailsCell.h
//  iKash
//
//  Created by IndiaNIC on 03/12/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryDetailsCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnCheck;

@property (strong, nonatomic) IBOutlet UILabel *lblCategoryDetailsName;
@end
