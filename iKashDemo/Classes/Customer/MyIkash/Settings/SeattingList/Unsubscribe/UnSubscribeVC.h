//
//  UnSubscribeVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnSubscribeVC : UIViewController

@property (assign, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrllView;
@property (assign, nonatomic) IBOutlet UITextField *txtFieldEmail;
@property (assign, nonatomic) IBOutlet UITextField *txtFieldPassword;
@property (assign, nonatomic) IBOutlet UITextField *txtFieldAnswer1;
@property (assign, nonatomic) IBOutlet UITextField *txtFieldAnswer2;
@property (assign, nonatomic) IBOutlet UITextField *txtFieldAnswer3;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle1;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle2;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblTitlePassword;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion1;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion2;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion3;
@end
