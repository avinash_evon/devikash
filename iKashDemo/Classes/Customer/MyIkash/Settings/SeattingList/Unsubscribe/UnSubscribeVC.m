//
//  UnSubscribeVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "UnSubscribeVC.h"
#import "LoginVC.h"

@interface UnSubscribeVC (){
    
    UIInterfaceOrientation orientationDevice;
}

@end

@implementation UnSubscribeVC

- (IBAction)UnsubscribeBtnBack:(UIButton *)sender {
    
    
      [self.navigationController popViewControllerAnimated:TRUE];
}




#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
       [self setNeedsStatusBarAppearanceUpdate];
    
    [_scrllView setContentSize:CGSizeMake(_scrllView.frame.size.width, 680.0)];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
         [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    
    [self SetUpPage];
    [AppDelegate sharedInstance].strMyikashSaveButton=@"1";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ScanCompleted" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneClick) name:@"EditData" object:nil];
    orientationDevice = [UIApplication sharedApplication].statusBarOrientation;
    if(!IS_IPAD)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark- Notification Methods
-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    orientationDevice=[UIApplication sharedApplication].statusBarOrientation;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    if(orientationDevice == UIInterfaceOrientationLandscapeLeft || orientationDevice == UIInterfaceOrientationLandscapeRight)
    {
        [UIView animateWithDuration:2.0 animations:^{
            [AppDelegate sharedInstance].WelCometopConstraint.constant=38;
        }];
    }
    NSLog(@"Hide");
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    if(orientationDevice == UIInterfaceOrientationLandscapeLeft || orientationDevice == UIInterfaceOrientationLandscapeRight)
    {
        [UIView animateWithDuration:2.0 animations:^{
            [AppDelegate sharedInstance].WelCometopConstraint.constant=0;
        }];
    }else{
        [AppDelegate sharedInstance].WelCometopConstraint.constant=64;
    }
    
    NSLog(@"Show");
}

#pragma mark- UIButton Click Action Methods

- (IBAction)unSubscribeBtnClk:(UIButton *)sender {

    [self  doneClick];

}


-(void)doneClick{
    if (![_txtFieldEmail.text isValidString]) {
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_EMAIL_ADDRESS"] Tag:@"0"];
        return;
    }else if (![_txtFieldPassword.text isValidString]) {
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_PASSWORD"] Tag:@"0"];
        return;
    }
    else if (![_txtFieldAnswer1.text isValidString]) {
        [appDelegate showAlertView:[appDelegate getString:@"QUESTION1"] Tag:@"0"];
        return;
    }
    else if (![_txtFieldAnswer2.text isValidString]) {
        [appDelegate showAlertView:[appDelegate getString:@"QUESTION2"] Tag:@"0"];
        return;
    }
    else if (![_txtFieldAnswer3.text isValidString]) {
        [appDelegate showAlertView:[appDelegate getString:@"QUESTION3"] Tag:@"0"];
        return;
    }
    
    [self UnsubsScribeWS];
}

#pragma mark- Web Service Methods

- (void)UnsubsScribeWS{
    NSDictionary *dic1=[NSDictionary dictionaryWithObjectsAndKeys:
                        @"1",@"feedback_id",
                        _txtFieldAnswer1.text,@"answer",
                        nil];
    NSDictionary *dic2=[NSDictionary dictionaryWithObjectsAndKeys:
                        @"2",@"feedback_id",
                        _txtFieldAnswer2.text,@"answer",
                        nil];
    NSDictionary *dic3=[NSDictionary dictionaryWithObjectsAndKeys:
                        @"3",@"feedback_id",
                        _txtFieldAnswer3.text,@"answer",
                        nil];
    
    NSMutableArray *mutArrAnswer=[[NSMutableArray alloc]initWithObjects:dic1,dic2,dic3, nil];
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                           [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                           _txtFieldEmail.text,@"email",
                           _txtFieldPassword.text,@"password",
                           mutArrAnswer,@"feedback_json",
                           nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_UnSubscribeWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:APPTITLE message:[[responseData objectForKey:@"settings"] objectForKey:@"message"] delegate:self cancelButtonTitle:[appDelegate getString:@"OK"] otherButtonTitles:nil, nil];
            [alertview show];
        }else{
          [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark AlertMethod

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    __block UINavigationController *aNv = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserLoginDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([[AppDelegate sharedInstance].window.rootViewController isKindOfClass:[MFSideMenuContainerViewController class]]) {
        LoginVC *objLoginVC = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        aNv = [[UINavigationController alloc] initWithRootViewController:objLoginVC];
        [AppDelegate sharedInstance].window.rootViewController = aNv;
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
        [AppDelegate sharedInstance].window.rootViewController = self.navigationController;
    }
}

#pragma mark SetUpPage

-(void)SetUpPage{
    
    _lblTitle1.text=[appDelegate getString:@"Unsubscribe your email address"];
    _lblTitle2.text=[appDelegate getString:@"We appreciate you fill-in the Survey below"];
    _lblTitleEmail.text=[appDelegate getStringWithStar:@"Email address"];
    _lblTitlePassword.text=[appDelegate getStringWithStar:@"Password"];
    
    _lblQuestion1.text=[appDelegate getStringWithStar:@"Why do you want to unsubscribe from the system?"];
    _lblQuestion2.text=[appDelegate getStringWithStar:@"Have the system added value for you?"];
    _lblQuestion3.text=[appDelegate getStringWithStar:@"What is your general opinion of the iKash Apps?"];
}

@end
