//
//  SettingsCell.h
//  iKash
//
//  Created by indianic on 21/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsCell : UITableViewCell
@property(nonatomic,assign)IBOutlet UILabel *lblTitle;
@end
