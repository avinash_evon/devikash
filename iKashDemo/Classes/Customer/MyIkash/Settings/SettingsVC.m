//
//  SettingsVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "SettingsVC.h"
#import "SettingsCell.h"
#import "EditProfileVC.h"
#import "ChangePasswordVC.h"
#import "CategoryVC.h"
#import "SupplierVC.h"
#import "UnSubscribeVC.h"
#import "NewsLetterEmailVC.h"
#import "SecurityVC.h"

@interface SettingsVC ()

@end

@implementation SettingsVC

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
   
    if (IS_IPAD) {
    
        NSIndexPath *initialIndex = [NSIndexPath indexPathForRow:0 inSection:0];
        
        [self.tblSettings selectRowAtIndexPath:initialIndex
                                    animated:YES
                              scrollPosition:UITableViewScrollPositionTop];
        
        [self tableView:self.tblSettings didSelectRowAtIndexPath:initialIndex];
        
    }

    [self.tblSettings setBackgroundColor:[UIColor colorWithHexString:@"#EFEEEA"]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
   
}
#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    SettingsCell *cell = (SettingsCell *) [tableView dequeueReusableCellWithIdentifier:@"SettingsCell"];
    
    if(indexPath.row==0){
        cell.lblTitle.text=[appDelegate getString:@"Personal"];
    }else if(indexPath.row==1){
        cell.lblTitle.text=[appDelegate getString:@"Security"];
    }else if(indexPath.row==2){
        cell.lblTitle.text=[appDelegate getString:@"Category"];
    }else if(indexPath.row==3){
        cell.lblTitle.text=[appDelegate getString:@"Supplier"];
    }else if(indexPath.row==4){
    cell.lblTitle.text=[appDelegate getString:@"Newsletter and Email"];
    }else{
        cell.lblTitle.text=[appDelegate getString:@"Unsubscribe"];
    }
    
    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    
    [AppDelegate sharedInstance].strFlag=@"0";
    
    if (IS_IPAD) {
    
        UIViewController *vc = [self.childViewControllers lastObject];
        
        if([vc class]!=[UIViewController class]){
            [vc.view removeFromSuperview];
            [vc removeFromParentViewController];
        }
    }

    if(indexPath.row==0){
    
        EditProfileVC *objEditProfileVC = [aStoryboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SettingsPersonalClick" object:nil];
        
        if (IS_IPAD) {
        
            objEditProfileVC.view.frame = self.containarView.bounds;
            [self.containarView addSubview:objEditProfileVC.view];
            [self addChildViewController:objEditProfileVC];
            [objEditProfileVC didMoveToParentViewController:self];
            
        }else{
            
          //  [self.navigationController pushViewController:objEditProfileVC animated:YES];
            
            [[[[self parentViewController] parentViewController]parentViewController ].navigationController pushViewController:objEditProfileVC animated:TRUE];
            
        }
    }else if(indexPath.row==1){

    SecurityVC *objSecurityVC = [aStoryboard instantiateViewControllerWithIdentifier:@"SecurityVC"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SettingsSecurityClick" object:nil];
        
        if (IS_IPAD) {
        
            objSecurityVC.view.frame = self.containarView.bounds;
            
            [self.containarView addSubview:objSecurityVC.view];
            
            [self addChildViewController:objSecurityVC];
            
            [objSecurityVC didMoveToParentViewController:self];
            
        }else{
            
          //  [self.navigationController pushViewController:objChangePasswordVC animated:YES];
             [[[[self parentViewController] parentViewController]parentViewController ].navigationController pushViewController:objSecurityVC animated:TRUE];
            
        }
    }else if(indexPath.row==2){

        CategoryVC *objCategoryVC = [aStoryboard instantiateViewControllerWithIdentifier:@"CategoryVC"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SettingsCategoryClick" object:nil];
        
        if (IS_IPAD) {
        
            objCategoryVC.view.frame = self.containarView.bounds;
            
            [self.containarView addSubview:objCategoryVC.view];
            
            [self addChildViewController:objCategoryVC];
            
            [objCategoryVC didMoveToParentViewController:self];
            
        }else{
          //  [self.navigationController pushViewController:objCategoryVC animated:YES];
            
              [[[[self parentViewController] parentViewController]parentViewController ].navigationController pushViewController:objCategoryVC animated:TRUE];
        }
    }else if(indexPath.row==3){

        SupplierVC *objSupplierVC = [aStoryboard instantiateViewControllerWithIdentifier:@"SupplierVC"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SettingsSupplierClick" object:nil];
        
        if (IS_IPAD) {
        
            objSupplierVC.view.frame = self.containarView.bounds;
            [self.containarView addSubview:objSupplierVC.view];
            [self addChildViewController:objSupplierVC];
            [objSupplierVC didMoveToParentViewController:self];
            
        }else{
            
           // [self.navigationController pushViewController:objSupplierVC animated:YES];
            
              [[[[self parentViewController] parentViewController]parentViewController ].navigationController pushViewController:objSupplierVC animated:TRUE];
        }
    }
    
    else if(indexPath.row==4){

    NewsLetterEmailVC *objNewsLetterEmailVC = [aStoryboard instantiateViewControllerWithIdentifier:@"NewsLetterEmailVC"];
    
  //  [[NSNotificationCenter defaultCenter] postNotificationName:@"SettingsSupplierClick" object:nil];
    
    if (IS_IPAD) {
    
        objNewsLetterEmailVC.view.frame = self.containarView.bounds;
        [self.containarView addSubview:objNewsLetterEmailVC.view];
        [self addChildViewController:objNewsLetterEmailVC];
        [objNewsLetterEmailVC didMoveToParentViewController:self];
        
    }else{
        
      //  [self.navigationController pushViewController:objSupplierVC animated:YES];
        
            [[[[self parentViewController] parentViewController]parentViewController ].navigationController pushViewController:objNewsLetterEmailVC animated:TRUE];
    }
    }
    else{

        UnSubscribeVC *objUnSubscribeVC = [aStoryboard instantiateViewControllerWithIdentifier:@"UnSubscribeVC"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SettingsUnsubscribeClick" object:nil];
        
        if (IS_IPAD) {
        
            objUnSubscribeVC.view.frame = self.containarView.bounds;
            [self.containarView addSubview:objUnSubscribeVC.view];
            [self addChildViewController:objUnSubscribeVC];
            [objUnSubscribeVC didMoveToParentViewController:self];
            
        }else{
            
          //  [self.navigationController pushViewController:objUnSubscribeVC animated:YES];
            
              [[[[self parentViewController] parentViewController]parentViewController ].navigationController pushViewController:objUnSubscribeVC animated:TRUE];
            
        }
    }
}





@end
