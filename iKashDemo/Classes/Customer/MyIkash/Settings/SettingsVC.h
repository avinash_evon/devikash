//
//  SettingsVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblSettings;
@property (strong, nonatomic) IBOutlet UIView *containarView;

@end
