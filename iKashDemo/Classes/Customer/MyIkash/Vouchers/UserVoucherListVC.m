//
//  UserVoucherListVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "UserVoucherListVC.h"
#import "VoucherCell.h"
#import "VoucherVC.h"
#import "VoucherCollectionViewCell.h"
#import "CampaignDetailsVC.h"
#import "AppDelegate.h"
//#import "SettingContainer.h"

static NSString *kCollectionViewIdentifier = @"VoucherCollectionViewCell";

@interface UserVoucherListVC (){
    
   // @property (nonatomic, strong) Class1 *class1;
  
    NSMutableArray *mutArrVoucher;
    NSString *strCampaignID;
    NSString *strVoucherId;
    NSIndexPath *indexPathTable;
}

@end

#pragma mark - View lifecycle

@implementation UserVoucherListVC

- (void)viewDidLoad {
    [super viewDidLoad];

}

-(void)viewWillAppear:(BOOL)animated{
     [super viewWillAppear:animated];

    _lblNoRecordFound.text=[appDelegate getString:@"NO_RECORD_FOUND_MESSAGE"];
    
    self.view.alpha = 1.0;
    
    _userVoucherClnView.hidden=TRUE;
    
    [self VoucherCampaignListWS];
    
    
 //   NSLog(@"Voucher count %d",mutArrVoucher.count);
    
  
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - UICollectionViewDataSource methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return mutArrVoucher.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    VoucherCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewIdentifier forIndexPath:indexPath];
    
    cell.viewBackground.layer.cornerRadius=5.0;
    
    cell.viewBackground.layer.borderColor = [appDelegate colorWithHexString:@"eeeeee"].CGColor;
    
    cell.viewBackground.layer.borderWidth = 0.5f;
    
    cell.imgCamapign.layer.cornerRadius= 0.0;
    
//    int aintStar=[[[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"averge_rating"] intValue];
    double aintStar=[[[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"averge_rating"] doubleValue];

    NSString *strColor=[[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"b_colour_code"];
    
    if(strColor.length >0){
        strColor = [strColor substringFromIndex:1];
    }else{
        strColor=@"000000";
    }
    
    cell.viewRating.starSize=CGSizeMake(9, 9);
    cell.viewRating.numberOfStars=5;
    cell.viewRating.rating=aintStar;
    cell.viewRating.padding=5.0;
    cell.viewRating.fillColor=[appDelegate colorWithHexString:strColor];
    
    cell.viewRating.unfilledColor=[UIColor clearColor];
    cell.viewRating.strokeColor=[appDelegate colorWithHexString:strColor];
    
    cell.lblCampaignTitle.text=[[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"c1_title"];
    cell.lblDescription.text= [appDelegate stringByStrippingHTML:[[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"c1_description"]];

        cell.lblVoucherNumber.text=[[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"camp_uniq_code"];
    cell.lblUniqueNumber.text=[[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"dv_voucher_unique_no"];
    
    NSString *aStrImgUrl = [[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"c1_logo"];
    NSURL *aUrlImg = [NSURL URLWithString:aStrImgUrl];
    [cell.imgCamapign sd_setImageWithURL:aUrlImg
                        placeholderImage:nil
                                 options:SDWebImageProgressiveDownload
                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                    [cell.activityIndicator startAnimating];
                                }
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   [cell.activityIndicator stopAnimating];
                                   cell.imgCamapign.clipsToBounds = YES;
                               }];
    
//    cell.btnDate.titleLabel.text = [[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"c1_end_date"];
    [cell.btnDate setTitle:[[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"c1_end_date"] forState:UIControlStateNormal];
    cell.btnDelete.tag=indexPath.row;
    
    [cell.btnDelete addTarget:self action:@selector(btnDeleteCampaignClick:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnView.tag=indexPath.row;
    
    [cell.btnView addTarget:self action:@selector(btnViewCampaignClick:) forControlEvents:UIControlEventTouchUpInside];

    if([[[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"dv_status"] isEqualToString:@"Redeemed"]){
       
        [cell.btnStatus setTitle:[appDelegate getString:@"Voucher is already redeemed"] forState:UIControlStateNormal];
        
    }else if([[[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"dv_status"] isEqualToString:@"Downloaded"]){
        
        [cell.btnStatus setTitle:[appDelegate getString:@"Display Voucher"] forState:UIControlStateNormal];
        
    }else{
        
        [cell.btnStatus setTitle:[appDelegate getString:@"Redeem"] forState:UIControlStateNormal];
        
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"indexPath.row = %ld",(long)indexPath.row);
    
    if (IS_IPAD) {
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    
        CampaignDetailsVC *objCampaignDetailsVC = [aStoryboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
        
        objCampaignDetailsVC.strCampaignId =  [[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"c1_campaign_id"];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objCampaignDetailsVC];
        
        navigationController.navigationBar.hidden = YES;
        
        self.campaignDetailsPopover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
        self.campaignDetailsPopover.delegate = self;
        self.campaignDetailsPopover.popoverContentSize = CGSizeMake(370.0, 645.0);
        self.view.alpha = 0.6;
       
        if ([self.campaignDetailsPopover respondsToSelector:@selector(backgroundColor)]) {
            self.campaignDetailsPopover.backgroundColor = [UIColor blackColor];
        }
        
        [self.campaignDetailsPopover presentPopoverFromRect:[_btnPopupView bounds]
                                                     inView:_btnPopupView
                                   permittedArrowDirections:0
                                                   animated:YES];
    }else{
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        
        CampaignDetailsVC *objCampaignDetailsVC = [storyboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
        
        objCampaignDetailsVC.isFromFavourites = @"Yes";
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objCampaignDetailsVC];
        
        objCampaignDetailsVC.strCampaignId = [[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"c1_campaign_id"];
        
        objCampaignDetailsVC.modalPresentationStyle = UIModalPresentationCustom;
        
        [self presentViewController:navigationController animated:YES completion:nil];
    }
}

#pragma mark - UICollectionViewFlowLayout methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (IS_IPAD) {
        return CGSizeMake(collectionView.frame.size.width/2-20, 160.f);
    }else{
        return CGSizeMake(collectionView.frame.size.width, 190.f);//160
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(15, 15, 10, 15);
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [self.userVoucherClnView.collectionViewLayout invalidateLayout];
}

#pragma mark - Button Event Click Methods
-(IBAction)btnViewCampaignClick:(id)sender{
    
    
    UIButton *btnVoucher=(UIButton *)sender;
    
    int intId=btnVoucher.tag;
    
    if (IS_IPAD) {
    
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        
        VoucherVC *objVoucherVC = [storyboard instantiateViewControllerWithIdentifier:@"VoucherVC"];
        
        objVoucherVC.strCampaignId=[[mutArrVoucher objectAtIndex:[sender tag]] objectForKey:@"c1_campaign_id"];
        
        self.userVoucherPopover = [[UIPopoverController alloc] initWithContentViewController:objVoucherVC];
        
        self.userVoucherPopover.delegate = self;
        
        self.userVoucherPopover.popoverContentSize = CGSizeMake(370.0, 600.0);
        
        self.view.alpha = 0.6;
        
        if ([self.userVoucherPopover respondsToSelector:@selector(backgroundColor)]) {
        
            self.userVoucherPopover.backgroundColor = [UIColor blackColor];
            
        }

        [self.userVoucherPopover presentPopoverFromRect:[_btnPopupView bounds]
                                                       inView:_btnPopupView
                                     permittedArrowDirections:0
                                                     animated:YES];
    }else{
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        
        VoucherVC *objVoucherVC = [storyboard instantiateViewControllerWithIdentifier:@"VoucherVC"];
        
        objVoucherVC.strCampaignId=[[mutArrVoucher objectAtIndex:intId] objectForKey:@"c1_campaign_id"];

        objVoucherVC.modalPresentationStyle = UIModalPresentationCustom;
        
        [self presentViewController:objVoucherVC animated:YES completion:nil];
        
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{

    self.view.alpha = 1.0;
    
    self.userVoucherPopover.delegate = nil;
    
    self.campaignDetailsPopover.delegate = nil;
    
    return YES;
}




-(IBAction)btnDeleteCampaignClick:(id)sender{
    
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:APPTITLE message:[appDelegate getString:@"DELETEVOUCHER"] delegate:self cancelButtonTitle:[appDelegate getString:@"No"] otherButtonTitles:[appDelegate getString:@"Yes"], nil];
    alertView.tag=200;
    [alertView show];
    
    strVoucherId=[[mutArrVoucher objectAtIndex:[sender tag]] objectForKey:@"downloaded_voucher_id"];
   
    indexPathTable = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    
}
#pragma mark- Web Service Methods
- (void)VoucherCampaignListWS{
   
  if([self NointerNetCheking])
  {
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetdownalodedVoucherWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
    
        NSLog(@"responseData = %@", responseData);
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
        
            _userVoucherClnView.hidden=FALSE;
            
            [mutArrVoucher removeAllObjects];
            
            mutArrVoucher = [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
            
            [_userVoucherClnView reloadData];
            
            if (mutArrVoucher.count == 0) {
                
                _lblNoRecordFound.hidden = NO;
            }
        }else{
            
            _userVoucherClnView.hidden = YES;
        
            _lblNoRecordFound.hidden=FALSE;

        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
  }
}

- (void)DeleteVoucherWS:(NSString *)strVoucher_Id indexPath:(NSIndexPath *)strIndex{

    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  strVoucher_Id,@"voucher_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_deleteVoucherWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
    
        NSLog(@"responseData = %@", responseData);
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            [self VoucherCampaignListWS];

        }else{

            _userVoucherClnView.hidden = YES;
            
            _lblNoRecordFound.hidden=FALSE;

        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark Alert Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag==200){
        if(buttonIndex==1){
            [self DeleteVoucherWS:strVoucherId indexPath:indexPathTable];
        }
    }
}
-(BOOL)NointerNetCheking{
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        
        
        _userVoucherClnView.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        return FALSE;
    }else{
        _userVoucherClnView.hidden=FALSE;
        _viewNoInternet.hidden=TRUE;
        return TRUE;
        
    }
}

@end
