//
//  VoucherCollectionViewCell.h
//  iKash
//
//  Created by IndiaNIC on 11/12/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DJWStarRatingView.h"

@interface VoucherCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imgCamapign;
@property (weak, nonatomic) IBOutlet UILabel *lblCampaignTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet DJWStarRatingView *viewRating;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UILabel *lblVoucherNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblUniqueNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnDate;
@property (weak, nonatomic) IBOutlet UIButton *btnView;
@property (weak, nonatomic) IBOutlet UIButton *btnStatus;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
