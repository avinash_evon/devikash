//
//  NotificationVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "NotificationVC.h"
#import "NotificationCell.h"

@interface NotificationVC (){
    NSMutableArray *mutArrrNotification;
}

@end

@implementation NotificationVC

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (IS_IPHONE) {
        _lblNavTitle.text = [appDelegate getString:@"Notifications"];
    }
    [self NotificationWS];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [AppDelegate sharedInstance].strGotNotification=@"0";
    
    if(!IS_IPAD){
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark- Notification Methods

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

# pragma mark - Tableview Delegate Method

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [mutArrrNotification count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NotificationCell *notificationCell = (NotificationCell *)[tableView dequeueReusableCellWithIdentifier:@"NotificationCell"];
    [notificationCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [notificationCell.lblNotificationText sizeToFit];
    notificationCell.lblNotificationText.numberOfLines=0;
    notificationCell.lblNotificationText.text=[[mutArrrNotification objectAtIndex:indexPath.row] objectForKey:@"push_message"];
    return notificationCell;
}

#pragma mark - Button Event Click Methods

- (IBAction)btnActionToBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- Notification Methods

- (void)NotificationWS{
    if([self NointerNetCheking]){
        NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                      nil];
        NSLog(@"customer_NotificationWS parameters = %@",aDict);
        
        [[Webservice sharedInstance] callWebserviceWithMethodName:customer_NotificationWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            
            NSLog(@"customer_NotificationWS responseData = %@", responseData);
            
            if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
                mutArrrNotification = [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
                [_tblViewNotification reloadData];
                if (mutArrrNotification.count == 0) {
                    _tblViewNotification.hidden = YES;
                    _lblNoRecordFound.hidden = NO;
                }
            }else{
                _tblViewNotification.hidden = YES;
                _lblNoRecordFound.hidden = NO;
                _lblNoRecordFound.text=[appDelegate getString:@"NO_NOTIFICATION_FOUND_MESSAGE"];
            }
            
            //Change notification count
            [AppDelegate sharedInstance].NotificationCount = 0;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GotNotification" object:nil];
            
        } withFailureBlock:^(NSError *error) {
            _tblViewNotification.hidden=TRUE;
            _viewNoInternet.hidden=FALSE;
            _lblNoInternet.text=[appDelegate getString:@"WEBSERVICEISSUE"];
            NSLog(@"%@",error);
        }];
    }
}

-(BOOL)NointerNetCheking{
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        _tblViewNotification.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        return FALSE;
    }else{
        _tblViewNotification.hidden=FALSE;
        _viewNoInternet.hidden=TRUE;
        return TRUE;
    }
}

@end
