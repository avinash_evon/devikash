//
//  NotificationCell.h
//  iKashDemo
//
//  Created by IndiaNIC on 05/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgViewStar;
@property (strong, nonatomic) IBOutlet UILabel *lblNotificationText;

@end
