//
//  SideMenuCellTableViewCell.h
//  Vanished
//
//  Created by IndiaNIC on 11/09/15.
//  Copyright (c) 2015 IndiaNIC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgViewMenuIcon;

@property (strong, nonatomic) IBOutlet UILabel *lblMenuName;
@property (strong, nonatomic) IBOutlet UILabel *lblSubMenuName;
@property(strong,nonatomic)IBOutlet UIButton    *btnSubSlection;
@property(strong,nonatomic)IBOutlet UIButton    *btnMenuSlection;
@end
