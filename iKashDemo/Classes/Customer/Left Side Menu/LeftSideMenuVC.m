//
//  LeftSideMenuVC.m
//  Vanished
//
//  Created by IndiaNIC on 10/09/15.
//  Copyright (c) 2015 IndiaNIC. All rights reserved.
//

#import "LeftSideMenuVC.h"
#import "SideMenuCell.h"
#import "ViewController.h"

@interface LeftSideMenuVC (){
    BOOL CallSpanOpne;
}

@end

@implementation LeftSideMenuVC

#pragma mark - Synthesize

@synthesize tblViewSideMenu, dataModelArray;

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CallSpanOpne=FALSE;
    [self GetCategoriesWS];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SetUpPage) name:@"DisplayNameInSidePenal" object:nil];
    
    [self SetUpPage];
    
    NSError *er_1;
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:self.arForTable options:NSJSONWritingPrettyPrinted error:&er_1];
    NSError *er_2;
    self.arForTable = [NSJSONSerialization JSONObjectWithData:dataFromDict options:NSJSONReadingMutableContainers error:&er_2];
    for (NSMutableDictionary *mutDict in self.arForTable){
        [mutDict setObject:@"0" forKey:@"parent_open"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource methods

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arForTable count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"SideMenuCell";
    SideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SideMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    // cell.btnMenuSlection.hidden = TRUE;
    // cell.btnSubSlection.hidden = TRUE;
    NSLog(@"indexPath.row = %ld",(long)indexPath.row);
    if([[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"CallSpan"] isEqualToString:@"CallSpan"]){
        [cell.btnMenuSlection setImage:nil forState:UIControlStateNormal];
        cell.btnSubSlection.tag = indexPath.row;
        cell.lblMenuName.text=[NSString stringWithFormat:@"    %@",[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"c_branch_name"]];
        
        int intCampaign=[[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"num_of_camp"] intValue];
        if (intCampaign == 0){
            cell.lblMenuName.textColor=kAppSupportedColorDisableColor;
            [cell.btnSubSlection setImage:[UIImage imageNamed:@"ic-check"] forState:UIControlStateNormal];
            cell.btnSubSlection.userInteractionEnabled = FALSE;
        }else{
            if ([[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"isSelected"] isEqualToString:@"1"]){
                [cell.btnSubSlection setImage:[UIImage imageNamed:@"ic-dwr-check"] forState:UIControlStateNormal];
                cell.lblMenuName.textColor = kAppSupportedColorThemeColor;
            }else{
                [cell.btnSubSlection setImage:[UIImage imageNamed:@"ic-check"] forState:UIControlStateNormal];
                cell.lblMenuName.textColor=kAppSupportedColorNormalColor;
            }
            
            cell.btnSubSlection.hidden = FALSE;
            
            [cell.btnSubSlection addTarget:self action:@selector(btnSelectSubMenuClick:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnSubSlection.userInteractionEnabled = TRUE;
        }
        [cell setIndentationLevel:1];
        
    }else{
        if([[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"parent_open"] isEqualToString:@"0"]){
            [cell.btnMenuSlection setImage:[UIImage imageNamed:@"ic-dwr-plus"] forState:UIControlStateNormal];
        }else{
            [cell.btnMenuSlection setImage:[UIImage imageNamed:@"ic-dwr-minus"] forState:UIControlStateNormal];
        }
        cell.btnMenuSlection.hidden = FALSE;
        cell.btnSubSlection.hidden = TRUE;
        cell.lblMenuName.text=[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"branch_name"];
        if([[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"first_child"] count]==0){
            cell.lblMenuName.textColor=kAppSupportedColorDisableColor;
        }else{
            cell.lblMenuName.textColor=kAppSupportedColorLightColor;
        }
        [cell setIndentationLevel:0];
    }
    
    //    if (cell.btnSubSlection.selected == YES) {
    //        cell.lblMenuName.textColor = kAppSupportedColorThemeColor;
    //    }else{
    //        cell.lblMenuName.textColor = kAppSupportedColorNormalColor;
    //    }
    //
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    //cell.backgroundColor=[UIColor blackColor];
    //  cell.btnMenuSlection.backgroundColor=[UIColor redColor];
    return cell;
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"%@",[AppDelegate sharedInstance].mutArrSidePenalCategoryList);
    NSMutableDictionary *d = [self.arForTable objectAtIndex:indexPath.row];
    if([[d valueForKey:@"first_child"] count]>0) {
        NSLog(@"%@",[self.arForTable objectAtIndex:indexPath.row]);
        SideMenuCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if([[[self.arForTable objectAtIndex:indexPath.row] valueForKey:@"parent_open"] isEqualToString:@"1"]){
            [[self.arForTable objectAtIndex:indexPath.row] setObject:@"0" forKey:@"parent_open"];
            [cell.btnMenuSlection setImage:[UIImage imageNamed:@"ic-dwr-plus"] forState:UIControlStateNormal];
            [self miniMizeThisRows:[d valueForKey:@"first_child"]];
        }else{
            [[self.arForTable objectAtIndex:indexPath.row] setObject:@"1" forKey:@"parent_open"];
            [cell.btnMenuSlection setImage:[UIImage imageNamed:@"ic-dwr-minus"] forState:UIControlStateNormal];
            
            NSUInteger count = indexPath.row+1;
            NSMutableArray *arCells=[NSMutableArray array];
            for(NSDictionary *dInner in [d valueForKey:@"first_child"] ) {
                NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] initWithDictionary:dInner];
                [mutDict setObject:@"CallSpan" forKey:@"CallSpan"];
                [mutDict setObject:@"0" forKey:@"isSelected"];
                [arCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [self.arForTable insertObject:mutDict atIndex:count++];
            }
            [tableView beginUpdates];
            [tableView insertRowsAtIndexPaths:arCells withRowAnimation:UITableViewRowAnimationNone];
            [tableView endUpdates];
        }
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        
    }
    //    else{
    //        if([d valueForKey:@"first_child"]){
    //            [appDelegate showAlertView:@"No sub-category found" Tag:@"0"];
    //        }
    //    }
}

#pragma mark- UIButton Click Action Methods

-(IBAction)btnSelectSubMenuClick:(id)sender{
    UIButton *button = (UIButton *)sender;
    NSIndexPath *indedsd=[NSIndexPath indexPathForRow:[sender tag] inSection:0];
    SideMenuCell *cell = [self.tblForCollapse cellForRowAtIndexPath:indedsd];
    NSString *strTag= [[self.arForTable objectAtIndex:[sender tag]] valueForKey:@"c_branch_id"];
    if(button.selected){
        cell.lblMenuName.textColor = kAppSupportedColorNormalColor;
        [button setImage:nil forState:UIControlStateNormal];
        [button setTitleColor:[appDelegate colorWithHexString:@"666666"] forState:UIControlStateNormal];
        button.selected=FALSE;
        for(int i=0;i<[[AppDelegate sharedInstance].mutArrSidePenalCategoryList count];i++){
            if([strTag isEqualToString:[[AppDelegate sharedInstance].mutArrSidePenalCategoryList objectAtIndex:i]]){
                [[AppDelegate sharedInstance].mutArrSidePenalCategoryList removeObjectAtIndex:i];
                [[AppDelegate sharedInstance].mutArrSidePenalCategoryName removeObjectAtIndex:i];
                //break;
            }
        }
        [[self.arForTable objectAtIndex:[sender tag]] setObject:@"0" forKey:@"isSelected"];
        
    }else{
        for(int i=0;i<[[AppDelegate sharedInstance].mutArrSidePenalCategoryList count];i++){
            if([strTag isEqualToString:[[AppDelegate sharedInstance].mutArrSidePenalCategoryList objectAtIndex:i]]){
                [[AppDelegate sharedInstance].mutArrSidePenalCategoryList removeObjectAtIndex:i];
                [[AppDelegate sharedInstance].mutArrSidePenalCategoryName removeObjectAtIndex:i];
                //break;
            }
        }
        
        //To solve random crash
        if(![[self.arForTable objectAtIndex:[button tag]] valueForKey:@"c_branch_id"])
            return;
        
        cell.lblMenuName.textColor = kAppSupportedColorThemeColor;
        [button setImage:[UIImage imageNamed:@"ic-dwr-check"] forState:UIControlStateNormal];
        [button setTitleColor:[appDelegate colorWithHexString:@"fe7935"] forState:UIControlStateNormal];
        [[AppDelegate sharedInstance].mutArrSidePenalCategoryList addObject:[[self.arForTable objectAtIndex:[button tag]] valueForKey:@"c_branch_id"]] ;
        [[AppDelegate sharedInstance].mutArrSidePenalCategoryName addObject:[[self.arForTable objectAtIndex:[button tag]] valueForKey:@"c_branch_name"]] ;
        button.selected=TRUE;
        [[self.arForTable objectAtIndex:[button tag]] setObject:@"1" forKey:@"isSelected"];
    }
}

-(void)miniMizeThisRows:(NSArray*)ar{
    for(NSDictionary *dInner in ar ) {
        NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] initWithDictionary:dInner];
        NSArray *filtered = [self.arForTable filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(c_branch_id == %@)", mutDict[@"c_branch_id"]]];
        
        NSUInteger indexToRemove = [self.arForTable indexOfObject:[filtered objectAtIndex:0]];
        [self.arForTable removeObject:[filtered objectAtIndex:0]];
        NSIndexPath *indedsd=[NSIndexPath indexPathForRow:indexToRemove inSection:0];
        SideMenuCell *cell = [self.tblForCollapse cellForRowAtIndexPath:indedsd];
        [self.tblForCollapse deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                                     [NSIndexPath indexPathForRow:indexToRemove inSection:0]]
                                   withRowAnimation:UITableViewRowAnimationNone];
        double delayInSeconds = 0.5;
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [cell.btnSubSlection setImage:nil forState:UIControlStateNormal];
            [cell.btnSubSlection setTitleColor:[appDelegate colorWithHexString:@"666666"] forState:UIControlStateNormal];
            cell.btnSubSlection.selected=FALSE;
            NSString *strTag= [mutDict objectForKey:@"c_branch_id"];
            for(int i=0;i<[[AppDelegate sharedInstance].mutArrSidePenalCategoryList count];i++){
                NSLog(@"%@",[[AppDelegate sharedInstance].mutArrSidePenalCategoryList objectAtIndex:i]);
                if([strTag isEqualToString:[[AppDelegate sharedInstance].mutArrSidePenalCategoryList objectAtIndex:i]]){
                    [[AppDelegate sharedInstance].mutArrSidePenalCategoryList removeObjectAtIndex:i];
                    [[AppDelegate sharedInstance].mutArrSidePenalCategoryName removeObjectAtIndex:i];
                    break;
                }
            }
        });
        // }
    }
}

- (void)GetCategoriesWS{
    self.arForTable=[[NSMutableArray alloc]initWithArray:[USERDEFAULTS objectForKey:@"CategoryList"] ];
    [self.tblForCollapse reloadData];
}

#pragma mark SetUpPage

-(void)SetUpPage{
    
    NSDictionary *aDictLocationData = [[NSUserDefaults standardUserDefaults] objectForKey:@"LocationData"];
    //        NSDictionary *aUserLoginDetailsData = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserLoginDetails"];
    
    UIFont *regularFont = [UIFont fontWithName:FontRobotoRegular size:18.0];
    UIFont *lightFont = [UIFont fontWithName:FontRobotoLight size:18.0];
    NSMutableAttributedString *aAttrString = nil;
    NSString *string = nil;
    string = [USERDEFAULTS objectForKey:@"FirstName"];
    NSString *strLastName=[USERDEFAULTS objectForKey:@"LastName"];
    if (string.length==0) {
        string = [NSString stringWithFormat:@"Guest"];
        aAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ Guest",[appDelegate getString:@"Welcome"]] attributes: nil];
    }else{
        if(strLastName.length>0){
            string = [NSString stringWithFormat:@"%@ %@",[USERDEFAULTS objectForKey:@"FirstName"],strLastName];
        }else{
            string = [NSString stringWithFormat:@"%@",[USERDEFAULTS objectForKey:@"FirstName"]];
        }
        
        NSData *dataa = [string dataUsingEncoding:NSUTF8StringEncoding];
        NSString *valueEmoj = [[NSString alloc] initWithData:dataa encoding:NSNonLossyASCIIStringEncoding];
        aAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",[appDelegate getString:@"Welcome"],valueEmoj] attributes: nil];
    }
    [aAttrString setAttributes:@{ NSFontAttributeName: lightFont } range:NSMakeRange(0, 8)];
    NSRange range = [aAttrString.string rangeOfString:string];
    [aAttrString setAttributes:@{ NSFontAttributeName: regularFont } range:range];
    lblUserName.attributedText = aAttrString;
    
    lblTitleExplore.text=[appDelegate getString:@"Explore all coupons"];
    
    [btnClearall setTitle:[appDelegate getString:@"Clear all"] forState:UIControlStateNormal];
    
    if (![aDictLocationData objectForKey:@"mc_city_image"]) {
//        [self.imgViewBackground setImage:[UIImage imageNamed:@"BGHome"]];
        self.imgViewBackground.image = [UIImage imageNamed:@"BGHome"];
    }else{
        
        
        [self.imgViewBackground sd_setImageWithURL:[aDictLocationData objectForKey:@"mc_city_image"]
                                  placeholderImage:nil
                                           options:SDWebImageProgressiveDownload
                                          progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                          }
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                             self.imgViewBackground.clipsToBounds = YES;
                                             if (image == nil){
                                                 self.imgViewBackground.image = [UIImage imageNamed:@"ic_place_holder"];
//                                                 self.imgViewBackground.image = [UIImage imageNamed:@"BGHome"];
                                             }
                                         }];
    }
    
    [self.tblForCollapse reloadData];
}

-(IBAction)ClearAllSelection:(id)sender{
    
    [self.arForTable removeAllObjects];
    self.arForTable=[[NSMutableArray alloc]initWithArray:[USERDEFAULTS objectForKey:@"CategoryList"] ];
    
    NSError *er_1;
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:self.arForTable options:NSJSONWritingPrettyPrinted error:&er_1];
    NSError *er_2;
    self.arForTable = [NSJSONSerialization JSONObjectWithData:dataFromDict options:NSJSONReadingMutableContainers error:&er_2];
    for (NSMutableDictionary *mutDict in self.arForTable){
        [mutDict setObject:@"0" forKey:@"parent_open"];
    }
    
    [[AppDelegate sharedInstance].mutArrSidePenalCategoryList removeAllObjects];
    [[AppDelegate sharedInstance].mutArrSidePenalCategoryName removeAllObjects];
    [self.tblForCollapse reloadData];
}

@end
