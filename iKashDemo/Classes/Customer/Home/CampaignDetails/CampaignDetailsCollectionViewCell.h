//
//  CampaignDetailsCollectionViewCell.h
//  iKash
//
//  Created by IndiaNIC on 04/12/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CampaignDetailsCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblNumberText;

@property (strong, nonatomic) IBOutlet UILabel *lblText;
@end
