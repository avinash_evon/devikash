//
//  CampaignDetailsVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "CampaignDetailsVC.h"
#import "SupplierDetailsVC.h"
#import "ReviewList.h"
#import "VoucherVC.h"
#import "CampaignDetailsCollectionViewCell.h"

@interface CampaignDetailsVC (){
    NSMutableDictionary *mutCampaignDetails;
    NSString *strCampaignStatus;
    BOOL isFavCamp;
}

@end

@implementation CampaignDetailsVC

#pragma mark - Synthesize

@synthesize strCampaignId,strCampaignStatus;

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isFavCamp = FALSE;
    
    [AppDelegate sharedInstance].campingDetailFlag = @"1";
    
      NSLog(@"Inside campaing >>>>>>%@",[AppDelegate sharedInstance].campingDetailFlag);
    
    self.navigationController.navigationBarHidden = YES;
    if (IS_IPHONE) {
        // Hide tabBar...
        self.tabBarController.tabBar.hidden = YES;
        self.lblNavTitle.text = [appDelegate getString:@"Campaign Detail"];
        
        // Set spacing accroding to device...
//        if (IS_IPHONE_4) {
//            imgViewCampaignHeightConstraint.constant = 180;
//        }else if (IS_IPHONE_5) {
//            imgViewCampaignHeightConstraint.constant = 180;
//        }else if (IS_IPHONE_6) {
//            imgViewCampaignHeightConstraint.constant = 220;
//        }else if (IS_IPHONE_6_PLUS) {
//            imgViewCampaignHeightConstraint.constant = 250;
//        }
        NSLog(@"%@",mutCampaignDetails);
    }
}

-(void)viewDidLayoutSubviews{
    if (IS_IPHONE) {
        _constImgHeight.constant = _imgCampaign.frame.size.width*402/370;
    }
}

-(void)viewWillAppear:(BOOL)animated{
//     [self viewWillAppear:animated];
    
    _scrollView.hidden=TRUE;
    _btnDownaloadVoucherClick.hidden=TRUE;
    
    [self CampaignDetailsWS:strCampaignId];
    
    if(strCampaignStatus.length==0){
        strCampaignStatus=@"Live";
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ResetDownloadButton) name:@"ResetDownloadButton" object:nil];
    
    if(!IS_IPAD){
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

#pragma mark UIInterfaceOrientation Methods
- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- UIButton Click Action Methods

- (IBAction)btnActionToBack:(id)sender {
    if ([self.isFromFavourites isEqualToString:@"Yes"]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
       // [self.navigationController popViewControllerAnimated:YES];
        
         [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)btnActionToClickSupplierDetails:(UIButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    SupplierDetailsVC *objSupplierDetailsVC = [storyboard instantiateViewControllerWithIdentifier:@"SupplierDetailsVC"];
    objSupplierDetailsVC.strSupplierId=[mutCampaignDetails objectForKey:@"c_supplier_id"];
    objSupplierDetailsVC.strCampaignId=[mutCampaignDetails objectForKey:@"c_campaign_id"];
    [self.navigationController pushViewController:objSupplierDetailsVC animated:YES];
}

- (IBAction)btnActionToClickReviewsDetails:(UIButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    ReviewList *objReviewList = [storyboard instantiateViewControllerWithIdentifier:@"ReviewList"];
    objReviewList.strCampaignId=[mutCampaignDetails objectForKey:@"c_campaign_id"];
    [self.navigationController pushViewController:objReviewList animated:YES];
}

- (IBAction)btnActionToDownloadVoucherClick:(id)sender {
    if([[AppDelegate sharedInstance].strRedeemed isEqualToString:@"0"]){
        [AppDelegate sharedInstance].strRedeemed=@"1";
    }
    [self DisplayRedeemVoucher];
}

-(void)DisplayRedeemVoucher{
    
    if([[mutCampaignDetails objectForKey:@"available_vouchers"] isEqualToString:@"0"] || [strCampaignStatus isEqualToString:@"Expired"]){
        [appDelegate showAlertView:[appDelegate getString:@"Sorry, Voucher limit has reached for this campaign."] Tag:@"1023"];
        return;
    }
    
    //@"Sorry, Voucher limit has reached for this campaign."
    //@"Sorry, Bon limiet heeft bereikt voor deze campagne."
    
    int intCustomerId=[[USERDEFAULTS objectForKey:@"customer_id"] intValue];
    if(intCustomerId==0){
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        objPopOverAlertVC = [aStoryboard instantiateViewControllerWithIdentifier:@"PopOverAlertVC"];
        [[AppDelegate sharedInstance].window addSubview:objPopOverAlertVC.view];
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        VoucherVC *objVoucherVC = [storyboard instantiateViewControllerWithIdentifier:@"VoucherVC"];
        objVoucherVC.strCampaignId=[mutCampaignDetails objectForKey:@"c_campaign_id"];
        // objVoucherVC.strCampaignStatus=strCampaignStatus;
        if (IS_IPAD) {
            objVoucherVC.isFromCampaignDetails = @"CampaignDetails";
            [self.navigationController pushViewController:objVoucherVC animated:YES];
        }else{
            //            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objVoucherVC];
            objVoucherVC.modalPresentationStyle = UIModalPresentationCustom;
            [self presentViewController:objVoucherVC animated:YES completion:nil];
        }
    }
}
- (IBAction)btnFavouriteClick:(id)sender {
    int intCustomerId=[[USERDEFAULTS objectForKey:@"customer_id"] intValue];
    if(intCustomerId==0){
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        objPopOverAlertVC = [aStoryboard instantiateViewControllerWithIdentifier:@"PopOverAlertVC"];
        [[AppDelegate sharedInstance].window addSubview:objPopOverAlertVC.view];
    }else{
        UIButton *btnFav=(UIButton *)sender;
        if(isFavCamp){
            [btnFav setImage:[UIImage imageNamed:@"ic-like"] forState:UIControlStateNormal];
            [self UnFavouriteCampaignWS:strCampaignId];
        }else{
            [btnFav setImage:[UIImage imageNamed:@"ic-like1"] forState:UIControlStateNormal];//liked
            [self FavouriteCampaignWS:strCampaignId];
        }
    }
}

#pragma mark - Webservice Methods
- (void)UnFavouriteCampaignWS:(NSString *)strCampaign_Id {
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  strCampaign_Id,@"campaign_id",
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  [USERDEFAULTS objectForKey:@"access_token"],@"access_token",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_UnFavouriteCampaignWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            [self CampaignDetailsWS:strCampaignId];
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)FavouriteCampaignWS:(NSString *)strCampaign_Id {
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  strCampaign_Id,@"campaign_id",
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_FavouriteCampaignWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            [self CampaignDetailsWS:strCampaignId];
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)CampaignDetailsWS:(NSString *)strCampaign_Id {
    if([self NointerNetCheking]){
        NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      strCampaign_Id,@"campaign_id",
                                      [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                      nil];
        [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCampaignDetailsWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            NSLog(@"responseData = %@", responseData);
            if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
                _scrollView.hidden=FALSE;
                if([[responseData objectForKey:@"data"] count]>0){
                    mutCampaignDetails=[[NSMutableDictionary alloc]initWithDictionary:[[responseData objectForKey:@"data"] objectAtIndex:0]];
                    [self SetUpPage];
                    [self UpdateViewWS];
                }
            }else{
                [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
            }
        } withFailureBlock:^(NSError *error) {
            _scrollView.hidden=TRUE;
            _viewNoInternet.hidden=FALSE;
            _lblNoInternet.text=[appDelegate getString:@"WEBSERVICEISSUE"];
        }];
    }
}

- (void)VoucherDetailsWS {
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  strCampaignId,@"campaign_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_DownaloadVoucherWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            [self DisplayRedeemVoucher];
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
} 
- (void)UpdateViewWS {
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  strCampaignId,@"campaign_id",
                                  [USERDEFAULTS objectForKey:@"DeviceToken"],@"device_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_IncreaseViewCountWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

# pragma mark - CollectionView Delegate Method

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return mutAryViewData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"CampaignDetailsCollectionViewCell";
    
    CampaignDetailsCollectionViewCell *campaignDetailsCollectionViewCell = (CampaignDetailsCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    int intValue=[[[mutAryViewData objectAtIndex:indexPath.row] objectForKey:@"Value"] intValue];
    NSLog(@"mutAryViewData = %@",mutAryViewData);
    campaignDetailsCollectionViewCell.lblNumberText.text = [[mutAryViewData objectAtIndex:indexPath.row] objectForKey:@"Value"];
    
    NSString *strTitle=[[mutAryViewData objectAtIndex:indexPath.row] objectForKey:@"Text"];
    
    if(intValue>1){
        campaignDetailsCollectionViewCell.lblText.text = strTitle;
    }else{
        campaignDetailsCollectionViewCell.lblText.text= [strTitle substringToIndex:[strTitle length]-1];
    }
    
    return campaignDetailsCollectionViewCell;
}

#pragma mark - UICollectionViewFlowLayout methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    float cellWidth = collectionView.frame.size.width/mutAryViewData.count; //Replace the divisor with the column count requirement. Make sure to have it in float.
    CGSize size = CGSizeMake(cellWidth, 36);
    return size;
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [self.CollectionViewList.collectionViewLayout invalidateLayout];
}

#pragma mark SetUpPage

-(void)SetTextForController{
    NSURL *urlProudctImage = [NSURL URLWithString:[mutCampaignDetails objectForKey:@"camp_image_android"]];
    [_imgCampaign sd_setImageWithURL:urlProudctImage
                    placeholderImage:nil
                             options:SDWebImageProgressiveDownload
                            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                            }
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                               _imgCampaign.clipsToBounds = YES;
                           }];
    
    _lblCampaignTitle.text=[mutCampaignDetails objectForKey:@"c_title_"];
    _txtViewDescription.text = [appDelegate stringByStrippingHTML:[mutCampaignDetails objectForKey:@"c_description"]];
    _lblViewsCount.text=[mutCampaignDetails objectForKey:@"cs_total_clicks"];
    _lblLikesCount.text=[mutCampaignDetails objectForKey:@"cs_total_clicks"];
    _lblDownloadCount.text=[mutCampaignDetails objectForKey:@"cs_total_downloads"];
    _lblReviewCount.text=[mutCampaignDetails objectForKey:@"num_of_reviews"];
    
    NSString *strColor = [mutCampaignDetails objectForKey:@"b_colour_code"];
    if(strColor.length>0){
        strColor = [strColor substringWithRange:NSMakeRange(1, [strColor length]-1)];
    }else{
        strColor=@"000000";
    }
    
    _lblColor.backgroundColor=[appDelegate colorWithHexString:strColor];
    
    double aintStar=[[mutCampaignDetails objectForKey:@"averge_rating"] doubleValue];
    
    _starRatingView.starSize=CGSizeMake(16, 16);//11,11
    _starRatingView.numberOfStars=5;
    _starRatingView.rating=aintStar;
    _starRatingView.padding=10.0;
    _starRatingView.fillColor=[appDelegate colorWithHexString:@"F3602C"];//#F3602C  strColor
    _starRatingView.unfilledColor=[appDelegate colorWithHexString:strColor];
    _starRatingView.unfilledColor=[UIColor clearColor];
    _starRatingView.strokeColor=[appDelegate colorWithHexString:@"F3602C"];
    
    [self.view addSubview:objRateView];
    if([[mutCampaignDetails objectForKey:@"is_fav"] isEqualToString:@"1"]){
        isFavCamp = TRUE;
        [_btnFavourite setImage:[UIImage imageNamed:@"liked"] forState:UIControlStateNormal];
    }else{
        isFavCamp = FALSE;
        [_btnFavourite setImage:[UIImage imageNamed:@"ic-like1"] forState:UIControlStateNormal];
    }
}

-(void)SetUpPage{
//    if([[mutCampaignDetails objectForKey:@"available_vouchers"] isEqualToString:@"0"] || [strCampaignStatus isEqualToString:@"Expired"]){
//        _btnDownaloadVoucherClick.hidden=TRUE;
////        scrllViewCampaignHeightConstraint.constant = -40.0f;
//        _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, _scrollView.frame.size.height+40);
//    }else{
//        _btnDownaloadVoucherClick.hidden=FALSE;
//    }
    
    _btnDownaloadVoucherClick.hidden= FALSE;
    
    if([[mutCampaignDetails objectForKey:@"dv_status"] isEqualToString:@""] && [[mutCampaignDetails objectForKey:@"vouch_deleted"] isEqualToString:@""]){
        [_btnDownaloadVoucherClick setTitle:[appDelegate getString:@"Download Voucher"] forState:UIControlStateNormal];
        [AppDelegate sharedInstance].strRedeemed=@"0";
        
    }else if([[mutCampaignDetails objectForKey:@"dv_status"] isEqualToString:@"Downloaded"] && [[mutCampaignDetails objectForKey:@"vouch_deleted"] isEqualToString:@"0"]){
        [_btnDownaloadVoucherClick setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_btnDownaloadVoucherClick setTitle:[appDelegate getString:@"Display Voucher"] forState:UIControlStateNormal];
        [AppDelegate sharedInstance].strRedeemed=@"1";
        
    }else if([[mutCampaignDetails objectForKey:@"dv_status"] isEqualToString:@"Downloaded"] && [[mutCampaignDetails objectForKey:@"vouch_deleted"] isEqualToString:@"1"]){
        //       [_btnDownaloadVoucherClick setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        //       [_btnDownaloadVoucherClick setTitle:@"Display Voucher" forState:UIControlStateNormal];
        //                   [AppDelegate sharedInstance].strRedeemed=@"2";
        
        [_btnDownaloadVoucherClick setTitle:[appDelegate getString:@"Download Voucher"] forState:UIControlStateNormal];
        [AppDelegate sharedInstance].strRedeemed=@"0";
        
    }else if([[mutCampaignDetails objectForKey:@"dv_status"] isEqualToString:@"Redeemed"] && [[mutCampaignDetails objectForKey:@"vouch_deleted"] isEqualToString:@"1"]){
        [_btnDownaloadVoucherClick setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_btnDownaloadVoucherClick setTitle:[appDelegate getString:@"Display Redeemed Voucher"] forState:UIControlStateNormal];
        [AppDelegate sharedInstance].strRedeemed=@"3";
        
    }else if([[mutCampaignDetails objectForKey:@"dv_status"] isEqualToString:@"Redeemed"] && [[mutCampaignDetails objectForKey:@"vouch_deleted"] isEqualToString:@"0"]){
        [_btnDownaloadVoucherClick setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [AppDelegate sharedInstance].strRedeemed=@"4";
        [_btnDownaloadVoucherClick setTitle:[appDelegate getString:@"Display Redeemed Voucher"] forState:UIControlStateNormal];
        
    }
    
    if([[mutCampaignDetails objectForKey:@"c_with_voucher"] isEqualToString:@"No"] ){
        [_btnDownaloadVoucherClick setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        
        _btnDownaloadVoucherClick.hidden = YES;
      //  [_btnDownaloadVoucherClick setTitle:[appDelegate getString:@"No voucher required"] forState:UIControlStateNormal];
        _btnDownaloadVoucherClick.userInteractionEnabled = NO;
        _btnDownaloadVoucherClick.backgroundColor = [UIColor whiteColor];//lightGrayColor
        _btnDownaloadVoucherClick.hidden = NO;
    }
    
    mutAryViewData = [[NSMutableArray alloc] init];
    NSDictionary *aDict;
    NSString *strValue;
    if (![[mutCampaignDetails objectForKey:@"cs_total_clicks"] isEqualToString:@"Not Applicable"]) {
        strValue=[mutCampaignDetails objectForKey:@"cs_total_clicks"];
        if(strValue.length==0){
            strValue=@"0";
        }
        aDict = [NSDictionary dictionaryWithObjectsAndKeys:
                 [appDelegate getString:@"Views"],@"Text",
                 strValue,@"Value",
                 nil];
        [mutAryViewData addObject:aDict];
    }
    
    
    if (![[mutCampaignDetails objectForKey:@"cs_total_favourites"] isEqualToString:@"Not Applicable"]) {
        strValue=[mutCampaignDetails objectForKey:@"cs_total_favourites"];
        if(strValue.length==0){
            strValue=@"0";
        }
        aDict = [NSDictionary dictionaryWithObjectsAndKeys:
                 [appDelegate getString:@"Likes"],@"Text",
                 strValue,@"Value",
                 nil];
        [mutAryViewData addObject:aDict];
    }else{
        _btnFavourite.hidden = YES;
    }
    
    
    if (![[mutCampaignDetails objectForKey:@"cs_total_downloads"] isEqualToString:@"Not Applicable"]) {
        strValue=[mutCampaignDetails objectForKey:@"cs_total_downloads"];
        if(strValue.length==0){
            strValue=@"0";
        }
        aDict = [NSDictionary dictionaryWithObjectsAndKeys:
                 [appDelegate getString:@"Downloads"],@"Text",
                 strValue,@"Value",
                 nil];
        [mutAryViewData addObject:aDict];
    }
    if (![[mutCampaignDetails objectForKey:@"num_of_reviews"] isEqualToString:@"Not Applicable"]) {
        strValue=[mutCampaignDetails objectForKey:@"num_of_reviews"];
        if(strValue.length==0){
            strValue=@"0";
        }
        aDict = [NSDictionary dictionaryWithObjectsAndKeys:
                 [appDelegate getString:@"Reviews"],@"Text",
                 strValue,@"Value",
                 nil];
        [mutAryViewData addObject:aDict];
    }else{
        _btnReviews.hidden = YES;
        _starRatingView.hidden = YES;
    }
    
    _viewTopSeparator.hidden = [[mutCampaignDetails objectForKey:@"cs_total_clicks"] isEqualToString:@"Not Applicable"] && [[mutCampaignDetails objectForKey:@"cs_total_favourites"] isEqualToString:@"Not Applicable"] && [[mutCampaignDetails objectForKey:@"cs_total_downloads"] isEqualToString:@"Not Applicable"] && [[mutCampaignDetails objectForKey:@"num_of_reviews"] isEqualToString:@"Not Applicable"];
    
    if (mutAryViewData.count == 0) {
        _CollectionViewList.hidden = YES;
    }
    [_CollectionViewList reloadData];
    [self SetTextForController];
}

-(void)ResetDownloadButton{
    _btnDownaloadVoucherClick.hidden=FALSE;
    if([[AppDelegate sharedInstance].strRedeemed isEqualToString:@"0"]){
        [_btnDownaloadVoucherClick setTitle:[appDelegate getString:@"Download Voucher"] forState:UIControlStateNormal];
        
    }else if([[AppDelegate sharedInstance].strRedeemed isEqualToString:@"1"]){
        [_btnDownaloadVoucherClick setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_btnDownaloadVoucherClick setTitle:[appDelegate getString:@"Display Voucher"] forState:UIControlStateNormal];
        
    }else if([[AppDelegate sharedInstance].strRedeemed isEqualToString:@"2"]){
        [_btnDownaloadVoucherClick setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_btnDownaloadVoucherClick setTitle:[appDelegate getString:@"Display Voucher"] forState:UIControlStateNormal];
        
    }else if([[AppDelegate sharedInstance].strRedeemed isEqualToString:@"3"]){
        [_btnDownaloadVoucherClick setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_btnDownaloadVoucherClick setTitle:[appDelegate getString:@"Display Redeemed Voucher"] forState:UIControlStateNormal];
        
    }else if([[AppDelegate sharedInstance].strRedeemed isEqualToString:@"4"]){
        [_btnDownaloadVoucherClick setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_btnDownaloadVoucherClick setTitle:[appDelegate getString:@"Display Redeemed Voucher"] forState:UIControlStateNormal];
    }
}

-(BOOL)NointerNetCheking{
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        _scrollView.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        return FALSE;
    }else{
        // _scrollView.hidden=FALSE;
        _viewNoInternet.hidden=TRUE;
        return TRUE;
    }
}
@end
