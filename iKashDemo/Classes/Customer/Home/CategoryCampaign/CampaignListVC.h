//
//  CampaignListVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DXStarRatingView.h"


@interface CampaignListVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblCampaignList;
@property (weak, nonatomic) IBOutlet UILabel *lblAllNoCampaign;
@property(nonatomic,assign)NSInteger *intTabIndex;
@property (strong, nonatomic) IBOutlet UIButton *btnPopupView;
@property (nonatomic, strong) UIPopoverController *campaignDetailsPopover;
@property(nonatomic,strong)NSString *strFlgSearch;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;

-(void)SetUpPage;
@end
