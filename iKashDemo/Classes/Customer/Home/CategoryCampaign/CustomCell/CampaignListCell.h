//
//  CampaignListCell.h
//  iKashDemo
//
//  Created by indianic on 02/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CampaignListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryName;
@property (weak, nonatomic) IBOutlet UILabel *lblNoCampaign;
@property (weak, nonatomic) IBOutlet UIButton *btnViewAll;

@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;

@property (strong, nonatomic) IBOutlet UICollectionView *CollectionViewList;
@end
