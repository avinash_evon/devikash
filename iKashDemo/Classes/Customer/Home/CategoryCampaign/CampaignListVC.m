//
//  CampaignListVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "CampaignListVC.h"
#import "CampaignListCell.h"
#import "CampaignCollectionCell.h"
#import "CampaignDetailsVC.h"
#import "ViewAllCampaignVC.h"
#import "AppDelegate.h"

@interface CampaignListVC ()<UIAlertViewDelegate> {
    NSString *strBranchid;
    NSMutableArray *mutArrCampaign;
    NSMutableArray *subBranchArr;
    NSMutableArray *mutArrCampaignSurprice;
    int intIndexTab;
    int intScrollHeight;
    int intCurrentPage,intNextPage;
    BOOL boolWebCallInProgress;
    UIActivityIndicatorView *spinner;
    UIRefreshControl *refreshControl;
    NSString *aCityId;
    
//    BOOL isCampaignFetchingProgress
}

@end

@implementation CampaignListVC

#pragma mark - Synthesize

@synthesize intTabIndex, campaignDetailsPopover, strFlgSearch;

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"campaing detail flag >>>>>>%@",[AppDelegate sharedInstance].campingDetailFlag);
    
  //  if([[AppDelegate sharedInstance].campingDetailFlag isEqualToString:@"0"])
  //  {
    
    //[AppDelegate sharedInstance].strHomeCatIndex = @"0";
    mutArrCampaignSurprice=[[NSMutableArray alloc]init];
    
    subBranchArr = [[NSMutableArray alloc]init];
    
    [[AppDelegate sharedInstance] setShouldCallAPI:YES];
    strFlgSearch=@"0";
    
    refreshControl = [[UIRefreshControl alloc] init];
    
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
    //    [_tblCampaignList addSubview:refreshControl];
    [_tblCampaignList setBackgroundView:refreshControl];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpForceFullyReloadDataWhenLanguageChange) name:@"ForceFullyReloadDataWhenLanguageChange" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GetCampaignWS) name:@"SearchCityCampaign" object:nil];
    
    
    //Reload capaings when location permission changes
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GetCityImageAndLatLong) name:@"reloadCampaigns" object:nil];
   
    //Reload HomeVC campaign when tab changes and any change in Fav category (Solved issue : If deselected any category from myikash-> category)
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PullToRefresh) name:@"reloadHomeCampaign" object:nil];
    
    
    //Call service to get data
    self.view.alpha = 1.0;
    
    intCurrentPage=1;
    mutArrCampaign=[[NSMutableArray alloc]init];
    
      [self checkLocation];
 //   }
//    else{
//        [AppDelegate sharedInstance].campingDetailFlag = @"0";
//        
//      //   [self checkLocation];
//    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.view.alpha = 1.0;
//    
//    intCurrentPage=1;
//    mutArrCampaign=[[NSMutableArray alloc]init];
//    
////    Check location before calling get camp. service
//    [self checkLocation];
//
//    //IF
//    if ([AppDelegate sharedInstance].isLocationGetCalledBeforeListVCLoaded){
//        //Check location before calling get camp. service
//        [self checkLocation];
//    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SearchCityCampaign" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForceFullyReloadDataWhenLanguageChange" object:nil];
    
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reloadCampaigns" object:nil];
}

- (void)refresh:(UIRefreshControl *)refreshControlView {
    [self PullToRefresh];
    
    [refreshControl endRefreshing];
    intScrollHeight=0;
}

-(void)setUpForceFullyReloadDataWhenLanguageChange{
    [_tblCampaignList reloadData];
}

#pragma mark - UIScrollView Delegate Methods

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(!IS_IPAD){
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
    
    intScrollHeight=0;
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@">>>>>>>>>>>>%@", [alertView textFieldAtIndex:0].text);
    
    NSArray *namesArray = [[alertView textFieldAtIndex:0].text componentsSeparatedByString:@" "];
    
    
    NSLog(@"my string component : %@",[namesArray objectAtIndex:0]);
    
    
    NSString *str = [namesArray objectAtIndex:0];
     NSString *str1 = [namesArray objectAtIndex:1];
    
    
      [USERDEFAULTS setObject:str forKey:@"latitude"];
      [USERDEFAULTS setObject:str1 forKey:@"longitude"];
    
    [USERDEFAULTS synchronize];
       
    
    NSLog(@"##############>%@",str);
    
    [self checkLocation];
      
      [[NSNotificationCenter defaultCenter] postNotificationName:@"SetTopCategory" object:nil];
}

-(void)PullToRefresh{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Title"
                                                              message:@"Enter latitude and Longiture"
                                                             delegate:self
                                                    cancelButtonTitle:@"Done"
                                                    otherButtonTitles:nil];
              alert.alertViewStyle = UIAlertViewStylePlainTextInput;
              [alert show];
    
    
    
    //Check location before calling get camp. service
//    [self checkLocation];
//
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetTopCategory" object:nil];
}

//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    NSLog(@"%@", [alertView textFieldAtIndex:0].text);
//}

-(void)checkLocation{
    
  //  [USERDEFAULTS setObject:@"52.298664" forKey:@"latitude"];
  //  [USERDEFAULTS setObject:@"5.629619" forKey:@"longitude"];
  //  [USERDEFAULTS synchronize];
    
    
  //  NSLog(@"my lati : %@", [USERDEFAULTS objectForKey:@"latitude"]);
   //  NSLog(@"my longi : %@", [USERDEFAULTS objectForKey:@"longitude"]);
    
       //  if (status == kCLAuthorizationStatusDenied) {
//         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Title"
//                                                            message:@"Message"
//                                                           delegate:self
//                                                  cancelButtonTitle:@"Done"
//                                                  otherButtonTitles:nil];
//            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
//            [alert show];
    
  
    
    
    
   // NSLog(@"#########################");
       
         //  }
    
//    CLGeocoder *ceo = [[CLGeocoder alloc]init];
//    CLLocation *loc = [[CLLocation alloc]initWithLatitude:(double)[USERDEFAULTS objectForKey:@"latitude"] longitude:[USERDEFAULTS objectForKey:@"longitude"]];
//
//    [ceo reverseGeocodeLocation: loc completionHandler:
//     ^(NSArray *placemarks, NSError *error) {
//         CLPlacemark *placemark = [placemarks objectAtIndex:0];
//         NSLog(@"placemark %@",placemark);
//         //String to hold address
//         NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
//         NSLog(@"addressDictionary %@", placemark.addressDictionary);
//
//         NSLog(@"placemark %@",placemark.region);
//         NSLog(@"placemark %@",placemark.country);  // Give Country Name
//         NSLog(@"placemark %@",placemark.locality); // Extract the city name
//         NSLog(@"location %@",placemark.name);
//         NSLog(@"location %@",placemark.ocean);
//         NSLog(@"location %@",placemark.postalCode);
//         NSLog(@"location %@",placemark.subLocality);
//
//         NSLog(@"location %@",placemark.location);
//         //Print the location to console
//         NSLog(@"I am currently at %@",locatedAt);
//     }];
    
    
    
//    NSLog(@"%@ %@", [USERDEFAULTS objectForKey:@"latitude"],[USERDEFAULTS objectForKey:@"longitude"]);
//       CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
//       [geocoder reverseGeocodeLocation:bestLocation
//                      completionHandler:^(NSArray *placemarks, NSError *error)
//       {
//           if (error){
//               NSLog(@"Geocode failed with error: %@", error);
//               return;
//           }
//           CLPlacemark *placemark = [placemarks objectAtIndex:0];
//           NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
//           NSLog(@"locality %@",placemark.locality);
//           NSLog(@"postalCode %@",placemark.postalCode);
//
//       }];

  

   // NSDictionary *dic = [[json objectForKey:kResults] objectAtIndex:0];
  //  NSString *cityName = [[[dic objectForKey:@"address_components"] objectAtIndex:1] objectForKey:@"long_name"];
    
    
    intCurrentPage=1;
    int intLocationStatus=0;
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusAuthorizedAlways: {
            intLocationStatus=1;
            NSLog(@"IN");
        }
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse: {
            intLocationStatus=1;
            NSLog(@"IN");
        }
            break;
        default:
            break;
    }
    
    NSLog(@"OUT");
    
    
    if(intLocationStatus==1){
        NSLog(@"%@",[USERDEFAULTS objectForKey:@"City_Name"]);
        NSLog(@"%@",[USERDEFAULTS objectForKey:@"Userlatitude"]);
        NSLog(@"%@",[USERDEFAULTS objectForKey:@"longitude"]);//Userlongitude
        
       [AppDelegate sharedInstance].strCityId = [USERDEFAULTS objectForKey:@"City_id"];
        [AppDelegate sharedInstance].strCampaignPageTitle =  [USERDEFAULTS objectForKey:@"City_Name"];
     //   [USERDEFAULTS setObject:[USERDEFAULTS objectForKey:@"Userlatitude"] forKey:@"latitude"];
     //   [USERDEFAULTS setObject:[USERDEFAULTS objectForKey:@"Userlongitude"] forKey:@"longitude"];
        
     //  float aLat = [[USERDEFAULTS objectForKey:@"Userlatitude"] floatValue];
     //   float along = [[USERDEFAULTS objectForKey:@"Userlongitude"] floatValue];
        
      //  [AppDelegate sharedInstance].CurrentLat = aLat;
      //  [AppDelegate sharedInstance].CurrentLong = along;
        
     //   [USERDEFAULTS synchronize];
        
        [self GetCityImageAndLatLong];
        
    }else{
        
        //If location not found then show all campaign
        [USERDEFAULTS setObject:@"" forKey:@"latitude"];
        [USERDEFAULTS setObject:@"" forKey:@"longitude"];
        
        [AppDelegate sharedInstance].CurrentLat = 0.0;
        [AppDelegate sharedInstance].CurrentLong = 0.0;
        
        [AppDelegate sharedInstance].strFilterCityId = @"";
        
        [self getCampaigns];
    }
    
}

#pragma mark UIInterfaceOrientation Methods

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

# pragma mark - Tableview Delegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [mutArrCampaign count];
}
//---------avinash


- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10,20, 30); // top, left, bottom, right
}




//----------Avinash





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CampaignListCell *CampaignListTableCell = (CampaignListCell*)[tableView dequeueReusableCellWithIdentifier:@"CampaignListCell"];
    CampaignListTableCell.btnViewAll.tag=indexPath.row;
    CampaignListTableCell.CollectionViewList.tag = indexPath.row;
    
    //Register cell from XIB
    [CampaignListTableCell.CollectionViewList registerNib:[UINib nibWithNibName:tileCellXibNibName bundle:nil] forCellWithReuseIdentifier:tileCellReuseIdentifier];
    
    [CampaignListTableCell.CollectionViewList reloadData];
    [CampaignListTableCell.CollectionViewList layoutSubviews];
    
    
    
    // [CampaignListTableCell.btnViewAll setTitle:@"See all" forState:UIControlStateNormal];
    [CampaignListTableCell.btnViewAll setTitle:[appDelegate getString:@"See all"] forState:UIControlStateNormal];
    [CampaignListTableCell.btnViewAll addTarget:self action:@selector(btnViewAllClick:) forControlEvents:UIControlEventTouchUpInside];
    
    CampaignListTableCell.lblCategoryName.text= [[[[mutArrCampaign objectAtIndex:indexPath.row] objectForKey:@"city_wise_campaign"] objectAtIndex:0]objectForKey:@"b_branch_name"];
    //@"####";//[[mutArrCampaign objectAtIndex:indexPath.row] objectForKey:@"b_branch_name"];
    
   // [[[mutArrCampaign objectAtIndex:indexPath.row] objectForKey:@"city_wise_campaign"]objectForKey:@"b_branch_name"];
    
    NSLog(@"$$$$$$$$$$$$$$$$$ $%@",mutArrCampaign);
    
//    NSLog(@"@@@@@@@@@@@@@@@@ %@",[[[mutArrCampaign objectAtIndex:indexPath.row] objectForKey:@"city_wise_campaign" objectAtIndex:0]);
    
    
    NSLog( @"@@@@@@@@@@@@@@@@@@@ %@",[[[[mutArrCampaign objectAtIndex:indexPath.row] objectForKey:@"city_wise_campaign"] objectAtIndex:0]objectForKey:@"b_branch_name"]);
    
    if([[[mutArrCampaign objectAtIndex:indexPath.row] objectForKey:@"city_wise_campaign"] count]==0){
        CampaignListTableCell.CollectionViewList.hidden=TRUE;
        CampaignListTableCell.btnViewAll.hidden=TRUE;
        CampaignListTableCell.lblNoCampaign.hidden=FALSE;
    }else{
        CampaignListTableCell.CollectionViewList.hidden=FALSE;
        CampaignListTableCell.btnViewAll.hidden=FALSE;
        CampaignListTableCell.lblNoCampaign.hidden=TRUE;
    }
    
    if([[AppDelegate sharedInstance].strFlgSearch isEqualToString:@"1"]){
        [CampaignListTableCell.CollectionViewList reloadData];
        [CampaignListTableCell.CollectionViewList scrollRectToVisible:CGRectZero animated:NO];
    }
    [CampaignListTableCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    CGFloat aScreenWidth = [UIScreen mainScreen].bounds.size.width;
    BOOL aShouldShowSeeAll =  CampaignListTableCell.CollectionViewList.contentSize.width < aScreenWidth;
    CampaignListTableCell.btnViewAll.hidden = aShouldShowSeeAll;
    CampaignListTableCell.imgArrow.hidden = aShouldShowSeeAll;
    
    
    return CampaignListTableCell;
}
/// by Avinash
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    if (indexPath.row == 1)
//    {
//        return 500;
//    }
//    else{
//        return 306;
//    }
//
//
//}



-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [_tblCampaignList reloadData];
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    if (IS_IPAD) {
//        return 285;
//    }else{
//        return 400;//250
//    }
//}

#pragma mark - Button Event Click Methods

-(void)btnViewAllClick:(id)sender{
    
    [[AppDelegate sharedInstance].mutArrSelectedCampaignWomenListAdded removeAllObjects];
    UIButton *btn=(UIButton *)sender;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    ViewAllCampaignVC *objViewAllCampaignVC = [storyboard instantiateViewControllerWithIdentifier:@"ViewAllCampaignVC"];
    objViewAllCampaignVC.mutAllCampaign=[[mutArrCampaign objectAtIndex:btn.tag] objectForKey:@"city_wise_campaign"];
    objViewAllCampaignVC.strTitle= [[mutArrCampaign objectAtIndex:btn.tag] objectForKey:@"b_branch_name"];
    objViewAllCampaignVC.strBranchID = [NSString stringWithFormat:@"%@", [[mutArrCampaign objectAtIndex:btn.tag] objectForKey:@"b_branch_id"]];
    
    [USERDEFAULTS setValue:[[mutArrCampaign objectAtIndex:btn.tag] objectForKey:@"b_branch_id"]  forKey:@"branch_id"];
    
    [AppDelegate sharedInstance].strFilterSearch=@"0";
    
    if([strBranchid isEqualToString:@""]){
        objViewAllCampaignVC.strCategoryId=[[mutArrCampaign objectAtIndex:btn.tag] objectForKey:@"b_branch_id"];
        [USERDEFAULTS setValue:@"All"  forKey:@"SubBranchId"];
        [USERDEFAULTS synchronize];
    }else{
        objViewAllCampaignVC.strCategoryId=strBranchid;
        if([[mutArrCampaign objectAtIndex:btn.tag] objectForKey:@"b_branch_id"]){
            [USERDEFAULTS setObject:[[mutArrCampaign objectAtIndex:btn.tag] objectForKey:@"b_branch_id"] forKey:@"SubBranchId"];
        }else{
            [USERDEFAULTS setObject:[[mutArrCampaign objectAtIndex:btn.tag] objectForKey:@"subbranch_id"] forKey:@"SubBranchId"];
        }
        [USERDEFAULTS synchronize];
    }
    
    [self.navigationController pushViewController:objViewAllCampaignVC animated:YES];
}

# pragma mark - CollectionView Delegate Method

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [[[mutArrCampaign objectAtIndex:collectionView.tag] objectForKey:@"city_wise_campaign"] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    CampaignCollectionCell *shopCollectionViewCell = (CampaignCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:tileCellReuseIdentifier forIndexPath:indexPath];
    
    [shopCollectionViewCell setDataWithDic:[[[mutArrCampaign objectAtIndex:collectionView.tag] objectForKey:@"city_wise_campaign"] objectAtIndex:indexPath.row]];
    
    return shopCollectionViewCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //----------
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SearchCityCampaign" object:nil];
//     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ForceFullyReloadDataWhenLanguageChange" object:nil];
//
//      [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reloadCampaigns" object:nil];
    
    
    
    //---------
    
    NSArray *arrListFlipdata;
    if(mutArrCampaignSurprice.count>0){
        NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"c_campaign_id =[c] %@", [[[[mutArrCampaign objectAtIndex:collectionView.tag] objectForKey:@"city_wise_campaign"] objectAtIndex:indexPath.row] objectForKey:@"c_campaign_id"]];
        arrListFlipdata = [mutArrCampaignSurprice filteredArrayUsingPredicate:aPredicate] ;
        NSLog(@"%@",arrListFlipdata);
    }
    if([[[[[mutArrCampaign objectAtIndex:collectionView.tag] objectForKey:@"city_wise_campaign"] objectAtIndex:indexPath.row] objectForKey:@"c_is_surprise"] isEqualToString:@"Yes"] && arrListFlipdata.count==0){
        [mutArrCampaignSurprice addObject:[[[mutArrCampaign objectAtIndex:collectionView.tag] objectForKey:@"city_wise_campaign"] objectAtIndex:indexPath.row]];
        CampaignCollectionCell *shopCollectionViewCell = (CampaignCollectionCell*) [collectionView cellForItemAtIndexPath:indexPath];
        [UIView transitionWithView:shopCollectionViewCell.imgSurprice duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                               NSURL *urlProudctImage = [NSURL URLWithString:[[[[mutArrCampaign objectAtIndex:collectionView.tag] objectForKey:@"city_wise_campaign"] objectAtIndex:indexPath.row] objectForKey:@"camp_image_ios"]];
                               
                               [shopCollectionViewCell.imgSurprice sd_setImageWithURL:urlProudctImage
                                                                     placeholderImage:nil
                                                                              options:SDWebImageProgressiveDownload
                                                                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                             }
                                                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                                shopCollectionViewCell.imgSurprice.clipsToBounds = YES;
                                                                            }];
                           } completion:nil];
    }else{
        if (IS_IPAD) {
            UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            CampaignDetailsVC *objCampaignDetailsVC = [aStoryboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
            objCampaignDetailsVC.strCampaignId =  [[[[mutArrCampaign objectAtIndex:collectionView.tag] objectForKey:@"city_wise_campaign"] objectAtIndex:indexPath.row] objectForKey:@"c_campaign_id"];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objCampaignDetailsVC];
            navigationController.navigationBar.hidden = YES;
            self.campaignDetailsPopover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
            self.campaignDetailsPopover.delegate = self;
            self.campaignDetailsPopover.popoverContentSize = CGSizeMake(370.0, 645.0);
            self.view.alpha = 0.6;
            if ([self.campaignDetailsPopover respondsToSelector:@selector(backgroundColor)]) {
                self.campaignDetailsPopover.backgroundColor = [UIColor blackColor];
            }
            [self.campaignDetailsPopover presentPopoverFromRect:[_btnPopupView bounds]
                                                         inView:_btnPopupView
                                       permittedArrowDirections:0
                                                       animated:YES];
        }else{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            CampaignDetailsVC *objCampaignDetailsVC = [storyboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
            objCampaignDetailsVC.hidesBottomBarWhenPushed = YES;
            objCampaignDetailsVC.strCampaignId =  [[[[mutArrCampaign objectAtIndex:collectionView.tag] objectForKey:@"city_wise_campaign"] objectAtIndex:indexPath.row] objectForKey:@"c_campaign_id"];
       //     [self.navigationController pushViewController:objCampaignDetailsVC animated:YES];
            
            
            
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objCampaignDetailsVC];
                  
                //  objCampaignDetailsVC.strCampaignId = [[mutArrVoucher objectAtIndex:indexPath.row] objectForKey:@"c1_campaign_id"];
                  
                  objCampaignDetailsVC.modalPresentationStyle = UIModalPresentationCustom;
                  
                  [self presentViewController:navigationController animated:YES completion:nil];
            
            
         //   [self.navigationController ]
        }
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    self.view.alpha = 1.0;
    return YES;
}

#pragma mark - Webservice Methods

- (void)GetCampaignWS{
    
    NSLog(@"I AM IN GetCampaignWS SERVICE.");
    
       NSLog(@">>>>>>>>select city lat :%@",[AppDelegate sharedInstance].selectedCityLatitude);
    
    if([self NointerNetCheking]){
        NSLog(@"%@",[USERDEFAULTS objectForKey:@"branch_id"]);
        intIndexTab=[[AppDelegate sharedInstance].strHomeCatIndex intValue];
        if(intIndexTab==0){ 
            [self GetAllCampaignWS];
        }else{
            [self SetRadius];
            NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          @"",@"sub_branch_id",
                                         @"",@"is_city",
    ([USERDEFAULTS objectForKey:@"latitude"] == nil)?@"":[USERDEFAULTS objectForKey:@"latitude"],@"latitude",
    ([USERDEFAULTS objectForKey:@"longitude"] == nil)?@"":[USERDEFAULTS objectForKey:@"longitude"],@"longitude",
                                          [USERDEFAULTS objectForKey:@"branch_id"],@"branch_id",
                                          [AppDelegate sharedInstance].strCityId,@"city_id",
                                          [AppDelegate sharedInstance].strRadius,@"radius",
                                          [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                      
                                         // @"all",@"category_type",/////avinash changes
                                          nil];
            
            
            NSLog(@"customer_GetCampaignWS parameters campaignlist = %@",aDict);
            [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCampaignWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
                NSLog(@"customer_GetCampaignWS responseData = %@", responseData);
                if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
                    [mutArrCampaign removeAllObjects];
                    
                    bool gotCityImage = NO;
                    
                    NSMutableArray *mutArrSubCampaign=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
                    
                //    NSLog(@"sub categories @@@@@@@@@@@@ %@",[[[[mutArrSubCampaign objectAtIndex:0] objectForKey:@"city_wise_campaign"]objectForKey:@"sub_branch_name"]objectAtIndex:0]);
                    
                    
                    
                    for(int i=0;i<[mutArrSubCampaign count];i++){
                        if([[[mutArrSubCampaign objectAtIndex:i] objectForKey:@"city_wise_campaign"] count]>0){
                            [mutArrCampaign addObject:[mutArrSubCampaign objectAtIndex:i]] ;
                            
                            if (!gotCityImage){
                                //Get city image URL and store in USERDEFAULT for side drawer
                                [self getCityImageFromDic:[mutArrSubCampaign objectAtIndex:i]];
                                gotCityImage = YES;
                            }
                        }
                    }
                    
                    [_tblCampaignList reloadData];
                }else{
                    [mutArrCampaign removeAllObjects];
                    
                    bool gotCityImage = NO;
                    
                    NSMutableArray *mutArrSubCampaign=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
                    for(int i=0;i<[mutArrSubCampaign count];i++){
                        if([[[mutArrSubCampaign objectAtIndex:i] objectForKey:@"city_wise_campaign"] count]>0){
                            [mutArrCampaign addObject:[mutArrSubCampaign objectAtIndex:i]] ;
                            
                            if (!gotCityImage){
                                //Get city image URL and store in USERDEFAULT for side drawer
                                [self getCityImageFromDic:[mutArrSubCampaign objectAtIndex:i]];
                                gotCityImage = YES;
                            }
                            
                        }
                    }
                    
                    [_tblCampaignList reloadData];
                    
                    if([[[responseData objectForKey:@"settings"] objectForKey:@"success"] isEqualToString:@"100"]){
                        [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
                    }else{
                        [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:@"50"];
                    }
                    
                }
            } withFailureBlock:^(NSError *error) {
                
                _tblCampaignList.hidden=TRUE;
                _viewNoInternet.hidden=FALSE;
                _lblNoInternet.text=[appDelegate getString:@"WEBSERVICEISSUE"];
                NSLog(@"%@",error);
            }];
        }
    }
}

- (void)GetAllCampaignWS{
    
    NSLog(@"I AM IN ALL SERVICE.");
    
    if([self NointerNetCheking]){
        NSLog(@"%@",[USERDEFAULTS objectForKey:@"branch_id"]);
        [self SetRadius];
        NSMutableDictionary *aDict;
        
        
        aCityId = ([AppDelegate sharedInstance].CurrentLat != 0.00000 && [AppDelegate sharedInstance].CurrentLong != 0.00000) ? [AppDelegate sharedInstance].strCityId : ![[AppDelegate sharedInstance].strFilterCityId isEqualToString:@""] ? [AppDelegate sharedInstance].strFilterCityId: @"";
                  aCityId = (aCityId == nil) ? @"" : aCityId;
        
        NSLog(@"My City Id %@",aCityId);
        
        //Don't show city and show all campigns if user not logged in
        if([AppDelegate sharedInstance].strCityId.length == 0 && [USERDEFAULTS objectForKey:@"customer_id"] != nil){
            
            [AppDelegate sharedInstance].strCityId=[USERDEFAULTS objectForKey:@"CustomerCity"];
        }
        NSLog(@"%@",[USERDEFAULTS objectForKey:@"latitude"]);
        NSString *strlatitude=[USERDEFAULTS objectForKey:@"latitude"];
        if(strlatitude.length==0){
            [USERDEFAULTS setValue:@"0" forKey:@"latitude"];
            [USERDEFAULTS setValue:@"0" forKey:@"longitude"];
        }
        aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                aCityId,@"is_city",
                 @"",@"branch_id",
                 @"",@"subbranch_id",
                [USERDEFAULTS objectForKey:@"latitude"],@"latitude",
                [USERDEFAULTS objectForKey:@"longitude"],@"longitude",
                [AppDelegate sharedInstance].strCityId,@"city_id",
                 [AppDelegate sharedInstance].strRadius,@"radius",
                [AppDelegate sharedInstance].selectedCityLatitude,@"device_lat",
               [AppDelegate sharedInstance].selectedCityLongitude,@"device_long",
                 [USERDEFAULTS objectForKey:@"customer_id"] != nil ? [USERDEFAULTS objectForKey:@"customer_id"] : @"",@"customer_id",
                 nil];
        
        if ([USERDEFAULTS objectForKey:@"customer_id"] != nil || [AppDelegate sharedInstance].strCityId.length != 0){
            //            [aDict setObject:[USERDEFAULTS objectForKey:@"latitude"] forKey:@"latitude"];
            //            [aDict setObject:[USERDEFAULTS objectForKey:@"longitude"] forKey:@"longitude"];
            
            NSString *aLat = [NSString stringWithFormat:@"%@",[AppDelegate sharedInstance].CurrentLat != 0.00000 ? [NSString stringWithFormat:@"%f",[AppDelegate sharedInstance].CurrentLat] : @"" ];
            NSString *aLong = [NSString stringWithFormat:@"%@",[AppDelegate sharedInstance].CurrentLong != 0.00000 ? [NSString stringWithFormat:@"%f",[AppDelegate sharedInstance].CurrentLong] : @"" ];
            
            
            [aDict setObject:[USERDEFAULTS objectForKey:@"latitude"] forKey:@"latitude"];
            [aDict setObject:[USERDEFAULTS objectForKey:@"longitude"] forKey:@"longitude"];
            
            //If user Continue without login and location not found then show all campaign
            //If user filer with city then show only campaigns belongs to that city
            
            aCityId = ([AppDelegate sharedInstance].CurrentLat != 0.00000 && [AppDelegate sharedInstance].CurrentLong != 0.00000) ? [AppDelegate sharedInstance].strCityId : ![[AppDelegate sharedInstance].strFilterCityId isEqualToString:@""] ? [AppDelegate sharedInstance].strFilterCityId: @"";
            aCityId = (aCityId == nil) ? @"" : aCityId;
            
            
            NSLog(@"my city id %@",aCityId);
            
            [aDict setObject:aCityId forKey:@"city_id"];
        }
        // JATIN : Add two new params from irish (device_lat & device_long)
        // if device location is on then pass current lat long of device othewise pass "0"
        if(strlatitude.length==0){
            
            [aDict setObject:[NSString stringWithFormat:@"%s","0"] forKey:@"device_lat"];
            [aDict setObject:[NSString stringWithFormat:@"%s","0"] forKey:@"device_long"];
            
        } else {
            
            
            
            
            [aDict setObject:[NSString stringWithFormat:@"%@", [AppDelegate sharedInstance].selectedCityLatitude] forKey:@"device_lat"];
            [aDict setObject:[NSString stringWithFormat:@"%@", [AppDelegate sharedInstance].selectedCityLongitude] forKey:@"device_long"];
        }
        
        NSLog(@"device latitude : %@",[USERDEFAULTS objectForKey:@"longitude"]);
        
        NSLog(@"customer_GetCampaignHomeWS paramters = %@",aDict);
        
        [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCampaignHomeWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            
            //[self CallStoptAnimating];
            NSLog(@"customer_GetCampaignHomeWS responseData = %@", responseData);
            if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
                [mutArrCampaign removeAllObjects];
                NSMutableArray *mutArrSubCampaign=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
                
                [mutArrCampaign mutableCopy];
                
                bool gotCityImage = NO;
                
                for(int i=0;i<[mutArrSubCampaign count];i++){
                    if([[[mutArrSubCampaign objectAtIndex:i] objectForKey:@"city_wise_campaign"] count]>0){
                        [mutArrCampaign addObject:[mutArrSubCampaign objectAtIndex:i]];
                        
                        if (!gotCityImage){
                            //Get city image URL and store in USERDEFAULT for side drawer
                            [self getCityImageFromDic:[mutArrSubCampaign objectAtIndex:i]];
                            gotCityImage = YES;
                        }
                    }
                }
                
                [_tblCampaignList reloadData];
            }else{
                if([[[responseData objectForKey:@"settings"] objectForKey:@"success"] isEqualToString:@"100"] ){
                    [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
                }else{
                    [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:@"50"];
                }
                
                NSMutableArray *mutArrSubCampaign=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
                [mutArrCampaign removeAllObjects];
                
                bool gotCityImage = NO;
                bool gotNotificationCount = NO;
                
                for(int i=0;i<[mutArrSubCampaign count];i++){
                    if([[[mutArrSubCampaign objectAtIndex:i] objectForKey:@"city_wise_campaign"] count]>0){
                        [mutArrCampaign addObject:[mutArrSubCampaign objectAtIndex:i]];
                        
                        if (!gotCityImage){
                        //Get city image URL and store in USERDEFAULT for side drawer
                            [self getCityImageFromDic:[mutArrSubCampaign objectAtIndex:i]];
                            gotCityImage = YES;
                        }
                        
                        if (!gotNotificationCount){
                            //Get notification count to show on headerView
                            [self getNotificationCountFromDic:[mutArrSubCampaign objectAtIndex:i]];
                            gotNotificationCount = YES;
                        }
                    
                    }
                }
                if(mutArrCampaign.count==0){
                    _tblCampaignList.hidden=TRUE;
                    _lblAllNoCampaign.hidden=FALSE;
                    _lblAllNoCampaign.text=[appDelegate getString:@"NO_RECORD_FOUND_MESSAGE"];
                }else{
                    _tblCampaignList.hidden=FALSE;
                    _lblAllNoCampaign.hidden=TRUE;
                    [_tblCampaignList reloadData];
                }
                
                //Call service to get new city image for side darwer
//                [self callWebServicesForGetLoginBackImage];
            }
            
        } withFailureBlock:^(NSError *error) {
            _tblCampaignList.hidden=TRUE;
            _viewNoInternet.hidden=FALSE;
            _lblNoInternet.text=[appDelegate getString:@"WEBSERVICEISSUE"];
            NSLog(@"%@",error);
        }];
    }
}

//notification_count

-(void)getNotificationCountFromDic : (NSDictionary*) aDic {

    NSString *aStrNotificationCount = [aDic objectForKey:@"city_wise_campaign"][0][@"notification_count"];
    
    int aCount = [aStrNotificationCount intValue];
    [AppDelegate sharedInstance].NotificationCount = aCount;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotNotification" object:nil];
    
    NSLog(@"=== * ===> NotificationCount => %d",aCount);
}

-(void)getCityImageFromDic : (NSDictionary*) aDic {
    //Get city image URL and store in USERDEFAULT for side drawer
    NSString *aStrCityImageURL = [aDic objectForKey:@"city_wise_campaign"][0][@"city_image"];
    
    if (aStrCityImageURL != nil && ![aStrCityImageURL isEqualToString:@""]){
        
        //Change city image for side drawer
        NSMutableDictionary *aDictLocationData = [[[NSUserDefaults standardUserDefaults] objectForKey:@"LocationData"] mutableCopy];
        
        [aDictLocationData setValue:aStrCityImageURL forKey:@"mc_city_image"];
        [[NSUserDefaults standardUserDefaults] setObject:aDictLocationData forKey:@"LocationData"];
        
    }
}

//Get city image and lat long that are stored in DB (Don't use current lat long of city)
//Use current city's lat long which are stored in DB to get currect result
-(void)GetCityImageAndLatLong{
    
    if([self NointerNetCheking]){
    
    NSString *strLatitude = [USERDEFAULTS objectForKey:@"latitude"];
    NSString *strLongitude = [USERDEFAULTS objectForKey:@"longitude"];
    NSString *strLanguageId = [NSString stringWithFormat:@"EN"];
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  strLatitude,@"lat",
                                  strLongitude,@"long",
                                  strLanguageId,@"lang_id",
                                  nil];
        
        NSLog(@"GetCityImageAndLatLong -> %@",aDict);
        
    [[Webservice sharedInstance] callWebserviceWithMethodName:get_background_imageWS withParams:aDict showConnectionError:YES showLoader:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            
            if([[responseData objectForKey:@"data"] count]>0)
            {
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"LocationData"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] setObject:[[responseData objectForKey:@"data"] objectAtIndex:0] forKey:@"LocationData"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                [AppDelegate sharedInstance].strCityId=[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"mc_city_id"];
                
                [AppDelegate sharedInstance].strCampaignPageTitle=[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"mc_city_2"];
                
                //JATIN ://If city found show data
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SetTopCategory" object:nil];
                
                [AppDelegate sharedInstance].CurrentLat = [[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"mc_latitude"] floatValue];
                [AppDelegate sharedInstance].CurrentLong = [[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"mc_longitude"] floatValue];
             
                [self getCampaigns];
            }
        }
        else{
            
            //If city not found still show data
            
            //JATIN : Add city title (if no any city found)
            [AppDelegate sharedInstance].strCampaignPageTitle= @"No City Found";//No City Found
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SetTopCategory" object:nil];
            [AppDelegate sharedInstance].CurrentLat = [strLatitude floatValue];
            [AppDelegate sharedInstance].CurrentLong = [strLongitude floatValue];
            [self getCampaigns];
        }
        
    } withFailureBlock:^(NSError *error) {
        [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedInstance].window animated:YES];
        [appDelegate showAlertView:[appDelegate getString:@"SOMETHINGWENTWRONG"] Tag:@"0"];
    }];
    
    }
}

//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//{
//
//    return 50.0;//10
//}
#pragma mark- Web Service Methods

//-(void)callWebServicesForGetLoginBackImage{
//    
//    
//    NSString *strLatitude = [USERDEFAULTS objectForKey:@"latitude"];
//    NSString *strLongitude = [USERDEFAULTS objectForKey:@"longitude"];
//    NSString *strLanguageId = [NSString stringWithFormat:@"EN"];
//    
//    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  strLatitude,@"lat",
//                                  strLongitude,@"long",
//                                  strLanguageId,@"lang_id",
//                                  nil];
//    
//    NSLog(@"LET LONG FOR IMG = %@", aDict);
//    
//    [[Webservice sharedInstance] callWebserviceWithMethodName:get_background_imageWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
//        NSLog(@"responseData = %@", responseData);
//        
//        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
//            
//            if([[responseData objectForKey:@"data"] count]>0)
//            {
//                //Change city image for side drawer
//                NSMutableDictionary *aDictLocationData = [[[NSUserDefaults standardUserDefaults] objectForKey:@"LocationData"] mutableCopy];
//                
//                [aDictLocationData setValue:[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"mc_city_image"] forKey:@"mc_city_image"];
//                [[NSUserDefaults standardUserDefaults] setObject:aDictLocationData forKey:@"LocationData"];
//                
//            }
//        }
//    } withFailureBlock:^(NSError *error) {
//        [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedInstance].window animated:YES];
//        [appDelegate showAlertView:[appDelegate getString:@"SOMETHINGWENTWRONG"] Tag:@"0"];
//    }];
//}

-(void)rateView:(RateView*)rateView didUpdateRating:(float)rating{
    NSLog(@"rateViewDidUpdateRating: %.1f", rating);
}

#pragma mark SetUpPage

-(void)SetUpPage{
    [[AppDelegate sharedInstance].mutArrSelectedCampaignWomenListAdded removeAllObjects];
    [[AppDelegate sharedInstance].mutArrSelectedCampaignCityListAdded removeAllObjects];
    [[AppDelegate sharedInstance].mutArrSelectedCampaignRadiusListAdded removeAllObjects];
    
    // Notifcation setup on Search for city and radius
    
    // Show tabBar...
    self.tabBarController.tabBar.hidden = NO;
}

-(void)getCampaigns{
    NSMutableArray *tempArr = [[NSMutableArray alloc]init];
    
    tempArr = [USERDEFAULTS objectForKey:@"branchID"];
    
    NSLog(@"mu branch ID %@",tempArr);
    
      NSLog(@"subBranch Name %@",[USERDEFAULTS objectForKey:@"ActiveInactiveBranchArr"]);
    
    intIndexTab=[[AppDelegate sharedInstance].strHomeCatIndex intValue];
    if(intIndexTab>0){
        
   
        
        NSMutableArray  *mutArrCategoryList=[[NSMutableArray alloc]initWithArray:[USERDEFAULTS objectForKey:@"CategoryList"]];
        
        if([[[mutArrCategoryList objectAtIndex:intIndexTab-1]objectForKey:@"branch_name" ]isEqualToString:[[USERDEFAULTS objectForKey:@"ActiveInactiveBranchArr"] objectAtIndex:intIndexTab-1]])
        {
            
            [subBranchArr removeAllObjects];
            for(int j=0;j<[[[mutArrCategoryList objectAtIndex:intIndexTab-1] objectForKey: @"first_child"]count];j++)
            {
                [subBranchArr addObject:[[[[mutArrCategoryList objectAtIndex:intIndexTab-1] objectForKey: @"first_child"]objectAtIndex:j]objectForKey:@"c_branch_name"]];
                
                
            }
            
            NSLog(@"selected sub category %@",subBranchArr);
            //         {
            //                 //         }
            
            
            
            
            //    [[subBranchArr objectAtIndex:intIndexTab-1] objectForKey:@"first_child"]objectAtIndex:<#(NSUInteger)#>
            
            // [subBranchArr obkec// objectAtIndex:intIndexTab-1]objectForKey:@"branch_name" ]]
            
            NSLog(@"my seleted subcategory");
        }
        
     //   NSLog("selected branch id :>>>>> %@",)
        
        strBranchid=[tempArr objectAtIndex:intIndexTab-1];// objectForKey:@"branch_id"];
        [USERDEFAULTS setObject:strBranchid forKey:@"branch_id"];
        [USERDEFAULTS synchronize];
        [self GetCampaignWS];
    }else{
        strBranchid=@"";
        [USERDEFAULTS setObject:strBranchid forKey:@"branch_id"];
        [USERDEFAULTS synchronize];
        [self GetAllCampaignWS];
    }
}

-(void)SetRadius{
    
    if([[AppDelegate sharedInstance].mutArrRadius count]>0){
        if([[[AppDelegate sharedInstance].mutArrRadius objectAtIndex:0] isEqualToString:@"NONE"]){
            [AppDelegate sharedInstance].strRadius=@"";
        }else{
            [AppDelegate sharedInstance].strRadius=[[AppDelegate sharedInstance].mutArrRadius objectAtIndex:0];
        }
    }else{
        [AppDelegate sharedInstance].strRadius=@"";
    }
    if([AppDelegate sharedInstance].strCityId.length==0){
        [AppDelegate sharedInstance].strCityId=@"";
    }
}

- (void) receiveTestNotification:(NSNotification *) notification{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    if ([[notification name] isEqualToString:@"TestNotification"])
        NSLog (@"Successfully received the test notification!");
}

-(BOOL)NointerNetCheking{
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        _tblCampaignList.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        return FALSE;
    }else{
        _tblCampaignList.hidden=FALSE;
        _viewNoInternet.hidden=TRUE;
        return TRUE;
    }
}

@end
