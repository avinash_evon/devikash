//
//  CampaignFilterVC.m
//  iKash
//
//  Created by indianic on 26/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "CampaignFilterVC.h"
#import "CampaignFilterCell.h"

@interface CampaignFilterVC (){
    NSMutableArray *mutTempWomenList;
    NSMutableArray *mutTempCityList;
    NSMutableArray *mutTempRadiousnList;
}

@end

@implementation CampaignFilterVC

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self SetUpPage];
    isSearching = NO;
    if(!IS_IPAD){
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark UIInterfaceOrientation Methods

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

#pragma mark - UICollectionViewDelegate methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([strSelectedCampaign isEqualToString:_strCatTitle]) {
        
        return [[dicCampaignDetails objectForKey:@"dicWomenDataList"] count];
    }else if ([strSelectedCampaign isEqualToString:@"City"]){
        
        if (isSearching) {
            return [mutArrCampaignCitySearchList count];
        }else{
            return [[dicCampaignDetails objectForKey:@"dicCityDataList"] count];
        }
    }else{
        if (isSearching) {
            return [mutArrCampaignRadiusSearchList count];
        }else{
            return [[dicCampaignDetails objectForKey:@"dicRadiusDataList"] count];
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CampaignFilterCell *cellCampaignFilter = [tableView dequeueReusableCellWithIdentifier:@"campaignfilter"];
    
    cellCampaignFilter.btnFilterSelection.btnCampaignFilterButtonIndexPath = indexPath;
    
    if ([strSelectedCampaign isEqualToString:_strCatTitle]) {
        
        cellCampaignFilter.lblFilterTitle.text = [[[dicCampaignDetails objectForKey:@"dicWomenDataList"] objectAtIndex:indexPath.row] objectForKey:@"b_branch_name"];
        
        [cellCampaignFilter.btnFilterSelection setBackgroundImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        
        NSMutableDictionary *dictItem =[[dicCampaignDetails objectForKey:@"dicWomenDataList"] objectAtIndex:indexPath.row];
        
        if ([mutArrSelectedCampaignWomenListAdded containsObject:dictItem]) {
            [cellCampaignFilter.btnFilterSelection setBackgroundImage:[UIImage imageNamed:@"checkbox-filled"] forState:UIControlStateNormal];
        }
    }else if ([strSelectedCampaign isEqualToString:@"City"]) {
        
        if (isSearching){
            
            cellCampaignFilter.lblFilterTitle.text = [[mutArrCampaignCitySearchList objectAtIndex:indexPath.row] objectForKey:@"city_name"];
            
            [cellCampaignFilter.btnFilterSelection setBackgroundImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
            
            NSMutableDictionary *dictItem =[mutArrCampaignCitySearchList objectAtIndex:indexPath.row];
            
            if ([mutArrSelectedCampaignCityListAdded containsObject:dictItem]) {
                [cellCampaignFilter.btnFilterSelection setBackgroundImage:[UIImage imageNamed:@"checkbox-filled"] forState:UIControlStateNormal];
            }
        }else{
            
            cellCampaignFilter.lblFilterTitle.text = [[[dicCampaignDetails objectForKey:@"dicCityDataList"] objectAtIndex:indexPath.row] objectForKey:@"city_name"];
            
            [cellCampaignFilter.btnFilterSelection setBackgroundImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
            
            NSMutableDictionary *dictItem =[[dicCampaignDetails objectForKey:@"dicCityDataList"] objectAtIndex:indexPath.row];
            
            if ([mutArrSelectedCampaignCityListAdded containsObject:dictItem]) {
                [cellCampaignFilter.btnFilterSelection setBackgroundImage:[UIImage imageNamed:@"checkbox-filled"] forState:UIControlStateNormal];
            }
        }
    }else{
        if (isSearching){
            if([[mutArrCampaignRadiusSearchList objectAtIndex:indexPath.row] isEqualToString:@"NONE"]){
                cellCampaignFilter.lblFilterTitle.text = [mutArrCampaignRadiusSearchList objectAtIndex:indexPath.row];
            }else{
                cellCampaignFilter.lblFilterTitle.text = [NSString stringWithFormat:@"%@%@",[mutArrCampaignRadiusSearchList objectAtIndex:indexPath.row],@" km"];
            }
            
            [cellCampaignFilter.btnFilterSelection setBackgroundImage:[UIImage imageNamed:@"btn-radio"] forState:UIControlStateNormal];
            
            NSMutableDictionary *dictItem =[mutArrCampaignRadiusSearchList objectAtIndex:indexPath.row];
            
            if ([mutArrSelectedCampaignRadiusListAdded containsObject:dictItem]) {
                [cellCampaignFilter.btnFilterSelection setBackgroundImage:[UIImage imageNamed:@"btn-radio-filled"] forState:UIControlStateNormal];
                [cellCampaignFilter.btnFilterSelection setImage:[UIImage imageNamed:@"ic-check"] forState:UIControlStateNormal];
            }
        }else{
            
            if([[[dicCampaignDetails objectForKey:@"dicRadiusDataList"] objectAtIndex:indexPath.row] isEqualToString:@"NONE"]){
                cellCampaignFilter.lblFilterTitle.text = [[dicCampaignDetails objectForKey:@"dicRadiusDataList"] objectAtIndex:indexPath.row];
            }else{
                cellCampaignFilter.lblFilterTitle.text = [NSString stringWithFormat:@"%@%@",[[dicCampaignDetails objectForKey:@"dicRadiusDataList"] objectAtIndex:indexPath.row],@" km"];
            }
            [cellCampaignFilter.btnFilterSelection setBackgroundImage:[UIImage imageNamed:@"btn-radio"] forState:UIControlStateNormal];
            
            NSMutableDictionary *dictItem =[[dicCampaignDetails objectForKey:@"dicRadiusDataList"] objectAtIndex:indexPath.row];
            
            if ([mutArrSelectedCampaignRadiusListAdded containsObject:dictItem]) {
                [cellCampaignFilter.btnFilterSelection setBackgroundImage:[UIImage imageNamed:@"btn-radio-filled"] forState:UIControlStateNormal];
                [cellCampaignFilter.btnFilterSelection setImage:[UIImage imageNamed:@"ic-check"] forState:UIControlStateNormal];
            }
        }
    }
    
    [[cellCampaignFilter btnFilterSelection] addTarget:self action:@selector(CampainFilterSelectionClick:) forControlEvents:UIControlEventTouchUpInside];
    return cellCampaignFilter;
}

#pragma mark ----------------------
#pragma mark CampainFilterSelectionClick Selection..

- (IBAction)CampainFilterSelectionClick:(CampaignFilterButton*)sender{
    
    indFilterPath = sender.btnCampaignFilterButtonIndexPath;
    NSLog(@"selected index = %ld",(long)indFilterPath.row);
    NSMutableDictionary *dictItem;
    if (isSearching){
        if ([strSelectedCampaign isEqualToString:_strCatTitle]) {
            dictItem =[[dicCampaignDetails objectForKey:@"dicWomenDataList"] objectAtIndex:indFilterPath.row];
        }else if ([strSelectedCampaign isEqualToString:@"City"]){
            dictItem =[mutArrCampaignCitySearchList objectAtIndex:indFilterPath.row];
        }else{
            dictItem =[mutArrCampaignRadiusSearchList objectAtIndex:indFilterPath.row];
        }
    }else{
        if ([strSelectedCampaign isEqualToString:_strCatTitle]) {
            dictItem =[[dicCampaignDetails objectForKey:@"dicWomenDataList"] objectAtIndex:indFilterPath.row];
        }else if ([strSelectedCampaign isEqualToString:@"City"]){
            dictItem =[[dicCampaignDetails objectForKey:@"dicCityDataList"] objectAtIndex:indFilterPath.row];
        }else{
            dictItem =[[dicCampaignDetails objectForKey:@"dicRadiusDataList"] objectAtIndex:indFilterPath.row];
        }
    }
    
    UITableViewCell *cell = [tblCampaignFliter cellForRowAtIndexPath:indFilterPath];
    UIButton *btnCampaignFilter = (UIButton *)[cell.contentView viewWithTag:100];
    
    if ([strSelectedCampaign isEqualToString:@"Radius"]) {
        
        if ([mutArrSelectedCampaignCityListAdded count] > 1) {
            [appDelegate showAlertView:[appDelegate getString:@"RADIOUSALERT"] Tag:@"0"];
        }else{
            [mutArrSelectedCampaignRadiusListAdded removeAllObjects];
            [mutArrSelectedCampaignRadiusListAdded addObject:dictItem];
            [tblCampaignFliter reloadData];
        }
    }else if ([strSelectedCampaign isEqualToString:_strCatTitle]) {
        
        if ([mutArrSelectedCampaignWomenListAdded containsObject:dictItem]){
            [btnCampaignFilter setBackgroundImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
            [mutArrSelectedCampaignWomenListAdded removeObject:dictItem];
        }else{
            [btnCampaignFilter setBackgroundImage:[UIImage imageNamed:@"checkbox-filled"] forState:UIControlStateNormal];
            [btnCampaignFilter setImage:[UIImage imageNamed:@"ic-check"] forState:UIControlStateNormal];
            [mutArrSelectedCampaignWomenListAdded addObject:dictItem];
        }
    }else{
        
        if ([mutArrSelectedCampaignCityListAdded containsObject:dictItem]){
            [btnCampaignFilter setBackgroundImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
            [mutArrSelectedCampaignCityListAdded removeObject:dictItem];
        }else{
            [btnCampaignFilter setBackgroundImage:[UIImage imageNamed:@"checkbox-filled"] forState:UIControlStateNormal];
            [btnCampaignFilter setImage:[UIImage imageNamed:@"ic-check"] forState:UIControlStateNormal];
            [mutArrSelectedCampaignCityListAdded addObject:dictItem];
        }
        if ([mutArrSelectedCampaignCityListAdded count] > 1) {
            [mutArrSelectedCampaignRadiusListAdded removeAllObjects];
        }
    }
}

#pragma mark- UIButton Click Action Methods

- (IBAction)btnBackAction:(id)sender {
    
    NSLog(@"City: %@",mutTempCityList);
    NSLog(@"Women: %@",mutTempWomenList);
    NSLog(@"Radious:  %@",mutTempRadiousnList);
    
    [AppDelegate sharedInstance].mutArrSelectedCampaignWomenListAdded = mutTempWomenList;
    [AppDelegate sharedInstance].mutArrSelectedCampaignCityListAdded = mutTempCityList;
    [AppDelegate sharedInstance].mutArrSelectedCampaignRadiusListAdded = mutTempRadiousnList;
    
    [AppDelegate sharedInstance].strFilterSearch=@"0";
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnApplyAction:(id)sender {
    NSLog(@"Women array = %@",mutArrSelectedCampaignWomenListAdded);
    NSLog(@"City array = %@",mutArrSelectedCampaignCityListAdded);
    NSLog(@"Radius array = %@",mutArrSelectedCampaignRadiusListAdded);
    
    [AppDelegate sharedInstance].mutArrSelectedCampaignWomenListAdded = mutArrSelectedCampaignWomenListAdded;
    [AppDelegate sharedInstance].mutArrSelectedCampaignCityListAdded = mutArrSelectedCampaignCityListAdded;
    [AppDelegate sharedInstance].mutArrSelectedCampaignRadiusListAdded = mutArrSelectedCampaignRadiusListAdded;
    
    [[AppDelegate sharedInstance].mutArrSubBranchIDS removeAllObjects];
    [[AppDelegate sharedInstance].mutArrCityIDS removeAllObjects];
    [AppDelegate sharedInstance].strPageTitleFilter=@"";
    [[AppDelegate sharedInstance].mutArrRadius removeAllObjects];
    
    [AppDelegate sharedInstance].mutArrRadius=mutArrSelectedCampaignRadiusListAdded;
    
    for (int i=0; i<[mutArrSelectedCampaignWomenListAdded count]; i++) {
        [[AppDelegate sharedInstance].mutArrSubBranchIDS addObject:[[mutArrSelectedCampaignWomenListAdded objectAtIndex:i] objectForKey:@"b_branch_id"]];
    }
    
    for (int i=0; i<[mutArrSelectedCampaignCityListAdded count]; i++) {
        [[AppDelegate sharedInstance].mutArrCityIDS addObject:[[mutArrSelectedCampaignCityListAdded objectAtIndex:i] objectForKey:@"city_id"]];
    }
    NSLog(@"city lat>>>>>>>> : %@", [mutArrSelectedCampaignCityListAdded objectAtIndex:4]);
    
    NSMutableArray *mutArrSubBranchNames = [[NSMutableArray alloc] init];
    for (int i=0; i<[mutArrSelectedCampaignWomenListAdded count]; i++) {
        [mutArrSubBranchNames addObject:[[mutArrSelectedCampaignWomenListAdded objectAtIndex:i] objectForKey:@"b_branch_name"]];
    }
    [AppDelegate sharedInstance].strFilterSearch=@"1";
    [AppDelegate sharedInstance].strPageTitleFilter = [mutArrSubBranchNames componentsJoinedByString:@","];
    [self dismissViewControllerAnimated:YES completion:^{
        if (_popoverDismiss) {
            _popoverDismiss();
        }
    }];
}

- (IBAction)btnWomenAction:(id)sender {
    
    isSearching = NO;
    searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    searchBar.keyboardType = UIKeyboardTypeDefault;
    strSelectedCampaign = _strCatTitle;
    [tblCampaignFliter reloadData];
    searchBar.hidden = YES;
    if (IS_IPHONE) {
        tblCampaignFliter.frame = CGRectMake(140,64,180,460);
        topTableCampaign.constant = -44.0f;
    }else{
        topTableCampaign.constant = 0.0f;
        //        tblCampaignFliter.frame = CGRectMake(tblCampaignFliter.frame.origin.x, 0, tblCampaignFliter.frame.size.width, tblCampaignFliter.frame.size.height);
    }
    [btnWomen setBackgroundColor:[UIColor whiteColor]];
    [btnWomen setTitleColor:[UIColor colorWithRed:42.0/255.0 green:92.0/255.0 blue:230.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [btnCity setBackgroundColor:[UIColor clearColor]];
    [btnCity setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnRadis setBackgroundColor:[UIColor clearColor]];
    [btnRadis setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

- (IBAction)btnCityAction:(id)sender {
    
    isSearching = NO;
    searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    searchBar.keyboardType = UIKeyboardTypeDefault;
    
    searchBar.placeholder = [appDelegate getString:@"Search City"];
    
    strSelectedCampaign = @"City";
    [tblCampaignFliter reloadData];
    searchBar.hidden = NO;
    if (IS_IPHONE) {
        tblCampaignFliter.frame = CGRectMake(140,108,180,460);
        topTableCampaign.constant = 0.0f;
    }else{
        topTableCampaign.constant = 44.0f;
        //        tblCampaignFliter.frame = CGRectMake(tblCampaignFliter.frame.origin.x, 44, tblCampaignFliter.frame.size.width, tblCampaignFliter.frame.size.height);
    }
    [btnCity setBackgroundColor:[UIColor whiteColor]];
    [btnCity setTitleColor:[UIColor colorWithRed:42.0/255.0 green:92.0/255.0 blue:230.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [btnWomen setBackgroundColor:[UIColor clearColor]];
    [btnWomen setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnRadis setBackgroundColor:[UIColor clearColor]];
    [btnRadis setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    //Get Campaign City Deails.
    [self GetCampaignCityFilterWS];
}

- (IBAction)btnRadiusAction:(id)sender {
    
    isSearching = NO;
    searchBar.placeholder = [appDelegate getString:@"km"];
    searchBar.text=@"";
    
    [searchBar resignFirstResponder];
    searchBar.keyboardType = UIKeyboardTypeNumberPad;
    
    strSelectedCampaign = @"Radius";
    [tblCampaignFliter reloadData];
    searchBar.hidden = NO;
    if (IS_IPHONE) {
        tblCampaignFliter.frame = CGRectMake(140,108,180,460);
        topTableCampaign.constant = 0.0f;
    }else{
        topTableCampaign.constant = 44.0f;
        //        tblCampaignFliter.frame = CGRectMake(tblCampaignFliter.frame.origin.x, 44, tblCampaignFliter.frame.size.width, tblCampaignFliter.frame.size.height);
    }
    [btnRadis setBackgroundColor:[UIColor whiteColor]];
    [btnRadis setTitleColor:[UIColor colorWithRed:42.0/255.0 green:92.0/255.0 blue:230.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [btnWomen setBackgroundColor:[UIColor clearColor]];
    [btnWomen setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnCity setBackgroundColor:[UIColor clearColor]];
    [btnCity setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

- (IBAction)btnClearAllAction:(id)sender {
    [mutArrSelectedCampaignCityListAdded removeAllObjects];
    [mutArrSelectedCampaignWomenListAdded removeAllObjects];
    [mutArrSelectedCampaignRadiusListAdded removeAllObjects];
    [tblCampaignFliter reloadData];
}

#pragma mark- UISearchbar Methods

-(void)searchBarSearchButtonClicked:(UISearchBar *)aSearchBar{
    [searchBar resignFirstResponder];
    isSearching = YES;
    if ([strSelectedCampaign isEqualToString:@"City"]) {
        NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"city_name BEGINSWITH[c] %@", aSearchBar.text];
        mutArrCampaignCitySearchList = [[mutArrCampaignCityList filteredArrayUsingPredicate:aPredicate] mutableCopy];
    }else{
        NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS %@", aSearchBar.text];
        mutArrCampaignRadiusSearchList = [[mutArrCampaignRadiusList filteredArrayUsingPredicate:aPredicate] mutableCopy];
    }
    [tblCampaignFliter reloadData];
}

-(void)searchBar:(UISearchBar *)aSearchBar textDidChange:(NSString *)aSearchText{
    if (![aSearchText isValidString]) {
        isSearching = NO;
        [aSearchBar resignFirstResponder];
        [tblCampaignFliter reloadData];
    }else{
        isSearching = YES;
        if ([strSelectedCampaign isEqualToString:@"City"]) {
            NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"city_name BEGINSWITH[c] %@", aSearchText];
            mutArrCampaignCitySearchList = [[mutArrCampaignCityList filteredArrayUsingPredicate:aPredicate] mutableCopy];
        }else{
            NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS %@", aSearchText];
            mutArrCampaignRadiusSearchList = [[mutArrCampaignRadiusList filteredArrayUsingPredicate:aPredicate] mutableCopy];
        }
        [tblCampaignFliter reloadData];
    }
    
    if([aSearchText length] == 0) {
        [searchBar performSelector: @selector(resignFirstResponder) withObject: nil afterDelay: 0.1];
    }
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)aSearchBar{
    //    if ([strSelectedCampaign isEqualToString:@"Radius"]) {
    //        aSearchBar.keyboardType = UIKeyboardTypeNumberPad;
    //    }else{
    //        aSearchBar.keyboardType =UIKeyboardTypeDefault;
    //    }
    //
    //    [aSearchBar setShowsCancelButton:YES animated:YES];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)aSearchBar{
    isSearching = NO;
    [aSearchBar resignFirstResponder];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)aSearchBar{
    [aSearchBar resignFirstResponder];
    if ([aSearchBar.text length] <= 0){
        isSearching = NO;
        [tblCampaignFliter reloadData];
    }else{
        isSearching = YES;
        [tblCampaignFliter reloadData];
    }
}

#pragma mark SetUpPage

-(void)SetUpPage{
    
    self.navigationController.navigationBarHidden = YES;
    NSLog(@"women selected = %@",[AppDelegate sharedInstance].mutArrSubBranchIDS);
    NSLog(@"City selected = %@",[AppDelegate sharedInstance].mutArrCityIDS);
    NSLog(@"radius selected = %@",[AppDelegate sharedInstance].mutArrRadius);
    
    [btnWomen setTitle:_strCatTitle forState:UIControlStateNormal];
    
    mutArrCampaignCitySearchList = [[NSMutableArray alloc] init];
    mutArrCampaignRadiusSearchList = [[NSMutableArray alloc] init];
    
    mutArrCampaignWomenList = [[NSMutableArray alloc] init];
    mutArrCampaignCityList = [[NSMutableArray alloc] init];
    
    mutArrSelectedCampaignWomenListAdded = [[NSMutableArray alloc] init];
    mutArrSelectedCampaignCityListAdded = [[NSMutableArray alloc] init];
    mutArrSelectedCampaignRadiusListAdded = [[NSMutableArray alloc] init];
    
    if ([[AppDelegate sharedInstance].mutArrSelectedCampaignWomenListAdded count] > 0) {
        mutArrSelectedCampaignWomenListAdded = [AppDelegate sharedInstance].mutArrSelectedCampaignWomenListAdded;
    }
    [self GetCampaignFilterWS];
    if ([[AppDelegate sharedInstance].mutArrSelectedCampaignCityListAdded count] > 0) {
        mutArrSelectedCampaignCityListAdded = [AppDelegate sharedInstance].mutArrSelectedCampaignCityListAdded;
    }
    if ([[AppDelegate sharedInstance].mutArrSelectedCampaignRadiusListAdded count] > 0) {
        mutArrSelectedCampaignRadiusListAdded = [AppDelegate sharedInstance].mutArrSelectedCampaignRadiusListAdded;
        
        mutTempRadiousnList=[[NSMutableArray alloc]initWithArray:mutArrSelectedCampaignRadiusListAdded];
    }
    mutArrCampaignRadiusList = [NSMutableArray arrayWithObjects:@"NONE",@"3",@"5",@"10",@"15",@"20",@"25",@"30",@"35",@"50",@"75", nil];
    dicCampaignDetails = [NSMutableDictionary dictionaryWithObjects:@[mutArrCampaignWomenList,mutArrCampaignCityList,mutArrCampaignRadiusList] forKeys:@[@"dicWomenDataList",@"dicCityDataList",@"dicRadiusDataList"]];
    
    strSelectedCampaign = _strCatTitle;
    [btnWomen setBackgroundColor:[UIColor whiteColor]];
    [btnWomen setTitleColor:[UIColor colorWithRed:42.0/255.0 green:92.0/255.0 blue:230.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [btnCity setBackgroundColor:[UIColor clearColor]];
    [btnCity setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnRadis setBackgroundColor:[UIColor clearColor]];
    [btnRadis setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    searchBar.hidden = YES;
    [searchBar setImage:[[UIImage imageNamed: @"ic-nav-search"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    searchBar.backgroundColor = [UIColor whiteColor];
    UITextField *textField = [searchBar searchTextField];
    textField.layer.borderWidth = 0.5f; //To hide the square corners
    textField.layer.borderColor = [[appDelegate colorWithHexString:@"CCCCCC"] CGColor]; //assigning the default border color
    textField.layer.cornerRadius = 2.5f;
    [textField setFont:[UIFont fontWithName:FontRobotoLight size:12]];
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#666666"]}];
//    [searchBar setValue:[UIColor colorWithHexString:@"#666666"] forKeyPath:@"_searchField._placeholderLabel.textColor"];
    searchBar.layer.borderColor = [[UIColor whiteColor] CGColor];//.CGColor;
    searchBar.backgroundColor = [UIColor whiteColor];
    searchBar.layer.borderWidth = 1;
    
    if (IS_IPHONE) {
        topTableCampaign.constant = -44.0f;
        tblCampaignFliter.frame = CGRectMake(140,64,180,460);
    }else{
        topTableCampaign.constant = 0.0f;
        tblCampaignFliter.frame = CGRectMake(tblCampaignFliter.frame.origin.x, 0, tblCampaignFliter.frame.size.width, tblCampaignFliter.frame.size.height);
    }
    
    tblCampaignFliter.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [_btnCancel setTitle:[appDelegate getString:@"Cancel"] forState:UIControlStateNormal];
    [_btnApply setTitle:[appDelegate getString:@"Apply"] forState:UIControlStateNormal];
    [btnCity setTitle:[appDelegate getString:@"City"] forState:UIControlStateNormal];
    [btnRadis setTitle:[appDelegate getString:@"Radius"] forState:UIControlStateNormal];
    [_btnClearAll setTitle:[appDelegate getString:@"Clear all"] forState:UIControlStateNormal];
    _lblNavTitle.text=[appDelegate getString:@"Filter"];
    //Get Comapign Category Deails.
}

#pragma mark- Web Service Methods

- (void)GetCampaignFilterWS{
    
    NSMutableDictionary *aDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        _strCatId,@"cat_id",
                                        [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                        nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCategoryWS withParams:aDictionary showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            if([[responseData objectForKey:@"data"] count]>0){
                mutArrCampaignWomenList = [[responseData objectForKey:@"data"] objectForKey:@"get_sub"];
                NSLog(@"%@",[USERDEFAULTS objectForKey:@"SubBranchId"]);
                if ([[AppDelegate sharedInstance].mutArrSelectedCampaignWomenListAdded count] == 0) {
                    if([[USERDEFAULTS objectForKey:@"SubBranchId"] isEqualToString:@"All"]){
                        for(int i=0;i<[mutArrCampaignWomenList count];i++){
                            [mutArrSelectedCampaignWomenListAdded addObject:[mutArrCampaignWomenList objectAtIndex:i]];
                        }
                    }else{
                        for(int i=0;i<[mutArrCampaignWomenList count];i++){
                            if([[[mutArrCampaignWomenList objectAtIndex:i] objectForKey:@"b_branch_id"] isEqualToString:[USERDEFAULTS objectForKey:@"SubBranchId"]]){
                                [mutArrSelectedCampaignWomenListAdded addObject:[mutArrCampaignWomenList objectAtIndex:i]];
                            }
                        }
                    }
                    [AppDelegate sharedInstance].mutArrSelectedCampaignWomenListAdded = mutArrSelectedCampaignWomenListAdded;
                }
                mutTempWomenList=[[NSMutableArray alloc]initWithArray:mutArrSelectedCampaignWomenListAdded];
                dicCampaignDetails[@"dicWomenDataList"] = mutArrCampaignWomenList;
                [tblCampaignFliter reloadData];
            }
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)GetCampaignCityFilterWS{
    
    NSMutableDictionary *aDictionary = [NSMutableDictionary dictionaryWithObjects:@[@""] forKeys:@[@"city"]];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCityWS withParams:aDictionary showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            mutArrCampaignCityList = [responseData objectForKey:@"data"];
            dicCampaignDetails[@"dicCityDataList"] = mutArrCampaignCityList;
            
            if([mutArrSelectedCampaignCityListAdded count]==0 ){
                if(![[AppDelegate sharedInstance].strCityId isEqualToString:@"0"]){
                    NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"city_id matches[c] %@", [AppDelegate sharedInstance].strCityId];
                    mutArrSelectedCampaignCityListAdded = [[mutArrCampaignCityList filteredArrayUsingPredicate:aPredicate] mutableCopy];
                    mutTempCityList=[[NSMutableArray alloc]initWithArray:mutArrSelectedCampaignCityListAdded];
                }
            }else{
                mutTempCityList=[[NSMutableArray alloc]initWithArray:[AppDelegate sharedInstance].mutArrSelectedCampaignCityListAdded];
            }
            [tblCampaignFliter reloadData];
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
