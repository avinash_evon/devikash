//
//  CampaignFilterCell.h
//  iKash
//
//  Created by indianic on 26/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CampaignFilterButton.h"

@interface CampaignFilterCell : UITableViewCell

@property (strong, nonatomic) IBOutlet CampaignFilterButton *btnFilterSelection;
@property (strong, nonatomic) IBOutlet UILabel *lblFilterTitle;

@end
