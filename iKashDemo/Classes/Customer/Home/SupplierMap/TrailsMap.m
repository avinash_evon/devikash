//
//  TrailsMap.m
//  iKash
//
//  Created by indianic on 04/12/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "TrailsMap.h"

@implementation TrailsMap
@synthesize coordinate,title,image,subtitle;

- (id)initWithLocation:(CLLocationCoordinate2D)coord{
    
    self = [super init];
    if (self) {
        coordinate = coord;
        
    }
    return self;
}

@end
