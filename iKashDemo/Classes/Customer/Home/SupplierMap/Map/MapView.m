//
//  MapViewController.m
//

#import "MapView.h"
#import <CoreLocation/CoreLocation.h>

@interface MapView()

@end

@implementation MapView
@synthesize mapView;

- (id) initWithFrame:(CGRect) frame{
    self = [super initWithFrame:frame];
    mutArrLocationPath = [[NSMutableArray alloc] init];
    if (self != nil) {
        [self initialize];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self initialize];
}

-(void)initialize{
    mutArrLocationPath = [[NSMutableArray alloc] init];
    mapView = [[GMSMapView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    mapView.settings.compassButton = YES;
    mapView.settings.myLocationButton = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView.myLocationEnabled = YES;
    });
    
    [mapView setDelegate:self];
    [self addSubview:mapView];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    mapView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

-(void)setmapFrame:(CGRect) frame{
    mutArrLocationPath = [[NSMutableArray alloc] init];
    if (self != nil) {
        mapView = [[GMSMapView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        mapView.settings.compassButton = YES;
        mapView.settings.myLocationButton = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            mapView.myLocationEnabled = YES;
        });
        
        [mapView setDelegate:self];
        [self addSubview:mapView];
    }
}

-(void)drawPathFrom:(CLLocationCoordinate2D)source toDestination:(CLLocationCoordinate2D)destination{
    NSString *baseUrl = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=true", source.latitude,  source.longitude, destination.latitude,  destination.longitude];
    
    NSURL *url = [NSURL URLWithString:[baseUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    //    NSLog(@"Url: %@", url);
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if(!connectionError){
            NSDictionary *result  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            routes  = [result objectForKey:@"routes"];
            if([routes isKindOfClass:[NSArray class]] && routes.count>0){
                NSDictionary *firstRoute    = [routes objectAtIndex:0];
                NSString *encodedPath       = [firstRoute[@"overview_polyline"] objectForKey:@"points"];
                
                GMSPolyline *polyPath       =  [GMSPolyline polylineWithPath:[GMSPath pathFromEncodedPath:encodedPath]];
                polyPath.strokeColor        = kAppSupportedColorThemeColor;
                polyPath.strokeWidth        = 3.5f;
                polyPath.map                = mapView;
                
                if (!isShowCenter){
                    isShowCenter = YES;
                    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:source coordinate:destination];
                    [mapView moveCamera: [GMSCameraUpdate fitBounds:bounds withPadding:100]];
                }
            }
        }else{
            
        }
    }];
}

-(void)showRouteFromSourceLatitude:(float)srcLat SourceLongitude:(float)srcLong DestinationLatitude:(float)destLat DestinationLongitude:(float)destLong{
    if(routes) {
        [mapView clear];
    }
    CLLocationCoordinate2D aSrcLocation = CLLocationCoordinate2DMake(srcLat,srcLong);
    CLLocationCoordinate2D aDestLocation = CLLocationCoordinate2DMake(destLat,destLong);
    
    // Annotation
    [self addMarkerAtLocation:aDestLocation isDefault:NO withTitle:@""];
    [self drawPathFrom:aSrcLocation toDestination:aDestLocation];
}

-(void)addMarkerAtLocation:(CLLocationCoordinate2D)coordinate  isDefault:(BOOL)flag withTitle:(NSString*)title {
    GMSMarker *aObjMarker;
    aObjMarker = [[GMSMarker alloc] init];
    aObjMarker.flat = YES;
    aObjMarker.groundAnchor = CGPointMake(0.43, 1.0);
    aObjMarker.map = mapView;
    aObjMarker.opacity = 0.99;
    aObjMarker.position = coordinate;
    
    if(![title isEqualToString:@""]){
        aObjMarker.title = title;
    }
    
    [mapView setSelectedMarker:aObjMarker];
    
    if (!flag){
        aObjMarker.icon=[UIImage imageNamed:@"ic-pointer-orange"];}
    
    mapView.camera = [[GMSCameraPosition alloc] initWithTarget:coordinate zoom:10 bearing:0 viewingAngle:10];
}

@end
