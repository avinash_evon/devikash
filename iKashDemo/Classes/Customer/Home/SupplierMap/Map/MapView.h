//
//  MapViewController.h
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Place.h"
#import "PlaceMark.h"


@interface MapView : UIView<GMSMapViewDelegate>
{
    NSMutableArray *mutArrLocationPath;
	NSArray* routes;
    BOOL isShowCenter;
}
@property (nonatomic, strong) GMSMapView *mapView;

- (id) initWithFrame:(CGRect) frame;
-(void)setmapFrame:(CGRect) frame;
-(void)addMarkerAtLocation:(CLLocationCoordinate2D)coordinate  isDefault:(BOOL)flag withTitle:(NSString*)title;
-(void)showRouteFromSourceLatitude:(float)srcLat SourceLongitude:(float)srcLong DestinationLatitude:(float)destLat DestinationLongitude:(float)destLong;

@end
