//
//  ReviewsListCell.h
//  iKashDemo
//
//  Created by IndiaNIC on 09/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DJWStarRatingView.h"

@interface ReviewsListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet DJWStarRatingView *starRatingView;
@property (weak, nonatomic) IBOutlet UILabel *lblReviewDetail;

@end
