//
//  ReviewList.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "ReviewList.h"
#import "ReviewsListCell.h"

@interface ReviewList (){
    NSMutableArray *mutArrReviewList;
    NSString *starRatingValue;
    
    int intCurrentPage,intNextPage;
    BOOL boolWebCallInProgress;
    UIActivityIndicatorView *spinner;
}

@end

@implementation ReviewList

#pragma mark - Synthesize

@synthesize strCampaignId;

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self SetUpPage];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    intCurrentPage=1;
    [AppDelegate sharedInstance].strStarflag=@"0";
    // Add keyboard events observers...
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if(!IS_IPAD){
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Remove keyboard events observers...
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIInterfaceOrientation Methods

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void)showAddReviewPopup {
    [AppDelegate sharedInstance].strStarflag=@"1";
    [UIView animateWithDuration:2.0f animations:^{
        viewAddReview.hidden = NO;
    } completion:nil];
}

- (void)hideAddReviewPopup {
    [AppDelegate sharedInstance].strStarflag=@"0";
    [UIView animateWithDuration:2.0f animations:^{
        viewAddReview.hidden = YES;
    } completion:nil];
}

#pragma mark --------------------------------
#pragma mark UIGestureRecognizer Events

- (void)setupGestures {
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.delegate = self;
    [viewAddReview addGestureRecognizer:tapGestureRecognizer];
}

- (void)handleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer {
    // Hide keyboard on tap event..
    [self btnDoneClick:nil];
    [self hideAddReviewPopup];
}

# pragma mark -----------------------
# pragma mark UIGestureRecognizerDelegate Events

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    // Disable tap gesture for textView...
    if ([touch.view isDescendantOfView:txtViewAddReview] || [touch.view isDescendantOfView:AddstarRatingView] || [touch.view isDescendantOfView:btnSubmitReview]) {
        return NO;
    }
    return YES;
}

#pragma mark --------------------------------
#pragma mark UIKeyboardNotification Methods

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *aDictInfo = [notification userInfo];
    //    NSValue *kbFrame = [aDictInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[aDictInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    //    CGRect keyboardFrame = [kbFrame CGRectValue];
    
    //    CGFloat height = keyboardFrame.size.height;
    
    // Animate view upside...
    //    if (IS_IPHONE) {
    //        self.view.frame = CGRectMake(0, -167, self.view.frame.size.width, self.view.frame.size.height);
    //    }else{
    //        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    //    }
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary *info = [notification userInfo];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    // Animate view to original position...
    //viewContentTopConstraint.constant = 0;
    //    if (IS_IPHONE) {
    //        self.view.frame = CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height);
    //    }else{
    //        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    //    }
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark- UIButton Click Action Methods

- (IBAction)btnActionToBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnActionAddReview:(id)sender {
    int intCustomerId=[[USERDEFAULTS objectForKey:@"customer_id"] intValue];
    if(intCustomerId==0){
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        objPopOverAlertVC = [aStoryboard instantiateViewControllerWithIdentifier:@"PopOverAlertVC"];
        [[AppDelegate sharedInstance].window addSubview:objPopOverAlertVC.view];
    }else{
        [self showAddReviewPopup];
    }
}

- (IBAction)btnActionSubmitReview:(id)sender {
    if (starRatingValue.length == 0) {
        [appDelegate showAlertView:[appDelegate getString:@"SELECT_REVIEW_POINTS"] Tag:@"0"];
        return;
    }else if (![txtViewAddReview.text isValidString]) {
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_REVIEW"] Tag:@"0"];
        return;
    }
    [txtViewAddReview resignFirstResponder];
    // Hide keyboard on tap event..
    //[self btnDoneClick:nil];
    
    [self AddReviewWS];
}

- (IBAction)btnDoneClick:(id)sender {
    // Hide keyboard on tap event..
    if ([txtViewAddReview isFirstResponder]) {
        [txtViewAddReview resignFirstResponder];
    }
}

#pragma mark --------------------------------
#pragma mark UITextView Delegate Methods

- (void)textViewDidChange:(id)sender {
    if ([txtViewAddReview.text isValidString]) {
        // Enable submit button...
        btnSubmitReview.enabled = YES;
    }else{
        // Disable submit button...
        btnSubmitReview.enabled = NO;
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [mutArrReviewList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ReviewsListCell *reviewsListCell = (ReviewsListCell*)[tableView dequeueReusableCellWithIdentifier:@"ReviewsListCell"];
    [reviewsListCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if ([[[mutArrReviewList objectAtIndex:indexPath.row] objectForKey:@"c_first_name"] isEqualToString:@""]) {
        reviewsListCell.lblName.text = @"Anonymouse";
    }else{
        reviewsListCell.lblName.text = [[mutArrReviewList objectAtIndex:indexPath.row] objectForKey:@"c_first_name"];
    }
    
    reviewsListCell.lblReviewDetail.text = [[mutArrReviewList objectAtIndex:indexPath.row] objectForKey:@"r_review"];
    
    NSData *dataa = [reviewsListCell.lblReviewDetail.text dataUsingEncoding:NSUTF8StringEncoding];
    NSString *valueEmoj = [[NSString alloc] initWithData:dataa encoding:NSNonLossyASCIIStringEncoding];
    
    reviewsListCell.lblReviewDetail.text = valueEmoj;
    
    // reviewsListCell.lblReviewDetail.text = @"have enough space (dynamic space) size display of reviews Allow smileys . Please consult Sujal for YOLO code changed from Trushal Shah to Zalak Patel changed from New to Feedback";
    
    //[AppDelegate sharedInstance].strStarflag=@"0";
    float aFloatStar=[[[mutArrReviewList objectAtIndex:indexPath.row] objectForKey:@"r_rating"]floatValue];
    reviewsListCell.starRatingView.starSize=CGSizeMake(9, 9);
    reviewsListCell.starRatingView.numberOfStars=5;
    reviewsListCell.starRatingView.rating=aFloatStar;
    reviewsListCell.starRatingView.padding=5.0;
    reviewsListCell.starRatingView.fillColor=[appDelegate colorWithHexString:@"fe7935"];
    
    reviewsListCell.starRatingView.unfilledColor=[UIColor clearColor];
    reviewsListCell.starRatingView.strokeColor=[appDelegate colorWithHexString:@"fe7935"];
    return reviewsListCell;
}

#pragma mark - Webservice Methods

- (void)CampaignReviewWS {
    if([self NointerNetCheking]){
        NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      strCampaignId,@"campaign_id",
                                      [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                      nil];
        [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCampaignReviewWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            NSLog(@"customer_GetCampaignReviewWS responseData = %@", responseData);
            if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
                if(intCurrentPage==1){
                    [mutArrReviewList removeAllObjects];
                }
                
                mutArrReviewList = [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
                
                intNextPage = [[[responseData objectForKey:@"settings"] objectForKey:@"next_page"] intValue];
                boolWebCallInProgress=NO;
                intCurrentPage++;
                [_tblViewReviewsList reloadData];
                _lblNoRecordFound.hidden = YES;
                _tblViewReviewsList.hidden = NO;
                if (mutArrReviewList.count == 0) {
                    _lblNoRecordFound.hidden = NO;
                }
            }else{
                _tblViewReviewsList.hidden = YES;
                _lblNoRecordFound.hidden = NO;
            }
            
            if ([mutArrReviewList count]>0) {
                [self SetAnimation];
            }
        } withFailureBlock:^(NSError *error) {
            _tblViewReviewsList.hidden=TRUE;
            _viewNoInternet.hidden=FALSE;
            _lblNoInternet.text=[appDelegate getString:@"WEBSERVICEISSUE"];
            NSLog(@"%@",error);
        }];
    }
}

#pragma mark - Pagination Scrollview

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(intNextPage>0){
        [self CallStartAnimating];
    }else{
        return;
    }
    
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    if(boolWebCallInProgress == NO){
        if (maximumOffset - currentOffset == 0){
            if(intNextPage>0){
                boolWebCallInProgress = YES;
                [_tblViewReviewsList setContentOffset:CGPointMake(0, scrollView.contentSize.height-scrollView.frame.size.height) animated:YES];
                [self performSelector:@selector(DelayWebservice) withObject:nil afterDelay:3.0];
            }else{
                [self CallStoptAnimating];
            }
        }else{
            [self CallStoptAnimating];
        }
    }else{
        [self CallStoptAnimating];
    }
}

-(void)SetAnimation{
    
    _tblViewReviewsList.tableFooterView = nil;
    if (spinner) {
        spinner = nil;
    }
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
    //[spinner startAnimating];
    spinner.hidesWhenStopped = YES;
    spinner.frame = CGRectMake(0, 0, 320, 44);
    _tblViewReviewsList.tableFooterView = spinner;
}

-(void)DelayWebservice{
    [spinner stopAnimating];
    [self CampaignReviewWS];
}

-(void)CallStartAnimating{
    [spinner startAnimating];
}

-(void)CallStoptAnimating{
    [spinner stopAnimating];
}

#pragma mark - Webservice Methods

- (void)AddReviewWS {
    
    NSString *aStarRating = [NSString stringWithFormat:@"%@", starRatingValue];
    
    NSData *data = [txtViewAddReview.text dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *valueUnicode = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  strCampaignId,@"campaign_id",
                                  aStarRating,@"frating",
                                  valueUnicode,@"review",
                                  @"Nice",@"review_headline",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_AddReviewWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            AddstarRatingView.rating=0.0;
            txtViewAddReview.text = @"";
            [UIView animateWithDuration:2.0f animations:^{
                viewAddReview.hidden = YES;
            } completion:nil];
            [self CampaignReviewWS];
        }else{
            AddstarRatingView.rating=0.0;
            txtViewAddReview.text = @"";
            viewAddReview.hidden = YES;
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        viewAddReview.hidden = YES;
        NSLog(@"%@",error);
    }];
}

#pragma mark SetUpPage

-(void)SetUpPage{
    
    self.lblNavTitle.text = [appDelegate getString:@"Reviews"];
    _lblNoRecordFound.text = [appDelegate getString:@"BE_THE_FIRST_REVIEW"];
    
    [self CampaignReviewWS];
    
    // Setup textViewBG rounded rect...
    viewTxtViewAddReviewBg.layer.cornerRadius = 4.0;
    
    // Intially disable submit review button...
    [btnSubmitReview setEnabled:YES];
    
    // Setup Gesture in viewAddReview...
    [self setupGestures];
    
    // Set Done button in txtViewAddReview...
    toolBarTextView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    NSMutableArray *items = [[NSMutableArray alloc] init];
    UIBarButtonItem *aBarBtnFlexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *aBarBtnDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(btnDoneClick:)];
    aBarBtnDone.tintColor = [UIColor blackColor];
    [items addObject:aBarBtnFlexibleItem];
    [items addObject:aBarBtnDone];
    [toolBarTextView setItems:items animated:NO];
    txtViewAddReview.inputAccessoryView = toolBarTextView;
    
    [btnSubmitReview setTitle:[appDelegate getString:@"Submit review"] forState:UIControlStateNormal];
    btnSubmitReview.layer.cornerRadius = 4.0;
    // Hide bottom seperators for tableView...
    self.tblViewReviewsList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //int aintStar=[[[mutArrReviewList objectAtIndex:indexPath.row] objectForKey:@"r_rating"]intValue];
    AddstarRatingView.starSize=CGSizeMake(30, 30);
    AddstarRatingView.numberOfStars=5;
    AddstarRatingView.rating=0.0;
    AddstarRatingView.padding=8.0;
    AddstarRatingView.fillColor=[appDelegate colorWithHexString:@"fe7935"];
    AddstarRatingView.unfilledColor=[UIColor clearColor];
    AddstarRatingView.strokeColor=[appDelegate colorWithHexString:@"fe7935"];
    AddstarRatingView.editable = YES;
    AddstarRatingView.delegate = self;
    AddstarRatingView.userInteractionEnabled=TRUE;
    AddstarRatingView.allowsSwipeWhenEditable = TRUE;
    AddstarRatingView.allowsHalfIntegralRatings = TRUE;
}

- (void)djwStarRatingChangedValue:(DJWStarRatingView *)view{
    starRatingValue = [NSString stringWithFormat:@"%.1f", view.rating];
}

-(BOOL)NointerNetCheking{
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        _tblViewReviewsList.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        return FALSE;
    }else{
        _tblViewReviewsList.hidden=FALSE;
        _viewNoInternet.hidden=TRUE;
        return TRUE;
    }
}
@end
