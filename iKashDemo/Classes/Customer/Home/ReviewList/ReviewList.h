//
//  ReviewList.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DJWStarRatingView.h"
#import "CustomButton.h"
#import "PopOverAlertVC.h"

@interface ReviewList : UIViewController<UIGestureRecognizerDelegate, UITextViewDelegate,DJWStarRatingViewDelegate> {
    
    IBOutlet UIView *viewAddReview;
    IBOutlet DJWStarRatingView *AddstarRatingView;
    IBOutlet UIView *viewTxtViewAddReviewBg;
    IBOutlet UITextView *txtViewAddReview;
    IBOutlet UIButton *btnSubmitReview;
    PopOverAlertVC *objPopOverAlertVC;
    UIToolbar *toolBarTextView;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (strong, nonatomic) IBOutlet UILabel *lblNavTitle;

@property (strong, nonatomic) IBOutlet UITableView *tblViewReviewsList;
@property (strong, nonatomic) NSString *strCampaignId;
@property (strong, nonatomic) IBOutlet UILabel *lblNoRecordFound;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;
- (IBAction)btnActionSubmitReview:(id)sender;

@end
