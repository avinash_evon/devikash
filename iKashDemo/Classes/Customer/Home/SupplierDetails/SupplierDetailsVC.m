//
//  SupplierDetailsVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "SupplierDetailsVC.h"
#import "SupplierMapVC.h"
#import "CustomWebViewVC.h"

@interface SupplierDetailsVC (){
    NSMutableArray *mutArrSupplier;
}

@end

@implementation SupplierDetailsVC

#pragma mark - Synthesize

@synthesize strSupplierId;

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    mutArrSupplier=[[NSMutableArray alloc]init];
    if (IS_IPHONE) {
        // Hide tabBar...
        //        self.tabBarController.tabBar.hidden = YES;
        self.lblNavTitle.text = [appDelegate getString:@"Supplier Detail"];
    }
//    _scrllView.hidden = YES;
    [self SupplierDetailsWS];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(!IS_IPAD){
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

#pragma mark UIInterfaceOrientation Methods

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

#pragma mark- UIButton Click Action Methods

- (IBAction)BtnActionToClickBack:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnActionToGoMapClick:(id)sender {
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusNotDetermined: {
            UIAlertView  *alert = [[UIAlertView alloc] initWithTitle:[appDelegate getString:@"App Permission Denied"]
                                                             message:[appDelegate getString:@"Please go to Settings and turn on Location Service for iKash app."]
                                                            delegate:nil
                                                   cancelButtonTitle:[appDelegate getString:@"OK"]
                                                   otherButtonTitles:nil];
            [alert show];
        }
            break;
        case kCLAuthorizationStatusDenied: {
            UIAlertView  *alert = [[UIAlertView alloc] initWithTitle:[appDelegate getString:@"App Permission Denied"]
                                                             message:[appDelegate getString:@"Please go to Settings and turn on Location Service for iKash app."]
                                                            delegate:nil
                                                   cancelButtonTitle:[appDelegate getString:@"OK"]
                                                   otherButtonTitles:nil];
            [alert show];
        }
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            SupplierMapVC *objSupplierMapVC = [storyboard instantiateViewControllerWithIdentifier:@"SupplierMapVC"];
            objSupplierMapVC.doubleLatitude=[[[mutArrSupplier objectAtIndex:0] objectForKey:@"o_latitude"] doubleValue];
            objSupplierMapVC.doubleLongitude=[[[mutArrSupplier objectAtIndex:0] objectForKey:@"o_longitude"] doubleValue];
            // Delhi 28.6139° N, 77.2090° E
            [self.navigationController pushViewController:objSupplierMapVC animated:YES];
        }
            break;
        case kCLAuthorizationStatusAuthorizedAlways: {
            // clear text
            //            label.text = @"";
            //            [locationManager startUpdatingLocation]; //Will update location immediately
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            SupplierMapVC *objSupplierMapVC = [storyboard instantiateViewControllerWithIdentifier:@"SupplierMapVC"];
            objSupplierMapVC.doubleLatitude=[[[mutArrSupplier objectAtIndex:0] objectForKey:@"o_latitude"] doubleValue];
            objSupplierMapVC.doubleLongitude=[[[mutArrSupplier objectAtIndex:0] objectForKey:@"o_longitude"] doubleValue];
            // Delhi 28.6139° N, 77.2090° E
            [self.navigationController pushViewController:objSupplierMapVC animated:YES];
        }
            break;
        default:
            break;
    }
}

- (IBAction)btnActionToOpenLinkClick:(id)sender {
    if (![_lblWebsite.text isEqualToString:@""]) {
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        CustomWebViewVC *objCustomWebViewVC = [aStoryboard instantiateViewControllerWithIdentifier:@"CustomWebViewVC"];
        objCustomWebViewVC.strURL = _lblWebsite.text;
        objCustomWebViewVC.modalPresentationStyle = UIModalPresentationCustom;
        [self presentViewController:objCustomWebViewVC animated:YES completion:nil];
    }
}

#pragma mark - Webservice Methods

- (void)SupplierDetailsWS {
    
    if (![self NointerNetCheking]) { return; }
    
        NSLog(@"%@",[USERDEFAULTS objectForKey:@"Language"]);
        NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      strSupplierId,@"supplier_id",
                                      _strCampaignId,@"campaign_id",
                                      [USERDEFAULTS objectForKey:@"Langauge"],@"lang_id",
                                      nil];
        NSLog(@"customer_GetSupplierDetailsWS parameters = %@",aDict);
        [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetSupplierDetailsWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            
            
            NSLog(@"customer_GetSupplierDetailsWS responseData = %@", responseData);
            if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
                mutArrSupplier=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedInstance].window animated:YES];
                    _scrllView.hidden = NO;
                    [self FillTextFields];
                });
            }else{
                [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
            }
        } withFailureBlock:^(NSError *error) {
            _lblSupplierName.hidden=TRUE;
            _lblDescription.hidden=TRUE;
            _lblWebsite.hidden=TRUE;
            _lblLocationAddress.hidden=TRUE;
            _viewNoInternet.hidden=FALSE;
            _imgViewMapImage.hidden=TRUE;
            _btnMap.hidden=TRUE;
            _lblNoInternet.text=[appDelegate getString:@"WEBSERVICEISSUE"];
            NSLog(@"%@",error);
        }];
    
}

#pragma mark SetUpPage

-(void)FillTextFields{
    if([mutArrSupplier count]>0){
    
        //Set Map
        double sourceLat =[[[mutArrSupplier objectAtIndex:0] objectForKey:@"o_latitude"] doubleValue];
        double sourceLong =[[[mutArrSupplier objectAtIndex:0] objectForKey:@"o_longitude"] doubleValue];
        
        NSString* aSupplierName = [[mutArrSupplier objectAtIndex:0] objectForKey:@"s_company_name"];
        
        _mapView.mapView.settings.compassButton = NO;
        _mapView.mapView.settings.myLocationButton = NO;
        [_mapView addMarkerAtLocation:CLLocationCoordinate2DMake(sourceLat, sourceLong) isDefault:YES withTitle:aSupplierName];
        
        //Map
        
        
        _lblSupplierName.text = aSupplierName;
        
        _lblDescription.text= [[mutArrSupplier objectAtIndex:0] objectForKey:@"c_description"] == nil ? @"" : [appDelegate stringByStrippingHTML: [[mutArrSupplier objectAtIndex:0] objectForKey:@"c_description"]];
        _lblWebsite.text=[[mutArrSupplier objectAtIndex:0] objectForKey:@"o_website"];
//        _lblLocationAddress.text=[[mutArrSupplier objectAtIndex:0] objectForKey:@"s_address"];
        _lblLocationAddress.text=[[mutArrSupplier objectAtIndex:0] objectForKey:@"o_address"];
        _lblPhoneNo.text = [[mutArrSupplier objectAtIndex:0] objectForKey:@"s_phone"];
        
        if([[[mutArrSupplier objectAtIndex:0] objectForKey:@"o_type"] isEqualToString:@"Website Address"]){
            lblDescBottomConstraint.constant = 0;
            _imgViewMapImage.hidden=TRUE;
            _btnMap.hidden=TRUE;
        }else{
            lblDescBottomConstraint.constant = 110;
            _imgViewMapImage.hidden=FALSE;
            _btnMap.hidden=FALSE;
        }
        
        NSURL *urlImage = [[mutArrSupplier objectAtIndex:0] objectForKey:@"s_logo"];
        [_imgViewSupplierLogo setContentMode:UIViewContentModeScaleAspectFit];
        
        [_imgViewSupplierLogo sd_setImageWithURL:urlImage
                                placeholderImage:nil
                                         options:SDWebImageProgressiveDownload
                                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                        }
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           _imgViewSupplierLogo.clipsToBounds = YES;
                                       }];
        
    }
}

-(void)fillData{
    _lblSupplierName.text=[[mutArrSupplier objectAtIndex:0] objectForKey:@"s_company_name"];
//    _lblDescription.text= [appDelegate stringByStrippingHTML: [[mutArrSupplier objectAtIndex:0] objectForKey:@"c_description"]];
    _lblWebsite.text=[[mutArrSupplier objectAtIndex:0] objectForKey:@"s_website"];
    _lblLocationAddress.text=[[mutArrSupplier objectAtIndex:0] objectForKey:@"s_address"];
    _lblPhoneNo.text = [[mutArrSupplier objectAtIndex:0] objectForKey:@"s_phone"];

}


-(BOOL)NointerNetCheking{
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        _lblSupplierName.hidden=TRUE;
        _lblDescription.hidden=TRUE;
        _lblWebsite.hidden=TRUE;
        _lblLocationAddress.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        _imgViewMapImage.hidden=TRUE;
        _btnMap.hidden=TRUE;
        return FALSE;
    }else{
        _lblSupplierName.hidden=FALSE;
        _lblDescription.hidden=FALSE;
        _lblWebsite.hidden=FALSE;
        _lblLocationAddress.hidden=FALSE;
        _imgViewMapImage.hidden=FALSE;
        _btnMap.hidden=FALSE;
        _viewNoInternet.hidden=TRUE;
        return TRUE;
    }
}

@end
