//
//  SidePenalCollectionCell.h
//  iKash
//
//  Created by indianic on 27/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DJWStarRatingView.h"
@interface SidePenalCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imgCampaign;
@property (weak, nonatomic) IBOutlet DJWStarRatingView *starRatingView;
@property (weak, nonatomic) IBOutlet UIImageView *imgCampaignBranch;
@property (weak, nonatomic) IBOutlet UILabel *lblColour;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@end
