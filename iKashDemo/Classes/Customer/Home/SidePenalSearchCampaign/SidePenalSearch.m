//
//  SidePenalSearch.m
//  iKash
//
//  Created by indianic on 27/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "SidePenalSearch.h"
#import "SidePenalCollectionCell.h"
#import "CampaignDetailsVC.h"
#import "LeftFooterCell.h"
#import "CampaignCollectionCell.h"

@interface SidePenalSearch (){
    
    NSString *strSubCategory;
    NSString *strSubCategoryNames;
    int intCurrentPage,intNextPage;
    BOOL boolWebCallInProgress;
    UIActivityIndicatorView *spinner;
}

@end

@implementation SidePenalSearch

#pragma mark - Synthesize

@synthesize mutAllCampaign;

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    mutAllCampaign=[[NSMutableArray alloc]init];
    strSubCategory = [[AppDelegate sharedInstance].mutArrSidePenalCategoryList componentsJoinedByString:@","];
    strSubCategoryNames = [[AppDelegate sharedInstance].mutArrSidePenalCategoryName componentsJoinedByString:@","];
    boolWebCallInProgress=YES;
    
    //Register cell from XIB
    [self.collectionViewAll registerNib:[UINib nibWithNibName:tileCellXibNibName bundle:nil] forCellWithReuseIdentifier:tileCellReuseIdentifier];
    
    [self GetSearchCampaignWS];
    [self SetNavigationHeader];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    intCurrentPage=1;
    _lblNoCampaign.hidden=TRUE;
    if(!IS_IPAD){
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark UIInterfaceOrientation Methods
- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (IBAction)btnActionToBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

# pragma mark - CollectionView Delegate Method

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [mutAllCampaign count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    CampaignCollectionCell *shopCollectionViewCell = (CampaignCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:tileCellReuseIdentifier forIndexPath:indexPath];
    
    [shopCollectionViewCell setDataWithDic:[mutAllCampaign objectAtIndex:indexPath.row]];
    
    return shopCollectionViewCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (IS_IPAD) {
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        CampaignDetailsVC *objCampaignDetailsVC = [aStoryboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
        objCampaignDetailsVC.strCampaignId =  [[mutAllCampaign objectAtIndex:indexPath.row] objectForKey:@"c_campaign_id"] ;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objCampaignDetailsVC];
        navigationController.navigationBar.hidden = YES;
        self.campaignDetailsPopover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
        self.campaignDetailsPopover.delegate = self;
        self.campaignDetailsPopover.popoverContentSize = CGSizeMake(370.0, 645.0);
        self.view.alpha = 0.6;
        [self.campaignDetailsPopover presentPopoverFromRect:[_btnPopupView bounds]
                                                     inView:_btnPopupView
                                   permittedArrowDirections:0
                                                   animated:YES];
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        CampaignDetailsVC *objCampaignDetailsVC = [storyboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
        objCampaignDetailsVC.strCampaignId =  [[mutAllCampaign objectAtIndex:indexPath.row] objectForKey:@"c_campaign_id"] ;
        [self.navigationController pushViewController:objCampaignDetailsVC animated:YES];
    }
}

-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    self.view.alpha = 1.0;
    //    self.campaignDetailsPopover.delegate = nil;
    return YES;
}

#pragma mark - UICollectionViewFlowLayout methods

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    if (IS_IPHONE) {
//        CGRect screenRect = [[UIScreen mainScreen] bounds];
//        CGFloat screenWidth = screenRect.size.width;
//        CGFloat screenHeight = screenRect.size.width;
//        float gridColumn;
//        float gridRow;
//        
//        UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
//        if(UIInterfaceOrientationIsPortrait(orientation)){
//            gridRow = 2.0;
//            gridColumn = 1.5;
//        }else{
//            gridRow = 3.0;
//            gridColumn = 2.5;
//        }
//        float cellWidth = (screenWidth / gridRow)-22.5; //Replace the divisor with the column count requirement. Make sure to have it in float.
//        float cellHeight = (screenHeight / gridColumn)-22.5; //Replace the divisor with the column count requirement. Make sure to have it in float.
//        CGSize size = CGSizeMake(cellWidth, cellHeight + 30); // to set Description lable in 3 lines
//        return size;
//    }else{
//        CGRect screenRect = [[UIScreen mainScreen] bounds];
//        CGFloat screenWidth = screenRect.size.width;
//        CGFloat screenHeight = screenRect.size.width;
//        float gridColumn;
//        float gridRow;
//        
//        UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
//        if(UIInterfaceOrientationIsPortrait(orientation)){
//            gridRow = 4.0;
//            gridColumn = 3.2;
//        }else{
//            gridRow = 5.0;
//            gridColumn = 4.2;
//        }
//        float cellWidth = (screenWidth / gridRow)-18; //Replace the divisor with the column count requirement. Make sure to have it in float.
//        float cellHeight = (screenHeight / gridColumn)-18; //Replace the divisor with the column count requirement. Make sure to have it in float.
//        CGSize size = CGSizeMake(cellWidth, cellHeight + 30); // to set Description lable in 3 lines
//        return size;
//    }
//}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (IS_IPHONE) {
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }else{
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (IS_IPAD) {
        return 15;
    }else{
        return 15;
    }
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [self.collectionViewAll.collectionViewLayout invalidateLayout];
}

-(void)SetNavigationHeader{
    self.lblNavTitle.text = strSubCategoryNames;
}

#pragma mark - Webservice Methods

- (void)GetSearchCampaignWS{
    
    if([self NointerNetCheking]){
        NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      strSubCategory,@"sub_branch_id",
                                      [USERDEFAULTS objectForKey:@"latitude"],@"latitude",
                                      [USERDEFAULTS objectForKey:@"longitude"],@"longitude",
                                      @"",@"branch_id",
                                      [AppDelegate sharedInstance].strCityId,@"city_id",
                                      @"1",@"is_city",
                                      @"",@"radius",
                                      [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                      nil];
        NSLog(@"customer_GetCampaignWS parameters = %@",aDict);
        [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCampaignWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            
            NSLog(@"customer_GetCampaignWS responseData = %@", responseData);
            if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
                [mutAllCampaign removeAllObjects];
                NSMutableArray *mutArrSubCampaign=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
                for(int i=0;i<[mutArrSubCampaign count];i++){
                    if([[[mutArrSubCampaign objectAtIndex:i] objectForKey:@"city_wise_campaign"] count]>0){
                        NSMutableArray *mutSubCampaign=[[NSMutableArray alloc]initWithArray:[[mutArrSubCampaign objectAtIndex:i] objectForKey:@"city_wise_campaign"]];
                        for(int j=0;j<[mutSubCampaign count];j++){
                            [mutAllCampaign addObject:[mutSubCampaign objectAtIndex:j]] ;
                        }
                    }
                }
                _lblNoCampaign.hidden=TRUE;
                _collectionViewAll.hidden = NO;
                [_collectionViewAll reloadData];
                
            }else{
                [mutAllCampaign removeAllObjects];
                NSMutableArray *mutArrSubCampaign=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
                for(int i=0;i<[mutArrSubCampaign count];i++){
                    if([[[mutArrSubCampaign objectAtIndex:i] objectForKey:@"city_wise_campaign"] count]>0){
                        NSMutableArray *mutSubCampaign=[[NSMutableArray alloc]initWithArray:[[mutArrSubCampaign objectAtIndex:i] objectForKey:@"city_wise_campaign"]];
                        for(int j=0;j<[mutSubCampaign count];j++){
                            [mutAllCampaign addObject:[mutSubCampaign objectAtIndex:j]] ;
                        }
                    }
                }
                if(mutAllCampaign.count>0){
                    _lblNoCampaign.hidden=TRUE;
                    _collectionViewAll.hidden = NO;
                    [_collectionViewAll reloadData];
                    if([[[responseData objectForKey:@"settings"] objectForKey:@"success"] isEqualToString:@"100"]){
                        [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
                    }else{
                        [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:@"50"];
                    }
                }else{
                    _lblNoCampaign.hidden=FALSE;
                    _collectionViewAll.hidden = YES;
                }
            }
        } withFailureBlock:^(NSError *error) {
            _collectionViewAll.hidden=TRUE;
            _viewNoInternet.hidden=FALSE;
            _lblNoInternet.text=[appDelegate getString:@"WEBSERVICEISSUE"];
            NSLog(@"%@",error);
        }];
    }
}

#pragma mark - Pagination Scrollview

-(void)DelayWebservice{
    [spinner stopAnimating];
    [self GetSearchCampaignWS];
}

-(void)CallStartAnimating{
    [spinner startAnimating];
}

-(void)CallStoptAnimating{
    [spinner stopAnimating];
}

-(BOOL)NointerNetCheking{
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        _collectionViewAll.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        return FALSE;
    }else{
        _collectionViewAll.hidden=FALSE;
        _viewNoInternet.hidden=TRUE;
        return TRUE;
    }
}

@end
