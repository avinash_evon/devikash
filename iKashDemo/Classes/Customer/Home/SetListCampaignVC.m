//
//  SetListCampaignVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "SetListCampaignVC.h"
#import "YSLContainerViewController.h"
#import "CampaignListVC.h"
#import "GlobalSearchLocationVC.h"
#import "NotificationVC.h"
#import "CarbonKit.h"
#import "PopOverAlertVC.h"
#import "SidePenalSearch.h"
#import "FilterHomePageVC.h"
#import "AppDelegate.h"

@interface SetListCampaignVC ()<YSLContainerViewControllerDelegate, CarbonTabSwipeDelegate>{
    CarbonTabSwipeNavigation *tabSwipe;
    NSMutableArray *mutArrCategoryList;
    NSMutableArray *arrCatName;
    NSMutableArray *arrDeactiveCatName;
    NSArray *arrCombinedCatName;
    NSMutableArray *branchID1;
    NSMutableArray *branchID2;
     NSArray *arrCombinedBranchID;
    NSArray *arrCombinedBranchName;
    NSMutableArray *ActiveBranchNameArr;
     NSMutableArray *InactiveBranchNameArr;
    
    int intSelectedIndex;
}

@end

@implementation SetListCampaignVC

- (void)viewDidLoad {
    [super viewDidLoad];
    intSelectedIndex=0;
    
    ActiveBranchNameArr = [[NSMutableArray alloc]init];
     InactiveBranchNameArr = [[NSMutableArray alloc]init];
    
    branchID1 = [[NSMutableArray alloc]init];
     branchID2 = [[NSMutableArray alloc]init];
    
    [self SetUpTabBar];
    
    UIColor *color = [UIColor whiteColor];//self.navigationController.navigationBar.barTintColor;
    arrCatName=[[NSMutableArray alloc]init];
    arrDeactiveCatName = [[NSMutableArray alloc]init];
    arrCombinedCatName = [[NSMutableArray alloc]init];
    arrCombinedBranchID = [[NSArray alloc]init];
    arrCombinedBranchName = [[NSArray alloc]init];
    
   NSLog(@"my setting Flag %@",[AppDelegate sharedInstance].SettingFlagForLogn);
    if([[AppDelegate sharedInstance].SettingFlagForLogn isEqualToString:@"0"])
    {
          [arrCatName removeAllObjects];
    NSString *strAll=@"My Categories";
    [arrCatName addObject:strAll];
    }
    else{
        
        [arrCatName removeAllObjects];
        
        NSString *strAll=@"All Categorie";
           [arrCatName addObject:strAll];
        
    }
    mutArrCategoryList=[[NSMutableArray alloc]initWithArray:[USERDEFAULTS objectForKey:@"CategoryList"]];
    
    
     NSLog(@"sub categories : %@",[[[mutArrCategoryList objectAtIndex:0] objectForKey: @"first_child"]objectAtIndex:1]);
    
    NSLog(@"sub categories @@@@@@ : %@",[[mutArrCategoryList objectAtIndex:0] objectForKey: @"first_child"]);
    
     NSLog(@"sub categories element @@@@@@ : %@",[[[[mutArrCategoryList objectAtIndex:0] objectForKey: @"first_child"]objectAtIndex:0]objectForKey:@"c_branch_name"]);
    
    
    
  //  NSLog(@"swime elements %@",[[mutArrCategoryList objectAtIndex:0]objectForKey:@"num_of_camp"]);
    
    for(int i=0;i<[mutArrCategoryList count];i++){
        
    //    branch_id
        
//         for(int j=0;j<[[[mutArrCategoryList objectAtIndex:i] objectForKey: @"first_child"]count];j++)
//         {
//             NSLog(@"sub branch categories >>>>>>>: %@",[[[[mutArrCategoryList objectAtIndex:i] objectForKey: @"first_child"]objectAtIndex:j]objectForKey:@"c_branch_name"]);
//
//             if([[[[mutArrCategoryList objectAtIndex:i] objectForKey: @"first_child"]objectAtIndex:j]objectForKey:@"c_branch_name"])
//         }
    //    [[[[[mutArrCategoryList objectAtIndex:i] objectForKey: @"first_child"]objectAtIndex:j] objectForKey:@"c_branch_name"] objectAtIndex:1]);
        
        NSLog(@">>>>>>branch id : %@",[[mutArrCategoryList objectAtIndex:i]objectForKey:@"branch_id"]);
        
         NSLog(@"swipe elements %@",[[mutArrCategoryList objectAtIndex:i]objectForKey:@"num_of_camp"]);
        
        if([(NSNumber*)[[mutArrCategoryList objectAtIndex:i]objectForKey:@"num_of_camp"]intValue] > 0)
        {
            
            [ActiveBranchNameArr addObject:[[mutArrCategoryList objectAtIndex:i]objectForKey:@"branch_name"]];
            
    [branchID1 addObject:[[mutArrCategoryList objectAtIndex:i]objectForKey:@"branch_id"]];
            [arrCatName addObject:[[mutArrCategoryList objectAtIndex:i] objectForKey:@"branch_name"]];
            NSLog(@"index wise swipe category %@",[[mutArrCategoryList objectAtIndex:i] objectForKey:@"branch_name"]);
    }
        else{
            
               [InactiveBranchNameArr addObject:[[mutArrCategoryList objectAtIndex:i]objectForKey:@"branch_name"]];
            
             [branchID2 addObject:[[mutArrCategoryList objectAtIndex:i]objectForKey:@"branch_id"]];
            
            [arrDeactiveCatName addObject:[[mutArrCategoryList objectAtIndex:i]objectForKey:@"branch_name"]];
        }
    }
    
    NSLog(@"active branch name %@",ActiveBranchNameArr);
     NSLog(@"inactive branch name %@",InactiveBranchNameArr);
    
arrCombinedBranchName = [arrCombinedBranchName arrayByAddingObjectsFromArray:ActiveBranchNameArr];
arrCombinedBranchName = [arrCombinedBranchName arrayByAddingObjectsFromArray:InactiveBranchNameArr];
    
     NSLog(@"combined branch name %@",arrCombinedBranchName);
    
    
    [USERDEFAULTS setObject:arrCombinedBranchName forKey:@"ActiveInactiveBranchArr"];
    [USERDEFAULTS synchronize];
    
    
   arrCombinedBranchID = [arrCombinedBranchID arrayByAddingObjectsFromArray:branchID1];
    arrCombinedBranchID = [arrCombinedBranchID arrayByAddingObjectsFromArray:branchID2];
    
    NSLog(@"Combined branchId Array %@",arrCombinedBranchID);
    
   
    
    [USERDEFAULTS setObject:arrCombinedBranchID forKey:@"branchID"];
    [USERDEFAULTS synchronize];
    
    arrCombinedCatName = [arrCombinedCatName arrayByAddingObjectsFromArray:arrCatName];
    arrCombinedCatName = [arrCombinedCatName arrayByAddingObjectsFromArray:arrDeactiveCatName];
    
    [AppDelegate sharedInstance].activeCategoryCount = [arrCatName count];
    
    
    NSLog(@"Active element count %d",[AppDelegate sharedInstance].activeCategoryCount);
    
    NSLog(@"combined array : %@",arrCombinedCatName );
    
    NSLog(@"swipe category %@",arrCatName);
    if([arrCatName count]>0){
        tabSwipe = [[CarbonTabSwipeNavigation alloc] createWithRootViewController:self tabNames:arrCombinedCatName tabCategorys:mutArrCategoryList tintColor:[UIColor whiteColor] delegate:self];
        
    
        
        //self->tabSwipe.toolbar.barTintColor = [UIColor greenColor];
       // self->tabSwipe.toolbar.translucent = NO;
        [tabSwipe setIndicatorHeight:3.f]; // default 3.f
        [tabSwipe addShadow];
    }
    
    lblNotificationCount.layer.cornerRadius = lblNotificationCount.frame.size.height / 2 ;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
     //self.ContraintHeight.constant=40;
    
    //Reload HomeVC campaign when tab changes (Solved issue : If deselected any category from myikash-> category)
    
    if (self.isDisAppear && [AppDelegate sharedInstance].IsFavCategoryUpdated) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadHomeCampaign" object:nil];
        [AppDelegate sharedInstance].IsFavCategoryUpdated = NO;
    }
    
    if([[AppDelegate sharedInstance].strGotNotification isEqualToString:@"1"]){
        lblNotificationCount.hidden=FALSE;
        lblNotificationCount.text = [NSString stringWithFormat:@"%d",[AppDelegate sharedInstance].NotificationCount];
    }else{
        lblNotificationCount.hidden=TRUE;
    }
    
    if([AppDelegate sharedInstance].NotificationCount > 0){
        lblNotificationCount.hidden=FALSE;
        lblNotificationCount.text = [NSString stringWithFormat:@"%d",[AppDelegate sharedInstance].NotificationCount];
    }else{
        lblNotificationCount.hidden=TRUE;
    }
    
    self.navigationController.navigationBarHidden = YES;
    self.tabBarController.tabBar.hidden = NO;
    NSLog(@"%@",[AppDelegate sharedInstance].strFlgSearch);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setGotNotifcation) name:@"GotNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpHeaderView) name:@"MFSideMenuStateClosed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SetPageHeader) name:@"PageTitle" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SetTopCategory) name:@"SetTopCategory" object:nil];
    
    [self SetTopCategory];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.isDisAppear = YES;
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait: self.ContraintHeight.constant=40;
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [AppDelegate sharedInstance].topConstraint.constant = -6;
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=40;//64
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [AppDelegate sharedInstance].topConstraint.constant = 0;
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [AppDelegate sharedInstance].topConstraint.constant = -6;
            [[UIApplication sharedApplication] setStatusBarHidden:YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

-(void)setGotNotifcation{
    if([AppDelegate sharedInstance].NotificationCount > 0){
        
    [AppDelegate sharedInstance].strGotNotification=@"1";
    lblNotificationCount.hidden=FALSE;
    lblNotificationCount.text = [NSString stringWithFormat:@"%d",[AppDelegate sharedInstance].NotificationCount];
        
    }else{
        [AppDelegate sharedInstance].strGotNotification=@"";
        lblNotificationCount.hidden=TRUE;
    }
}

-(void)setUpHeaderView{
    if([[AppDelegate sharedInstance].strTabSelectedIndex isEqualToString:@"0"] || [AppDelegate sharedInstance].strTabSelectedIndex.length ==0){
        if([[AppDelegate sharedInstance].mutArrSidePenalCategoryList count]>0){
            UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            SidePenalSearch *objSidePenalSearch = [aStoryboard instantiateViewControllerWithIdentifier:@"SidePenalSearch"];
            [self.navigationController pushViewController:objSidePenalSearch animated:YES];
            [[NSNotificationCenter defaultCenter] removeObserver:self
                                                            name:@"MFSideMenuStateClosed"
                                                          object:nil];
        }
    }
}

-(void)SetPageHeader{
    //    self.btnNavTitle.titleLabel.text = @"Amsterdam(5km)";
    
    UIFont *regularFont = [UIFont fontWithName:FontRobotoRegular size:17.0];
    UIFont *lightFont = [UIFont fontWithName:FontRobotoLight size:10.0];
    NSMutableAttributedString *aAttrString = nil;
    NSMutableAttributedString *aAttrStringTwo = nil;
    
    NSString *strTitle = [AppDelegate sharedInstance].strCampaignPageTitle;
    if(strTitle.length==0){
        strTitle=@"No City Found";
        [AppDelegate sharedInstance].strCampaignPageTitle=strTitle;
    }
    NSString *strKM=@"";
    if([[AppDelegate sharedInstance].mutArrRadius count]>0){
        NSString *strRadiosKM=[[AppDelegate sharedInstance].mutArrRadius objectAtIndex:0];
        if(strRadiosKM.length>0 &&  ![strRadiosKM isEqualToString:@"NONE"]){
            strKM = [NSString stringWithFormat:@"(%@km)",[[AppDelegate sharedInstance].mutArrRadius objectAtIndex:0]];// @"(5km)";
        }
    }
    
    aAttrString = [[NSMutableAttributedString alloc] initWithString:strTitle attributes: nil];
    aAttrStringTwo = [[NSMutableAttributedString alloc] initWithString:strKM attributes: nil];
    [aAttrString setAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: regularFont } range:NSMakeRange(0, strTitle.length)];
    [aAttrStringTwo setAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: lightFont } range:NSMakeRange(0, strKM.length)];
    [aAttrString appendAttributedString:aAttrStringTwo];
    [self.btnNavTitle setAttributedTitle:aAttrString forState:UIControlStateNormal];
    [self.btnNavTitle setImage:[UIImage imageNamed:@"ic-navbar-arrow-down"] forState:UIControlStateNormal];
    self.btnNavTitle.titleEdgeInsets = UIEdgeInsetsMake(0, -self.btnNavTitle.imageView.frame.size.width, 0, self.btnNavTitle.imageView.frame.size.width);
    self.btnNavTitle.imageEdgeInsets = UIEdgeInsetsMake(6, self.btnNavTitle.titleLabel.frame.size.width, 0, -self.btnNavTitle.titleLabel.frame.size.width-3);
    
    //    UILabel *titleLabel = [[UILabel alloc] init];
    //lblNavTitle.text = [AppDelegate sharedInstance].strCampaignPageTitle;
    //    titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    //    titleLabel.textColor = [UIColor whiteColor];
    //    [titleLabel sizeToFit];
    //    self.navigationItem.titleView = titleLabel;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - Carbon Tab Swipe Delegate
// required
- (UIViewController *)tabSwipeNavigation:(CarbonTabSwipeNavigation *)tabSwipe viewControllerAtIndex:(NSUInteger)index {
    [AppDelegate sharedInstance].strHomeCatIndex = [NSString stringWithFormat:@"%lu",(unsigned long)index];
    NSLog(@"Tab ->%@",[NSString stringWithFormat:@"%lu",(unsigned long)index]);
    //Reuse same object (issue solved)
//    CampaignListVC *viewController = [AppDelegate sharedInstance].HomeListVC;
//    [viewController SetUpPage];
    CampaignListVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CampaignListVC"];
    return viewController;
}

// optional
- (void)tabSwipeNavigation:(CarbonTabSwipeNavigation *)tabSwipe didMoveAtIndex:(NSInteger)index {
    NSLog(@"Current tab: %d", (int)index);
}

- (void)GetCategoriesWS{
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCategoryWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            [USERDEFAULTS setObject:[responseData objectForKey:@"data"] forKey:@"CategoryList"];
            UIColor *color = [UIColor whiteColor];//self.navigationController.navigationBar.barTintColor;
            arrCatName=[[NSMutableArray alloc]init];
            NSString *strAll=@"Featured";
            [arrCatName addObject:strAll];
            mutArrCategoryList=[[NSMutableArray alloc]initWithArray:[USERDEFAULTS objectForKey:@"CategoryList"]];
            for(int i=0;i<[mutArrCategoryList count];i++){
                [arrCatName addObject:[[mutArrCategoryList objectAtIndex:i] objectForKey:@"branch_name"]];
            }
            NSLog(@"%@",arrCatName);
            if([arrCatName count]>0){
                
                tabSwipe = [[CarbonTabSwipeNavigation alloc] createWithRootViewController:self tabNames:arrCatName tabCategorys:mutArrCategoryList tintColor:color delegate:self];
                [tabSwipe setIndicatorHeight:3.f]; // default 3.f
                [tabSwipe addShadow];
            }
        }
//        else{
//            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
//        }
    } withFailureBlock:^(NSError *error) {
        [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedInstance].window animated:YES];
//        [appDelegate showAlertView:[appDelegate getString:@"SOMETHINGWENTWRONG"] Tag:@"0"];
    }];
}

#pragma mark- UIButton Click Action Methods

- (IBAction)btnActionToClickSideMenu:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

- (IBAction)btnActionToClickNotification:(id)sender {
    if([[AppDelegate sharedInstance] checkNetworkStatus]){
        int intCustomerId=[[USERDEFAULTS objectForKey:@"customer_id"] intValue];
        if(intCustomerId==0){
            UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            objPopOverAlertVC = [aStoryboard instantiateViewControllerWithIdentifier:@"PopOverAlertVC"];
            [[AppDelegate sharedInstance].window addSubview:objPopOverAlertVC.view];
        }else{
            if (IS_IPAD) {
                UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
                NotificationVC *objNotificationVC = [aStoryboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
                self.notifyAndLocationPopover = [[UIPopoverController alloc] initWithContentViewController:objNotificationVC];
                self.notifyAndLocationPopover.delegate = self;
                self.notifyAndLocationPopover.popoverContentSize = CGSizeMake(320.0, 600.0);
                self.view.alpha = 0.6;
                [self.notifyAndLocationPopover presentPopoverFromRect:[(UIButton *)sender bounds]
                                                               inView:btnNotification
                                             permittedArrowDirections:UIPopoverArrowDirectionUp
                                                             animated:YES];
            }else{
                self.navigationController.navigationBarHidden = YES;
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
                NotificationVC *objNotificationVC = [storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
                [self.navigationController pushViewController:objNotificationVC animated:YES];
            }
        }
    }else{
        [[AppDelegate sharedInstance] showAlertView:[appDelegate getString:@"INTERNET_CONNECTION_ERROR"] Tag:@"0"];
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    self.view.alpha = 1.0;
    self.notifyAndLocationPopover.delegate = nil;
    return YES;
}

- (IBAction)btnActionToClickLocation:(id)sender{
    if (IS_IPAD) {
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        FilterHomePageVC *objGlobalSearchLocationVC = [aStoryboard instantiateViewControllerWithIdentifier:@"FilterHomePageVC"];
        //        objGlobalSearchLocationVC.strNavTitle = self.btnNavTitle.titleLabel.text;
        [objGlobalSearchLocationVC setPopoverDismiss:^{
            self.view.alpha = 1.0;
        }];
        self.notifyAndLocationPopover = [[UIPopoverController alloc] initWithContentViewController:objGlobalSearchLocationVC];
        self.notifyAndLocationPopover.delegate = self;
        self.notifyAndLocationPopover.popoverContentSize = CGSizeMake(320.0, 600.0);
        self.view.alpha = 0.6;
        [self.notifyAndLocationPopover presentPopoverFromRect:[(UIButton *)sender bounds]
                                                       inView:self.btnNavTitle
                                     permittedArrowDirections:UIPopoverArrowDirectionUp
                                                     animated:YES];
    }else{
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:ThirdStoryBoard bundle:nil];
        FilterHomePageVC *objGlobalSearchLocationVC = [aStoryboard instantiateViewControllerWithIdentifier:@"FilterHomePageVC"];
        objGlobalSearchLocationVC.strNavTitle = self.btnNavTitle.titleLabel.text;
        //        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objGlobalSearchLocationVC];
        objGlobalSearchLocationVC.modalPresentationStyle = UIModalPresentationCustom;
        [self presentViewController:objGlobalSearchLocationVC animated:YES completion:nil];
    }
}

#pragma mark -- YSLContainerViewControllerDelegate

- (void)containerViewItemIndex:(NSInteger)index currentController:(UIViewController *)controller{
    //    NSLog(@"current Index : %ld",(long)index);
    //    NSLog(@"current controller : %@",controller);
    [controller viewWillAppear:YES];
}

-(void)SetUpTabBar{
    self.tabBarController.delegate = (id)self;
    NSArray *imageNames = @[@"ic-tab-home", @"ic-tab-search", @"ic-tab-my-iKash", @"ic-tab-more"];
    NSMutableArray *TabTitleTxt = [[NSMutableArray alloc]init];
    
    TabTitleTxt =  @[@"Home", @"Search", @"My Ikash", @"More"];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor orangeColor] }
    forState:UIControlStateSelected];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor lightGrayColor] }
       forState:UIControlStateNormal];
    
    
    for (int i=0; i<4; i++) {
        UITabBarItem *item = [[self.tabBarController.tabBar items] objectAtIndex:i];
        item.title = [TabTitleTxt objectAtIndex:i]; //@"Home";
        NSString *imageName = [imageNames objectAtIndex:i];
        NSString *imageNameSelect = [imageName stringByAppendingString:@"-selected"];
        item.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        item.selectedImage =  [[UIImage imageNamed:imageNameSelect] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        if (IS_IPAD) {
            item.imageInsets = UIEdgeInsetsMake(9, 0, -9, 0);
        }else{
            item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        }
    }
    self.title = nil;
}

-(void)SetTopCategory{
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    [UIApplication sharedApplication].statusBarHidden = NO;
    
    UIFont *regularFont = [UIFont fontWithName:FontRobotoRegular size:17.0];
    UIFont *lightFont = [UIFont fontWithName:FontRobotoLight size:10.0];
    NSMutableAttributedString *aAttrString = nil;
    NSMutableAttributedString *aAttrStringTwo = nil;
    
    NSString *strTitle ;
    
    NSString *strlatitude=[USERDEFAULTS objectForKey:@"latitude"];
    if(strlatitude.length==0 || [strlatitude isEqualToString:@"0"] ){
        strTitle=@"No City Found";
        [AppDelegate sharedInstance].strCityId=@"0";
        
        // [AppDelegate sharedInstance].strRadius=@"0";
    }else{
        strTitle=([AppDelegate sharedInstance].strCampaignPageTitle.length > 0) ?[AppDelegate sharedInstance].strCampaignPageTitle:@"No City Found";
        
    }
    
    if ([strTitle isEqualToString:@"No City Found"]){
        
        //Remove city image for side drawer to set default image
        NSMutableDictionary *aDictLocationData = [[[NSUserDefaults standardUserDefaults] objectForKey:@"LocationData"] mutableCopy];
        [aDictLocationData removeObjectForKey:@"mc_city_image"];
        [[NSUserDefaults standardUserDefaults] setObject:aDictLocationData forKey:@"LocationData"];
    }
    
    int  intRadious =[[AppDelegate sharedInstance].strRadius intValue];
    NSLog(@"Radious=%d",intRadious);
    NSString *strKM;
    if(intRadious>5){
        strKM = [NSString stringWithFormat:@"%ikm",intRadious];
    }else {
        strKM = @"";
    }
    
    aAttrString = [[NSMutableAttributedString alloc] initWithString:strTitle attributes: nil];
    aAttrStringTwo = [[NSMutableAttributedString alloc] initWithString:strKM attributes: nil];
    
    [aAttrString setAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: regularFont } range:NSMakeRange(0, strTitle.length)];
    [aAttrStringTwo setAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: lightFont } range:NSMakeRange(0, strKM.length)];
    [aAttrString appendAttributedString:aAttrStringTwo];
    [self.btnNavTitle setAttributedTitle:aAttrString forState:UIControlStateNormal];
    [self.btnNavTitle setImage:[UIImage imageNamed:@"ic-navbar-arrow-down"] forState:UIControlStateNormal];
    self.btnNavTitle.titleEdgeInsets = UIEdgeInsetsMake(0, -self.btnNavTitle.imageView.frame.size.width, 0, self.btnNavTitle.imageView.frame.size.width);
    self.btnNavTitle.imageEdgeInsets = UIEdgeInsetsMake(6, self.btnNavTitle.titleLabel.frame.size.width, 0, -self.btnNavTitle.titleLabel.frame.size.width-3);
    
    if(!IS_IPAD){
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    }
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    [USERDEFAULTS setValue:@"" forKey:@"ProfileContinerPage"];
    [USERDEFAULTS synchronize];
    [AppDelegate sharedInstance].strStarflag=@"0";
    [AppDelegate sharedInstance].strSelectedIndex=[NSString stringWithFormat:@"%i",(int)tabBarController.selectedIndex];
    int intCustomerId=[[USERDEFAULTS objectForKey:@"customer_id"] intValue];
    if(intCustomerId==0){
        if(tabBarController.selectedIndex ==2 || tabBarController.selectedIndex ==3){
            UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            objPopOverAlertVC = [aStoryboard instantiateViewControllerWithIdentifier:@"PopOverAlertVC"];
            [[AppDelegate sharedInstance].window addSubview:objPopOverAlertVC.view];
            [self.tabBarController setSelectedIndex:intSelectedIndex];
        }else{
            intSelectedIndex=(int)tabBarController.selectedIndex;
        }
    }else{
        if(tabBarController.selectedIndex ==2){
            [[AppDelegate sharedInstance].objSettingContainer btnSettingClick:nil];
            //NSLog(@"%@", [self.navigationController viewControllers]);
        }
    }
    [AppDelegate sharedInstance].strTabSelectedIndex=[NSString stringWithFormat:@"%lu",(unsigned long)tabBarController.selectedIndex];
    NSLog(@"%@", tabBarController);
}

@end
