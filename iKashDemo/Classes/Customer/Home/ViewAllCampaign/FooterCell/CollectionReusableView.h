//
//  CollectionReusableView.h
//  iKash
//
//  Created by indianic on 04/02/16.
//  Copyright © 2016 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionReusableView : UICollectionReusableView

@end
