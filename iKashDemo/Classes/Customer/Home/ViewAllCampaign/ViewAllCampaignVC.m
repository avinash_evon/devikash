//
//  ViewAllCampaignVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "ViewAllCampaignVC.h"
#import "ViewAllCollectionCell.h"
#import "NotificationVC.h"
#import "CampaignFilterVC.h"
#import "CampaignDetailsVC.h"
#import "CampaignCollectionCell.h"

@interface ViewAllCampaignVC (){
    NSString *strSubCategory;
    NSString *strSubCategoryNames;
    NSString *strCityId;
    NSString *strRadious;
}

@end

@implementation ViewAllCampaignVC

#pragma mark - Synthesize

@synthesize mutAllCampaign,strCategoryId;

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Register cell from XIB
    [self.collectionViewAll registerNib:[UINib nibWithNibName:tileCellXibNibName bundle:nil] forCellWithReuseIdentifier:tileCellReuseIdentifier];
    
    [self SetNavigationHeader];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
    NSLog(@"Search");
    _lblNoCampaign.hidden=TRUE;
    
    [self doFilterIfApplied];
    //    int intFlagSearch=[[AppDelegate sharedInstance].strFilterSearch intValue];
    //    if(intFlagSearch>0){
    //
    //        strSubCategory = [[AppDelegate sharedInstance].mutArrSubBranchIDS componentsJoinedByString:@","];
    //
    //        if(![strSubCategory isEqualToString:@""]){
    //
    //            strCityId= [[AppDelegate sharedInstance].mutArrCityIDS componentsJoinedByString:@","];
    //
    //            strRadious = [[AppDelegate sharedInstance].mutArrRadius componentsJoinedByString:@","];
    //
    //            strSubCategoryNames = [AppDelegate sharedInstance].strPageTitleFilter;
    //           int intSelectedIndex= [[AppDelegate sharedInstance].strSelectedIndex intValue];
    //            int intCustomerId=[[USERDEFAULTS objectForKey:@"customer_id"] intValue];
    //            if(intCustomerId!=0)
    //            {
    //                [self GetSearchCampaignWS];
    //            }else{
    //                if(intSelectedIndex<1){
    //                    [self GetSearchCampaignWS];
    //
    //                }
    //            }
    //            [AppDelegate sharedInstance].strSelectedIndex=@"0";
    //        }
    //
    //    }else{
    //
    //      strSubCategoryNames=_strTitle;
    //
    //    }
    if(!IS_IPAD){
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
    //    [self TitlePage];
}

-(void)doFilterIfApplied{
    int intFlagSearch=[[AppDelegate sharedInstance].strFilterSearch intValue];
    if(intFlagSearch>0){
        strSubCategory = [[AppDelegate sharedInstance].mutArrSubBranchIDS componentsJoinedByString:@","];
        if(![strSubCategory isEqualToString:@""]){
            strCityId= [[AppDelegate sharedInstance].mutArrCityIDS componentsJoinedByString:@","];
            strRadious = [[AppDelegate sharedInstance].mutArrRadius componentsJoinedByString:@","];
            strSubCategoryNames = [AppDelegate sharedInstance].strPageTitleFilter;
            int intSelectedIndex= [[AppDelegate sharedInstance].strSelectedIndex intValue];
            int intCustomerId=[[USERDEFAULTS objectForKey:@"customer_id"] intValue];
            if(intCustomerId!=0){
                [self GetSearchCampaignWS];
            }else{
                if(intSelectedIndex<1){
                    [self GetSearchCampaignWS];
                }
            }
            [AppDelegate sharedInstance].strSelectedIndex=@"0";
        }else{
            _collectionViewAll.hidden=TRUE;
            _viewNoInternet.hidden=TRUE;
            _lblNoCampaign.hidden=FALSE;
            _lblNoCampaign.text=[appDelegate getString:@"No Campaign found."];
        }
    }else{
        strSubCategoryNames=_strTitle;
    }
    [self TitlePage];
}

#pragma mark UIInterfaceOrientation Methods

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

#pragma mark - Button Event Click Methods

- (IBAction)btnActionToClickNotification:(id)sender {
    if([[AppDelegate sharedInstance] checkNetworkStatus]){
        int intCustomerId=[[USERDEFAULTS objectForKey:@"customer_id"] intValue];
        if(intCustomerId==0){
            UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            objPopOverAlertVC = [aStoryboard instantiateViewControllerWithIdentifier:@"PopOverAlertVC"];
            [[AppDelegate sharedInstance].window addSubview:objPopOverAlertVC.view];
        }else{
            if (IS_IPAD) {
                UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
                NotificationVC *objNotificationVC = [aStoryboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
                self.notifyAndLocationPopover = [[UIPopoverController alloc] initWithContentViewController:objNotificationVC];
                self.notifyAndLocationPopover.delegate = self;
                self.notifyAndLocationPopover.popoverContentSize = CGSizeMake(320.0, 600.0);
                self.view.alpha = 0.6;
                [self.notifyAndLocationPopover presentPopoverFromRect:[(UIButton *)sender bounds]
                                                               inView:btnNotification
                                             permittedArrowDirections:UIPopoverArrowDirectionUp
                                                             animated:YES];
            }else{
                self.navigationController.navigationBarHidden = YES;
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
                NotificationVC *objNotificationVC = [storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
                [self.navigationController pushViewController:objNotificationVC animated:YES];
            }
        }
    }else{
        [[AppDelegate sharedInstance] showAlertView:[appDelegate getString:@"INTERNET_CONNECTION_ERROR"] Tag:@"0"];
    }
}

- (IBAction)btnActionToClickLocation:(id)sender{
    if([[AppDelegate sharedInstance] checkNetworkStatus]){
        if (IS_IPAD) {
            UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            CampaignFilterVC *objCampaignFilterVC = [aStoryboard instantiateViewControllerWithIdentifier:@"CampaignFilterVC"];
            objCampaignFilterVC.strCatTitle=_strTitle;//[[mutAllCampaign objectAtIndex:[sender tag]] objectForKey:@"b_branch_name"];
            objCampaignFilterVC.strCatId=strCategoryId;
            [objCampaignFilterVC setPopoverDismiss:^{
                self.view.alpha = 1.0;
                [self doFilterIfApplied];
            }];
            
            self.campaignFilterPopover = [[UIPopoverController alloc] initWithContentViewController:objCampaignFilterVC];
            self.campaignFilterPopover.delegate = self;
            self.campaignFilterPopover.popoverContentSize = CGSizeMake(320.0, 600.0);
            self.view.alpha = 0.6;
            [self.campaignFilterPopover presentPopoverFromRect:[(UIButton *)sender bounds]
                                                        inView:btnLocation
                                      permittedArrowDirections:UIPopoverArrowDirectionUp
                                                      animated:YES];
        }else{
            UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:ThirdStoryBoard bundle:nil];
            CampaignFilterVC *objCampaignFilterVC = [aStoryboard instantiateViewControllerWithIdentifier:@"CampaignFilterVC"];
            NSLog(@"%@",mutAllCampaign);
            objCampaignFilterVC.strCatTitle=_strTitle;//[[mutAllCampaign objectAtIndex:[sender tag]] objectForKey:@"b_branch_name"];
            objCampaignFilterVC.strCatId=strCategoryId;
            [self.navigationController presentViewController:objCampaignFilterVC animated:YES completion:nil];
        }
    }else{
        [[AppDelegate sharedInstance] showAlertView:[appDelegate getString:@"INTERNET_CONNECTION_ERROR"] Tag:@"0"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnActionToBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

# pragma mark - CollectionView Delegate Method

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [mutAllCampaign count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CampaignCollectionCell *shopCollectionViewCell = (CampaignCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:tileCellReuseIdentifier forIndexPath:indexPath];
    
    [shopCollectionViewCell setDataWithDic:[mutAllCampaign objectAtIndex:indexPath.row]];
    
    return shopCollectionViewCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPAD) {
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        CampaignDetailsVC *objCampaignDetailsVC = [aStoryboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
        objCampaignDetailsVC.strCampaignId =  [[mutAllCampaign objectAtIndex:indexPath.row] objectForKey:@"c_campaign_id"];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objCampaignDetailsVC];
        navigationController.navigationBar.hidden = YES;
        self.campaignDetailsPopover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
        self.campaignDetailsPopover.delegate = self;
        self.campaignDetailsPopover.popoverContentSize = CGSizeMake(370.0, 645.0);
        self.view.alpha = 0.6;
        if ([self.campaignDetailsPopover respondsToSelector:@selector(backgroundColor)]) {
            self.campaignDetailsPopover.backgroundColor = [UIColor blackColor];
        }
        [self.campaignDetailsPopover presentPopoverFromRect:[_btnPopupView bounds]
                                                     inView:_btnPopupView
                                   permittedArrowDirections:0
                                                   animated:YES];
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        CampaignDetailsVC *objCampaignDetailsVC = [storyboard instantiateViewControllerWithIdentifier:@"CampaignDetailsVC"];
        objCampaignDetailsVC.strCampaignId =  [[mutAllCampaign objectAtIndex:indexPath.row] objectForKey:@"c_campaign_id"] ;
        [self.navigationController pushViewController:objCampaignDetailsVC animated:YES];
    }
}

-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    self.view.alpha = 1.0;
    //    self.campaignDetailsPopover.delegate = nil;
    return YES;
}

#pragma mark - UICollectionViewFlowLayout methods

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    if (IS_IPHONE) {
//        CGRect screenRect = [[UIScreen mainScreen] bounds];
//        CGFloat screenWidth = screenRect.size.width;
//        CGFloat screenHeight = screenRect.size.width;
//        float gridColumn;
//        float gridRow;
//        
//        UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
//        if(UIInterfaceOrientationIsPortrait(orientation)){
//            gridRow = 2.0;
//            gridColumn = 1.5;
//        }else{
//            gridRow = 3.0;
//            gridColumn = 2.5;
//        }
//        float cellWidth = (screenWidth / gridRow)-22.5; //Replace the divisor with the column count requirement. Make sure to have it in float.
//        float cellHeight = (screenHeight / gridColumn)-22.5; //Replace the divisor with the column count requirement. Make sure to have it in float.
//        CGSize size = CGSizeMake(cellWidth, cellHeight + 30); //+10 to cover description lable
//        return size;
//    }else{
//        CGRect screenRect = [[UIScreen mainScreen] bounds];
//        CGFloat screenWidth = screenRect.size.width;
//        CGFloat screenHeight = screenRect.size.width;
//        float gridColumn;
//        float gridRow;
//        
//        UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
//        if(UIInterfaceOrientationIsPortrait(orientation)){
//            gridRow = 4.0;
//            gridColumn = 3.2;
//        }else{
//            gridRow = 5.0;
//            gridColumn = 4.2;
//        }
//        float cellWidth = (screenWidth / gridRow)-18; //Replace the divisor with the column count requirement. Make sure to have it in float.
//        float cellHeight = (screenHeight / gridColumn)-18; //Replace the divisor with the column count requirement. Make sure to have it in float.
//        CGSize size = CGSizeMake(cellWidth, cellHeight + 30);
//        return size;
//    }
//    
//    
//}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (IS_IPHONE) {
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }else{
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (IS_IPAD) {
        return 15;
    }else{
        return 15;
    }
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [self.collectionViewAll.collectionViewLayout invalidateLayout];
}

-(void)SetNavigationHeader{
    [self TitlePage];
}

-(void)TitlePage{
    NSLog(@"%@",mutAllCampaign);
    NSLog(@"%@",strSubCategoryNames);
    //    UILabel *titleLabel = [[UILabel alloc] init];
    lblNavTitle.text = [[mutAllCampaign objectAtIndex:0]objectForKey:@"b_branch_name"];//strSubCategoryNames;
}

#pragma mark - Webservice Methods

- (void)GetSearchCampaignWS{
    if([self NointerNetCheking]){
        mutAllCampaign=[[NSMutableArray alloc]init];
        NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      [USERDEFAULTS objectForKey:@"latitude"],@"latitude",
                                      [USERDEFAULTS objectForKey:@"longitude"],@"longitude",
                                      @"1",@"is_city",
                                      strSubCategory,@"sub_branch_id",
                                      strCityId,@"city_id",
                                      strRadious,@"radius",
                                      [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                      self.strBranchID,@"branch_id",
                                      nil];
        NSLog(@"customer_GetCampaignWS aDict %@",aDict);
        [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCampaignWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            NSLog(@"customer_GetCampaignWS responseData = %@", responseData);
            if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
                // [mutAllCampaign removeAllObjects];
                NSMutableArray *mutArrSubCampaign=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
                for(int i=0;i<[mutArrSubCampaign count];i++){
                    if([[[mutArrSubCampaign objectAtIndex:i] objectForKey:@"city_wise_campaign"] count]>0){
                        NSMutableArray *mutSubCampaign=[[NSMutableArray alloc]initWithArray:[[mutArrSubCampaign objectAtIndex:i] objectForKey:@"city_wise_campaign"]];
                        for(int j=0;j<[mutSubCampaign count];j++){
                            [mutAllCampaign addObject:[mutSubCampaign objectAtIndex:j]] ;
                        }
                    }
                }
                _lblNoCampaign.hidden=TRUE;
                _collectionViewAll.hidden=FALSE;
                [_collectionViewAll reloadData];
            }else{
                //  [mutAllCampaign removeAllObjects];
                NSMutableArray *mutArrSubCampaign=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
                for(int i=0;i<[mutArrSubCampaign count];i++){
                    if([[[mutArrSubCampaign objectAtIndex:i] objectForKey:@"city_wise_campaign"] count]>0){
                        NSMutableArray *mutSubCampaign=[[NSMutableArray alloc]initWithArray:[[mutArrSubCampaign objectAtIndex:i] objectForKey:@"city_wise_campaign"]];
                        for(int j=0;j<[mutSubCampaign count];j++){
                            [mutAllCampaign addObject:[mutSubCampaign objectAtIndex:j]] ;
                        }
                    }
                }
                
                _lblNoCampaign.hidden=TRUE;
                _collectionViewAll.hidden=FALSE;
                [_collectionViewAll reloadData];
                
                if([[[responseData objectForKey:@"settings"] objectForKey:@"success"] isEqualToString:@"100"]){
                    [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
                }else{
                    [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:@"50"];
                }
            }
        } withFailureBlock:^(NSError *error) {
            _collectionViewAll.hidden=TRUE;
            _viewNoInternet.hidden=FALSE;
            _lblNoInternet.text=[appDelegate getString:@"WEBSERVICEISSUE"];
            NSLog(@"%@",error);
        }];
    }
}

-(BOOL)NointerNetCheking{
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        _collectionViewAll.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        return FALSE;
    }else{
        _collectionViewAll.hidden=FALSE;
        _viewNoInternet.hidden=TRUE;
        return TRUE;
    }
}

@end
