//
//  VoucherVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "VoucherVC.h"
#import "CustomWebViewVC.h"

@interface VoucherVC (){
    NSMutableArray *mutArrVoucher;
    int intStatu;    
}

@end

@implementation VoucherVC

#pragma mark - Synthesize

@synthesize strCampaignId;

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
}

-(void)setUp{
    self.lblNavTitle.text = [appDelegate getString:@"Voucher Details"];
    self.lblIkashVoucherNumberBanner.text = [appDelegate getString:@"iKash Voucher Number:"];
    self.lblSupplierDetailsBanner.text = [appDelegate getString:@"Supplier Details"];
    
    if (!IS_IPHONE) {
        if ([_isFromCampaignDetails isEqualToString:@"CampaignDetails"]) {
            _btnBack.hidden = NO;
        }else{
            _btnBack.hidden = YES;
        }
    }
    _scrlViewVoucher.hidden=TRUE;
    _btnRedeem.hidden=TRUE;
    _btnRedeem.userInteractionEnabled=FALSE;
    [self VoucherDetailsWS];
}

-(void)viewDidLayoutSubviews{
    if (IS_IPHONE) {
        _imgViewCampaignHeightConstraint.constant = ([UIScreen mainScreen].bounds.size.width*406/370) + 43;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(!IS_IPAD){
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}


- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

#pragma mark UIInterfaceOrientation Methods

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

#pragma mark- UIButton Click Action Methods

- (IBAction)BtnActionToClickBack:(UIButton *)sender {
    if ([_isFromCampaignDetails isEqualToString:@"CampaignDetails"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ResetDownloadButton" object:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)btnActionToRedeemClick:(id)sender {
    if(intStatu==1){
        [appDelegate showAlertView:[appDelegate getString:@"DONWLOADED_VOUCHER"] Tag:@"0"];
    }else{
        
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:APPTITLE message:[appDelegate getString:@"REDEEM_VOUCHER"] delegate:self cancelButtonTitle:[appDelegate getString:@"No"] otherButtonTitles:[appDelegate getString:@"Yes"], nil];
        alertView.tag=200;
        [alertView show];
        
       
    }
}


#pragma mark - AlertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag==200){
        if(buttonIndex==1) {
            [self RedeemVoucherWS];
        }
    }
}
#pragma mark - Webservice Methods

- (void)VoucherDetailsWS {
    if([self NointerNetCheking]){
        NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                      strCampaignId,@"campaign_id",
                                      nil];
        [[Webservice sharedInstance] callWebserviceWithMethodName:customer_download_voucher_firstWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            NSLog(@"responseData = %@", responseData);
            
            mutArrVoucher=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"]];
            _scrlViewVoucher.hidden=FALSE;
            //_imgBackground.hidden=FALSE;
            [self SetUpPage];
            
            NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                          strCampaignId,@"campaign_id",
                                          [[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"dv_voucher_unique_no"],@"voucher_uniq_no",
                                          nil];
            [[Webservice sharedInstance] callWebserviceWithMethodName:customer_DownaloadVoucherWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
                NSLog(@"responseData = %@", responseData);
                
                //JATIN : Removed by Ali
              //  [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
                
            } withFailureBlock:^(NSError *error) {
//                _scrlViewVoucher.hidden=TRUE;
//                _viewNoInternet.hidden=FALSE;
//                _lblNoInternet.text=[appDelegate getString:@"WEBSERVICEISSUE"];
                NSLog(@"%@",error);
            }];
        } withFailureBlock:^(NSError *error) {
            _scrlViewVoucher.hidden=TRUE;
            _viewNoInternet.hidden=FALSE;
            _lblNoInternet.text=[appDelegate getString:@"WEBSERVICEISSUE"];
            NSLog(@"%@",error);
        }];
    }
}

- (void)RedeemVoucherWS {
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  [[mutArrVoucher objectAtIndex:0] objectForKey:@"downloaded_voucher_id"],@"download_voucher_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_RedeemVoucherWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            intStatu=1;
            [AppDelegate sharedInstance].strRedeemed=@"4";
            [_btnRedeem setTitle:[appDelegate getString:@"Voucher Redeemed"] forState:UIControlStateNormal];
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:@"0"];
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark SetUpPage

-(void)SetUpPage{
    if([mutArrVoucher count]>0){
        NSString *urlAddress;
        if([[[mutArrVoucher objectAtIndex:0] objectForKey:@"dv_status"] isEqualToString:@"Redeemed"]){
            urlAddress = [[mutArrVoucher objectAtIndex:0] objectForKey:@"voucher_file_path1"] ;
        }else{
            urlAddress = [[mutArrVoucher objectAtIndex:0] objectForKey:@"voucher_file_path"] ;
        }
        _btnRedeem.userInteractionEnabled=TRUE;
        if([[[mutArrVoucher objectAtIndex:0] objectForKey:@"dv_status"] isEqualToString:@""] && [[[mutArrVoucher objectAtIndex:0] objectForKey:@"vouch_deleted"] isEqualToString:@""]){
            intStatu=0;
            [_btnRedeem setTitle:[appDelegate getString:@"Redeem"] forState:UIControlStateNormal];
        }else if([[[mutArrVoucher objectAtIndex:0] objectForKey:@"dv_status"] isEqualToString:@"Downloaded"] && [[[mutArrVoucher objectAtIndex:0] objectForKey:@"vouch_deleted"] isEqualToString:@"0"]){
            intStatu=0;
            [_btnRedeem setTitle:[appDelegate getString:@"Redeem"] forState:UIControlStateNormal];
        }else if([[[mutArrVoucher objectAtIndex:0] objectForKey:@"dv_status"] isEqualToString:@"Downloaded"] && [[[mutArrVoucher objectAtIndex:0] objectForKey:@"vouch_deleted"] isEqualToString:@"1"]){
            intStatu=0;
            [_btnRedeem setTitle:[appDelegate getString:@"Redeem"] forState:UIControlStateNormal];
        }else if([[[mutArrVoucher objectAtIndex:0] objectForKey:@"dv_status"] isEqualToString:@"Redeemed"] && [[[mutArrVoucher objectAtIndex:0] objectForKey:@"vouch_deleted"] isEqualToString:@"1"]){
            intStatu=1;
            _btnRedeem.userInteractionEnabled=FALSE;
            [_btnRedeem setTitle:[appDelegate getString:@"Redeemed"] forState:UIControlStateNormal];
        }else if([[[mutArrVoucher objectAtIndex:0] objectForKey:@"dv_status"] isEqualToString:@"Redeemed"] && [[[mutArrVoucher objectAtIndex:0] objectForKey:@"vouch_deleted"] isEqualToString:@"0"]){
            intStatu=1;
            _btnRedeem.userInteractionEnabled=FALSE;
//            [_btnRedeem setTitle:[appDelegate getString:@"Voucher is already redeemed"] forState:UIControlStateNormal];
            [_btnRedeem setTitle:[appDelegate getString:@"Voucher Redeemed"] forState:UIControlStateNormal];
        }
    }
    [_imgSuplierLogo setContentMode:UIViewContentModeScaleAspectFit];
    
    NSURL *urlSupplerImage = [NSURL URLWithString:[[mutArrVoucher objectAtIndex:0] objectForKey:@"s_logo"]];
    [_imgSuplierLogo sd_setImageWithURL:urlSupplerImage
                       placeholderImage:nil
                                options:SDWebImageProgressiveDownload
                               progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                               }
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                  _imgSuplierLogo.clipsToBounds = YES;
                              }];
    
    NSURL *urlProudctImage = [NSURL URLWithString:[[mutArrVoucher objectAtIndex:0] objectForKey:@"camp_image_android"]];
    [_imgVoucher sd_setImageWithURL:urlProudctImage
                   placeholderImage:nil
                            options:SDWebImageProgressiveDownload
                           progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                           }
                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                              _imgVoucher.clipsToBounds = YES;
                          }];
    
    _lblVocuherNumber.text=[NSString stringWithFormat:@"#%@",[[mutArrVoucher objectAtIndex:0] objectForKey:@"dv_voucher_unique_no"]];
    _lblStartDate.text=[[mutArrVoucher objectAtIndex:0] objectForKey:@"c_start_date"];
    _lblEndDate.text=[[mutArrVoucher objectAtIndex:0] objectForKey:@"c_end_date"];
    _lblCampaignTitle.text=[[mutArrVoucher objectAtIndex:0] objectForKey:@"c_title"];
    _lblCampaignDesc.text=[[mutArrVoucher objectAtIndex:0] objectForKey:@"c_short_description"];
    _lblAdditionalInfo.text=[[mutArrVoucher objectAtIndex:0] objectForKey:@"c_additional_info"];
    _lblSupplierDescription.text=[[mutArrVoucher objectAtIndex:0] objectForKey:@"c_short_description"];
    _lblSupplierName.text=[[mutArrVoucher objectAtIndex:0] objectForKey:@"s_company_name"];
    
    
    _lblAddress.text=[[mutArrVoucher objectAtIndex:0] objectForKey:@"o_address"];
    _lblWebUrl.text=[[mutArrVoucher objectAtIndex:0] objectForKey:@"o_web_address"];
    //JATIN:  removed by iresh
//    _lblAddress.text=[[mutArrVoucher objectAtIndex:0] objectForKey:@"s_address"];
//    _lblWebUrl.text=[[mutArrVoucher objectAtIndex:0] objectForKey:@"s_website"];
    _lblResturantTime.text=@"Mon-Fri 9am-6pm CST";
    _lblPhone.text=[[mutArrVoucher objectAtIndex:0] objectForKey:@"s_phone_no"];
    if([[[mutArrVoucher objectAtIndex:0] objectForKey:@"voucher_exp"] isEqualToString:@"1"]){
        self.scrlViewBottomConstrait.constant = -_btnRedeem.frame.size.height;
        _btnRedeem.hidden=TRUE;
    }else{
        _btnRedeem.hidden=FALSE;
    }
}

-(BOOL)NointerNetCheking{
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        _scrlViewVoucher.hidden=TRUE;
        _viewNoInternet.hidden=FALSE;
        return FALSE;
    }else{
        //_scrlViewVoucher.hidden=FALSE;
        _viewNoInternet.hidden=TRUE;
        return TRUE;
    }
}

- (IBAction)btnActionToOpenLinkClick:(id)sender {
    if (![_lblWebUrl.text isEqualToString:@""]) {
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        CustomWebViewVC *objCustomWebViewVC = [aStoryboard instantiateViewControllerWithIdentifier:@"CustomWebViewVC"];
        objCustomWebViewVC.strURL = _lblWebUrl.text;
        objCustomWebViewVC.modalPresentationStyle = UIModalPresentationCustom;
        [self presentViewController:objCustomWebViewVC animated:YES completion:nil];
    }
}

@end
