//
//  VoucherVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoucherVC : UIViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *scrlViewBottomConstrait, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (strong, nonatomic) IBOutlet UILabel *lblNavTitle;

@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIWebView *webViewReview;

@property (weak, nonatomic) IBOutlet UIButton *btnRedeem;
@property (strong, nonatomic) NSString *strCampaignId, *isFromCampaignDetails;
@property (strong, nonatomic) NSString *strCampaignStatus;
- (IBAction)BtnActionToClickBack:(UIButton *)sender;
- (IBAction)btnActionToRedeemClick:(id)sender;
- (IBAction)btnActionToOpenLinkClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrlViewVoucher;
@property (weak, nonatomic) IBOutlet UIImageView *imgVoucher;
@property (weak, nonatomic) IBOutlet UIView *viewFirstInner;
@property (weak, nonatomic) IBOutlet UILabel *lblVocuherNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblCampaignTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCampaignDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblAdditionalInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblSupplierName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblWebUrl;
@property (weak, nonatomic) IBOutlet UILabel *lblSupplierDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblResturantTime;

@property (weak, nonatomic) IBOutlet UILabel *lblSupplierDetailsBanner;
@property (weak, nonatomic) IBOutlet UILabel *lblIkashVoucherNumberBanner;

@property (weak, nonatomic) IBOutlet UIImageView *imgSuplierLogo;
@property (weak, nonatomic) IBOutlet UIImageView *imgBackground;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewCampaignHeightConstraint;

@end
