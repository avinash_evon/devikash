//
//  SHKActivityIndicator.m
//  ShareKit
//
//  Created by Nathan Weiner on 6/16/10.

//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
//

#import "SHKActivityIndicator.h"
#import <QuartzCore/QuartzCore.h>

#define SHKdegreesToRadians(x) (M_PI * x / 180.0)

@implementation SHKActivityIndicator

@synthesize centerMessageLabel, subMessageLabel;
@synthesize spinner;

static SHKActivityIndicator *currentIndicator = nil;


+ (SHKActivityIndicator *)currentIndicator
{
	if (currentIndicator == nil)
	{
		//UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
		
//		CGFloat width = 120;
//		CGFloat height = 120;
//		CGRect centeredFrame = (UI_USER_INTERFACE_IDIOM())?CGRectMake(768/2 - 50,
//                                                                     1024/2 - 20 ,
//                                                                      width,
//                                                                      height):
//        
//        CGRectMake(100,200,width,height);
//		
//		currentIndicator = [[SHKActivityIndicator alloc] initWithFrame:centeredFrame];

        UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
        CGFloat width = 100;
        CGFloat height = 100;
        CGRect centeredFrame = CGRectMake(round(keyWindow.bounds.size.width/2 - width/2),
                                          round(keyWindow.bounds.size.height/2 - height/2),
                                          width,
                                          height);
        currentIndicator = [[SHKActivityIndicator alloc] initWithFrame:centeredFrame];
		currentIndicator.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
		currentIndicator.opaque = NO;
		currentIndicator.alpha = 0;
		
		currentIndicator.layer.cornerRadius = 10;
		
		currentIndicator.userInteractionEnabled = NO;
		currentIndicator.autoresizesSubviews = YES;
		currentIndicator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |  UIViewAutoresizingFlexibleTopMargin |  UIViewAutoresizingFlexibleBottomMargin;
		
        
        
//		[currentIndicator setProperRotation:NO];
//		
//		[[NSNotificationCenter defaultCenter] addObserver:currentIndicator
//												 selector:@selector(setProperRotation)
//													 name:UIDeviceOrientationDidChangeNotification
//												   object:nil];
        
	}
	
	return currentIndicator;
}

#pragma mark -

- (void)dealloc
{
    NSLog(@"Deaallloc Hide");
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
	
//	[centerMessageLabel release];
//	[subMessageLabel release];
		
	[super dealloc];
}

#pragma mark Creating Message

//- (void)show
//{	
//	if ([self superview] != [[UIApplication sharedApplication] keyWindow]) 
//		[[[UIApplication sharedApplication] keyWindow] addSubview:self];
//	
//	[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hide) object:nil];
//	
//	[UIView beginAnimations:nil context:NULL];
//	[UIView setAnimationDuration:0.1];
//	
//	self.alpha = 1;
//	
//	[UIView commitAnimations];
//}

- (void)show
{
     NSLog(@"show Called");
     if ([self superview] != [[UIApplication sharedApplication] keyWindow])
           [[[UIApplication sharedApplication] keyWindow] addSubview:self];
    
     [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hide) object:nil];
    
    
     [UIView animateWithDuration:0.1
                    delay:0
                   options:UIViewAnimationCurveEaseIn | UIViewAnimationOptionAllowUserInteraction
                  animations:^{
                        self.alpha = 1;
                      }
                  completion:NULL];
}

- (void)hideAfterDelay
{
	[self performSelector:@selector(hidden) withObject:nil afterDelay:0.6];
}

//- (void)hide
//{
//    [[UIApplication sharedApplication]endIgnoringInteractionEvents];
//	[UIView beginAnimations:nil context:NULL];
//	[UIView setAnimationDuration:0.1];
//	[UIView setAnimationDelegate:self];
//	[UIView setAnimationDidStopSelector:@selector(hidden)];
//	
//	self.alpha = 0;
//	
//	[UIView commitAnimations];
//}

- (void)hide
{
    NSLog(@"hide called");
    if ([[UIApplication sharedApplication]isIgnoringInteractionEvents]) {
          [[UIApplication sharedApplication]endIgnoringInteractionEvents];
    }
   
    
     [UIView animateWithDuration:0.1
                    delay:0
                   options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                  animations:^{
                        self.alpha = 0;
                      }
                  completion:^(BOOL finished){
                        [self hidden];
                      }];
     
}


- (void)persist
{	
	[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hide) object:nil];

    [UIView animateWithDuration:0.5 animations:^{
        
        self.alpha = 1;

    } completion:^(BOOL finished) {
        
    }];
}

- (void)hidden
{
    if (currentIndicator != nil) {
        [currentIndicator removeFromSuperview];
        //[currentIndicator release];
        //currentIndicator = nil;
    }
	if (currentIndicator.alpha > 0)
		return;
}

- (void)displayActivity:(NSString *)m
{
    NSLog(@"displayActivity called");
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
	[self setSubMessage:m];
	[self showSpinner];	
	
	[centerMessageLabel removeFromSuperview];
	centerMessageLabel = nil;
	
	if ([self superview] == nil)
		[self show];
	else
		[self persist];
}
- (void)displayActivityForFacebook:(NSString *)m
{
   
	[self setSubMessage:m];
	[self showSpinner];
	
	[centerMessageLabel removeFromSuperview];
	centerMessageLabel = nil;
	
	if ([self superview] == nil)
		[self show];
	else
		[self persist];
}
- (void)displayCompleted:(NSString *)m
{
    NSLog(@"displayCompleted called");
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
	[self setCenterMessage:@"✓"];
	[self setSubMessage:m];
	
	[self.spinner removeFromSuperview];
	self.spinner = nil;
	
	if ([self superview] == nil)
		[self show];
	else
		[self persist];
		
	[self hide];
}

- (void)setCenterMessage:(NSString *)message
{	
	if (message == nil && centerMessageLabel != nil)
		self.centerMessageLabel = nil;

	else if (message != nil)
	{
		if (centerMessageLabel == nil)
		{
			self.centerMessageLabel = [[[UILabel alloc] initWithFrame:CGRectMake(12,round(self.bounds.size.height/2-50/2),self.bounds.size.width-24,50)] autorelease];
			centerMessageLabel.backgroundColor = [UIColor clearColor];
			centerMessageLabel.opaque = NO;
			centerMessageLabel.textColor = [UIColor whiteColor];
			centerMessageLabel.font = [UIFont boldSystemFontOfSize:40];
			centerMessageLabel.textAlignment = NSTextAlignmentCenter;
			centerMessageLabel.shadowColor = [UIColor darkGrayColor];
			centerMessageLabel.shadowOffset = CGSizeMake(1,1);
			centerMessageLabel.adjustsFontSizeToFitWidth = YES;
			
			[self addSubview:centerMessageLabel];
		}
		
		centerMessageLabel.text = message;
	}
}

- (void)setSubMessage:(NSString *)message
{	
	if (message == nil && subMessageLabel != nil)
		self.subMessageLabel = nil;
	
	else if (message != nil)
	{
		if (subMessageLabel == nil)
		{
			self.subMessageLabel = [[[UILabel alloc] initWithFrame:CGRectMake(14,self.bounds.size.height-35,self.bounds.size.width-24,30)] autorelease];
			subMessageLabel.backgroundColor = [UIColor clearColor];
			subMessageLabel.opaque = NO;
			subMessageLabel.textColor = [UIColor whiteColor];
			subMessageLabel.font = [UIFont boldSystemFontOfSize:17];
			subMessageLabel.textAlignment = NSTextAlignmentCenter;
			subMessageLabel.shadowColor = [UIColor darkGrayColor];
			subMessageLabel.shadowOffset = CGSizeMake(1,1);
			subMessageLabel.adjustsFontSizeToFitWidth = YES;
			
			[self addSubview:subMessageLabel];
		}
		
		subMessageLabel.text = message;
	}
}
	 
- (void)showSpinner
{	
	if (self.spinner == nil)
	{
		
        UIActivityIndicatorView *aTempActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] ;
       

		aTempActivityIndicator.frame = CGRectMake(round(self.bounds.size.width/2 - self.spinner.frame.size.width/2),
								round(self.bounds.size.height/2 - self.spinner.frame.size.height/2),
								self.spinner.frame.size.width,
								self.spinner.frame.size.height);
        self.spinner = aTempActivityIndicator;
        [aTempActivityIndicator release];
		//[self.spinner release];// Optimization
	}
	if ([self.spinner isKindOfClass:[UIActivityIndicatorView class]]) {
        [self addSubview:self.spinner];
        [self.spinner startAnimating];
    }
}

#pragma mark -
#pragma mark Rotation

- (void)setProperRotation
{
	[self setProperRotation:YES];
}

- (void)setProperRotation:(BOOL)animated
{
	UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
	if (self.spinner == nil)
	{
		
        
        UIActivityIndicatorView *aTempActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] ;
        
        
		aTempActivityIndicator.frame = CGRectMake(round(self.bounds.size.width/2 - self.spinner.frame.size.width/2),
                                                  round(self.bounds.size.height/2 - self.spinner.frame.size.height/2),
                                                  self.spinner.frame.size.width,
                                                  self.spinner.frame.size.height);
        self.spinner = aTempActivityIndicator;
        [aTempActivityIndicator release];
		//[self.spinner release];	// Optimization
	}
	
	[self addSubview:self.spinner];
	[self.spinner startAnimating];
	if (animated)
	{
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.1];
	}
	    
    UIInterfaceOrientation toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait) 
    {
        currentIndicator.transform = CGAffineTransformRotate(CGAffineTransformIdentity, SHKdegreesToRadians(360));
    }
    else if(toInterfaceOrientation== UIInterfaceOrientationPortraitUpsideDown)
    {
       	currentIndicator.transform = CGAffineTransformRotate(CGAffineTransformIdentity, SHKdegreesToRadians(180));
    }
    if (orientation == UIDeviceOrientationPortraitUpsideDown)
		currentIndicator.transform = CGAffineTransformRotate(CGAffineTransformIdentity, SHKdegreesToRadians(180));	
	
    if (orientation == UIDeviceOrientationPortrait)
		currentIndicator.transform = CGAffineTransformRotate(CGAffineTransformIdentity, SHKdegreesToRadians(360));
    
    
	else if (orientation == UIDeviceOrientationLandscapeLeft)
		currentIndicator.transform = CGAffineTransformRotate(CGAffineTransformIdentity, SHKdegreesToRadians(90));	
	
	else if (orientation == UIDeviceOrientationLandscapeRight)
		currentIndicator.transform = CGAffineTransformRotate(CGAffineTransformIdentity, SHKdegreesToRadians(-90));
	
	if (animated)
		[UIView commitAnimations];
}


@end
