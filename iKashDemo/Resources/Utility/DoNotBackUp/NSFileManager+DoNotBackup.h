/********************************************************************************\
 *
 * File Name       Database.h
 * Author          $Author:: IndiaNIC Infotech Ltd  $: Author of last commit
 * Version         $Revision:: 01             $: Revision of last commit
 * Modified        $Date:: 2012-20-09 16:01:19#$: Date of last commit
 *
 * Copyright(c) 2011 IndiaNIC.com. All rights reserved.
 *
 \********************************************************************************/

@interface NSFileManager (DoNotBackup)

//- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
//- (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)aStrPath;
//- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
@end