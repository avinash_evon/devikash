//
//  ContainerViewController.m
//  EmbeddedSwapping
//
//  Created by Michael Luton on 11/13/12.
//  Copyright (c) 2012 Sandmoose Software. All rights reserved.
//  Heavily inspired by http://orderoo.wordpress.com/2012/02/23/container-view-controllers-in-the-storyboard/
//

#import "ContainerViewController.h"
#import "SettingsVC.h"
#import "NewsLetterEmailVC.h"
#import "FavouriteCampaignVC.h"
#import "ReviewRattingVC.h"
#import "VoucherVC.h"

//#define SegueIdentifierService @"embedSetting"
//#define SegueIdentifierExtra @"embedExtra"
//#define SegueIdentifierPackage @"embedPackage"


#define SegueIdentifierSetting @"embedSetting"
#define SegueIdentifierNewsletter @"embedNewsletter"
#define SegueIdentifierfavourite @"embedFavourite"
#define SegueIdentifierReview @"embedReview"
#define SegueIdentifierVoucher @"embedVoucher"



@interface ContainerViewController ()
//embedNewsletter embedFavourite embedReview embedVoucher
@property (strong, nonatomic) NSString *currentSegueIdentifier;
@property (strong, nonatomic) SettingsVC *SettingsVC;
@property (strong, nonatomic) NewsLetterEmailVC *NewsLetterEmailVC;
@property (strong, nonatomic) FavouriteCampaignVC *FavouriteCampaignVC;
@property (strong, nonatomic) ReviewRattingVC *ReviewRattingVC;
@property (strong, nonatomic) VoucherVC *VoucherVC;
@property (assign, nonatomic) BOOL transitionInProgress;

@end

@implementation ContainerViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    self.transitionInProgress = NO;
    self.currentSegueIdentifier = SegueIdentifierSetting;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Instead of creating new VCs on each seque we want to hang on to existing
    // instances if we have it. Remove the second condition of the following
    // two if statements to get new VC instances instead.
    if ([segue.identifier isEqualToString:SegueIdentifierSetting]) {
        self.SettingsVC = segue.destinationViewController;
    }
    if ([segue.identifier isEqualToString:SegueIdentifierNewsletter]) {
        self.NewsLetterEmailVC = segue.destinationViewController;
    }
    if ([segue.identifier isEqualToString:SegueIdentifierfavourite]) {
        self.FavouriteCampaignVC = segue.destinationViewController;
    }
    if ([segue.identifier isEqualToString:SegueIdentifierReview]) {
        self.ReviewRattingVC = segue.destinationViewController;
    }
    if ([segue.identifier isEqualToString:SegueIdentifierVoucher]) {
        self.VoucherVC = segue.destinationViewController;
    }
    
    // If we're going to the first view controller.
    if ([segue.identifier isEqualToString:SegueIdentifierSetting]) {
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0) {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.SettingsVC];
        }else{
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }else if ([segue.identifier isEqualToString:SegueIdentifierNewsletter]) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.NewsLetterEmailVC];
    }else if ([segue.identifier isEqualToString:SegueIdentifierfavourite]) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.FavouriteCampaignVC];
    }else if ([segue.identifier isEqualToString:SegueIdentifierReview]) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.ReviewRattingVC];
    }else if ([segue.identifier isEqualToString:SegueIdentifierVoucher]) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.VoucherVC];
    }
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
        self.transitionInProgress = NO;
    }];
}

- (void)swapViewControllers:(NSString *)strSegue{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if (self.transitionInProgress) {
        return;
    }
    NSLog(@"%@",self.childViewControllers);
    // NSLog(@"%@",self.childViewControllers);
    if ([self.currentSegueIdentifier isEqualToString: strSegue] ){
        if([strSegue isEqualToString:@"embedSetting"] || [strSegue isEqualToString:@"embedNewsletter"] || [strSegue isEqualToString:@"embedFavourite"] || [strSegue isEqualToString:@"embedReview"]|| [strSegue isEqualToString:@"embedVoucher"] ){
            
            if([self.childViewControllers count]>1){
                UIViewController *vc = [self.childViewControllers lastObject];
                if([vc class]!=[UIViewController class]){
                    [vc.view removeFromSuperview];
                    [vc removeFromParentViewController];
                }
            }
            self.transitionInProgress = YES;
            self.currentSegueIdentifier = strSegue;
            [self performSegueWithIdentifier:self.currentSegueIdentifier sender:self];
        }else{
            return;
        }
    }else{
        NSLog(@"%@",strSegue);
        NSLog(@"%@",self.childViewControllers);
        self.transitionInProgress = YES;
        self.currentSegueIdentifier = strSegue;
        [self performSegueWithIdentifier:self.currentSegueIdentifier sender:self];
    }
}

- (void)BackViewController{
    NSLog(@"%lu",(unsigned long)[self.childViewControllers count]);
    [self.childViewControllers[0] popViewControllerAnimated:YES];
}

@end
