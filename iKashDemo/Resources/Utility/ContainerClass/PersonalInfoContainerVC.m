//
//  PersonalInfoContainerVC.m
//  iKashDemo
//
//  Created by indianic on 17/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "PersonalInfoContainerVC.h"

#define SegueIdentifierEditProfile @"embedEditProfile"
#define SegueIdentifierCategory @"embedCategory"
#define SegueIdentifierSupplier @"embedSupplier"
#define SegueIdentifierSecurity @"embedSecurity"

@interface PersonalInfoContainerVC ()

@property (assign, nonatomic) BOOL transitionInProgress;
@end

@implementation PersonalInfoContainerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.transitionInProgress = NO;
    self.currentSegueIdentifier = SegueIdentifierEditProfile;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:self];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Instead of creating new VCs on each seque we want to hang on to existing
    // instances if we have it. Remove the second condition of the following
    // two if statements to get new VC instances instead.
    if ([segue.identifier isEqualToString:SegueIdentifierEditProfile]) {
        UINavigationController *aNav = segue.destinationViewController;
        self.EditProfileVC = [[aNav viewControllers] objectAtIndex:0];
        self.EditProfileVC.strEditEmailShouldEnable = _editEmailShouldEnable;
    }
    if ([segue.identifier isEqualToString:SegueIdentifierCategory]) {
        self.CategoryVC = segue.destinationViewController;
    }
    if ([segue.identifier isEqualToString:SegueIdentifierSupplier]) {
        self.SupplierVC = segue.destinationViewController;
    }
    if ([segue.identifier isEqualToString:SegueIdentifierSecurity]) {
        self.SecurityVC = segue.destinationViewController;
    }
    
    // If we're going to the first view controller.
    if ([segue.identifier isEqualToString:SegueIdentifierEditProfile]) {
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0) {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.EditProfileVC];
        }else{
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }else if ([segue.identifier isEqualToString:SegueIdentifierCategory]) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.CategoryVC];
    }else if ([segue.identifier isEqualToString:SegueIdentifierSupplier]) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.SupplierVC];
    }else if ([segue.identifier isEqualToString:SegueIdentifierSecurity]) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.SecurityVC];
    }
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
        self.transitionInProgress = NO;
    }];
}

- (void)swapViewControllers:(NSString *)strSegue{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if (self.transitionInProgress) {
        return;
    }
    NSLog(@"%@",self.childViewControllers);
    // NSLog(@"%@",self.childViewControllers);
    if ([self.currentSegueIdentifier isEqualToString: strSegue]){
        //[self.childViewControllers objectAtIndex:0];
        if([strSegue isEqualToString:@"embedEditProfile"] || [strSegue isEqualToString:@"embedCategory"] || [strSegue isEqualToString:@"embedSupplier"] || [strSegue isEqualToString:@"embedSecurity"]){
            if([self.childViewControllers count]>1){
                UIViewController *vc = [self.childViewControllers lastObject];
                if([vc class]!=[UIViewController class]){
                    [vc.view removeFromSuperview];
                    [vc removeFromParentViewController];
                }
            }
            self.transitionInProgress = YES;
            self.currentSegueIdentifier = strSegue;
            [self performSegueWithIdentifier:self.currentSegueIdentifier sender:self];
        }else{
            return;
        }
    }else{
        NSLog(@"%@",strSegue);
        NSLog(@"%@",self.childViewControllers);
        self.transitionInProgress = YES;
        self.currentSegueIdentifier = strSegue;
        [self performSegueWithIdentifier:self.currentSegueIdentifier sender:self];
    }
}

- (void)BackViewController{
    NSLog(@"%lu",(unsigned long)[self.childViewControllers count]);
    [self.childViewControllers[0] popViewControllerAnimated:YES];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
