//
//  PersonalInfoContainerVC.h
//  iKashDemo
//
//  Created by indianic on 17/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditProfileVC.h"
#import "CategoryVC.h"
#import "SecurityVC.h"
#import "SupplierVC.h"

@interface PersonalInfoContainerVC : UIViewController

@property (strong, nonatomic) NSString *currentSegueIdentifier;
@property (strong, nonatomic) EditProfileVC *EditProfileVC;
@property (strong, nonatomic) CategoryVC *CategoryVC;
@property (strong, nonatomic) SecurityVC *SecurityVC;
@property (strong, nonatomic) SupplierVC *SupplierVC;
@property (nonatomic,strong) NSString *editEmailShouldEnable;

- (void)swapViewControllers:(NSString *)strSegue;
- (void)BackViewController;
- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController;

@end
