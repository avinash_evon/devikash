//
//  Webservice.m
//  Nobby
//
//  Created by ind563 on 7/21/14.
//  Copyright (c) 2014 Indianic. All rights reserved.
//

#import "Webservice.h"
#import "AFNetworking.h"

@implementation Webservice

+(Webservice *)sharedInstance{
    
    static Webservice *sharedObj = nil;
    static dispatch_once_t onePredecate;
    
    dispatch_once(&onePredecate, ^{
        sharedObj = [[Webservice alloc] init];
    });
    
    return sharedObj;
}

-(void)callWebserviceWithMethodName:(NSString *)aStrServiceName withParams:(NSMutableDictionary *)aDict showConnectionError:(BOOL)aBoolVal showLoader : (BOOL) flagLoader forView:(UIView*)aVWObj withCompletionBlock:(void(^)(NSDictionary * responseData))completionBlock withFailureBlock:(void(^)(NSError * error))failureBlock
{

    [AppDelegate sharedInstance].strStarflag=@"0";
    NSString *aStrURL = [NSString stringWithFormat:@"%@%@",WSURL,aStrServiceName];
    
    if([[AppDelegate sharedInstance] checkNetworkStatus]){
        
        // Set AccessToken ,Language and Devicetype for all webservice
        [aDict setValue:[[USERDEFAULTS objectForKey:@"Language"] uppercaseString] forKey:@"lang_id"];
        [aDict setValue:[[USERDEFAULTS objectForKey:@"device_type"] uppercaseString] forKey:@"device_type"];
        
        NSString *straccesstoken=[USERDEFAULTS objectForKey:@"access_token"];
        
        if(straccesstoken.length>0){
            [aDict setValue:[[USERDEFAULTS objectForKey:@"access_token"] uppercaseString] forKey:@"access_token"];
        }
        
        NSMutableDictionary *aMutDict = [NSMutableDictionary dictionaryWithDictionary:aDict];
        
        if(aVWObj && ![aStrServiceName isEqualToString:@"get_background_image"] && ![aStrServiceName isEqualToString:@"get_city_id_from_latlong"] && ![aStrServiceName isEqualToString:@"check_customer_promotion_code"] && ![aStrServiceName isEqualToString:@"customer_download_voucher"]){
            dispatch_async(dispatch_get_main_queue(), ^{
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[AppDelegate sharedInstance].window animated:YES];
                hud.labelText = @"Loading...";
            });
            
        }
        
        if (flagLoader){
            dispatch_async(dispatch_get_main_queue(), ^{
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[AppDelegate sharedInstance].window animated:YES];
                hud.labelText = @"Loading...";
            });
        }
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
        NSLog(@"iKashConsumer making request=============>\nURL==> %@\nparams==> %@ ",aStrURL,aMutDict);
        [manager POST:aStrURL parameters:aMutDict success:^(AFHTTPRequestOperation *operation, id responseObject){
            [self hideHud];
            completionBlock((NSMutableDictionary *)responseObject);
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error){
            [self hideHud];
            failureBlock(error);
        }];
        
    }else{
        
        [self hideHud];
        [[AppDelegate sharedInstance] showAlertView:[appDelegate getString:@"INTERNET_CONNECTION_ERROR"] Tag:@"0"];
    }
    
}

-(void)callWebserviceWithMethodName:(NSString *)aStrServiceName withParams:(NSMutableDictionary *)aDict showConnectionError:(BOOL)aBoolVal forView:(UIView*)aVWObj withCompletionBlock:(void(^)(NSDictionary * responseData))completionBlock withFailureBlock:(void(^)(NSError * error))failureBlock
{
    
    [self callWebserviceWithMethodName:aStrServiceName withParams:aDict showConnectionError:aBoolVal showLoader:NO forView:aVWObj withCompletionBlock:completionBlock withFailureBlock:failureBlock];
    
}

-(void)hideHud{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideAllHUDsForView:[AppDelegate sharedInstance].window animated:YES];
    });
}
@end
