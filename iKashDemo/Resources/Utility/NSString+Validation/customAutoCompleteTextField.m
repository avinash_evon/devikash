//
//  customAutoCompleteTextField.m
//  iKash
//
//  Created by indianic on 06/10/17.
//  Copyright © 2017 indianic. All rights reserved.
//

#import "customAutoCompleteTextField.h"

@implementation customAutoCompleteTextField

-(void)setText:(NSString *)text
{
    super.text = [appDelegate getString:text];
}
-(void)awakeFromNib
{
    [super awakeFromNib];
    super.text = [appDelegate getString:super.text];
    super.placeholder = [appDelegate getString:super.placeholder];
//    [self setValue:[appDelegate colorWithHexString:@"666666"]
//        forKeyPath:@"_placeholderLabel.textColor"];
    //    super.font = [UIFont fontWithName:FontRobotoRegular size:13.0];
    //    super.textColor = [appDelegate colorWithHexString:@"666666"];
    
}

-(void)setPlaceholder:(NSString *)placeholder
{
    super.text = [appDelegate getString:placeholder];
}

-(void)setBorderColor:(UIColor *)borderColor
{
    self.layer.borderColor=borderColor.CGColor;
    
}

-(UIColor *)borderColor
{
    return [UIColor colorWithCGColor:self.layer.borderColor];
}

-(void)setBorderWidth:(CGFloat)borderWidth
{
    self.layer.borderWidth=borderWidth;
}

-(CGFloat)borderWidth
{
    return self.layer.borderWidth;
}

-(void)setCornerRadius:(CGFloat)cornerRadius
{
    self.layer.cornerRadius=cornerRadius;
}

-(CGFloat)cornerRadius
{
    return self.layer.cornerRadius;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    CGRect inset = CGRectMake(bounds.origin.x + _padding, bounds.origin.y, bounds.size.width - _padding, bounds.size.height);
    return inset;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    CGRect inset = CGRectMake(bounds.origin.x + _padding, bounds.origin.y, bounds.size.width - _padding, bounds.size.height);
    return inset;
}


@end
