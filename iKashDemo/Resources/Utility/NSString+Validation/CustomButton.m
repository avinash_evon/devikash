//
//  MyButton.m
//  Wekho
//
//  Created by indianic on 16/06/15.
//  Copyright (c) 2015 indianic. All rights reserved.
//

#import "CustomButton.h"

@class AppDelegate;

//IB_DESIGNABLE
@implementation CustomButton

-(void)setTitle:(NSString *)title forState:(UIControlState)state
{

    NSString* aNewTitle = [self getLanguageText:title];
    NSString* oldSetTitle = self.titleLabel.text;
    
    if (![oldSetTitle isEqualToString:aNewTitle]){
        [super setTitle:aNewTitle forState:state];

    }
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [super setTitle:[self getLanguageText:super.titleLabel.text] forState:UIControlStateNormal];
//    [super setTitleColor:[appDelegate colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
//    [super setBackgroundColor:[appDelegate colorWithHexString:@"fe7935"] ];
//    [super.titleLabel setFont:[UIFont fontWithName:FontRobotoMedium size:13]];
}

//Star for mandatory fields
-(NSString*)getLanguageText:(NSString*)str{
    //Remove star then get language text and add star again
    if ([str containsString:@" *"]){
        NSString *aStrWithOutStar = [str stringByReplacingOccurrencesOfString:@" *" withString:@""];
        return [NSString stringWithFormat:@"%@ *",[appDelegate getString:aStrWithOutStar]];
    }else{
        return [appDelegate getString:str];
    }
}

- (void) setCornerRadius:(CGFloat)cornerRadius {
    self.layer.cornerRadius = cornerRadius;
}

- (CGFloat) cornerRadius {
    return self.layer.cornerRadius;
}
- (void) setBorderColor:(UIColor *)borderColor {
    self.layer.borderColor = borderColor.CGColor;
}
- (UIColor *) borderColor {
    return [UIColor colorWithCGColor:self.layer.borderColor];
}
- (void) setBorderWidth:(CGFloat)borderWidth {
    self.layer.borderWidth = borderWidth;
}
- (CGFloat) borderWidth {
    return self.layer.borderWidth;
}

@end
