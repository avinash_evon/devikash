//
//  MyButton.h
//  iKash
//
//  Created by indianic on 02/02/16.
//  Copyright (c) 2016 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomButtonPopOver : UIButton

@property (nonatomic, assign) IBInspectable CGFloat borderWidth;
@property (nonatomic, strong) IBInspectable UIColor *borderColor;
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

@end
