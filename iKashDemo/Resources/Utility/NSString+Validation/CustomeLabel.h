//
//  VFSLabel.h
//  VFS
//
//  Created by indianic on 12/16/14.
//  Copyright (c) 2014 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomeLabel : UILabel{
    
}
@property (strong,nonatomic) NSString *strName;
@property (nonatomic) IBInspectable BOOL needStar;
@end
