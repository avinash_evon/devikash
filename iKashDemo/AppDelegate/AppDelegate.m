///
//  AppDelegate.m
//  CustomerBodikea
//
//  Created by indianic on 08/07/15.
//  Copyright (c) 2015 indianic. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "LoginVC.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "HomeChangePasswordVC.h"
#import "ProfileContainerVC.h"
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize strHomeCatIndex, mutArrSidePenalCategoryList, mutArrSidePenalCategoryName, strFlag, mutArrCityIDS, mutArrRadius,mutArrSubBranchIDS, strPageTitleFilter, strCampaignPageTitle, mutArrCategoryIDS, strStarflag,IsFavCategoryUpdated;
@synthesize locationManager, CurrentLat, CurrentLong,isLocationUpdate,strSelectedIndex;
@synthesize topConstraint, WelCometopConstraint,HomeListVC,SettingFlagForLogn,selectedCityLatitude,selectedCityLongitude,activeCategoryCount,branchId,securityEditFlag,campingDetailFlag;

+ (AppDelegate *)sharedInstance {
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

-(NSString *) stringByStrippingHTML:(NSString*)aStr {
    NSRange r;
    NSString *s = aStr;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    campingDetailFlag = @"0";
    securityEditFlag = @"0";
    branchId = @"";
    activeCategoryCount = 0;
    //Set it false first time
    _isLocationGetCalledBeforeListVCLoaded = FALSE;
  //  IsVoucherLblVisibl = TRUE
    
    SettingFlagForLogn = @"0";
    selectedCityLatitude = @"";
    selectedCityLongitude = @"";
    
   
   [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

//    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
//
//    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
//
//        statusBar.backgroundColor = [UIColor greenColor];//set whatever color you like
//    }
    

   
    
    //Make all search font bigger (Cline bug)
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{
                                                                                                 NSFontAttributeName: [UIFont fontWithName:@"Roboto-Regular" size:15],
                                                                                                }];
    //Update home list when user change fav category from setting
    IsFavCategoryUpdated = NO;
    
    
    
  
    
    UIStoryboard *aStory = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    HomeListVC = [aStory instantiateViewControllerWithIdentifier:@"CampaignListVC"];
    
    _strFilterCityId = @"";
    
    _shouldCallAPI = NO;
    isLocationUpdate = NO;
    _shouldUpdateImage = NO;
    SerchCityload=FALSE;
    if(_strCityId.length==0){
        _strCityId=[USERDEFAULTS objectForKey:@"City_id"];
    }
    if(strCampaignPageTitle.length==0){
        strCampaignPageTitle=[USERDEFAULTS objectForKey:@"City_Name"];
    }
    
    [AMPopTip appearance].font = [UIFont fontWithName:FontRobotoMedium size:12];
    
    self.popTip = [AMPopTip popTip];
    self.popTip.shouldDismissOnTap = YES;
    self.popTip.edgeMargin = 5;
    self.popTip.offset = 2;
    self.popTip.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    self.popTip.shouldDismissOnTap = YES;
    
    [GMSServices provideAPIKey:@"AIzaSyDHb8FGtG3EqtazMKrzHiYgdXfH8bI9zI4"];
    //Jatin: OLD API Key :- AIzaSyDnirMaH4MOun6ruftMtRvP-kajw_gqEyQ
    // OLD API Key Don't know which account is used to create
    // Above new API create from jatin.jpatel@indianic.com
    strPageTitleFilter=@"";
    mutArrCategoryIDS=[[NSMutableArray alloc]init];
    mutArrSidePenalCategoryList=[[NSMutableArray alloc]init];
    mutArrSidePenalCategoryName=[[NSMutableArray alloc]init];
    mutArrCityIDS=[[NSMutableArray alloc] init];
    mutArrRadius=[[NSMutableArray alloc]init];
    mutArrSubBranchIDS=[[NSMutableArray alloc]init];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [USERDEFAULTS setValue:@"ios" forKey:@"device_type"];
    [self notificationMethods:launchOptions];
    
    [Fabric with:@[[Crashlytics class]]];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"UserLoginDetails"]) {
        NSString *stFirstName=[USERDEFAULTS objectForKey:@"FirstName"];
        if(stFirstName.length==0){
            [AppDelegate sharedInstance].strFlag=@"1";
            
            UINavigationController *navCont =(UINavigationController *)self.window.rootViewController;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            ProfileContainerVC *objProfileContainerVC = [storyboard instantiateViewControllerWithIdentifier:@"ProfileContainerVC"];
            [navCont pushViewController:objProfileContainerVC animated:NO];
        }else{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            UITabBarController *tb = [storyboard instantiateViewControllerWithIdentifier:@"tabBarController"];
            tb.tabBar.barTintColor= [UIColor whiteColor];//[appDelegate colorWithHexString:@"eeeeee"];
            [tb.tabBar setBackgroundImage:nil];
            [tb.tabBar setClipsToBounds:YES];
            UIViewController *leftSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"leftSideMenuVC"];
            MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                            containerWithCenterViewController:tb
                                                            leftMenuViewController:leftSideMenuViewController
                                                            rightMenuViewController:nil];
            [AppDelegate sharedInstance].window.rootViewController = container;
            [[AppDelegate sharedInstance].window makeKeyAndVisible];
        }
    }
    
    // Location
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = (id)self;
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    NSLog(@"AUTHORIZATION - %d",[CLLocationManager authorizationStatus]);
    [self.locationManager startUpdatingLocation];
    
    // Override point for customization after application launch.
    return YES;
}

#pragma mark - Location Method

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    //     if (status == kCLAuthorizationStatusDenied) {
    //         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Service Dined"
    //                                                         message:@"To re-enable, please go to Settings and turn on Location Service for iKash app."
    //                                                        delegate:nil
    //                                               cancelButtonTitle:@"OK"
    //                                               otherButtonTitles:nil];
    //         [alert show];
    //
    //    }
    
    
    isLocationUpdate = YES;//NO
    _shouldUpdateImage = YES;
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    CLLocation *loc1 = newLocation;
    
    // Lat/Lon
    CurrentLat = loc1.coordinate.latitude;
    CurrentLong = loc1.coordinate.longitude;
    
    
//    CLGeocoder *ceo = [[CLGeocoder alloc]init];
//       CLLocation *loc = [[CLLocation alloc]initWithLatitude: CurrentLat longitude: CurrentLong];
//
//
//    [ceo reverseGeocodeLocation: loc completionHandler:
//        ^(NSArray *placemarks, NSError *error) {
//            CLPlacemark *placemark = [placemarks objectAtIndex:0];
//            NSLog(@"placemark %@",placemark);
//            //String to hold address
//            NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
//            NSLog(@"addressDictionary %@", placemark.addressDictionary);
//
//            NSLog(@"placemark %@",placemark.region);
//            NSLog(@"placemark %@",placemark.country);  // Give Country Name
//            NSLog(@"placemark %@",placemark.locality); // Extract the city name
//            NSLog(@"location %@",placemark.name);
//            NSLog(@"location %@",placemark.ocean);
//            NSLog(@"location %@",placemark.postalCode);
//            NSLog(@"location %@",placemark.subLocality);
//
//            NSLog(@"location %@",placemark.location);
//            //Print the location to console
//            NSLog(@"I am currently at %@",locatedAt);
//
//         [USERDEFAULTS setObject:[NSString stringWithFormat:@"%@",placemark.country] forKey:@"CurrentLoc"];
//        }];
    
   
    
    
    
      [self GetCityFilterWS];
    
   //  CurrentLat = 52.3702157;//52.3422025;//loc.coordinate.latitude;
  //   CurrentLong =4.8951679;//5.6367423; //loc.coordinate.longitude;
    
  //  [USERDEFAULTS setObject:[NSString stringWithFormat:@"%f",CurrentLat] forKey:@"latitude"];
  //  [USERDEFAULTS setObject:[NSString stringWithFormat:@"%f",CurrentLong] forKey:@"longitude"];
    [USERDEFAULTS synchronize];
    
    if(_shouldCallAPI && !isLocationUpdate) {
        [self GetCityFilterWS];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"getLocation" object:nil];
        isLocationUpdate = YES;
    }else if(_shouldUpdateImage){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"getLocation" object:nil];
        _shouldUpdateImage = NO;
    }
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //isDoesNotNeedToUpdate=FALSE;
    });
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager: (CLLocationManager *)manager didFailWithError: (NSError *)error {
    [manager stopUpdatingLocation];
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusNotDetermined: {
            UIAlertView  *alert = [[UIAlertView alloc] initWithTitle:[self getString:@"App Permission Denied"]
                                                             message:[self getString:@"Please go to Settings and turn on Location Service for iKash app."]
                                                            delegate:nil
                                                   cancelButtonTitle:[self getString:@"OK"]
                                                   otherButtonTitles:nil];
            [alert show];
            
            [self getCampaigns];
        }
            break;
        case kCLAuthorizationStatusDenied: {
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self getString:@"Location Service Disabled"]
                                                                message:[self getString:@"To re-enable, please go to Settings and turn on Location Service for iKash app."]
                                                               delegate:nil
                                                      cancelButtonTitle:[self getString:@"OK"]
                                                      otherButtonTitles:nil];
                [alert show];
                
            [self getCampaigns];
            });
        }
            break;
        default:
            break;
    }
    
}

-(void)getCampaigns{
    _isLocationGetCalledBeforeListVCLoaded = TRUE;
    
    self.CurrentLat = 0;
    self.CurrentLong = 0;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchCityCampaign" object:nil];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    
    //JATIN : Removed by Ali
   // [self checkStatusAndGetLocation];
    
}

-(void)checkStatusAndGetLocation {
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusNotDetermined: {
            [USERDEFAULTS setObject:@"0" forKey:@"latitude"];
            [USERDEFAULTS setObject:@"0" forKey:@"longitude"];
            
            [AppDelegate sharedInstance].strRadius=@"";
            [AppDelegate sharedInstance].strCampaignPageTitle=@"";
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SetTopCategory" object:nil];
            [self getCampaigns];
        }
            break;
        case kCLAuthorizationStatusDenied: {
            
            [USERDEFAULTS setObject:@"0" forKey:@"latitude"];
            [USERDEFAULTS setObject:@"0" forKey:@"longitude"];
            
            [AppDelegate sharedInstance].strRadius=@"";
            [AppDelegate sharedInstance].strCampaignPageTitle=@"";
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SetTopCategory" object:nil];
            
            [self getCampaigns];
        }
            break;
        default:
            break;
    }
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
//}
//
//- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
//    NSLog(@"Calling Application Bundle ID: %@", sourceApplication);
    NSLog(@"URL scheme:%@", [url scheme]);
    NSLog(@"URL query: %@", [url query]);
    
    if ([[FBSDKApplicationDelegate sharedInstance] application:app openURL:url options:options]){
        return true;
    }else{
        NSString *strCustomerId=[USERDEFAULTS objectForKey:@"customer_id"];
        if(strCustomerId.length>0){
            return NO;
        }else{
            
            NSString *strScheme=[url scheme];
            NSString *strKey=[url query];
            
            if(strKey.length>0){
                if([strScheme isEqualToString:@"ikashcustomer"] ){
                    
                    NSArray * array = [strKey componentsSeparatedByString:@"="];
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
                    __block UINavigationController *aNv = nil;
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserLoginDetails"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    if ([[AppDelegate sharedInstance].window.rootViewController isKindOfClass:[MFSideMenuContainerViewController class]]) {
                        HomeChangePasswordVC *objLoginVC = [storyboard instantiateViewControllerWithIdentifier:@"HomeChangePasswordVC"];
                        aNv = [[UINavigationController alloc] initWithRootViewController:objLoginVC];
                        [AppDelegate sharedInstance].window.rootViewController = aNv;
                    } else {
                        [USERDEFAULTS setObject:[array objectAtIndex:1] forKey:@"ChangePassword"];
                        UINavigationController *navCont =(UINavigationController *)self.window.rootViewController;
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
                        HomeChangePasswordVC *objHomeChangePasswordVC = [storyboard instantiateViewControllerWithIdentifier:@"HomeChangePasswordVC"];
                        [navCont pushViewController:objHomeChangePasswordVC animated:YES];
                    }
                }
            }
            return YES;
        }
    }
}

-(BOOL)checkNetworkStatus{
    [Reachability reachabilityWithHostname:@"http://google.com"];
    BOOL isAvailable;
    NetworkStatus hostStatus =[[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    switch (hostStatus){
        case NotReachable:{
            isAvailable = NO;
            break;
        }
        case ReachableViaWiFi:{
            isAvailable = YES;
            break;
        }
        case ReachableViaWWAN:{
            isAvailable = YES;
            break;
        }
    }
    return isAvailable;
}

#pragma mark - Push Notification

-(void)notificationMethods:(NSDictionary *)launchOptions{
    
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        //        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
//    }
//    else{
//
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
//         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
//    }
    
    if (![[NSUserDefaults standardUserDefaults]objectForKey:@"IsFirstTimeBool"]){
        //[self setApplicationBadgeNumber:0];
        
#if TARGET_IPHONE_SIMULATOR
        
        [[NSUserDefaults standardUserDefaults]setObject:@"Simulator" forKey:@"DeviceToken"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        NSLog(@"Running in Simulator");
#endif
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSLog(@"Extra Parameter - %@", userInfo);
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GotNotification" object:nil];
    
    if(![[USERDEFAULTS objectForKey:@"customer_id"] isEqualToString:@""]){
        //  [self showAlertView:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] Tag:@"0"];
    }else{
        _dictNotificationDetails = [[NSMutableDictionary alloc] initWithDictionary:userInfo];
    }
    // [self OpenNotificationDetail];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    // NSLog(@"%@",error);
    
#if TARGET_IPHONE_SIMULATOR
    [[NSUserDefaults standardUserDefaults] setObject:@"f72250f79e8d7993696858f12eb45835a1cc695a174347139e6e0af6a9bef03f" forKey:@"DeviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
#endif
    
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString *strDeviceTocken = [deviceToken description];
    
    strDeviceTocken = [strDeviceTocken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    strDeviceTocken = [strDeviceTocken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"Device Token - %@", strDeviceTocken);
    
    [[NSUserDefaults standardUserDefaults] setObject:strDeviceTocken forKey:@"DeviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)didReceiveNotificationWhenApplicationinForeGround:(NSDictionary*)aDictLanuchInfo{
    NSDictionary *aDictNotificationInfo = [aDictLanuchInfo objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (aDictNotificationInfo){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GotNotification" object:nil];
    }
}

-(void)OpenNotificationDetail{
    UINavigationController *navCont =(UINavigationController *)self.window.rootViewController;
    [navCont popToRootViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag==100){
        if(buttonIndex==0){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            __block UINavigationController *aNv = nil;
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserLoginDetails"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if ([[AppDelegate sharedInstance].window.rootViewController isKindOfClass:[MFSideMenuContainerViewController class]]) {
                LoginVC *objLoginVC = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                aNv = [[UINavigationController alloc] initWithRootViewController:objLoginVC];
                [AppDelegate sharedInstance].window.rootViewController = aNv;
                [self RemoveUserDefaults];
            }
            //            else {
            //                [self.navigationController popToRootViewControllerAnimated:YES];
            //                [AppDelegate sharedInstance].window.rootViewController = self.navigationController;
            //            }
        }
    }
}

#pragma mark - user define methods

-(void) showAlertView:(NSString *)aStrAlertMsg Tag:(NSString *)strTag{
    
    
    NSLog(@"my Tag :%@",strTag);
    
//    if([strTag isEqualToString:@"0"])
//    {
//        return;
//    }
//    else{
    
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:APPTITLE message:aStrAlertMsg delegate:self cancelButtonTitle:[self getString:@"OK"] otherButtonTitles:nil, nil];
    
    
    if([strTag isEqualToString:@"50"]){
        [self performSelector:@selector(dismissAlert:) withObject:alertView afterDelay:20.0f];
    }
    alertView.tag=[strTag intValue];
    [alertView show];
   // }
}

-(void)dismissAlert:(UIAlertView *) alertView{
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
}

- (UIColor *) colorWithHexString: (NSString *) stringToConvert{
    NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor blackColor];
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString length] != 6) return [UIColor blackColor];
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(NSString *)getString:(NSString *)keystr {
    
    NSString *strLanguage=[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    NSString *aStrPath = [[NSBundle mainBundle] pathForResource:strLanguage ofType:@"lproj"];
    NSBundle* languageBundle = [NSBundle bundleWithPath:aStrPath];
    NSString* aStr=[languageBundle localizedStringForKey:keystr value:@"" table:nil];
    return aStr;
}

-(void)RemoveUserDefaults{
    
    [USERDEFAULTS removeObjectForKey:@"customer_id"];
    [USERDEFAULTS removeObjectForKey:@"access_token"];
    [USERDEFAULTS removeObjectForKey:@"UserLoginDetails"];
    [USERDEFAULTS removeObjectForKey:@"CategoryList"];
    
    [USERDEFAULTS removeObjectForKey:@"FirstName"];
    
    [mutArrSidePenalCategoryList removeAllObjects];
    [mutArrSidePenalCategoryName removeAllObjects];
    [mutArrCategoryIDS removeAllObjects];
    [mutArrSubBranchIDS removeAllObjects];
    [mutArrCityIDS removeAllObjects];
    [mutArrRadius removeAllObjects];
    [_mutArrSelectedCampaignWomenListAdded removeAllObjects];
    [_mutArrSelectedCampaignCityListAdded removeAllObjects];
    [_mutArrSelectedCampaignRadiusListAdded removeAllObjects];
    _strFlgSearch=@"";
    strStarflag=@"0";
    //    strCampaignPageTitle=@"";
    strPageTitleFilter=@"";
}

- (void)GetCityFilterWS{
    
   // [USERDEFAULTS setObject:[USERDEFAULTS objectForKey:@"latitude"] forKey:@"Userlatitude"];
    
  // [USERDEFAULTS setObject:@"52.298664" forKey:@"latitude"];
    
  //  [USERDEFAULTS setObject:[USERDEFAULTS objectForKey:@"longitude"] forKey:@"Userlongitude"];
    
   // [USERDEFAULTS setObject:@"5.629619" forKey:@"longitude"];
    
  //  [USERDEFAULTS synchronize];
    
    
    
    
//    UIAlertView *alert = [[UIAlertView alloc]
//       initWithTitle:@"lat , long"
//                          message:[NSString stringWithFormat:@"%@ %@",[USERDEFAULTS objectForKey:@"latitude"],[USERDEFAULTS objectForKey:@"longitude"]]
//       delegate:self
//       cancelButtonTitle:@"Cancel"
//       otherButtonTitles:@"OK", nil];
//    [alert show];
    
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"latitude"],@"latitude",
                                  [USERDEFAULTS objectForKey:@"longitude"],@"longitude",
                                  nil];
    
    //to regenerate Consumer issue 42
    
//    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"52.1860938",@"latitude",
//                                  @"5.5985249",@"longitude",
//                                  nil];
    
    //            52.1860938,5.5985249
    
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCityLocationWS withParams:aDict showConnectionError:YES forView:nil withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData of GetCityFilterWS = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            if([[responseData objectForKey:@"data"] count]>0){
                [AppDelegate sharedInstance].strCampaignPageTitle=[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"city"];
                
                
                
                [AppDelegate sharedInstance].strCityId=[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"city_id_zip"];
                [USERDEFAULTS setObject:[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"city_id_zip"] forKey:@"City_id"];
                [USERDEFAULTS setObject:[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"city"] forKey:@"City_Name"];
                
                [USERDEFAULTS objectForKey:@"City_Name"];
                
                [AppDelegate sharedInstance].CurrentLat = [[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"mc_latitude"] floatValue];
                [AppDelegate sharedInstance].CurrentLong = [[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"mc_longitude"] floatValue];
                
                NSString  *strRadious =[AppDelegate sharedInstance].strRadius ;
                if(strRadious.length==0){
                    [AppDelegate sharedInstance].strRadius=@"";
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SetTopCategory" object:nil];
                
                _isLocationGetCalledBeforeListVCLoaded = TRUE;
                
                //Reload campaigns
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchCityCampaign" object:nil];
                
                
                
//                if(SerchCityload){
//                    SerchCityload=TRUE;
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchCityCampaign" object:nil];
//                }
            }else{
                
                //Server bug-> Sometime server doesn't give as city data in one try even if it exist
                //So this notification will again call above service in CampaignListVC and display data if found city
                //Or it will show 'no city found' title and campaigns accordingly
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadCampaigns" object:nil];
            }
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)btnActionToolTipTemplete:(id)sender scroll:(UIScrollView *)scrlView description:(NSString *)Description {
    
    if ([self.popTip isVisible]) {
        [self.popTip hide];
        return;
    }
    CGRect buttonPosition = [sender convertRect:CGRectZero toView:scrlView];
    buttonPosition.origin.x = buttonPosition.origin.x+15;
    buttonPosition.origin.y = buttonPosition.origin.y+15;
    self.popTip.popoverColor = kAppSupportedColorThemeColor;
    [self.popTip showText:Description direction:AMPopTipDirectionDown maxWidth:200 inView:scrlView fromFrame:buttonPosition];
}

-(NSString*)getStringWithStar :(NSString *)keystr{
    
    NSString *strLanguage=[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"];
    NSString *aStrPath = [[NSBundle mainBundle] pathForResource:strLanguage ofType:@"lproj"];
    NSBundle* languageBundle = [NSBundle bundleWithPath:aStrPath];
    NSString* aStr=[languageBundle localizedStringForKey:keystr value:@"" table:nil];
    
    return [NSString stringWithFormat:@"%@ *",aStr];
}

@end
