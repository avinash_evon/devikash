//
//  AppDelegate.h
//  CustomerBodikea
//
//  Created by indianic on 08/07/15.
//  Copyright (c) 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSFileManager+DoNotBackup.h"
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h>
#import "SettingContainer.h"
#import "AMPopTip.h"
#import "CampaignListVC.h"
#import "AppDelegate.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>{
    BOOL SerchCityload;
    NSString *SettingFlagForLogn;
    NSString *selectedCityLatitude;
    NSString *selectedCityLongitude;
    NSString *deviceCityLatitude;
    NSString *deviceCityLongitude;
    NSString *branchId;
    int activeCategoryCount;
    NSString *securityEditFlag;
    
    
}

@property (strong, nonatomic) NSString *securityEditFlag;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *SettingFlagForLogn;
@property (strong, nonatomic) NSString *selectedCityLatitude;
@property (strong, nonatomic) NSString *selectedCityLongitude;
@property (strong, nonatomic) NSString *deviceCityLatitude;
@property (strong, nonatomic) NSString *deviceCityLongitude;
@property (strong, nonatomic) NSString *branchId;
@property (assign) int activeCategoryCount;
+(AppDelegate *)sharedInstance;
 
-(BOOL)checkNetworkStatus;
//-(BOOL)IsSettingForLogin;



-(void) showAlertView:(NSString *)aStrAlertMsg Tag:(NSString *)strTag;
- (UIColor *) colorWithHexString: (NSString *) stringToConvert;
-(NSString *)getString:(NSString *)keystr;
- (void)btnActionToolTipTemplete:(id)sender scroll:(UIScrollView *)scrlView description:(NSString *)Description;
-(NSString *) stringByStrippingHTML:(NSString*)aStr;
-(NSString*)getStringWithStar :(NSString *)keystr;

// for landscape
@property (nonatomic, strong) NSLayoutConstraint *topConstraint;
@property (nonatomic, strong) NSLayoutConstraint *WelCometopConstraint;

@property (nonatomic,strong)NSMutableDictionary *dictNotificationDetails;
@property(nonatomic,strong)NSString *strHomeCatIndex;
@property(nonatomic,strong)NSMutableArray *mutArrSidePenalCategoryList;
@property(nonatomic,strong)NSMutableArray *mutArrSidePenalCategoryName;
@property(nonatomic,strong)NSString *strMyikashSaveButton;
@property(nonatomic,strong)NSString *strRedeemed;
@property(nonatomic,assign)BOOL isLocationUpdate,shouldCallAPI,shouldUpdateImage, IsFavCategoryUpdated,isLocationGetCalledBeforeListVCLoaded;
@property(nonatomic,strong)NSString *strSearchPageCity;
@property(nonatomic,strong)NSString *strCityId;
@property(nonatomic,strong)NSString *strFilterCityId;

@property(nonatomic,strong)NSString *campingDetailFlag;
@property(nonatomic,strong)NSString *strRadius;
@property(nonatomic,strong)NSString *strFlgSearch;
@property(nonatomic,strong)NSString *strFilterSearch;
@property(nonatomic,strong)NSString *strFlag;
@property(nonatomic,strong)NSString *strSelectedIndex;
@property(nonatomic,strong)NSString *strStarflag;
@property(nonatomic,strong)NSString *strCampaignPageTitle;
@property(nonatomic,strong)NSString *strPageTitleFilter;
@property(nonatomic,strong)NSString *strTabSelectedIndex;;
@property(nonatomic,strong)NSString *strGotNotification;
@property(nonatomic,strong)NSMutableArray *mutArrCategoryIDS;
@property(nonatomic,strong)NSMutableArray *mutArrSubBranchIDS;
@property(nonatomic,strong)NSMutableArray *mutArrCityIDS;
@property(nonatomic,strong)NSMutableArray *mutArrRadius;
@property(nonatomic,strong)SettingContainer *objSettingContainer;

//Filter search
@property(nonatomic,strong)NSMutableArray *mutArrSelectedCampaignWomenListAdded;
@property(nonatomic,strong)NSMutableArray *mutArrSelectedCampaignCityListAdded;
@property(nonatomic,strong)NSMutableArray *mutArrSelectedCampaignRadiusListAdded;
@property (nonatomic, strong) AMPopTip *popTip;
@property (nonatomic,strong) CampaignListVC *HomeListVC;

//Location
@property (nonatomic) float CurrentLat;
@property (nonatomic) float CurrentLong;
@property (nonatomic,strong) CLLocationManager *locationManager;

@property (nonatomic) int NotificationCount;


-(void)RemoveUserDefaults;
@end

